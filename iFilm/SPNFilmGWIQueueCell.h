//
//  SPNFilmGWIQueueCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNFilmGWIQueueCell : UITableViewCell

@property () IBOutlet UILabel *titleLabel;
@property () IBOutlet UILabel *genreLabel;
@property () IBOutlet UILabel *scoreLabel;
@property () IBOutlet UILabel *ratingLabel;
@property () IBOutlet UILabel *durationLabel;
@property () IBOutlet UILabel *releaseDateLabel;
@property () IBOutlet UILabel *releaseYearLabel;
@property () IBOutlet UILabel *synopsisLabel;

@property () IBOutlet UIButton *reviewButton;

@property () IBOutlet UIImageView *posterImageView;

-(void)initializeCellForMovie:(NSDictionary*)movie;
@end
