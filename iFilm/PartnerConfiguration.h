//
//  PartnerConfiguration.h
//  iFilm
//
//  Created by Vlad Getman on 18.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PartnerConfiguration : NSObject

@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, retain) NSString *info;
@property (nonatomic) CGSize size;

+ (PartnerConfiguration *)current;
- (void)load;
- (void)explain;

@end
