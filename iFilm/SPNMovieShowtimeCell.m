//
//  SPNMovieShowtimeCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNMovieShowtimeCell.h"
#import "ShadowButton.h"

@implementation SPNMovieShowtimeCell

-(void)initializeMovieShowtimeCell:(NSDictionary *)schedule forTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    
    [self.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [self.scoreLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.ratingLabel setFont:[UIFont spn_NeutraSmall]];
    [self.showtimesLabel setFont:[UIFont spn_NeutraSmallMedium]];
   
    if (!schedule) {
        return;
    }
    NSString *movieTitle = [schedule objectForKey:@"name"] ? [schedule objectForKey:@"name"] : [schedule objectForKey:@"title"];
     NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    NSString *urlImage;
    NSDictionary *posters = [schedule objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];
    [self.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
    
    NSString *stars = [schedule objectForKey:@"user-stars"];
    NSNumber *score = [NSNumber numberWithFloat:[stars floatValue]];
    if (!stars || [stars length] < 1 || score.floatValue < 1) {
        [self.scoreLabel setText:@"No votes"];
    }
    else {
        [self.scoreLabel setText:[NSString stringWithFormat:@"%d", score.intValue]];
    }
    
    NSString *qualityRating = [schedule objectForKey:@"rating"];
    if ([qualityRating isEqualToString:@"NR"]) {
        qualityRating = @"";
    }
    [self.ratingLabel setText:qualityRating];
    
    
    NSString *duration = [schedule objectForKey:@"runtime"];
    if (!duration) {
        self.ratingLabel.text = @"";
    }
    else if (self.ratingLabel.text.length > 0 && [duration length] > 0) {
        self.ratingLabel.text = [NSString stringWithFormat:@"%@ | %@ min.", self.ratingLabel.text, duration];
    }
    else if (![duration isKindOfClass:[NSArray class]] && [duration length] > 0) {
        self.ratingLabel.text = [NSString stringWithFormat:@"%@ min.", duration];
    }
    
    id releaseYearData = [schedule objectForKey:@"releases"];
    if ([releaseYearData isKindOfClass:[NSArray class]]) {
        releaseYearData = [releaseYearData firstObject];
    }
    if (releaseYearData) {
        self.ratingLabel.text = [NSString stringWithFormat:@"%@ | %@", self.ratingLabel.text, [releaseYearData substringFromIndex:[releaseYearData length]-4]];
    }
}


-(NSArray*)getShowsForThisMovie:(NSIndexPath*)indexPath forSchedule:(NSDictionary*)movieShowtimes forDate:(NSString*)dateString
{
    
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSDate *selectedDate = [showtimeDateFormatter dateFromString:dateString];
    BOOL isFutureDate = NO;
    if ([[selectedDate laterDate:[NSDate date]] isEqualToDate:selectedDate]) {
        isFutureDate = YES;
    }
    
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSMutableArray *allShows = [NSMutableArray new];
    
    id schedule = [movieShowtimes objectForKey:@"showtime"];
    if ([schedule isKindOfClass:[NSArray class]])
    {
        for (id showtime in schedule)
        {
            if ([showtime isKindOfClass:[NSArray class]]) {
                NSDate *date = [NSDate date];
                for (NSString *show in showtime)
                {
                    [showtimeDateFormatter setLocale:usLocale];
                    [showtimeDateFormatter setDefaultDate:[NSDate date]];
                    [showtimeDateFormatter setDateFormat:@"HH:mm"];
                    date = [showtimeDateFormatter dateFromString:show];
                    if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
                        [showtimeDateFormatter setDateFormat:@"h:mma"];
                        NSString* showDate = [showtimeDateFormatter stringFromDate:date];
                        [allShows addObject:showDate];
                    }
                }
    
            }
            else if ([showtime isKindOfClass:[NSString class]]) {
                
                [showtimeDateFormatter setLocale:usLocale];
                [showtimeDateFormatter setDefaultDate:[NSDate date]];
                [showtimeDateFormatter setDateFormat:@"HH:mm"];
                 NSDate *date = [showtimeDateFormatter dateFromString:showtime];
                if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
                    [showtimeDateFormatter setDateFormat:@"h:mma"];
                    NSString* showDate = [showtimeDateFormatter stringFromDate:date];
                    [allShows addObject:showDate];
                }
            }
        }
    }
    else if ([schedule isKindOfClass:[NSString class]]) {
        [showtimeDateFormatter setDateFormat:@"HH:mm"];
         NSDate *date = [showtimeDateFormatter dateFromString:schedule];
        if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
            [showtimeDateFormatter setDateFormat:@"h:mma"];
            [allShows addObject:schedule];
        }
    }
    
    NSArray *finalShows = [[NSSet setWithArray:allShows] allObjects];
    
    
    NSArray * sortedDatesArray = [finalShows sortedArrayUsingComparator: ^(id a, id b) {
        NSDate *d1 = [showtimeDateFormatter dateFromString:a];
        NSDate *d2 = [showtimeDateFormatter dateFromString:b];
        return [d1 compare: d2];
    }];
                        
    return sortedDatesArray;

}

-(void)setShowForCell:(NSArray*)shows withBrandColor:(UIColor*)showColor
{
    [[self.showtimeContainerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if ([shows count] < 1) {
        UILabel *showLabel = [UILabel new];
        [showLabel setFrame:CGRectMake(0, 0,195,25)];
        [showLabel setNumberOfLines:0];
        [showLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [showLabel setText:@"No more showtimes for today"];
        
        [showLabel setFont:[UIFont spn_NeutraBoldSmall]];
        if (showColor) {
            [showLabel.layer setBorderColor:showColor.CGColor];
        }
        else {
           [showLabel.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        }
        [showLabel setFont:[UIFont spn_NeutraSmallMedium]];
        [showLabel setTextColor:[UIColor darkGrayColor]];
        
        [showLabel setTextAlignment:NSTextAlignmentLeft];
        
        [self.showtimeContainerView addSubview:showLabel];
    }
    for (int i = 0; i < [shows count]; i ++)
    {
        int y = (i / 4);
    
        ShadowButton *showBtn = [ShadowButton buttonWithType:UIButtonTypeCustom];
        showBtn.layer.cornerRadius = 2.5;
        
        [showBtn setFrame:CGRectMake((i % 4)*60, y*25, 55,20)];
        [showBtn setTitle:[shows objectAtIndex:i] forState:UIControlStateNormal];
        
        [showBtn setBackgroundColor:[UIColor spn_buttonColor]];
    
        
        [showBtn.titleLabel setFont:[UIFont spn_NeutraSmallMedium]];
        [showBtn.titleLabel setTextColor:[UIColor whiteColor]];
        [showBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [showBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [self.showtimeContainerView addSubview:showBtn];
        [showBtn setTag:i+100];
        showBtn.layer.shadowColor = [UIColor spn_buttonShadowColor].CGColor;
        
    }
    [self layoutIfNeeded];
}

@end
