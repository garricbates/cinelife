//
//  SPNFilmPostReviewViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarRatingControl.h"
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"

@protocol SPNFilmPostReviewViewControllerDelegate <NSObject, UIAlertViewDelegate>
-(void)updatePosts:(BOOL)reload withReview:(NSString*)review;
@end

@interface SPNFilmPostReviewViewController : GAITrackedViewController <StarRatingDelegate, UIAlertViewDelegate, UITextFieldDelegate, SPNCinelifeQueriesDelegate>

@property (nonatomic, strong) NSDictionary *filmInformation;
@property NSDictionary *previousReview;
@property (nonatomic, assign) id <SPNFilmPostReviewViewControllerDelegate> delegate;

@property IBOutlet UIScrollView *scrollView;
@property IBOutlet UIView *containerView;
@property IBOutlet UIView *sliceView;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *postReviewButton;

@property (nonatomic, strong) IBOutlet UILabel *ratingLabel;
@property (nonatomic, strong) IBOutlet UILabel *commentLabel;
@property IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) IBOutlet UILabel *notLoggedMessageLabel;

@property (nonatomic, strong) IBOutlet StarRatingControl *starRatingControl;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property IBOutlet UITextField *titleTextField;

+(void)showWReviewPostForMovie:(NSDictionary *)movie
            overViewController:(id)viewController
                  withDelegate:(id)viewControllerDelegate;

+(void)showWReviewPostForMovie:(NSDictionary *)movie
            overViewController:(id)viewController
                  withDelegate:(id)viewControllerDelegate
                 withOldReview:(NSDictionary*)review
;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                movie:(NSDictionary*)movie;

-(void)show;


@end
