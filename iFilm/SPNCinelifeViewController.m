//
//  SPNCinelifeViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNCinelifeViewController.h"
#import "SPNIndieRSSCell.h"
#import "SPNShowtimesCollectionView.h"
#import "SPNIndiePostCell.h"
#import "SPNIndieHeaderCell.h"
#import "SPNIndieRSSFullListViewController.h"
#import "SPNIndiePostDetailsViewController.h"
#import "SPNTriviaQuestionViewController.h"
#import "SPNFavoriteFilmsViewController.h"
#import "WebViewController.h"
//#import <IntensifyUp/IntensifyUp.h>

#define kMAX_POST_COUNT 20




@interface SPNCinelifeViewController ()
{
    NSArray *cinelifeTiles;
    
    NSArray *selectedPostFeed;
    NSString *selectedFeedName;
    
    NSDictionary *triviaQuestions;
    NSMutableArray *triviaList;
}
@end

@implementation SPNCinelifeViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"CineLife";
    // Do any additional setup after loading the view.
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    
    [[SPNCinelifeQueries sharedInstance] getMyCineLife];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection view functions
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(SPNShowtimesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *PostIdentifier = @"PostCell";
    SPNIndiePostCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PostIdentifier
                                                                           forIndexPath:indexPath];
    
    NSDictionary *feed = [cinelifeTiles objectAtIndex:indexPath.row];
    NSString *title = [NSString stringWithFormat:@"%@  ", [feed objectForKey:@"name"]];

    NSString *imageLink = [feed objectForKey:@"picture"];
    
    if (imageLink) {
        [cell.thumbnailImageView setImageURL:[NSURL URLWithString:imageLink]];
        [cell.thumbnailImageView setImage:nil];
    }
    [cell.titleLabel setText:title];

    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [cinelifeTiles count];
}

-(void)collectionView:(SPNShowtimesCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tile = [cinelifeTiles objectAtIndex:indexPath.row];
    
    if ([[tile objectForKey:@"type"] isEqualToString:@"Trivia"]) {
        triviaList =  [NSMutableArray arrayWithArray:[tile objectForKey:@"trivias"]];
        UIStoryboard *triviaStoryboard = [UIStoryboard storyboardWithName:@"WhatsNextStoryboard"
                                                                   bundle:nil];
        SPNTriviaQuestionViewController *questionController =  [triviaStoryboard instantiateViewControllerWithIdentifier:@"TriviaStart"];
        
        [questionController setTriviaQuestions:triviaList];
        [questionController setIsLandingTrivia:YES];
        [self.navigationController pushViewController:questionController animated:YES];
    } else if ([[tile objectForKey:@"type"] isEqualToString:@"Favorites"]) {
        [self getFavoriteFilms];
    } else if ([[tile objectForKey:@"type"] isEqualToString:@"Feed"]) {
        [self showAllPostsForFeed:indexPath];
    } else {
        WebViewController *webViewController = [[WebViewController alloc] init];
        webViewController.urlString = tile[@"favorites_trivia_url"];
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(SPNIndiePostCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.thumbnailImageView.imageURL];
    
}
#pragma mark - RSS functions
-(void)showAllPostsForFeed:(NSIndexPath*)indexPath
{
    NSDictionary *selectedPage = [cinelifeTiles objectAtIndex:indexPath.row];
    selectedFeedName = [selectedPage objectForKey:@"name"];
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    dispatch_async(kBgQueue, ^{
        NSMutableArray *unsortedFeed = [NSMutableArray new];
        NSArray *feed = [SPNCinelifeQueries getNewsfeedStoriesForPages:@[selectedPage]
                                                         withPostCount:kMAX_POST_COUNT];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
        
        for (NSDictionary *entry in feed)
        {
            [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
            
            NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"publishedDate"]];
            
            if (!realDate) {
                realDate = [NSDate date];
            }
            NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
            [entryWithDate setObject:realDate
                              forKey:@"publishedDate"];
        
                [unsortedFeed  addObject:entryWithDate];
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
        selectedPostFeed = [unsortedFeed sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            [self performSegueWithIdentifier:@"showAllPosts" sender:self];
        });
    });
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.view.frame.size.width*0.5-2.5;
    return CGSizeMake(width, width*1.25);
}

#pragma mark - Navigation functions
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showAllPosts"]) {
        SPNIndieRSSFullListViewController *rssListController = [segue destinationViewController];
        [rssListController setPostFeed:selectedPostFeed];
        [rssListController setFeedName:selectedFeedName];
    }
}

#pragma mark - Favorite functions
-(void)getFavoriteFilms
{
    if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not now"
                           withOtherTitles:@"Sign In"];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [[SPNCinelifeQueries sharedInstance] getFavoritesMoviesAndFestivalsWithCompletion:^(NSArray *movies, NSArray *festivals, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            
            UIStoryboard *filmsStoryboard = [UIStoryboard storyboardWithName:@"FilmsStoryboard" bundle:nil];
            
            if (movies.count == 0 && festivals.count == 0) {
                [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeGoWatchItNoQueue];
            } else {
                SPNFavoriteFilmsViewController *controller = [filmsStoryboard instantiateViewControllerWithIdentifier:@"FavoriteFilms"];
                controller.movies = movies;
                controller.festivals = festivals;
                [self.navigationController pushViewController:controller
                                                     animated:YES];
                
            }
        }];
    }
}

#pragma mark Alert view functions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Sign In"]) {
        [alertView dismissWithClickedButtonIndex:buttonIndex
                                        animated:NO];
        self.tabBarController.selectedIndex = 3;
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
  }
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery myCinelifeTiles:(NSArray *)tiles
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    cinelifeTiles = tiles;
    [self.indieCollectionView reloadData];
}


- (IBAction)callPromotionsGui:(id)sender {
//    NSDictionary *user = [SPNUser getCurrentUserId];
//    NSString *userId = [user objectForKey:@"id"];
//    if (userId) {
//        UIViewController *vc = [Intesify
//                                loadIntesifyViewWithBaseURL:@"https://cinemaup.intensify-solutions.com" openPostId:NULL
//                                userId:userId deviceToken:@"" language:@"" viewOptions:nil];
//        if (vc != nil) {
//            [self presentViewController:vc animated:YES completion:nil];
//
//        }
//    } else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not now"
                           withOtherTitles:@"Sign In"];
//    }
}
@end
