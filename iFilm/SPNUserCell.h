//
//  SPNUserCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNUserCell : UITableViewCell

@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *emailTextField;

@property IBOutlet UILabel *userTitleLabel;
@property IBOutlet UILabel *storeDataMessageLabel;

@property IBOutlet UIView *storePersonalDataContainerView;
@property IBOutlet UIImageView *checkIcon;
@end
