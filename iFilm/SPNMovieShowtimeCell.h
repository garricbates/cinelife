//
//  SPNMovieShowtimeCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNMovieShowtimeCell : UITableViewCell
@property () IBOutlet UILabel *titleLabel;
@property () IBOutlet UILabel *ratingLabel;
@property () IBOutlet UILabel *scoreLabel;
@property IBOutlet UILabel *metaScoreLabel;
@property () IBOutlet UIImageView *metaIconImageView;

@property () IBOutlet UIButton *loadMovieDataButton;
@property () IBOutlet UIImageView *posterImageView;
@property () IBOutlet UIView *showtimeContainerView;
@property () IBOutlet UILabel *showtimesLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaIconWidthConstraint;

-(void)initializeMovieShowtimeCell:(NSDictionary *)schedule
                      forTableView:(UITableView*)tableView
                      forIndexPath:(NSIndexPath*)indexPath;

-(NSArray*)getShowsForThisMovie:(NSIndexPath*)indexPath
                    forSchedule:(id)schedule
                        forDate:(NSString*)dateString;

-(void)setShowForCell:(NSArray*)shows withBrandColor:(UIColor*)showColor;
@end
