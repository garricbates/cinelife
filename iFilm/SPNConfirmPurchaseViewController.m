//
//  SPNConfirmPurchaseViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/4/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNConfirmPurchaseViewController.h"
#import "SPNPassbookViewController.h"

static NSString *CellIdentifier = @"PurchaseOrderCell";

@interface SPNConfirmPurchaseViewController ()
{
    BOOL shouldAddBackground;
    PKPass *generatedPass;
    NSDictionary *transactionInfo;
    UIColor *sectionColor, *titleSectionColor, *mainColor;
    NSMutableArray *passbookTicketArray;
}
@end

@implementation SPNConfirmPurchaseViewController


-(void)viewWillAppear:(BOOL)animated
{
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(abortPurchase:)
                                                 name:@"cancelPurchase"
                                               object:nil];
    
    if (!shouldAddBackground) {
    
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 333, self.scrollView.bounds.size.width+150, 600)];
        [bottomView setBackgroundColor:[UIColor spn_brownColor]];
        if (mainColor) {
            [bottomView setBackgroundColor:mainColor];
        }
        [self.scrollView addSubview:bottomView];
        [self.scrollView sendSubviewToBack:bottomView];
        [self addGradientToView:self.swipeLabel];
        shouldAddBackground = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setClipsToBounds:YES];
    // Do any additional setup after loading the view.
    UISwipeGestureRecognizer *swiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(confirmPurchase)];
    [self.view addGestureRecognizer:swiper];
    [self initializeFilmData:self.movieInformation];
    [self customizeUI];
    [self.ticketContainerView.layer setBorderColor:[UIColor spn_darkBrownColor].CGColor];
    [self.swipeLabel setBackgroundColor:[UIColor spn_darkBrownColor]];
    
    NSDictionary *metacritic = [self.movieInformation objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    [self.theatreBackgroundView setBackgroundColor:[UIColor spn_darkBrownColor]];
    
    [self initializeFilmData:self.movieInformation];
    [self initializeTickets];
    [self getBrandColors:self.branding];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)abortPurchase:(id)sender
{
    [UIAlertView showAlertViewWthTitle:@"Showtime not available" andMessage:@"The showtime you selected is not available anymore. Please select a new showtime to purchase tickets!"];
    
    NSArray *controllers = [self.navigationController childViewControllers];
    [self.navigationController popToViewController:[controllers objectAtIndex:1] animated:YES];
    
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark UI customization
-(void)customizeUI
{
    
    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.genresLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.userRatingLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel.layer setBorderColor:[UIColor whiteColor].CGColor];
    
   
    [self.totalAmountLabel setFont:[UIFont spn_NeutraBoldLarge]];
    [self.ticketsLabel setFont:[UIFont spn_NeutraSmallMedium]];
    
    [self.convenienceChargeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.totalConvenienceChargeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    
    [self.totalLabel setFont:[UIFont spn_NeutraBoldLarge]];
    [self.ticketsTitleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    
    [self.dateLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.showtimeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    
    [self.swipeLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [self.confirmLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    
    [self.sectionView setBackgroundColor:[UIColor spn_darkBrownColor]];
    
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        
        NSString *logo = [branding objectForKey:@"logo"];
        if ([logo length] > 0) {
            
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
        }

        else {
            [self.brandLogoHeightConstraint setConstant:0];
            [self.scrollOriginTopConstraint setConstant:22];
        }

        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
            [self.sectionView setBackgroundColor:sectionColor];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            titleSectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
            
            [self.confirmLabel setTextColor:titleSectionColor];
        }
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            [self.view setBackgroundColor:mainColor];
            [self.theatreBackgroundView setBackgroundColor:mainColor];
        }
    }
    else {
        [self.brandLogoHeightConstraint setConstant:0];
        [self.scrollOriginTopConstraint setConstant:22];
    }
}

#pragma mark - Film initialization
-(void)initializeFilmData:(NSDictionary*)film
{
    NSString *movieTitle =[film objectForKey:@"name"] ? [film objectForKey:@"name"] : [film objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *urlImage;
    NSDictionary *posters = [film objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];

    self.posterImageView.imageURL = [NSURL URLWithString:urlImage];
    
    
    id genres =[film objectForKey:@"genres"];
    
    if ([genres isKindOfClass:[NSArray class]]) {
        self.genresLabel.text = [genres componentsJoinedByString:@" | "];
    }
    else if ([genres isKindOfClass:[NSString class]]) {
        self.genresLabel.text = genres;
    }
    else {
        self.genresLabel.text = @"No genre information available";
    }
    NSString *rating = [film objectForKey:@"rating"];
    
    if ([rating length] > 0) {
        self.classificationLabel.text = [NSString stringWithFormat:@"%@", rating];
    }
    else {
        self.classificationLabel.text = @"";
    }
    
    NSString *runTime = [film objectForKey:@"runtime"];
    if (!runTime || [runTime length] < 1) {
        runTime = @"0";
    }
    NSString *duration = [NSString stringWithFormat:@"%@ min", runTime];
    self.durationLabel.text = duration;
    
    NSDictionary *metacritic = [film objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    } 
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        [self.userRatingLabel setText:[NSString stringWithFormat:@"%g", [userScore floatValue]]];
    }
    else {
        [self.userRatingLabel setText:@"No votes"];
    }
}


#pragma mark - Ticket initialization
-(void)initializeTickets
{
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEEdMMMM" options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    
    NSDate *selectedDate = [self.showtimeInformation objectForKey:@"showDate"];
    NSString *todayString = [NSString stringWithFormat:@"Date: %@", [dateFormatter stringFromDate:selectedDate]];
    

    NSRange boldFontRange = NSMakeRange(0, [@"Date:" length]);
    
    NSMutableAttributedString *dateWithTitle = [[NSMutableAttributedString alloc] initWithString:todayString];
    [dateWithTitle beginEditing];
    [dateWithTitle addAttribute:NSFontAttributeName
                         value:[UIFont spn_NeutraBoldMediumLarge]
                         range:boldFontRange];
    [dateWithTitle endEditing];
    [self.dateLabel setAttributedText:dateWithTitle];

    
    boldFontRange = NSMakeRange(0, [@"Showtime:" length]);
    
    
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [showtimeDateFormatter setLocale:usLocale];
    [showtimeDateFormatter setDefaultDate:[NSDate date]];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [showtimeDateFormatter dateFromString:[self.showtimeInformation objectForKey:@"showTime"]];
    [showtimeDateFormatter setDateFormat:@"h:mma"];
    NSString* showDate = [showtimeDateFormatter stringFromDate:date];
    
    NSString *showString = [NSString stringWithFormat:@"Showtime: %@",showDate];
    NSMutableAttributedString *showWithTitle = [[NSMutableAttributedString alloc] initWithString:showString];
    [showWithTitle beginEditing];
    [showWithTitle addAttribute:NSFontAttributeName
                          value:[UIFont spn_NeutraBoldMediumLarge]
                          range:boldFontRange];
    [showWithTitle endEditing];
    [self.showtimeLabel setAttributedText:showWithTitle];
    
    NSDictionary *tickets = [self.paymentInformation objectForKey:@"tickets"];
    NSString *totalTickets = @"";
    NSMutableArray *ticketList = [NSMutableArray new];
    for (NSString *ticketType in [tickets allKeys])
    {
        NSDictionary *ticket = [tickets objectForKey:ticketType];
        NSString *ticketAndAmount = [NSString stringWithFormat:@"-%@ (%@)", ticketType, [ticket objectForKey:@"amount"]];
        [ticketList addObject:ticketAndAmount];
    }
    totalTickets = [[ticketList componentsJoinedByString:@"\r\n"]
                    stringByAppendingString:@"\r\n"];
    [self.ticketsLabel setText:totalTickets];
    CGFloat ccharge = [[self.paymentInformation objectForKey:@"ccharge"] floatValue];
    [self.totalAmountLabel setText:[NSString stringWithFormat:@"$%.2f", [[self.paymentInformation objectForKey:@"total"] floatValue]+ccharge]];
    [self.totalConvenienceChargeLabel setText:[NSString stringWithFormat:@"$%.2f", [[self.paymentInformation objectForKey:@"ccharge"] floatValue]]];
}


-(void)addGradientToView:(UIView*)view
{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    [self.view layoutIfNeeded];
    CGFloat gradientWidth = 100.0;
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = screenBounds;
    CGFloat gradientSize = gradientWidth / screenBounds.size.width;
    UIColor *gradient = [UIColor colorWithWhite:1.0f alpha:0.5];

    NSArray *startLocations = @[[NSNumber numberWithFloat:0.0f], [NSNumber numberWithFloat:(gradientSize / 2)], [NSNumber numberWithFloat:gradientSize]];
    NSArray *endLocations = @[[NSNumber numberWithFloat:(1.0f - gradientSize)], [NSNumber numberWithFloat:(1.0f -(gradientSize / 2))], [NSNumber numberWithFloat:1.0f]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    
    gradientMask.colors = @[(id)gradient.CGColor, (id)[UIColor blackColor].CGColor, (id)gradient.CGColor];
    gradientMask.locations = startLocations;
    gradientMask.startPoint = CGPointMake(0 - (gradientSize * 2), .5);
    gradientMask.endPoint = CGPointMake(1 + gradientSize, .5);
    
    //[view removeFromSuperview];
    view.layer.mask = gradientMask;
   // [superview addSubview:view];
    
    animation.fromValue = startLocations;
    animation.toValue = endLocations;
    animation.repeatCount = 3.4e38f;
    animation.duration  = 3.0f;
    
    [gradientMask addAnimation:animation forKey:@"animateGradient"];
}


#pragma mark - Purchase confirmation
-(void)confirmPurchase
{
    // Validate Purchase
   
    CGFloat totalPlusCCharge = [[self.paymentInformation objectForKey:@"ccharge"] floatValue] + [[self.paymentInformation objectForKey:@"total"] floatValue];
    
    NSString *amount = [NSString stringWithFormat:@"%f",totalPlusCCharge];
    NSDictionary *tickets = [self.paymentInformation objectForKey:@"tickets"];
    NSMutableArray *ticketArray = [NSMutableArray new];
    passbookTicketArray = [NSMutableArray new];
    NSMutableArray *ticketNames = [NSMutableArray new];
    NSArray *ticketTypes = [tickets allKeys];
    for (NSString *ticketType in ticketTypes) {
        NSDictionary *ticket = [tickets objectForKey:ticketType];
        NSDictionary *newTicket = [NSDictionary dictionaryWithObjects:@[[ticket objectForKey:@"amount"], [ticket objectForKey:@"ticket_type"]]
                                                              forKeys:@[@"amount", @"type"]];
        
        NSDictionary *newPassTicket = [NSDictionary dictionaryWithObjects:@[[ticket objectForKey:@"amount"], ticketType]
                                                              forKeys:@[@"number", @"type"]];
        [ticketArray addObject:newTicket];
        [passbookTicketArray addObject:newPassTicket];
        
        CGFloat cost = [[ticket objectForKey:@"total"] floatValue] / [[ticket objectForKey:@"amount"] floatValue];
        
        NSDictionary *newTicketName = [NSDictionary dictionaryWithObjects:@[ [ticket objectForKey:@"amount"], [ticket objectForKey:@"ticket_type"], [NSNumber numberWithFloat:cost], ticketType]
                                                                  forKeys:@[@"amount", @"type", @"price", @"name"]];
        [ticketNames addObject:newTicketName];
    }
    NSMutableDictionary *filmDetails = [NSMutableDictionary new];
    [filmDetails setObject:[self.movieInformation objectForKey:@"name"]
                    forKey:@"name"];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    NSDate *showDate = [self.showtimeInformation objectForKey:@"showDate"];
    NSString *newDate = [dateFormatter stringFromDate:showDate];
    
    [filmDetails setObject:[self.movieInformation objectForKey:@"id"]
                    forKey:@"id"];
    
    [filmDetails setObject:newDate
                    forKey:@"showdate"];
    [filmDetails setObject:[self.showtimeInformation objectForKey:@"showTime"]
                    forKey:@"showtime"];
    
    NSMutableDictionary *theater = [NSMutableDictionary new];
    [theater setObject:[self.showtimeInformation objectForKey:@"theatreName"]
                forKey:@"name"];
    [theater setObject:[self.showtimeInformation objectForKey:@"theatreId"]
                forKey:@"id"];
    [theater setObject:[self.ticketingInformation objectForKey:@"rts_id"]
                forKey:@"rts_id"];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    
    NSString *email = self.email;
    NSString *username = self.name;
    
    username = [username length] > 0 ? username : @"customer";
    
    NSDictionary *customer = @{@"email" : email, @"name" : username };
    [[SPNCinelifeQueries sharedInstance] postPurchaseTickets:ticketNames
                                                   forAmount:amount
                                            forPerformanceId:[self.ticketingInformation objectForKey:@"performance_id"]
                                                  forTheatre:theater
                                               forAuditorium:[self.ticketingInformation objectForKey:@"auditorium"]
                                               withTicketFee:[self.ticketingInformation objectForKey:@"ticket_fee"]
                                         withCardInformation:self.cardInformation
                                                 andCustomer:customer
                                              andFilmDetails:filmDetails];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PurchaseDetails"]) {
        SPNPassbookViewController *passbookController = [segue destinationViewController];
        [passbookController setMovieInformation:self.movieInformation];
        [passbookController setShowtimeInformation:self.showtimeInformation];
        [passbookController setPaymentInformation:self.paymentInformation];
        [passbookController setGeneratedPass:generatedPass];
        [passbookController setTransactionInformation:transactionInfo];
        [passbookController setBranding:self.branding];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if (transactionInfo) {
           [self performSegueWithIdentifier:@"PurchaseDetails" sender:self];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery purchasedTickets:(NSDictionary *)purchaseInformation
{
    transactionInfo = nil;
    if (!purchaseInformation || [purchaseInformation objectForKey:@"errors"]) {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        NSString *errorMessage = @"We could not process your purchase at this time. No charge was made to your card. Sorry for the inconvenience.";
        errorMessage =  [purchaseInformation objectForKey:@"error_msg"] ? [purchaseInformation objectForKey:@"error_msg"]  : errorMessage;
       
        [UIAlertView showAlertViewWthTitle:@"There was an error in the purchase"
                                andMessage:errorMessage];
        
    }
    else {
        
        NSMutableDictionary *passbookInformation = [NSMutableDictionary new];
        
        transactionInfo = purchaseInformation;
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        [passbookInformation setObject:[purchaseInformation objectForKey:@"transaction"]
                                forKey:@"transaction"];
        [passbookInformation setObject:[purchaseInformation objectForKey:@"customer_email"]
                                forKey:@"email"];
        
        [[SPNCinelifeQueries sharedInstance] postCreatePassbookForPurchase:passbookInformation];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery createdPassbook:(PKPass *)passbook
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    generatedPass = passbook;
    [self performSegueWithIdentifier:@"PurchaseDetails" sender:self];
}
@end
