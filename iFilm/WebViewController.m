//
//  WebViewController.m
//  iFilm
//
//  Created by Vlad Getman on 16.12.16.
//  Copyright © 2016 La Casa de los Pixeles. All rights reserved.
//

#import "WebViewController.h"
#import <WebKit/WebKit.h>

@interface WebViewController () <WKNavigationDelegate>

@end

@implementation WebViewController

- (void)loadView {
    WKWebView *webView = [WKWebView new];
    webView.navigationDelegate = self;
    self.view = webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:self.urlString];
    [[self webView] loadRequest:[NSURLRequest requestWithURL:url]];
}

- (WKWebView *)webView {
    return (WKWebView *)self.view;
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    self.title = webView.title;
}

@end
