//
//  SPNFilmReviewCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNFilmReviewCell : UITableViewCell

@property () IBOutlet UILabel *reviewLabel;
@property () IBOutlet UILabel *reviewTitleLabel;

@property () IBOutlet UIImageView *star1ImageView;
@property () IBOutlet UIImageView *star2ImageView;
@property () IBOutlet UIImageView *star3ImageView;
@property () IBOutlet UIImageView *star4ImageView;
@property () IBOutlet UIImageView *star5ImageView;


-(void)initializeReviewCell:(NSDictionary*)filmReview;
@end
