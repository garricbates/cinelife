//
//  SPNAppDelegate.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationsHelper.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface SPNAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) MFMailComposeViewController *globalMailComposer;

@property (nonatomic, strong) CLLocation *myLocation;
- (NSURL *)applicationDocumentsDirectory;

-(void)cycleTheGlobalMailComposer;

- (void)handleNotification:(NotificationType)type objectId:(NSString *)objectId manual:(BOOL)manual;

+ (NSString *)uuid;

@end
