//
//  SPNTheatreSearchTableViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 8/4/15.
//  Copyright (c) 2015 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreSearchTableViewController.h"
#import "SPNFilmDetailsViewController.h"
#import "SPNTheatreDetailsViewController.h"
#import "SPNTheatreMapViewController.h"
#import "SPNFestivalCell.h"
#import "Festival.h"
#import "UIColor+HexComponent.h"

#import "SPNShowMoreCell.h"
#import "SPNMovieCollectionCell.h"
#import "SPNShowtimesCollectionView.h"
#import "SPNFestivalViewController.h"
#import "SPNFilterCell.h"
#import "SPNFestivalFilterCell.h"

#import "SPNAppDelegate.h"

@interface SPNTheatreSearchTableViewController () <SPNCinelifeQueriesDelegate, CLLocationManagerDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate, SPNTheatreCellDelegate, SPNFestivalCellDelegate, SPNFestivalFilterCellDelegate> {
    BOOL isFetchingLocation;
    NSInteger theatersCellsToDisplay;
    NSInteger festivalsCellsToDisplay;
    NSMutableDictionary *contentOffsetDictionary;
    NSDictionary *selectedTheatreForFilm;
    NSInteger radiusAmplifier;
    
    NSDictionary *selectedMovie;
    NSMutableDictionary *selectedFilmAndShowtimes;
    BOOL shouldGetBothFilmAndTheater;
    BOOL justOpenedApp;
    BOOL onlyWantLocation;
    BOOL isFetchingFavorites;
    BOOL isFetchingPinned;
    BOOL shouldShowFilterOptions;
    
    BOOL canLoadMoreFestivals;
    
    NSString *filterText;
    NSDictionary *_filters;
    NSString *currentZip;
}

- (IBAction)swapToOtherTheaterList:(UISegmentedControl *)sender;

@property (nonatomic, weak) IBOutlet UISearchBar *zipcodePicker;

@property (nonatomic, weak) IBOutlet UIButton *mapButton;

@property (nonatomic) SPNTheatreCell *prototypeCell;

@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic, retain) CLGeocoder *geoCoder;

@property (nonatomic, retain) NSString *lastSearchLocation;

@property (nonatomic, retain) NSMutableArray *theatersNearby;
@property (nonatomic, retain) NSMutableArray <Festival *> *festivals;
@property (nonatomic, retain) NSMutableArray <Festival *> *pinnedFestivals;

@end

static NSString* FilterCellIdentifier = @"FilterCell";
static NSString* TheatreCellIdentifier = @"TheatreCell";
static NSString* FestivalCellIdentifier = @"FestivalCell";
static NSString* ShowCellIdentifier = @"MoreCell";

#define kNumberOfCellsToAdd 10
#define INITIAL_RADIUS 10
#define RADIUS_ENHANCE 10
#define MAX_RADIUS 30

@implementation SPNTheatreSearchTableViewController

-(SPNTheatreCell *)prototypeCell
{
    if (!_prototypeCell) {
        _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:TheatreCellIdentifier];
    }
    return _prototypeCell;
}

-(CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        
    #ifdef __IPHONE_8_0
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [_locationManager requestWhenInUseAuthorization];
        }
    #endif
    }
    return _locationManager;
}

-(CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (isFetchingFavorites) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeUI];
    
    _filters = @{@"near_me":@YES};
    shouldShowFilterOptions = NO;
    [self invalidateFilter];
    
    justOpenedApp = YES;
    shouldGetBothFilmAndTheater = NO;
    radiusAmplifier = 0;
    contentOffsetDictionary = [NSMutableDictionary new];
    isFetchingLocation = NO;
    
    [self.tableView registerCellClass:[SPNTheatreCell class] withIdentifier:TheatreCellIdentifier];
    [self.tableView registerCellClass:[SPNFestivalCell class] withIdentifier:FestivalCellIdentifier];
    [self.tableView registerCellClass:[SPNShowMoreCell class] withIdentifier:ShowCellIdentifier];
    [self.tableView registerCellClass:[SPNFilterCell class] withIdentifier:FilterCellIdentifier];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addedTheaterToFavorites:)
                                                 name:kNotificationAddedTheaterToFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedTheaterFromFavorites:)
                                                 name:kNotificationRemovedTheaterFromFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addedFestivalToFavorites:)
                                                 name:kNotificationAddedFestivalToFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedFestivalFromFavorites:)
                                                 name:kNotificationRemovedFestivalFromFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable)
                                                 name:@"UserLogout"
                                               object:nil];
    
    NSString* storedZipcode = [prefs objectForKey:@"zip"];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    if ([SPNUser userLoggedIn]) {
        onlyWantLocation = YES;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                             animated:YES];
        [self.locationManager startUpdatingLocation];
        isFetchingFavorites = YES;
        [[SPNCinelifeQueries sharedInstance] getFavoritesTheatersAndFestivalsWithCompletion:^(NSArray *theaters, NSArray *festivals, NSError *error) {
            [self favoritesLoadedWithTheaters:theaters festivals:festivals];
        }];
    }
    else {
        if (storedZipcode) {
            [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
            [self decodeAddress:storedZipcode forFesivals:NO];
            onlyWantLocation = YES;
            [self.locationManager startUpdatingLocation];
        }
        else {
            isFetchingLocation = YES;
            onlyWantLocation = NO;
            [self.locationManager startUpdatingLocation];
        }
    }
}
    
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.locationManager && ![appDelegate myLocation] && [CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    }
    if (self.selectedTheaterType == SPNTheatreTypeInRegion) {
        [self.zipcodePicker setUserInteractionEnabled:YES];
    }
        
}
    
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.zipcodePicker resignFirstResponder];
}

-(void)reloadTable
{
    [self.theaterSegmentControl setSelectedSegmentIndex:0];
    [self.myTheaters removeAllObjects];
    self.selectedTheaterType = SPNTheatreTypeInRegion;
    [self.tableView reloadData];
}

- (void)addedFestivalToFavorites:(NSNotification *)notification {
    NSNumber *festivalId = [[notification userInfo] objectForKey:@"id"];
    Festival *festival = [_festivals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"festivalId == %@", festivalId]].firstObject;
    if (!festival) {
        return;
    }
    festival.isFavorited = @YES;
    if (self.selectedTheaterType == SPNTheatreTypeFestival) {
        [self.tableView reloadData];
    }
}

- (void)removedFestivalFromFavorites:(NSNotification *)notification {
    NSNumber *festivalId = [[notification userInfo] objectForKey:@"id"];
    Festival *festival = [_festivals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"festivalId == %@", festivalId]].firstObject;
    if (!festival) {
        return;
    }
    festival.isFavorited = @NO;
    if (self.selectedTheaterType == SPNTheatreTypeFestival) {
        [self.tableView reloadData];
    }
}

- (void)addedTheaterToFavorites:(NSNotification *)notification
{
    NSString *theaterId = [[notification userInfo] objectForKey:@"id"];
    NSInteger index = [self.theatersNearby indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj isKindOfClass:[NSDictionary class]]) {
            return NO;
        }
        return [theaterId isEqualToString:[obj objectForKey:@"id"]];
    }];
    if (index != NSNotFound) {
        NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:[self.theatersNearby objectAtIndex:index]];
        [theater setObject:@1 forKey:@"is_favorite"];
        [self.theatersNearby replaceObjectAtIndex:index withObject:theater];
        NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:1];
        SPNTheatreCell *cell = (SPNTheatreCell*)[self.tableView cellForRowAtIndexPath:path];
        [cell.favoriteBtn setImage:[UIImage imageNamed:@"heart-filled"]
                          forState:UIControlStateNormal];
    }

    if ([self.myTheaters count] < 1) {
        self.myTheaters = [NSMutableArray new];
        NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:[self.theatersNearby objectAtIndex:index]];
        [theater setObject:@1 forKey:@"is_favorite"];
        [self.myTheaters addObject:theater];
        if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
            [self.tableView reloadData];
        }
    }
    else {
        NSInteger favIndex = [self.myTheaters indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [theaterId isEqualToString:[obj objectForKey:@"id"]];
        }];
        if (favIndex == NSNotFound) {
            
            if ([self.theatersNearby count] < 1) {
                NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:[notification userInfo]];
                [theater setObject:@1 forKey:@"is_favorite"];
                [self.myTheaters addObject:theater];
                if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
                    [self.tableView reloadData];
                }
            }
            else {
                NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:[self.theatersNearby objectAtIndex:index]];
                [theater setObject:@1 forKey:@"is_favorite"];
                [self.myTheaters addObject:theater];
                if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
                    [self.tableView reloadData];
                }
            }
        }
    }
}


-(void)removedTheaterFromFavorites:(NSNotification*)notification
{
    NSString *theaterId = [[notification userInfo] objectForKey:@"id"];
    NSInteger index = [self.theatersNearby indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if (![obj isKindOfClass:[NSDictionary class]]) {
            return NO;
        }
        return [theaterId isEqualToString:[obj objectForKey:@"id"]];
    }];
    if (index != NSNotFound) {
        NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:[self.theatersNearby objectAtIndex:index]];
        [theater setObject:@0 forKey:@"is_favorite"];
        [self.theatersNearby replaceObjectAtIndex:index withObject:theater];
        NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:1];
        SPNTheatreCell *cell = (SPNTheatreCell*)[self.tableView cellForRowAtIndexPath:path];
        [cell.favoriteBtn setImage:[UIImage imageNamed:@"heart-icon"]
                          forState:UIControlStateNormal];
    }
    
   
    NSInteger favIndex = [self.myTheaters indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [theaterId isEqualToString:[obj objectForKey:@"id"]];
        }];
    if (favIndex != NSNotFound) {
        [self.myTheaters removeObjectAtIndex:favIndex];
        if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
            [self.tableView reloadData];
        }
    }
}

- (void)setPinnedFestivals:(NSMutableArray *)pinnedFestivals {
    _pinnedFestivals = pinnedFestivals;
    [self reloadTheaters];
}

- (void)setTheatersNearby:(NSMutableArray *)theatersNearby {
    _theatersNearby = theatersNearby;
    [self reloadTheaters];
}
    
- (void)reloadTheaters {
    if (_pinnedFestivals.count == 0) {
        return;
    }
    if (_theatersNearby.count == 0 && _pinnedFestivals.count > 0) {
        _theatersNearby = @[_pinnedFestivals.firstObject].mutableCopy;
    }
    
    NSInteger count = _theatersNearby.count;
    
    [_theatersNearby insertObject:_pinnedFestivals.firstObject atIndex:0];
    if (_pinnedFestivals.count > 1) {
        [_theatersNearby insertObject:_pinnedFestivals[1] atIndex:1];
    }
    
    for (NSInteger i = 1; i <= 2; i++) {
        NSInteger index = 3 * i + 1;
        if (count > index) {
            NSInteger pinnedIndex = i + 1;
            if (_pinnedFestivals.count > pinnedIndex) {
                [_theatersNearby insertObject:_pinnedFestivals[pinnedIndex] atIndex:index];
            }
        } else {
            break;
        }
    }
    
    [self.tableView reloadData];
}

-(void)customizeUI
{
    theatersCellsToDisplay = kNumberOfCellsToAdd;
    festivalsCellsToDisplay = kNumberOfCellsToAdd;
    
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView setBackgroundColor:[UIColor spn_brownColor]];
    
    [self.zipcodePicker setBackgroundImage:[UIImage new]];
    
    [self.mapButton.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
    self.mapButton.layer.cornerRadius = 2.5;
    self.mapButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
    
    UIImage *image = [self.mapButton imageForState:UIControlStateNormal];
    image = [image scaledToFitSize:CGSizeMake(24, 15)];
    [self.mapButton setImage:image forState:UIControlStateNormal];
    
    if (@available(iOS 11.0, *)) {
        UIView *headerView = self.tableView.tableHeaderView;
        CGRect frame = headerView.frame;
        frame.size.height = 56;
        headerView.frame = frame;
        self.tableView.tableHeaderView = headerView;
    }
    
    [self initializeKeyboard];
    
    [self.mapButton addTarget:self action:@selector(mapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark Map Functions
- (void)mapButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"Map" sender:self];
}

#pragma mark - Custom leyboard features
-(void)initializeKeyboard
{
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    [keyboardDoneButtonView setBarTintColor:[UIColor spn_darkGrayColor]];
    keyboardDoneButtonView.translucent = YES;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    
    UIBarButtonItem* getZipButton = [[UIBarButtonItem alloc] initWithTitle:@"Nearby"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(activateGPSLocation:)];
    
    UIBarButtonItem* useSavedLocButton = [[UIBarButtonItem alloc] initWithTitle:@"Saved"
                                                                          style:UIBarButtonItemStylePlain target:self
                                                                         action:@selector(savedLocation:)];
    
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(cancelClicked:)];
    
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, getZipButton, useSavedLocButton, cancelButton, nil]];
    self.zipcodePicker.inputAccessoryView = keyboardDoneButtonView;
    [self.zipcodePicker setKeyboardType:UIKeyboardTypeDefault];
    [self.zipcodePicker setSpellCheckingType:UITextSpellCheckingTypeNo];
    self.zipcodePicker.delegate = self;
}

-(void)doneClicked:(id)sender
{
    radiusAmplifier = 0;
    self.lastSearchLocation = [self.zipcodePicker text];
    [self decodeAddress:self.lastSearchLocation
            forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
    [self.zipcodePicker resignFirstResponder];
}

-(void)activateGPSLocation:(id)sender
{
    radiusAmplifier = 0;

    isFetchingLocation = YES;
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self.locationManager startUpdatingLocation];
    [self.zipcodePicker resignFirstResponder];
}

-(void)savedLocation:(id)sender
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* storedZipcode = [prefs objectForKey:@"zip"];
    if (storedZipcode) {
        radiusAmplifier = 0;
        [self decodeAddress:storedZipcode
                forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
    }
    else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeNoSavedLocation];
    }
    [self.zipcodePicker resignFirstResponder];
}

-(void)cancelClicked:(id)sender
{
    [self.zipcodePicker resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.lastSearchLocation = [self.zipcodePicker text];
    [self decodeAddress:self.lastSearchLocation
            forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
    [self.zipcodePicker resignFirstResponder];
}
#pragma mark - location manager stuff
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (isFetchingLocation) {
        isFetchingLocation = NO;
        
        [[[UIAlertView alloc] initWithTitle:@"Could not find you" message:@"We could not determine your location. Make sure you have granted location services to the app and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    if (_filters[@"near_me"]) {
        NSMutableDictionary *filters = _filters.mutableCopy;
        [filters removeObjectForKey:@"near_me"];
        _filters = filters.copy;
    }
    
    [self.tableView reloadData];
    NSLog(@"Error locating");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *userLocation = [locations lastObject];
    
    [appDelegate setMyLocation:userLocation];
    [self.locationManager stopUpdatingLocation];
    
    if (onlyWantLocation) {
        onlyWantLocation = NO;
        return;
    }
    if (isFetchingLocation) {
        isFetchingLocation = NO;
        if (NO) {//shouldFetchSavedLocationTheatres) {
          //  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
         //   NSString* storedZipcode = [prefs objectForKey:@"zip"];
            //[self getLocationWithUserInput:storedZipcode];
        }
        else {
            CLGeocoder *geocoder = [CLGeocoder new];
            [geocoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                
                if ([placemarks count] > 0) {
                    
                    CLPlacemark *place = [placemarks firstObject];
                    
                    NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                    if (zip) {
                        if (self.selectedTheaterType == SPNTheatreTypeFestival) {
                            [self getFestivalsWithZip:_filters[@"near_me"] ? nil : zip
                                        orCoordinates:_filters[@"near_me"] ? [appDelegate myLocation] : nil];
                        } else {
                            [self getTheatersWithZip:zip
                                       orCoordinates:userLocation];
                        }
                        if (!_filters[@"near_me"]) {
                            [self setCityAndStateToSearchBar:place.addressDictionary];
                        }
                    } else {
                        
                        if (self.selectedTheaterType == SPNTheatreTypeFestival) {
                            [self getFestivalsWithZip:nil
                                        orCoordinates:_filters[@"near_me"] ? [appDelegate myLocation] : nil];
                        } else {
                            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                            [self.tableView reloadData];
                        }
                    }
                }
                else if (error.domain == kCLErrorDomain) {
                    switch (error.code)
                    {
                        case kCLErrorDenied:
                            self.zipcodePicker.text = @"Location Services Denied by User";
                            break;
                        case kCLErrorNetwork:
                            self.zipcodePicker.text = @"No Network";
                            break;
                        case kCLErrorGeocodeFoundNoResult:
                            self.zipcodePicker.text = @"No Result Found";
                            break;
                        default:
                            self.zipcodePicker.text = error.localizedDescription;
                            break;
                    }
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                }
                else {
                    self.zipcodePicker.text = error.localizedDescription;
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                }
            }];
        }
    }
}

-(void)decodeAddress:(NSString *)address forFesivals:(BOOL)forFestivals
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    
    if (![address.lowercaseString containsString:@"us"]) {
        address = [address stringByAppendingString:@" us"];
    }
    
    [self.geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark *usPlacemark = [placemarks firstObject];
            for (CLPlacemark* placemark in placemarks) {
                if ([placemark.ISOcountryCode isEqualToString:@"US"]) {
                    usPlacemark = placemark;
                }
            }
            [self.geoCoder reverseGeocodeLocation:usPlacemark.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                currentZip = zip;
                if (zip) {
                    [self.zipcodePicker setText:zip];
                    if (forFestivals) {
                        [self getFestivalsWithZip:zip orCoordinates:nil];
                    } else {
                        [self getTheatersWithZip:zip orCoordinates:nil];
                    }
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                }
                else {
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                    
                }
            }];
        }
        else if (error.domain == kCLErrorDomain) {
            switch (error.code)
            {
                case kCLErrorDenied:
                    self.zipcodePicker.text = @"Location Services Denied by User";
                    break;
                case kCLErrorNetwork:
                    self.zipcodePicker.text = @"No Network";
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    self.zipcodePicker.text = @"No Result Found";
                    break;
                default:
                    self.zipcodePicker.text = error.localizedDescription;
                    break;
            }
            currentZip = nil;
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
        else {
            currentZip = nil;
            self.zipcodePicker.text = error.localizedDescription;
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
        
    }];
}

-(void)setCityAndStateToSearchBar:(NSDictionary*)placeDictionary
{
    NSString *country = [placeDictionary objectForKey:@"CountryCode"];
    NSString *zipcode = [placeDictionary objectForKey:@"ZIP"];
    
    if ([country isEqualToString:@"US"]) {
        NSString *city = [placeDictionary objectForKey:@"City"];
        NSString *state = [placeDictionary objectForKey:@"State"];
        NSString *location = [NSString stringWithFormat:@"%@, %@ (%@)", city, state, zipcode];
        [self.zipcodePicker setText:location];
        self.lastSearchLocation = zipcode;
    } else {
        [self.zipcodePicker setText:zipcode];
        self.lastSearchLocation = zipcode;
    }
    currentZip = zipcode;
}

#pragma mark - Filter

- (void)didApplyFilters:(NSDictionary *)newFilters andCloseFilterCell:(BOOL)close {
    BOOL isNearMe = [newFilters[@"near_me"] boolValue];
    BOOL isNearMeChanged = [_filters[@"near_me"] boolValue] != isNearMe;
    
    _filters = newFilters;
    
    [self.tableView beginUpdates];
    [self invalidateFilter];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
    if (close) {
        shouldShowFilterOptions = !close;
    }
    [self.tableView endUpdates];
    
    if (isNearMe) {
        if (isNearMeChanged) {
            self.zipcodePicker.text = nil;
        }
        [self getFestivalsWithZip:nil orCoordinates:[appDelegate myLocation]];
    } else {
        if (isNearMeChanged) {
            isFetchingLocation = YES;
            [self.locationManager startUpdatingLocation];
        } else {
            [self getFestivalsWithZip:currentZip
                        orCoordinates:currentZip ? nil : [appDelegate myLocation]];
        }
    }
}

- (void)invalidateFilter {
    [self.tableView beginUpdates];
    
    if (_filters.count > 0) {
        
        NSDictionary *info = [SPNFestivalFilterCell titles];
        
        NSMutableArray *filterComponents = [NSMutableArray new];
        if (_filters[@"search"]) {
            [filterComponents addObject:[NSString stringWithFormat:@"\"%@\"", _filters[@"search"]]];
        }
        
        for (NSString *key in [SPNFestivalFilterCell sortedKeys]) {
            if (_filters[key]) {
                [filterComponents addObject:info[key]];
            }
        }
        
        filterText = [filterComponents componentsJoinedByString:@" "];
    } else {
        filterText = @"All festivals";
    }
    [self.tableView endUpdates];
}

- (Festival *)festivalAt:(NSIndexPath *)indexPath {
    Festival *festival;
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeInRegion:
            festival = _theatersNearby[indexPath.row];
            break;
            
        case SPNTheatreTypeFavourites:
            festival = _myFestivals[indexPath.row];
            break;
            
        case SPNTheatreTypeFestival:
            festival = _festivals[indexPath.row];
            break;
    }
    return festival;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
        return 2;
    }
    else {
        NSInteger sections = 0;
        BOOL isFestival = self.selectedTheaterType == SPNTheatreTypeFestival;
        NSInteger count = (isFestival ? self.festivals : self.theatersNearby).count;
        if ((isFestival ? festivalsCellsToDisplay : theatersCellsToDisplay) < count) {
            sections = 2;
        } else if (isFestival && festivalsCellsToDisplay == count && canLoadMoreFestivals) {
            sections = 2;
        } else if (radiusAmplifier+INITIAL_RADIUS > MAX_RADIUS) {
            sections = 1;
        } else {
            sections = 1;
        }
        sections++;
        return sections;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
        return (section == 0 ? self.myTheaters : self.myFestivals).count;
    } else {
        if (section == 0) {
            if (self.selectedTheaterType == SPNTheatreTypeInRegion) {
                return 0;
            } else {
                return 2;
            }
        } else if (section == 1) {
            BOOL isFestival = self.selectedTheaterType == SPNTheatreTypeFestival;
            NSInteger count = (isFestival ? self.festivals : self.theatersNearby).count;
            return MIN((isFestival ? festivalsCellsToDisplay : theatersCellsToDisplay), count);
        } else {
            return 1;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedTheaterType == SPNTheatreTypeFestival && indexPath.section == 0) {
        if (indexPath.row == 0) {
            SPNFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:FilterCellIdentifier];
            cell.titleLabel.text = @"Filter events by:";
            cell.filterTextLabel.text = filterText;
            cell.separatorInset = UIEdgeInsetsMake(0, CGRectGetWidth(tableView.frame), 0, 0);
            return cell;
        } else {
            SPNFestivalFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterOptionsCell"];
            cell.delegate = self;
            cell.filters = _filters;
            return cell;
        }
    } else if ((self.selectedTheaterType == SPNTheatreTypeFestival && indexPath.section < 2) ||
        (self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 1) ||
        (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1 && [_theatersNearby[indexPath.row] isKindOfClass:[Festival class]])) {
        SPNFestivalCell *cell = [tableView dequeueReusableCellWithIdentifier:FestivalCellIdentifier];
        cell.delegate = self;
        
        Festival *festival = [self festivalAt:indexPath];
        [cell configureForFestival:festival];
        return cell;
    } else {
        if ((self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 0) ||
            (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1)) {
            NSArray *theaterList;
            switch (self.selectedTheaterType) {
                case SPNTheatreTypeFavourites:
                    theaterList = self.myTheaters;
                    break;
                default:
                    theaterList = self.theatersNearby;
                    break;
            }
            
            SPNTheatreCell *cell = [tableView dequeueReusableCellWithIdentifier:TheatreCellIdentifier];
            cell.delegate = self;
            NSDictionary *theater = [theaterList objectAtIndex:indexPath.row];
            
            [cell configureForTheater:theater];
            
            if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
                [cell.favoriteBtn setImage:[UIImage imageNamed:@"heart-filled"]
                                  forState:UIControlStateNormal];
            }
            
            NSArray *movies = [theater objectForKey:@"movies"];
            if ([movies count] > 0) {
                [cell.noShowsLabel setHidden:YES];
                cell.movies = movies;
                
            }
            else {
                [cell.noShowsLabel setHidden:NO];
            }
            for (SPNMovieCollectionCell *movieCell in [cell.horizontalCollectionView visibleCells])
            {
                [movieCell.thumbnail setImage:[UIImage imageNamed:@"poster-not-available"]];
            }
            return cell;
        } else {
            SPNShowMoreCell *showCell = [tableView dequeueReusableCellWithIdentifier:ShowCellIdentifier];
            showCell.showMoreButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
            [showCell.showMoreButton.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
            return showCell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeFavourites:
            if (indexPath.section == 0) {
                selectedTheatreForFilm = [self.myTheaters objectAtIndex:indexPath.row];
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                                     animated:YES];
                [[SPNCinelifeQueries sharedInstance] setDelegate:self];
                
                [[SPNCinelifeQueries sharedInstance] getTheaterInformation:[selectedTheatreForFilm objectForKey:@"id"]
                                                           withCredentials:[SPNUser getCurrentUserId]];
            } else {
                [self performSegueWithIdentifier:@"FestivalDetails" sender:_myFestivals[indexPath.row]];
            }
            break;
            
        case SPNTheatreTypeInRegion:
            if (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1 && [_theatersNearby[indexPath.row] isKindOfClass:[Festival class]]) {
                [self performSegueWithIdentifier:@"FestivalDetails" sender:_theatersNearby[indexPath.row]];
            } else if (indexPath.section == 1) {
                selectedTheatreForFilm = [self.theatersNearby objectAtIndex:indexPath.row];
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                                     animated:YES];
                [[SPNCinelifeQueries sharedInstance] setDelegate:self];
                
                [[SPNCinelifeQueries sharedInstance] getTheaterInformation:[selectedTheatreForFilm objectForKey:@"id"]
                                                           withCredentials:[SPNUser getCurrentUserId]];
                
            } else {
                if (theatersCellsToDisplay < [self.theatersNearby count]) {
                    NSUInteger i, totalNumberOfItems = [self.theatersNearby count];
                    NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, theatersCellsToDisplay + kNumberOfCellsToAdd);
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    
                    for (i=theatersCellsToDisplay; i<numberOfItemsToDisplay; i++)
                    {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                    }
                    
                    theatersCellsToDisplay = numberOfItemsToDisplay;
                    
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
                    
                    if (theatersCellsToDisplay == [self.theatersNearby count]  && radiusAmplifier > MAX_RADIUS) {
                        [tableView deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationTop];
                    }
                    [tableView endUpdates];
                }
                else {
                    radiusAmplifier += RADIUS_ENHANCE;
                    [self decodeAddress:self.zipcodePicker.text forFesivals:NO];
                }
            }
            break;
            
        case SPNTheatreTypeFestival:
            if (indexPath.section == 1) {
                [self performSegueWithIdentifier:@"FestivalDetails" sender:_festivals[indexPath.row]];
            } else if (indexPath.section == 0 && indexPath.row == 0) {
                shouldShowFilterOptions = !shouldShowFilterOptions;
                [self.view endEditing:YES];
                [self.tableView beginUpdates];
                [self.tableView endUpdates];
            } else if (indexPath.section == 2) {
                if (festivalsCellsToDisplay < self.festivals.count) {
                    NSUInteger i, totalNumberOfItems = self.festivals.count;
                    NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, festivalsCellsToDisplay + kNumberOfCellsToAdd);
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    
                    for (i=festivalsCellsToDisplay; i<numberOfItemsToDisplay; i++)
                    {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                    }
                    
                    festivalsCellsToDisplay = numberOfItemsToDisplay;
                    
                    [tableView beginUpdates];
                    [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
                    
                    if (!canLoadMoreFestivals && festivalsCellsToDisplay == self.festivals.count/* && radiusAmplifier > MAX_RADIUS*/) {
                        [tableView deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationTop];
                    }
                    [tableView endUpdates];
                } else {
                    NSMutableDictionary *f = [_filters mutableCopy];
                    f[@"offset"] = @(self.festivals.count);
                    _filters = [f copy];
                    [self getFestivalsWithZip:currentZip
                                orCoordinates:currentZip ? nil : [appDelegate myLocation]];
                }
            }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectedTheaterType == SPNTheatreTypeFestival && indexPath.section == 0) {
        if (indexPath.row == 0) {
            NSString *title = @"Filter events by:";
            CGFloat titleWidth = [title findWidthForHeight:30 andFont:[UIFont boldSystemFontOfSize:14]];
            CGFloat filterWidth = CGRectGetWidth(tableView.frame) - titleWidth - 12 - 32;
            CGFloat height = [filterText findHeightForWidth:filterWidth andFont:[UIFont systemFontOfSize:12]];
            return MAX(37, (16 + height));
        } else {
            if (shouldShowFilterOptions) {
                CGFloat height = 400;
                if (@available(iOS 11.0, *)) {
                    height += 10;
                }
                return height;
            } else {
                return 0;
            }
        }
    } else if ((self.selectedTheaterType == SPNTheatreTypeFestival && indexPath.section < 2) ||
        (self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 1) ||
        (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1 && [_theatersNearby[indexPath.row] isKindOfClass:[Festival class]])) {
        Festival *festival = [self festivalAt:indexPath];
        return [SPNFestivalCell heightForFestival:festival withWidth:CGRectGetWidth(tableView.frame)];
    } else {
        if ((self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 0) ||
            (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1)) {
            
            NSArray *theaterList;
            switch (self.selectedTheaterType) {
                case SPNTheatreTypeFavourites:
                    theaterList = self.myTheaters;
                    break;
                default:
                    theaterList = self.theatersNearby;
                    break;
            }
            [self.prototypeCell configureForTheater:theaterList[indexPath.row]];
            CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            return size.height + 1;
        }
        else {
            return kShowMoreCellHeight;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SPNTheatreCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (((self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 0) ||
         (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1)) && [cell isKindOfClass:[SPNTheatreCell class]]) {
        [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
        NSInteger index = cell.horizontalCollectionView.index;
        CGFloat horizontalOffset = [contentOffsetDictionary[[@(index) stringValue]] floatValue];

        [cell.horizontalCollectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
    }
    cell.backgroundColor = [UIColor spn_lightBrownColor];
    
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(SPNTheatreCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (((self.selectedTheaterType == SPNTheatreTypeFavourites && indexPath.section == 0) ||
         (self.selectedTheaterType == SPNTheatreTypeInRegion && indexPath.section == 1)) && [cell isKindOfClass:[SPNTheatreCell class]]) {
        CGFloat horizontalOffset = cell.horizontalCollectionView.contentOffset.x;
        NSInteger index = cell.horizontalCollectionView.index;
        contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
        
    }
}

#pragma mark - Collection view functions
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(SPNShowtimesCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ((self.selectedTheaterType == SPNTheatreTypeFavourites) ||
        (self.selectedTheaterType == SPNTheatreTypeInRegion)) {
        NSArray *theaterList;
        NSDictionary *theatre = nil;
    
        switch (self.selectedTheaterType) {
            case SPNTheatreTypeFavourites:
                theaterList = self.myTheaters;
                break;
            default:
                theaterList = self.theatersNearby;
                break;
        }
        
        if (collectionView.index < 1) {
            theatre = [theaterList firstObject];
        }
        else {
            theatre = [theaterList objectAtIndex:collectionView.index % [theaterList count]];
        }
        
        NSArray *movies = [theatre objectForKey:@"movies"];
        NSString *name = theatre[@"name"];
        if ([name containsString:@"Egypt"]) {
            NSLog(@"%@", movies);
        }
        return [movies count];
    } else {
        return 0;
    }
}

-(UICollectionViewCell *)collectionView:(SPNShowtimesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MovieCell";
    SPNMovieCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.thumbnail.image = [UIImage imageNamed:@"poster-not-available"];
    NSDictionary *theatre = nil;
    
    
    NSArray *theaterList;
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeFavourites:
            theaterList = self.myTheaters;
            break;
        default:
            theaterList = self.theatersNearby;
            break;
    }
    if (collectionView.index < 1) {
        theatre = [theaterList firstObject];
    }
    else {
        theatre = [theaterList objectAtIndex:collectionView.index % [theaterList count]];
    }

    NSArray *movies = [theatre objectForKey:@"movies"];

    [cell initializeMovieSchedule:[movies objectAtIndex:indexPath.row]
                forCollectionView:(SPNShowtimesCollectionView*)collectionView
                     forIndexPath:indexPath];
    
    return cell;
}

-(void)collectionView:(UICollectionView*)collectionView willDisplayItem:(SPNMovieCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.thumbnail.image = [UIImage imageNamed:@"poster-not-available"];
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(SPNMovieCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.thumbnail.imageURL];
    cell.thumbnail.image = [UIImage imageNamed:@"poster-not-available"];
}

-(void)collectionView:(SPNShowtimesCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    shouldGetBothFilmAndTheater = YES;
    
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeFavourites:
            selectedTheatreForFilm = [self.myTheaters objectAtIndex:collectionView.index];
            break;
        default:
            selectedTheatreForFilm = [self.theatersNearby objectAtIndex:collectionView.index];

            break;
    }
    
    NSArray* movies = [selectedTheatreForFilm objectForKey:@"movies"];
    
    NSDictionary *selectedFilm = [movies objectAtIndex:indexPath.row];
    
    NSString *movieId = [selectedFilm objectForKey:@"id"];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] getMovieInformation:movieId
                                                  forZipcode:nil
                                               orCoordinates:nil
                                                  withRadius:0 withCredentials:[SPNUser getCurrentUserId]];
    
  
}


#pragma mark - Cinelife query delegate calls
-(void)getTheaters
{
    [self decodeAddress:self.zipcodePicker.text forFesivals:NO];
}

-(void)getTheatersWithZip:(NSString*)zipcode orCoordinates:(CLLocation*)coordinates
{
    if (![MBProgressHUD HUDForView:self.tabBarController.view]) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
    }
    [self.zipcodePicker resignFirstResponder];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getTheatersForZipcode:zipcode
                                                 orCoordinates:coordinates
                                                    withRadius:INITIAL_RADIUS + radiusAmplifier
                                               withCredentials:[SPNUser getCurrentUserId]];
    isFetchingPinned = YES;
    [[SPNCinelifeQueries sharedInstance] getFestivalsForZipcode:zipcode
                                                  orCoordinates:coordinates
                                                     withRadius:INITIAL_RADIUS + radiusAmplifier
                                                     onlyPinned:YES
                                                        filters:nil];
}

- (void)getFestivals {
    [self decodeAddress:self.zipcodePicker.text forFesivals:YES];
}

- (void)getFestivalsWithZip:(NSString *)zipcode orCoordinates:(CLLocation *)coordinates {
    if (![MBProgressHUD HUDForView:self.tabBarController.view]) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
    }
    [self.zipcodePicker resignFirstResponder];
    NSInteger radius = INITIAL_RADIUS + radiusAmplifier;
    if (!zipcode && !coordinates) {
        coordinates = [appDelegate myLocation];
    }
    if (!_filters[@"near_me"]) {
        radius = 1000000000;
    }
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getFestivalsForZipcode:zipcode
                                                  orCoordinates:coordinates
                                                     withRadius:radius
                                                     onlyPinned:NO
                                                        filters:_filters];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    isFetchingFavorites = NO;
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}

- (void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery festivalList:(NSArray<Festival *> *)festivals {
    isFetchingFavorites = NO;
    if (isFetchingPinned) {
        isFetchingPinned = NO;
        self.pinnedFestivals = festivals.mutableCopy;
        [self.tableView reloadData];
    } else {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        [self.refreshControl endRefreshing];
        
        self.selectedTheaterType = SPNTheatreTypeFestival;
        
        NSMutableArray *sortedFestivals = [festivals mutableCopy];
        
        // not the first query
        if (radiusAmplifier > 0) {
            if ([sortedFestivals count] <= [self.festivals count]) {
                // No new theatres found
                if (radiusAmplifier + INITIAL_RADIUS > MAX_RADIUS) {
                    [self.tableView beginUpdates];
                    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:1]
                                  withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                    [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                                message:@"We did not find other theatres nearby. Please try using a different location."
                                               delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil]
                     show];
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                                message:@"We did not find other theatres nearby. You can try again and we will search on a larger radius."
                                               delegate:self
                                      cancelButtonTitle:@"It's ok"
                                      otherButtonTitles:@"Search again!", nil]
                     show];
                }
            }
            else {
                // We have to append new festivals
                // We get the list of ids for all theaters we currently have
                NSMutableSet *festivalIds = [NSMutableSet new];
                for (Festival *festival in self.festivals) {
                    [festivalIds addObject:festival.festivalId];
                }
                // For each theater in the new query, we remove the ones which already appear on the old list
                NSIndexSet *festivalsToRemove = [sortedFestivals indexesOfObjectsPassingTest:^BOOL(Festival *obj, NSUInteger idx, BOOL *stop) {
                    return [festivalIds containsObject:obj.festivalId];
                }];
                [sortedFestivals removeObjectsAtIndexes:festivalsToRemove];
                
                festivalsCellsToDisplay = [self.festivals count];
                [self.festivals addObjectsFromArray:sortedFestivals];
                
                if (festivalsCellsToDisplay < [self.festivals count]) {
                    NSUInteger totalNumberOfItems = [self.festivals count];
                    NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, festivalsCellsToDisplay + kNumberOfCellsToAdd);
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    
                    for (NSUInteger i = festivalsCellsToDisplay; i<numberOfItemsToDisplay; i++)
                    {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                    
                    festivalsCellsToDisplay = numberOfItemsToDisplay;
                    
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:indexPaths
                                          withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                }
            }
        }
        else {
            canLoadMoreFestivals = sortedFestivals.count == 100;
            if (_filters[@"offset"]) {
                NSMutableSet *festivalIds = [NSMutableSet new];
                for (Festival *festival in self.festivals) {
                    [festivalIds addObject:festival.festivalId];
                }
                NSIndexSet *festivalsToRemove = [sortedFestivals indexesOfObjectsPassingTest:^BOOL(Festival *obj, NSUInteger idx, BOOL *stop) {
                    return [festivalIds containsObject:obj.festivalId];
                }];
                [sortedFestivals removeObjectsAtIndexes:festivalsToRemove];
                
                [self.festivals addObjectsFromArray:sortedFestivals];
                
                if (festivalsCellsToDisplay < [self.festivals count]) {
                    NSUInteger totalNumberOfItems = [self.festivals count];
                    NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, festivalsCellsToDisplay + kNumberOfCellsToAdd);
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    
                    for (NSUInteger i = festivalsCellsToDisplay; i < numberOfItemsToDisplay; i++) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                    }
                    
                    festivalsCellsToDisplay = numberOfItemsToDisplay;
                    
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:indexPaths
                                          withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                }
            } else {
                self.festivals = sortedFestivals;
                [self.tableView reloadData];
            }
        }
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery theaterList:(NSArray *)theaters
{
    isFetchingFavorites = NO;
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [self.refreshControl endRefreshing];
    self.selectedTheaterType = SPNTheatreTypeInRegion;
    
    NSMutableArray *sortedTheaters = [NSMutableArray arrayWithArray:theaters];
   // NSMutableArray *sortedTheaters = [SPNCinelifeQueries sortTheatersByDistance:theaters withLocation:[appDelegate myLocation] withPartnersFirst:YES ];
    // not the first query
    if (radiusAmplifier > 0) {
        if ([sortedTheaters count] <= [self.theatersNearby count]) {
            // No new theatres found
            if (radiusAmplifier+INITIAL_RADIUS > MAX_RADIUS) {
                [self.tableView beginUpdates];
                [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:1]
                              withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
                [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                            message:@"We did not find other theatres nearby. Please try using a different location."
                                           delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil]
                 show];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                            message:@"We did not find other theatres nearby. You can try again and we will search on a larger radius."
                                           delegate:self
                                  cancelButtonTitle:@"It's ok"
                                  otherButtonTitles:@"Search again!", nil]
                 show];
            }
        }
        else {
            // We have to append new theaters
            // We get the list of ids for all theaters we currently have
            NSMutableArray *theareIdList = [NSMutableArray new];
            for (NSDictionary *theatre in self.theatersNearby)
            {
                [theareIdList addObject:[theatre objectForKey:@"id"]];
            }
            // For each theater in the new query, we remove the ones which already appear on the old list
            NSIndexSet *theatresToRemove = [sortedTheaters indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                NSString *theatreId = [obj objectForKey:@"id"];
                return [theareIdList indexOfObject:theatreId] != NSNotFound;
            }];
            [sortedTheaters removeObjectsAtIndexes:theatresToRemove];
            
            
            NSUInteger i = 0;
            theatersCellsToDisplay = [self.theatersNearby count];
            [self.theatersNearby addObjectsFromArray:sortedTheaters];
            
            if (theatersCellsToDisplay < [self.theatersNearby count]) {
                NSUInteger totalNumberOfItems = [self.theatersNearby count];
                NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, theatersCellsToDisplay + kNumberOfCellsToAdd);
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                
                for (i=theatersCellsToDisplay; i<numberOfItemsToDisplay; i++)
                {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
                
                theatersCellsToDisplay = numberOfItemsToDisplay;
                
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:indexPaths
                                      withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
                
            }
        }
    }
    else {
        self.theatersNearby = sortedTheaters;
        [self.tableView reloadData];

    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieInformation:(NSDictionary *)movie
{
    selectedMovie = movie;
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] getTheaterInformation:[selectedTheatreForFilm objectForKey:@"id"] withCredentials:[SPNUser getCurrentUserId]];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery theaterInformation:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];

    NSMutableDictionary *theaterWithDistance = [NSMutableDictionary dictionaryWithDictionary:theater];
    if ([selectedTheatreForFilm objectForKey:@"distance"]) {
        [theaterWithDistance setObject:[selectedTheatreForFilm objectForKey:@"distance"] forKey:@"distance"];
    }
    selectedTheatreForFilm = theaterWithDistance;
    
    if (shouldGetBothFilmAndTheater) {
        shouldGetBothFilmAndTheater = NO;
        UIStoryboard *filmsStoryboard = [UIStoryboard storyboardWithName:@"FilmsStoryboard"
                                                                  bundle:nil];
        SPNFilmDetailsViewController *spnFilmDetails = [filmsStoryboard instantiateViewControllerWithIdentifier:@"FilmDetails"];
        [spnFilmDetails setFilmInformation:selectedMovie];
        
        [self appendShowtimesForFilm:selectedMovie];
        spnFilmDetails.theatreShowtimesInRegion = @[selectedFilmAndShowtimes];
        spnFilmDetails.showtimeInformation = [SPNCinelifeQueries sortShowdatesForTheaters:@[selectedFilmAndShowtimes]];
        
        [self.navigationController pushViewController:spnFilmDetails
                                             animated:YES];
        
    }
    else {
        [self performSegueWithIdentifier:@"TheatreDetails"
                                  sender:self];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery removedTheater:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery addedTheater:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}


- (void)favoritesLoadedWithTheaters:(NSArray *)favoriteTheaters festivals:(NSArray *)favoriteFestivals {
    isFetchingFavorites = NO;

    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [self.zipcodePicker setUserInteractionEnabled:YES];
    
    if (favoriteTheaters.count == 0 && favoriteFestivals.count == 0) {
        self.selectedTheaterType = SPNTheatreTypeInRegion;
        
        if (!justOpenedApp) {
            [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNoFavoriteTheatres];
        }
        justOpenedApp = NO;
        self.theaterSegmentControl.selectedSegmentIndex = 0;
        
        if ([self.theatersNearby count] < 1) {
            NSString* storedZipcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"zip"];
            if (storedZipcode) {
                [self decodeAddress:storedZipcode forFesivals:NO];
                
            }
            else {
                isFetchingLocation = YES;
                [self.locationManager startUpdatingLocation];
            }
        }
    } else {
        [self.zipcodePicker setUserInteractionEnabled:NO];
        
        NSMutableArray *sortedTheaters = [SPNCinelifeQueries sortTheatersByDistance:favoriteTheaters withLocation:[appDelegate myLocation] withPartnersFirst:YES ];
        
        self.theaterSegmentControl.selectedSegmentIndex = 1;
        self.selectedTheaterType = SPNTheatreTypeFavourites;
        self.myTheaters = sortedTheaters;
        self.myFestivals = [favoriteFestivals mutableCopy];
        [self.tableView reloadData];
    }
}

- (void)didPressOnFavoriteInTheaterCell:(SPNTheatreCell *)cell {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString* showTootip = [preferences objectForKey:@"showTheaterInfo"];
    
    NSDictionary *user = [SPNUser getCurrentUserId];
    
    if (!showTootip) {
        [SPNToolTipViewController showWithToolTipText:@"Tap on the heart icon to add a theatre to your favorite theatre list. You can remove theatres from your favorites by tapping on the heart icon again."
                                   overViewController:self.tabBarController];
        
        [preferences setObject:@"no"
                        forKey:@"showTheaterInfo"];
        [preferences synchronize];
    }
    else if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not Now"
                           withOtherTitles:@"Sign In"];
    }
    else {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        NSArray *theaterList;
        switch (self.selectedTheaterType) {
            case SPNTheatreTypeFavourites:
                theaterList = self.myTheaters;
                break;
            default:
                theaterList = self.theatersNearby;
                break;
        }
        
        NSDictionary *theater = theaterList[indexPath.row];
        NSString *theaterId = [theater objectForKey:@"id"];
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        if (self.selectedTheaterType == SPNTheatreTypeFavourites) {
            [[SPNCinelifeQueries sharedInstance] setDelegate:self];
            
            [[SPNCinelifeQueries sharedInstance] postRemoveFavoriteTheater:theaterId
                                                           withCredentials:user];
        }
        else {
            if ([theater objectForKey:@"is_favorite"] && [[theater objectForKey:@"is_favorite"] integerValue] > 0) {
                [[SPNCinelifeQueries sharedInstance] setDelegate:self];
                
                [[SPNCinelifeQueries sharedInstance] postRemoveFavoriteTheater:theaterId
                                                               withCredentials:user];
            }
            else {
                [[SPNCinelifeQueries sharedInstance] setDelegate:self];

                [[SPNCinelifeQueries sharedInstance] postAddFavoriteTheater:theaterId
                                                            withCredentials:user];
            }
        }
    }
}

- (void)didPressOnFavoriteInFestivalCell:(SPNFestivalCell *)cell {
    if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not Now"
                           withOtherTitles:@"Sign In"];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Festival *festival;
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeInRegion:
            festival = _theatersNearby[indexPath.row];
            break;
            
        case SPNTheatreTypeFavourites:
            festival = _myFestivals[indexPath.row];
            break;
            
        case SPNTheatreTypeFestival:
            festival = _festivals[indexPath.row];
            break;
    }
    if (festival.isFavorited.boolValue) {
        [[SPNCinelifeQueries sharedInstance] unfavoriteFestival:festival.festivalId
                                                     completion:^(NSError *error) {
                                                         [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                                                     }];
    } else {
        [[SPNCinelifeQueries sharedInstance] favoriteFestival:festival.festivalId
                                                   completion:^(NSError *error) {
                                                       [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                                                   }];
    }
}

#pragma mark Segue setup
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *identifier = segue.identifier;
    if ([identifier isEqualToString:@"TheatreDetails"]) {
        SPNTheatreDetailsViewController *spnTheatreDetails = [segue destinationViewController];
        [spnTheatreDetails setTheatreInformation:selectedTheatreForFilm];
    }
    else if ([identifier isEqualToString:@"Map"]) {
        SPNTheatreMapViewController *spnTheatreMap = [segue destinationViewController];
        spnTheatreMap.theatersNearby = self.theatersNearby;
        spnTheatreMap.myTheaters = self.myTheaters;
        spnTheatreMap.myFestivals = self.myFestivals;
        spnTheatreMap.festivals = self.festivals;
        spnTheatreMap.selectedTheaterType = self.selectedTheaterType;
        spnTheatreMap.segmentedControl.selectedSegmentIndex = self.selectedTheaterType;
    } else if ([identifier isEqual:@"FestivalDetails"]) {
        SPNFestivalViewController *details = [segue destinationViewController];
        details.festival = sender;
    }
    
}

-(void)appendShowtimesForFilm:(NSDictionary*)movie {
    NSDateFormatter *dateFormatter2 = [NSDateFormatter new];
    [dateFormatter2 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    selectedFilmAndShowtimes = [NSMutableDictionary dictionaryWithDictionary:selectedTheatreForFilm];
    [selectedFilmAndShowtimes removeObjectForKey:@"movies"];
    NSDictionary *showtimeInformation = [SPNCinelifeQueries sortShowdatesForOneTheatre:[selectedTheatreForFilm objectForKey:@"movies"]];
    NSArray *showArray = [[showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSDate *date in showArray)
    {
        NSArray *showList = [showtimeInformation objectForKey:date];
        for (NSDictionary *showStuff in showList)
        {
            NSString *movieId = [showStuff objectForKey:@"id"];
            if ([movieId isEqualToString:[movie objectForKey:@"id"]]) {
                NSMutableDictionary *showToAdd = [NSMutableDictionary new];
                NSString *stringDate = [dateFormatter2 stringFromDate:date];
                [showToAdd setObject:stringDate forKey:@"showdate"];
                [showToAdd setObject:[showStuff objectForKey:@"showtime"] forKey:@"showtime"];
                
                if (![selectedFilmAndShowtimes objectForKey:@"showtimes"]) {
                    NSMutableArray *showtimes = [NSMutableArray new];
                    [showtimes addObject:showToAdd];
                    [selectedFilmAndShowtimes setObject:showtimes forKey:@"showtimes"];
                }
                else {
                    NSMutableArray *showtimes = [selectedFilmAndShowtimes objectForKey:@"showtimes"];
                    [showtimes addObject:showToAdd];
                    [selectedFilmAndShowtimes setObject:showtimes forKey:@"showtimes"];
                    
                }
            }
        }
    }
}

#pragma mark - Alert view functinos
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Sign In"]) {
        [alertView dismissWithClickedButtonIndex:buttonIndex
                                        animated:NO];
        self.tabBarController.selectedIndex = 3;
    }
    else if ([title isEqualToString:@"Search again!"]) {
        radiusAmplifier+=RADIUS_ENHANCE;
        [self decodeAddress:self.zipcodePicker.text
                forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
    }
    else if ([title isEqualToString:@"Sign In"]) {
        [alertView dismissWithClickedButtonIndex:buttonIndex
                                        animated:NO];
        self.tabBarController.selectedIndex = 3;
    }
}

-(void)swapToOtherTheaterList:(UISegmentedControl *)sender
{
    NSInteger selectedIndex = sender.selectedSegmentIndex;
    switch (selectedIndex) {
        case 1: {
            if ([SPNUser userLoggedIn]) {
                // if ([self.myTheaters count] < 1) {
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                isFetchingFavorites = YES;
                [[SPNCinelifeQueries sharedInstance] getFavoritesTheatersAndFestivalsWithCompletion:^(NSArray *theaters, NSArray *festivals, NSError *error) {
                    [self favoritesLoadedWithTheaters:theaters festivals:festivals];
                }];
                /*  }
                 else {
                 self.selectedTheaterType = SPNTheatreTypeFavourites;
                 [self.tableView reloadData];
                 }
                 */
            }
            else {
                [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                                      withDelegate:self
                                      forAlertType:UIAlertViewStyleDefault
                                   withCancelTitle:@"Not Now"
                                   withOtherTitles:@"Sign In"];
                sender.selectedSegmentIndex = 0;
                self.selectedTheaterType = SPNTheatreTypeInRegion;
                [self.tableView reloadData];
            }
        }
            break;
            
        default: {
            if (selectedIndex == 0) {
                self.selectedTheaterType = SPNTheatreTypeInRegion;
            } else {
                self.selectedTheaterType = SPNTheatreTypeFestival;
            }
            [self.zipcodePicker setUserInteractionEnabled:YES];
            BOOL isFestival = self.selectedTheaterType == SPNTheatreTypeFestival;
            NSInteger count = (isFestival ? self.festivals : self.theatersNearby).count;
            if (count == 0) {
                NSString *storedZipcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"zip"];
                if (storedZipcode) {
                    [self decodeAddress:storedZipcode
                            forFesivals:isFestival];
                }
                else {
                    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                    isFetchingLocation = YES;
                    [self.locationManager startUpdatingLocation];
                    
                }
            }
            else {
                [self.tableView reloadData];
            }
            break;
        }
    }
}

@end
