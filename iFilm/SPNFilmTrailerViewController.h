//
//  SPNFilmTrailerViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@protocol SPNFilmTrailerViewControllerDelegate
-(void)spnFilmTrailerDidFinish;
@end


@interface SPNFilmTrailerViewController : GAITrackedViewController

@property (nonatomic, assign) id <SPNFilmTrailerViewControllerDelegate> delegate;

@property (nonatomic, strong) NSString *trailerURL;

@end
