//
//  SPNIndieRSSCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNIndieRSSCell.h"

@implementation SPNIndieRSSCell


-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.horizontalCollectionView.dataSource = dataSourceDelegate;
    self.horizontalCollectionView.delegate = dataSourceDelegate;
    self.horizontalCollectionView.index = index;
    [self.horizontalCollectionView reloadData];
}

@end
