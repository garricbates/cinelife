//
//  SPNBaseTheaterViewController.m
//  iFilm
//
//  Created by Vlad Getman on 03.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNBaseTheaterViewController.h"
#import "SPNCinelifeQueries.h"
#import "SPNAppDelegate.h"

#define INITIAL_RADIUS 10
#define RADIUS_ENHANCE 10
#define MAX_RADIUS 30

@interface SPNBaseTheaterViewController () <CLLocationManagerDelegate, SPNCinelifeQueriesDelegate> {
    BOOL isFetchingFavorites;
    BOOL isFetchingLocation;
    
    NSString *currentZip;
}

@property (nonatomic, retain) NSString *lastSearchLocation;

@end

@implementation SPNBaseTheaterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeKeyboard];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (isFetchingFavorites) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    }
}


-(CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

-(CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        
#ifdef __IPHONE_8_0
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [_locationManager requestWhenInUseAuthorization];
        }
#endif
    }
    return _locationManager;
}

- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender {
    NSInteger selectedIndex = sender.selectedSegmentIndex;
    switch (selectedIndex) {
        case 1: {
            if ([SPNUser userLoggedIn]) {
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                isFetchingFavorites = YES;
                [[SPNCinelifeQueries sharedInstance] getFavoritesTheatersAndFestivalsWithCompletion:^(NSArray *theaters, NSArray *festivals, NSError *error) {
                    [self favoritesLoadedWithTheaters:theaters festivals:festivals];
                }];
            } else {
                [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                                      withDelegate:self
                                      forAlertType:UIAlertViewStyleDefault
                                   withCancelTitle:@"Not Now"
                                   withOtherTitles:@"Sign In"];
                self.selectedTheaterType = SPNTheatreTypeInRegion;
                
                self.segmentedControl.selectedSegmentIndex = 0;
                
                if (self.theatersNearby.count == 0) {
                    NSString* storedZipcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"zip"];
                    if (storedZipcode) {
                        [self decodeAddress:storedZipcode forFesivals:NO];
                    } else {
                        isFetchingLocation = YES;
                        [self.locationManager startUpdatingLocation];
                    }
                } else {
                    [self reloadData];
                }
            }
            break;
        }
            
        default: {
            if (selectedIndex == 0) {
                self.selectedTheaterType = SPNTheatreTypeInRegion;
            } else {
                self.selectedTheaterType = SPNTheatreTypeFestival;
            }
            [self.zipcodePicker setUserInteractionEnabled:YES];
            BOOL isFestival = self.selectedTheaterType == SPNTheatreTypeFestival;
            NSInteger count = (isFestival ? self.festivals : self.theatersNearby).count;
            if (count == 0) {
                NSString *storedZipcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"zip"];
                if (storedZipcode && [self isTable]) {
                    [self decodeAddress:storedZipcode
                            forFesivals:isFestival];
                }
                else {
                    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                    isFetchingLocation = YES;
                    [self.locationManager startUpdatingLocation];
                    
                }
            } else {
                [self reloadData];
            }
            break;
        }
    }
}

- (void)reloadData {
    
}

- (BOOL)isTable {
    return NO;
}

-(void)decodeAddress:(NSString *)address forFesivals:(BOOL)forFestivals
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    
    if (![address.lowercaseString containsString:@"us"]) {
        address = [address stringByAppendingString:@" us"];
    }
    
    [self.geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark *usPlacemark = [placemarks firstObject];
            for (CLPlacemark* placemark in placemarks) {
                if ([placemark.ISOcountryCode isEqualToString:@"US"]) {
                    usPlacemark = placemark;
                }
            }
            [self.geoCoder reverseGeocodeLocation:usPlacemark.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                currentZip = zip;
                if (zip) {
                    [self.zipcodePicker setText:zip];
                    if (forFestivals) {
                        [self getFestivalsWithZip:zip orCoordinates:nil];
                    } else {
                        [self getTheatersWithZip:zip orCoordinates:nil];
                    }
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                }
                else {
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                    
                }
            }];
        }
        else if (error.domain == kCLErrorDomain) {
            switch (error.code)
            {
                case kCLErrorDenied:
                    self.zipcodePicker.text = @"Location Services Denied by User";
                    break;
                case kCLErrorNetwork:
                    self.zipcodePicker.text = @"No Network";
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    self.zipcodePicker.text = @"No Result Found";
                    break;
                default:
                    self.zipcodePicker.text = error.localizedDescription;
                    break;
            }
            currentZip = nil;
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        } else {
            currentZip = nil;
            self.zipcodePicker.text = error.localizedDescription;
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
        
    }];
}

- (void)setCityAndStateToSearchBar:(NSDictionary *)placeDictionary {
    NSString *country = [placeDictionary objectForKey:@"CountryCode"];
    NSString *zipcode = [placeDictionary objectForKey:@"ZIP"];
    
    if ([country isEqualToString:@"US"]) {
        NSString *city = [placeDictionary objectForKey:@"City"];
        NSString *state = [placeDictionary objectForKey:@"State"];
        NSString *location = [NSString stringWithFormat:@"%@, %@ (%@)", city, state, zipcode];
        [self.zipcodePicker setText:location];
        self.lastSearchLocation = zipcode;
    } else {
        [self.zipcodePicker setText:zipcode];
        self.lastSearchLocation = zipcode;
    }
    currentZip = zipcode;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Sign In!"]) {
        self.tabBarController.selectedIndex = 3;
    }
}

#pragma mark - Custom leyboard features

-(void)initializeKeyboard {
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    [keyboardDoneButtonView setBarTintColor:[UIColor spn_darkGrayColor]];
    keyboardDoneButtonView.translucent = YES;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    
    UIBarButtonItem* getZipButton = [[UIBarButtonItem alloc] initWithTitle:@"Nearby"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(activateGPSLocation:)];
    
    UIBarButtonItem* useSavedLocButton = [[UIBarButtonItem alloc] initWithTitle:@"Saved"
                                                                          style:UIBarButtonItemStylePlain target:self
                                                                         action:@selector(savedLocation:)];
    
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(cancelClicked:)];
    
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, getZipButton, useSavedLocButton, cancelButton, nil]];
    self.zipcodePicker.inputAccessoryView = keyboardDoneButtonView;
    [self.zipcodePicker setKeyboardType:UIKeyboardTypeDefault];
    [self.zipcodePicker setSpellCheckingType:UITextSpellCheckingTypeNo];
}

- (void)doneClicked:(id)sender {
    [self.zipcodePicker resignFirstResponder];
    [self doneClicked];
}

-(void)activateGPSLocation:(id)sender {
    [self.zipcodePicker resignFirstResponder];
    [self activateGPSLocation];
}

-(void)savedLocation:(id)sender {
    [self.zipcodePicker resignFirstResponder];
    [self savedLocation];
}

-(void)cancelClicked:(id)sender {
    [self.zipcodePicker resignFirstResponder];
}

- (void)doneClicked {
    
}

- (void)activateGPSLocation {
    
}

- (void)savedLocation {
    
}

-(void)getTheatersWithZip:(NSString*)zipcode orCoordinates:(CLLocation*)coordinates
{
    if (![MBProgressHUD HUDForView:self.tabBarController.view]) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
    }
    [self.zipcodePicker resignFirstResponder];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getTheatersForZipcode:zipcode
                                                 orCoordinates:coordinates
                                                    withRadius:INITIAL_RADIUS
                                               withCredentials:[SPNUser getCurrentUserId]];
}

- (void)getFestivalsWithZip:(NSString *)zipcode orCoordinates:(CLLocation *)coordinates {
    if (![MBProgressHUD HUDForView:self.tabBarController.view]) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
    }
    [self.zipcodePicker resignFirstResponder];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getFestivalsForZipcode:zipcode
                                                  orCoordinates:coordinates
                                                     withRadius:INITIAL_RADIUS
                                                     onlyPinned:NO
                                                        filters:_filters];
}

#pragma mark - Cinelife queries

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error {
    
}


- (void)favoritesLoadedWithTheaters:(NSArray *)favoriteTheaters festivals:(NSArray *)favoriteFestivals {
    isFetchingFavorites = NO;
    
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [self.zipcodePicker setUserInteractionEnabled:YES];
    
    if (favoriteTheaters.count == 0 && favoriteFestivals.count == 0) {
        self.selectedTheaterType = SPNTheatreTypeInRegion;
        
        [self favoritesNotFound];
        
        self.segmentedControl.selectedSegmentIndex = 0;
        
        if (self.theatersNearby.count == 0) {
            NSString* storedZipcode = [[NSUserDefaults standardUserDefaults] objectForKey:@"zip"];
            if (self.isTable && storedZipcode) {
                [self decodeAddress:storedZipcode forFesivals:NO];
            } else {
                isFetchingLocation = YES;
                [self.locationManager startUpdatingLocation];
            }
        }
    } else {
        [self.zipcodePicker setUserInteractionEnabled:NO];
        
        NSMutableArray *sortedTheaters = [SPNCinelifeQueries sortTheatersByDistance:favoriteTheaters withLocation:[appDelegate myLocation] withPartnersFirst:YES ];
        
        self.segmentedControl.selectedSegmentIndex = 1;
        self.selectedTheaterType = SPNTheatreTypeFavourites;
        self.myTheaters = sortedTheaters;
        self.myFestivals = [favoriteFestivals mutableCopy];
        [self reloadData];
    }
    
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

- (void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery festivalList:(NSArray<Festival *> *)festivals {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    self.selectedTheaterType = SPNTheatreTypeFestival;
    self.festivals = festivals.mutableCopy;
    [self reloadData];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery theaterList:(NSArray *)theaters {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    self.theatersNearby = theaters.mutableCopy;
    self.selectedTheaterType = SPNTheatreTypeInRegion;
    [self reloadData];
}


- (void)favoritesNotFound {
    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNoFavoriteTheatres];
}

@end
