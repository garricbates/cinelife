//
//  SPNPurchaseCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Purchases.h"

@interface SPNPurchaseCell : UITableViewCell

@property IBOutlet UIImageView *posterImageView;
@property IBOutlet UILabel *movieTitleLabel;
@property IBOutlet UILabel *showDateLabel;
@property IBOutlet UILabel *showTimeLabel;
@property IBOutlet UILabel *ticketsLabel;

-(void)initializePurchaseCell:(Purchases*)purchaseInformation forRowAtIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView withDateFormatter:(NSDateFormatter*)dateFormatter;

@end
