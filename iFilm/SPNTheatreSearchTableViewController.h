//
//  SPNTheatreSearchTableViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 8/4/15.
//  Copyright (c) 2015 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPNTheatreCell.h"

#import "SPNCinelifeQueries.h"  
#import "SPNBaseTheaterViewController.h"

@interface SPNTheatreSearchTableViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UISegmentedControl *theaterSegmentControl;

@property (nonatomic, retain) NSMutableArray *myTheaters;
@property (nonatomic, retain) NSMutableArray *myFestivals;

@property (nonatomic) SPNTheatreType selectedTheaterType;

@end
