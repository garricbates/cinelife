//
//  SPNReviewOptionCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNReviewOptionCell : UITableViewCell

@property IBOutlet UIButton *userReviewsButton;
@property IBOutlet UIButton *criticReviewsButton;

@property IBOutlet UIView *userReviewsBackgroundView;
@property IBOutlet UIView *criticReviewsBackgroundView;

@property IBOutlet UILabel *userScoreLabel;
@property IBOutlet UILabel *userReviewAmountLabel;

@property IBOutlet UILabel *criticScoreLabel;
@property IBOutlet UILabel *criticReviewAmountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;

@end
