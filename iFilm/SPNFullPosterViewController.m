//
//  SPNFullPosterViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFullPosterViewController.h"

@interface SPNFullPosterViewController ()

@end

@implementation SPNFullPosterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Film Full Poster";
    
    // Do any additional setup after loading the view.
    self.posterImageView.imageURL = [NSURL URLWithString:self.posterURL];
    if (self.isFestivalFilm) {
        [self.posterImageView setContentMode:UIViewContentModeScaleAspectFit];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
