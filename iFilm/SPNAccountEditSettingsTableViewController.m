//
//  SPNAccountEditSettingsTableViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAccountEditSettingsTableViewController.h"
#import "SPNAccountEditCell.h"

@interface SPNAccountEditSettingsTableViewController ()
{
    CLLocationManager *locationManager;
    UITextField *editInformationTextField;
}
@end

@implementation SPNAccountEditSettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Get Zipcode!"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(getZipcode)];
    
    [rightButton setTintColor:[UIColor spn_aquaColor]];
    
    self.navigationItem.rightBarButtonItem = rightButton;
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    editInformationTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* EditSettingsCellIdentifier = @"EditSettingsCell";
    static NSString* EditInformationCellIdentifier = @"InformationCell";
    switch (indexPath.row) {
        case 0:
        {
            SPNAccountEditCell *editCell = [tableView dequeueReusableCellWithIdentifier:EditSettingsCellIdentifier];
            editInformationTextField = editCell.editInformationTextField;
            [editInformationTextField becomeFirstResponder];
            return editCell;
        }
            break;
        default:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:EditInformationCellIdentifier forIndexPath:indexPath];
            [cell.textLabel setFont:[UIFont spn_NeutraMedium]];
            return cell;
        }
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > 0) {
        return kAccountEditDescriptionCellHeight;
    }
    else {
        return kAccountOptionsCellHeight;
    }
}

#pragma mark Text field functions
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [editInformationTextField resignFirstResponder];
    [self getLocationWithUserInput:editInformationTextField.text];
    return YES;
}
#pragma mark Storing information functions
-(void)saveSettings:(NSString*)updatedValue
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:updatedValue
              forKey:@"zip"];
    
    [prefs synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Location functions

-(void)getZipcode
{
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *userLocation = [locations lastObject];
    CLGeocoder *geocoder = [CLGeocoder new];
    
    [locationManager stopUpdatingLocation];
    
    [geocoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *place = [placemarks firstObject];
        
        NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
        if (zip) {
            [self setCityAndStateToSearchBar:place.addressDictionary];
        }
        
    }];
}

-(void)getLocationWithUserInput:(NSString*)address
{
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if([placemarks count] > 0) {
            CLPlacemark *usPlacemark = [placemarks firstObject];
            for (CLPlacemark* placemark in placemarks) {
                if ([placemark.ISOcountryCode isEqualToString:@"US"]) {
                    usPlacemark = placemark;
                }
            }
            [geoCoder reverseGeocodeLocation:usPlacemark.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                if (zip) {
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                }
            }];
        }
        else if (error.domain == kCLErrorDomain) {
            switch (error.code)
            {
                case kCLErrorDenied:
                    editInformationTextField.text = @"Location Services Denied by User";
                    break;
                case kCLErrorNetwork:
                    editInformationTextField.text = @"No Network";
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    editInformationTextField.text = @"No Result Found";
                    break;
                default:
                    editInformationTextField.text = error.localizedDescription;
                    break;
            }
        }
        else {
            editInformationTextField.text = error.localizedDescription;
        }
        
    }];
}

-(void)setCityAndStateToSearchBar:(NSDictionary*)placeDictionary
{
    NSString *country = [placeDictionary objectForKey:@"CountryCode"];
    NSString *zipcode = [placeDictionary objectForKey:@"ZIP"];
    
    if ([country isEqualToString:@"US"]) {
        NSString *city = [placeDictionary objectForKey:@"City"];
        NSString *state = [placeDictionary objectForKey:@"State"];
        NSString *location = [NSString stringWithFormat:@"%@, %@ (%@)", city, state, zipcode];
        [editInformationTextField setText:location];
    }
    else {
        [editInformationTextField setText:zipcode];
    }
    
    [editInformationTextField resignFirstResponder];
    
    [self performSelector:@selector(saveSettings:)
               withObject:editInformationTextField.text
               afterDelay:1];
}
@end
