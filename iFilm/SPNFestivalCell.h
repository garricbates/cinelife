//
//  SPNFestivalCell.h
//  iFilm
//
//  Created by Vlad Getman on 21.03.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPNFestivalCell;
@class Festival;

@protocol SPNFestivalCellDelegate <NSObject>

- (void)didPressOnFavoriteInFestivalCell:(SPNFestivalCell *)cell;

@end

@interface SPNFestivalCell : UITableViewCell

@property (nonatomic, weak) id <SPNFestivalCellDelegate> delegate;

- (void)configureForFestival:(Festival *)festival;
+ (CGFloat)heightForFestival:(Festival *)festival withWidth:(CGFloat)width;

@end
