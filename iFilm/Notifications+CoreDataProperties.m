//
//  Notifications+CoreDataProperties.m
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Notifications+CoreDataProperties.h"

@implementation Notifications (CoreDataProperties)

@dynamic notification_title;
@dynamic notification_body;
@dynamic notification_date;
@dynamic info;

@end
