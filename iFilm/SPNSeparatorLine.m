//
//  SPNSeparatorLine.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNSeparatorLine.h"

@implementation SPNSeparatorLine


-(void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = [self.backgroundColor CGColor];
    self.layer.borderWidth = (1.0 / [UIScreen mainScreen].scale) / 2;
    
    self.backgroundColor = [UIColor clearColor];
}

@end
