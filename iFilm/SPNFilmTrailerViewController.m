//
//  SPNFilmTrailerViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmTrailerViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface SPNFilmTrailerViewController ()
{
    MPMoviePlayerController *mediaPlayer;
}
@end

@implementation SPNFilmTrailerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Film Trailer";
    [self initializeMediaPlayerWithURL:[NSURL URLWithString:self.trailerURL]];
    
      // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}

#pragma mark Media player functions
-(void)initializeMediaPlayerWithURL:(NSURL*)trailerURL
{
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    mediaPlayer = [[MPMoviePlayerController alloc] initWithContentURL:trailerURL];
    [mediaPlayer setControlStyle:MPMovieControlStyleFullscreen];
    [mediaPlayer setFullscreen:YES];
    [mediaPlayer.view setFrame: CGRectMake(0, 0, self.view.bounds.size.height, self.view.bounds.size.width)];
   
    // We rotate the mediaPlayer 90º, then we readjust the center to fit the screen
   /*
    [[mediaPlayer view] setTransform:CGAffineTransformMakeRotation(M_PI / 2)];
    [mediaPlayer.view setCenter:self.view.center];*/
    [self.view addSubview: mediaPlayer.view];
    
    // May help to reduce latency
    [mediaPlayer prepareToPlay];
    [mediaPlayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish)
                                                 name:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey
                                               object:nil];

}
-(void)moviePlayBackDidFinish
{
    [self dismissMoviePlayerViewControllerAnimated];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate spnFilmTrailerDidFinish];
    }];
    
 
}

- (BOOL) prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


@end
