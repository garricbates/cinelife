//
//  SPNRSSParser.m
//  iFilm
//
//  Created by Dmytro Gladush on 1/15/17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNRSSParser.h"

@implementation SPNRSSParser

- (NSDictionary *)parseRSS:(NSString *)rssUrl numberOfEntities:(NSInteger)numberOfEntities {

    self.dateFormatter = [NSDateFormatter new];
    [self.dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
    
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];

    self.res = [NSMutableDictionary new];
    self.entries = [NSMutableArray new];
    self.numberOfEntities = numberOfEntities;
    
    NSMutableDictionary *responseData = [NSMutableDictionary new];
    [self.res setObject:responseData forKey:@"responseData"];
    NSMutableDictionary *feed = [NSMutableDictionary new];
    [responseData setObject:feed forKey:@"feed"];
    [feed setObject:self.entries forKey:@"entries"];

    NSURL *feedURL = [NSURL URLWithString:rssUrl];
    MWFeedParser *feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
    feedParser.delegate = self;
    feedParser.feedParseType = ParseTypeFull;
    feedParser.connectionType = ConnectionTypeSynchronously;
    [feedParser parse];

    return self.res;
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
    
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    if (self.entries.count >= self.numberOfEntities) {
        return;
    }
    NSMutableDictionary *itemDictionary = [NSMutableDictionary new];
    if (item.title != nil) {
        [itemDictionary setValue:item.title forKey:@"title"];
    }
    if (item.summary != nil) {
        [itemDictionary setValue:item.summary forKey:@"summary"];
    }
    if (item.link != nil) {
        [itemDictionary setValue:item.link forKey:@"link"];
    }
    if (item.content != nil) {
        [itemDictionary setValue:item.content forKey:@"content"];
    }
    if (item.content != nil) {
        [itemDictionary setValue:[item.content substringToIndex:150] forKey:@"contentSnippet"];
    }
    if (item.date != nil) {
        [itemDictionary setValue:[self.dateFormatter stringFromDate: item.date] forKey:@"publishedDate"];
    }
    [self.entries addObject:itemDictionary];
}

@end
