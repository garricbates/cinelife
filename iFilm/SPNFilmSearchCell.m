//
//  SPNFilmSearchCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmSearchCell.h"
#import "SPNMovieCollectionCell.h"

@implementation SPNFilmSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.horizontalCollectionView registerCellClass:[SPNMovieCollectionCell class]
                                      withIdentifier:@"MovieCell"];
}

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.horizontalCollectionView.dataSource = dataSourceDelegate;
    self.horizontalCollectionView.delegate = dataSourceDelegate;
    self.horizontalCollectionView.index = index;
    [self.horizontalCollectionView reloadData];
}
@end
