//
//  TheatreCell.m
//  Spotlight
//
//  Created by La Casa de los Pixeles on 11/19/13.
//  Copyright (c) 2013 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreCell.h"
#import "SPNShowtimesCollectionView.h"
#import "SPNPartnerButton.h"

@interface SPNTheatreCell ()

@property (weak, nonatomic) IBOutlet SPNPartnerButton *partnerButton;

@end

@implementation SPNTheatreCell

-(NSString *)reuseIdentifier
{
    return @"TheatreCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 140;
    self.addressLabel.preferredMaxLayoutWidth = self.titleLabel.preferredMaxLayoutWidth = width;
    
    self.titleLabel.font = [UIFont spn_NeutraBoldLarge];
    self.addressLabel.font = self.noShowsLabel.font = [UIFont spn_NeutraMedium];
    self.milesAwayLabel.font = [UIFont spn_NeutraBoldTiny];
    
    self.partnerButton.visible = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"MovieViewCell" bundle:nil];
    [_horizontalCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"MovieCell"];
}

- (void)configureForTheater:(NSDictionary *)theater {
    
    NSString *faveImage;
    if ([SPNUser userLoggedIn] && [theater objectForKey:@"is_favorite"] && [[theater objectForKey:@"is_favorite"] integerValue] > 0) {
        faveImage = @"heart-filled";
    }
    else {
        faveImage = @"heart-icon";
    }
    [self.favoriteBtn setImage:[UIImage imageNamed:faveImage]
                      forState:UIControlStateNormal];
    
    CGFloat distance = [[theater objectForKey:@"distance"] floatValue];
    
    NSString *distanceString = @"";
    if (distance > 50.0) {
        distanceString = [NSString stringWithFormat:@"50+\r\nmiles"];
    }
    else {
        distanceString = [NSString stringWithFormat:@"%.1f\r\nmiles",distance];
    }
    
    NSMutableAttributedString *distanceAttributeString = [[NSMutableAttributedString alloc] initWithString:distanceString];
    NSRange boldFontRange = NSMakeRange(0, 4);
    [distanceAttributeString beginEditing];
    [distanceAttributeString addAttribute:NSFontAttributeName
                                    value:[UIFont spn_NeutraBoldMedium]
                                    range:boldFontRange];
    [distanceAttributeString endEditing];
    [self.milesAwayLabel setAttributedText:distanceAttributeString];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@", [theater objectForKey:@"name"]];
    NSString *address = [SPNCinelifeQueries getTheatreAddress:theater];
    self.addressLabel.text = address;
    
    self.partnerButton.visible = [[theater objectForKey:@"is_partner"] boolValue];
}

- (IBAction)favoriteAction {
    [self.delegate didPressOnFavoriteInTheaterCell:self];
}

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{

    self.horizontalCollectionView.dataSource = dataSourceDelegate;
    self.horizontalCollectionView.delegate = dataSourceDelegate;
    self.horizontalCollectionView.index = index;
    [self.horizontalCollectionView reloadData];
}

-(void)initializeTheatreCellContents:(NSDictionary*)theatre forIndexPath:(NSIndexPath*)indexPath
{
    CGFloat distance = [[theatre objectForKey:@"distance"] floatValue];
    
    NSString *distanceString = @"";
    if (distance > 50.0) {
        distanceString = [NSString stringWithFormat:@"50+\r\nmiles"];
    }
    else {
        distanceString = [NSString stringWithFormat:@"%.1f\r\nmiles",distance];
    }
    
    [self.milesAwayLabel setFont:[UIFont spn_NeutraBoldTiny]];
    NSMutableAttributedString *distanceAttribtueString = [[NSMutableAttributedString alloc] initWithString:distanceString];
    NSRange boldFontRange = NSMakeRange(0, 4);
    [distanceAttribtueString beginEditing];
    [distanceAttribtueString addAttribute:NSFontAttributeName
                         value:[UIFont spn_NeutraBoldMedium]
                         range:boldFontRange];
    [distanceAttribtueString endEditing];
    [self.milesAwayLabel setAttributedText:distanceAttribtueString];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@", [theatre objectForKey:@"name"]];
    NSString *address = [SPNCinelifeQueries getTheatreAddress:theatre];
    self.addressLabel.text = address;
    
    [self.titleLabel setFont:[UIFont spn_NeutraBoldLarge]];
    [self.addressLabel setFont:[UIFont spn_NeutraMedium]];
}


@end
