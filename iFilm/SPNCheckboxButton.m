//
//  SPNCheckboxButton.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/28/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNCheckboxButton.h"

@implementation SPNCheckboxButton
//Setter
@synthesize checkedImage, checkedBackgroundColor, uncheckedBackgroundColor;

-(id)initWithFrame:(CGRect)frame andCheckedImage:(UIImage *)image
{
    if (self = [super initWithFrame:frame]) {
        
        checkedImage = image;
        [self setBackgroundColor:uncheckedBackgroundColor];
        [self setImage:nil forState:UIControlStateNormal];
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 1;
        [self addTarget:self
                 action:@selector(checkInside:)
       forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

-(void)checkInside:(UIButton*)sender
{
    if (_isChecked) {
        [self setImage:nil forState:UIControlStateNormal];
        [self setBackgroundColor:uncheckedBackgroundColor];
    }
    else {
        [self setImage:checkedImage forState:UIControlStateNormal];
        [self setBackgroundColor:checkedBackgroundColor];
    }
    _isChecked = !_isChecked;
}


@end
