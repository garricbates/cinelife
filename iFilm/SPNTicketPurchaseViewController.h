//
//  SPNTicketPurchaseViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTicketPurchaseViewController : UIViewController
@property () IBOutlet UIWebView *ticketingWebView;
@property () IBOutlet UIActivityIndicatorView *spinner;
@property () IBOutlet UILabel *loadingLabel;

@property () NSString* url;
@end
