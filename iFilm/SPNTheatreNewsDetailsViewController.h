//
//  SPNTheatreNewsDetailsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SPNTheatreNewsDetailsViewController : GAITrackedViewController <UIWebViewDelegate>

@property NSString *newsLink;
@property NSDictionary *newsPostDetails;

@property  NSDictionary *brandColors;

@property IBOutlet UIScrollView *scrollView;
@property  IBOutlet UILabel *newsTitleLabel;
@property  IBOutlet UILabel *newsDateLabel;
@property  IBOutlet UITextView *newsContentTextView;
@property  IBOutlet UIWebView *newsWebView;

@property IBOutlet UIImageView *theatreLogoImageView;
@property IBOutlet UIView *theatreBackgroundView;
@property IBOutlet UIView *newsContainerView;

@property IBOutlet UIView *webContainerView;
@end
