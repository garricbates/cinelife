//
//  SPNTheatreNewsfeedCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/16/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreNewsfeedCell.h"

@interface SPNTheatreNewsfeedCell () {
    NSDateFormatter *dateFormatter;
}

@end

@implementation SPNTheatreNewsfeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.newsTitleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    [self.newsTitleLabel setPreferredMaxLayoutWidth:width-16];
    [self.newsSnippetLabel setPreferredMaxLayoutWidth:width-16];
    
    [self.newsTitleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
    [self.newsSnippetLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.newsDateLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.newsTypeLabel setFont:[UIFont spn_NeutraBoldSmall]];
    
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
    
    [self.newsTypeBackgroundView setBackgroundColor:[UIColor spn_brownColor]];
}

- (void)configureForFeed:(BOOL)forFeed feed:(NSDictionary *)feed htmlSnippet:(NSAttributedString *)htmlSnippet {
    NSString *snippet;
    
    if (forFeed) {
        /*NSDate *realDate = [feed objectForKey:@"created_time"];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        NSString *stringDate = [dateFormatter stringFromDate:realDate];
        if (!stringDate) {
            stringDate = [dateFormatter stringFromDate:[NSDate date]];
        };*/
        [self.newsTypeLabel setText:[NSString stringWithFormat:@"%@  ",[feed objectForKey:@"page"]]];
        [self.newsTypeIconImageView setImage:nil];
        [self.iconWidthConstraint setConstant:0];
        snippet = [feed objectForKey:@"content"];
        
        NSString *feedType = [feed objectForKey:@"page"];
        if ([feedType isEqualToString:@"Facebook"]) {
            [self.newsTypeBackgroundView setBackgroundColor:[UIColor fb_blueColor]];
            [self.newsTypeIconImageView setImage:[UIImage imageNamed:@"fb-icon"]];
            // snippet = [NSString stringWithFormat:@"Date: %@", stringDate];
            [self.iconWidthConstraint setConstant:10];
            [self.feedNameHeightConstraint setConstant:17];
            [self.feedNameBottomConstraint setConstant:8];
            
        }
        else if ([feedType isEqualToString:@"Twitter"]) {
            [self.newsTypeBackgroundView setBackgroundColor:[UIColor tw_blueColor]];
            [self.newsTypeIconImageView setImage:[UIImage imageNamed:@"tw-icon"]];
            //snippet = [NSString stringWithFormat:@"Date: %@", stringDate];
            [self.iconWidthConstraint setConstant:10];
            [self.feedNameHeightConstraint setConstant:17];
            [self.feedNameBottomConstraint setConstant:8];
        }
        else {
            //            else if ([feedType isEqualToString:@"RSS"]){
            [self.newsTypeBackgroundView setBackgroundColor:[UIColor rss_orangeColor]];
            [self.newsTypeIconImageView setImage:[UIImage imageNamed:@"rss-icon"]];
            [self.iconWidthConstraint setConstant:0];
            [self.feedNameHeightConstraint setConstant:0];
            [self.feedNameBottomConstraint setConstant:0];
        }
    } else {
        if ([feed objectForKey:@"category"]) {
            [self.newsTypeLabel setText:[feed objectForKey:@"category"]];
            
            if (![[feed objectForKey:@"summary"] isKindOfClass:[NSNull class]]) {
                snippet = [feed objectForKey:@"summary"];
            }
            else {
                snippet = @"Check out these new promotions!";
            }
            [self.iconWidthConstraint setConstant:0];
            [self.feedNameHeightConstraint setConstant:0];
            [self.feedNameBottomConstraint setConstant:0];
        }
        else {
            [self.newsTypeLabel setText:[feed objectForKey:@"page"]];
            [self.newsTypeBackgroundView setBackgroundColor:[UIColor rss_orangeColor]];
            [self.newsTypeIconImageView setImage:[UIImage imageNamed:@"rss-icon"]];
            snippet = [feed objectForKey:@"contentSnippet"];
            [self.iconWidthConstraint setConstant:0];
            [self.feedNameHeightConstraint setConstant:0];
            [self.feedNameBottomConstraint setConstant:0];
        }
    }
    
    
    NSString *title = [[feed objectForKey:@"title"] stringByDecodingHTMLEntities];
    self.newsTitleLabel.text = title;
    if (title && title.length > 0) {
        [self.newsTitleLabel setText:title];
        [self.feedTitleHeightConstraint setActive:NO];
    } else {
        [self.newsTitleLabel setText:@""];
        [self.feedTitleHeightConstraint setActive:YES];
    }
    //  [self.newsDateLabel setText: [feed objectForKey:@"publishedDate"]];
    snippet = [snippet stringByDecodingHTMLEntities];
    if ([snippet length] > 140) {
        snippet = [snippet substringWithRange:NSMakeRange(0, 140)];
        snippet = [snippet stringByAppendingString:@"..."];
    }
    NSAttributedString *attrStr;
    if ([feed objectForKey:@"category"] && htmlSnippet) {
        attrStr = htmlSnippet;
    } else {
        attrStr = [[NSAttributedString alloc] initWithData:[snippet dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    }
    self.newsSnippetLabel.attributedText = attrStr;
}

+ (CGFloat)heightForFeed:(BOOL)forFeed feed:(NSDictionary *)feed htmlSnippet:(NSAttributedString *)htmlSnippet {
    CGFloat height = 8 + 6;
    NSString *snippet;
    
    if (forFeed) {
        snippet = [feed objectForKey:@"content"];
        
        NSString *feedType = [feed objectForKey:@"page"];
        if ([feedType isEqualToString:@"Facebook"] || [feedType isEqualToString:@"Twitter"]) {
            height += 25;
        }
    } else {
        if ([feed objectForKey:@"category"]) {
            if (![[feed objectForKey:@"summary"] isKindOfClass:[NSNull class]]) {
                snippet = [feed objectForKey:@"summary"];
            }
            else {
                snippet = @"Check out these new promotions!";
            }
        }
        else {
            snippet = [feed objectForKey:@"contentSnippet"];
        }
    }
    
    CGFloat width = CGRectGetWidth([UIScreen mainScreen].bounds) - 16;
    
    NSString *title = [[feed objectForKey:@"title"] stringByDecodingHTMLEntities];
    
    if (title && title.length > 0) {
        height += [title findHeightForWidth:width andFont:[UIFont spn_NeutraBoldSmallMedium]];
    }
    
    snippet = [snippet stringByDecodingHTMLEntities];
    if ([snippet length] > 140) {
        snippet = [snippet substringWithRange:NSMakeRange(0, 140)];
        snippet = [snippet stringByAppendingString:@"..."];
    }
    NSMutableAttributedString *attrStr;
    if ([feed objectForKey:@"category"] && htmlSnippet) {
        attrStr = htmlSnippet.mutableCopy;
    } else {
        attrStr = [[NSMutableAttributedString alloc] initWithData:[snippet dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    }
    
    height += ceilf([attrStr sizeWithMaxSize:CGSizeMake(width, CGFLOAT_MAX)].height);
    
    height += 12;
    
    return height;
}

@end
