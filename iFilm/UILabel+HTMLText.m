//
//  UILabel+HTMLText.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "UILabel+HTMLText.h"

@implementation UILabel (HTMLText)

-(void)setHTMLText:(NSString*)htmlText
{
    NSError *parseError = nil;
    NSAttributedString *attributedHTMLText = [[NSAttributedString alloc] initWithData:[htmlText dataUsingEncoding:NSUTF8StringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:&parseError];
    
    if (!parseError) {
        self.attributedText = attributedHTMLText;
    }
    else {
        self.attributedText = nil;
    }
}

-(void)assignMetaScore:(NSString *)metaScore
{
    if (!metaScore || [metaScore isKindOfClass:[NSNull class]] || [metaScore isEqualToString:@"NR"]) {
        self.text = @"-";
        self.backgroundColor = [UIColor darkGrayColor];
        self.textColor = [UIColor lightGrayColor];
    }
    else {
        self.textColor = [UIColor whiteColor];
        NSInteger scoreValue = [metaScore integerValue];
        if (scoreValue > 60) {
            self.backgroundColor = [UIColor mta_greenColor];
        }
        else if (scoreValue > 39) {
            self.backgroundColor = [UIColor mta_yellowColor];
        }
        else {
            self.backgroundColor = [UIColor mta_redColor];
        }
        self.text = metaScore;
    }
}

@end
