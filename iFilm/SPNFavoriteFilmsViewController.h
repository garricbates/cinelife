//
//  SPNFavoriteFilmsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"  

@interface SPNFavoriteFilmsViewController : GAITrackedViewController

@property (nonatomic, retain) NSArray *movies;
@property (nonatomic, retain) NSArray *festivals;

@end
