    //
//  SPNServerQueries.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNServerQueries.h"

static NSString *myPosterURL = @"";
static NSString *myTrailerURL = @"";

@implementation SPNServerQueries

+(NSString *)posterURL
{
    return myPosterURL;
}

+(void)setPosterURL:(NSString*)posterURL
{
    myPosterURL = posterURL;
}

+(NSString *)trailerURL
{
    return myTrailerURL;
}

+(void)setTrailerURL:(NSString *)trailerURL
{
    myTrailerURL = trailerURL;
}

+(NSDictionary*)queryURL:(NSURL*)dataURL
{
    if (!dataURL) {
        return nil;
    }

    NSURLRequest* request = [NSURLRequest requestWithURL:dataURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    NSHTTPURLResponse* response = nil;
    NSError* error = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!responseData) {
        NSLog(@"Error: %@", error.localizedDescription);
        return nil;
    }
    
    id json = [NSJSONSerialization
               JSONObjectWithData:responseData
               options:NSJSONReadingMutableContainers
               error:&error];
    if (!json) {
        NSLog(@"Error JSON: %@", error.localizedDescription);
        return nil;
    }
    return json;
}


+(NSArray*)getTheatreLastPosts:(NSString*)theatreId
{
    NSArray *newsFeed = [NSArray new];
    NSString *firstURL = [NSString stringWithFormat:@"%@/getPostsByTheaterId?id=%@&limit=4", apiServer, theatreId];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *result = [self queryURL:urlQuery];
    id error = [result objectForKey:@"error_msg"];
    if (!error) {
        id posts = [result objectForKey:@"posts"];
        if (![posts isKindOfClass:[NSNull class]]) {
            newsFeed = posts;
        }
    }
    NSMutableArray *filteredNews = [NSMutableArray new];
    for (NSDictionary *news in newsFeed) {
        if (![[news objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
            [filteredNews addObject:news];
            if ([filteredNews count] > 2) {
                break;
            }
        }
    }
    return filteredNews;
}

+(NSArray*)getTheatreAllPosts:(NSString *)theatreId
{
    NSArray *newsFeed = nil;
    NSString *firstURL = [NSString stringWithFormat:@"%@/getPostsByTheaterId?id=%@&limit=20", apiServer, theatreId];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *result = [self queryURL:urlQuery];
    NSString *error = [result objectForKey:@"error_msg"];
    if (!error) {
        id posts = [result objectForKey:@"posts"];
        if (![posts isKindOfClass:[NSNull class]]) {
            newsFeed = posts;
        }
    }
    NSMutableArray *filteredNews = [NSMutableArray new];
    for (NSDictionary *news in newsFeed) {
        if (![[news objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
            [filteredNews addObject:news];
        }
    }
    return filteredNews;
}

+(NSDictionary*)getTheatrePostDetails:(NSString*)postId
{
    NSDictionary *postInformation = nil;
    NSString *firstURL = [NSString stringWithFormat:@"%@/postDetail?id=%@", apiServer, postId];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *result = [self queryURL:urlQuery];
    NSString* error = [result objectForKey:@"error_msg"];
    if (!error) {
        postInformation = result;
    }
    return postInformation;
}

+(NSDictionary *)getComingSoonPostsForTheatre:(NSString *)theatreId
{

    NSDictionary *latestPost = nil;
    NSString *firstURL = [NSString stringWithFormat:@"%@/GetPostsByTheaterId?id=%@&cat=Coming Soon", apiServer, theatreId];
    firstURL = [firstURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *result = [self queryURL:urlQuery];
    NSString* error = [result objectForKey:@"error_msg"];
    if (!error) {
        NSArray *postList = [result objectForKey:@"posts"];
        latestPost = [postList firstObject];
        return latestPost;
    }
    return latestPost;
}

//----------------------Movie queries ---------------------//
+(NSDictionary*)getInfoForMovie:(NSString*)movieId
{
    NSString *firstURL = [NSString stringWithFormat:@"%@/getInfoForMovieId?movieId=%@", apiServer, movieId];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    NSDictionary *movieInfo = [self queryURL:urlQuery];
    return movieInfo;
}

+(NSString*)getMovieImageURL:(NSString*)movieId
{
    NSString *newMovieId = @"";
    if ([movieId length] < 6) {
        newMovieId = [movieId stringByPaddingToLength:6 withString:@"0" startingAtIndex:0];
    }
    else {
        newMovieId = movieId;
    }
    NSString *scale = @"0";
    if (IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        scale = @"h";
    }
    newMovieId = [NSString stringWithFormat:@"%@%@1", newMovieId, scale];
    NSString *url = [[SPNServerQueries posterURL] stringByReplacingOccurrencesOfString:@"%MOVIEID%" withString:newMovieId];
    
    return url;
}

+(NSArray*)getMovieStills:(NSString*)movieId
{
    NSMutableArray *movieStills = [NSMutableArray new];
    NSString *newMovieId = @"";
    if ([movieId length] < 6) {
        newMovieId = [movieId stringByPaddingToLength:6
                                           withString:@"0"
                                      startingAtIndex:0];
    }
    else {
        newMovieId = movieId;
    }
    for (int i = 2; i < 8; i++)
    {
        NSString *photoId = [NSString stringWithFormat:@"%@r%d", newMovieId, i];
        NSString *url = [[SPNServerQueries posterURL] stringByReplacingOccurrencesOfString:@"%MOVIEID%" withString:photoId];
        BOOL isURLValid = [self isValidURL:[NSURL URLWithString:url]];
        if (isURLValid) {
            [movieStills addObject:url];
        }
        else { // No more stills
            break;
        }
    }
    return movieStills;
}

+(NSString*)getTrailerURLForMovie:(NSString*)movieId
{

    NSString *url = [[SPNServerQueries trailerURL] stringByReplacingOccurrencesOfString:@"%MOVIEID%" withString:movieId];
    
    if (![self isValidURL:[NSURL URLWithString:url]]) {
        url = nil;
    }
    return url;
}

+ (BOOL)isValidURL:(NSURL*)url
{
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *res = nil;
    NSError *err = nil;
    [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
    BOOL isGoodURL = (err == nil && [res statusCode] != 404);
    return isGoodURL;
}

+(BOOL)addCommentOrRatingForUser:(NSString*)username forMovie:(NSString*)movieId withComment:(NSString*)comment andRating:(NSUInteger)rating andTitle:(NSString*)title
{
    NSString *newQuery;
    NSString *url= [NSString stringWithFormat:@"%@/addCommentOrRating?movieId=%@", apiServer, movieId];

    BOOL successfullyAddedComment = NO;
    if (username) {
        NSString *newTitle = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                                  (CFStringRef)title,
                                                                                                  NULL,
                                                                                                  (CFStringRef)@"!*'\"();@&=+$,/?%#[]% ",
                                                                                                  CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&uid=%@&title=%@", username, newTitle]];
        
    }
    
    
    if (comment) {
        newQuery = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                        (CFStringRef)comment,
                                                                                        NULL,
                                                                                        (CFStringRef)@"!*'\"();@&=+$,/?%#[]% ",
                                                                                        CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&comment=%@",newQuery]];
    }
    if (rating > 0) {
        url = [url stringByAppendingString:[NSString stringWithFormat:@"&rating=%lu",(unsigned long)rating]];
    }
    
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *result = [self queryURL:urlQuery];
    
    NSNumber *success = [result objectForKey:@"success"];
    if (success.intValue < 1) {
        successfullyAddedComment = NO;
    }
    else {
        successfullyAddedComment =  YES;
    }

    return successfullyAddedComment;
}

+(NSDictionary*)getCommentOrRatingForMovie:(NSString*)movieId
{
    NSString *url= [NSString stringWithFormat:@"%@/getCommentOrRating?movieId=%@", apiServer, movieId];
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *reviewsForMovie = [self queryURL:urlQuery];
    return reviewsForMovie;
}

+(NSDictionary *)getCommentOfUser:(NSString *)username forMovie:(NSString *)movieId
{
    NSString *url = [NSString stringWithFormat:@"%@/GetCommentAndRatingForUserIdAndMovieId?uid=%@&movieId=%@", apiServer, username, movieId];
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *result = [self queryURL:urlQuery];
    if ([result objectForKey:@"message"]) {
        return nil;
    }
    else {
        return [result objectForKey:@"comment"];
    }
}

+(NSDictionary*)getCastAndCrewForMovie:(NSDictionary*)movie
{
    
    NSMutableDictionary *movieCast = [NSMutableDictionary new];
    
    NSArray *elementsInCast = @[@"actors", @"directors", @"writers", @"producers"];
    for (NSString *castType in elementsInCast)
    {
        id castMembers = [movie objectForKey:castType];
        if ([castMembers isKindOfClass:[NSArray class]]) {
            [movieCast setObject:castMembers
                          forKey:castType];
        }
        else if ([castMembers isKindOfClass:[NSString class]]) {
            [movieCast setObject:@[castMembers]
                          forKey:castType];
        }
    }

    return movieCast;
}

+(id)getTicketsForTheatre:(NSString*)theatreId forMovie:(NSString*)movieId forDate:(NSDate*)date andShowtime:(NSString*)showtime
{
    NSString *url = nil;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];
    NSString *firstURL = [NSString stringWithFormat:@"%@/relay?houseId=%@&movieId=%@&showdate=%@&showtime=%@", apiServer, theatreId, movieId, newDate, showtime];
    firstURL = [firstURL stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    NSDictionary *result = [self queryURL:urlQuery];
    if (result) {
        url = [result objectForKey:@"url"];
        if (url) {
            return url;
        }
        NSString *performanceId = [result objectForKey:@"performance_id"];
        if (performanceId) {
            return result;
        }
    }
    return nil;
}

+(id)purchaseTickets:(NSArray *)tickets forAmount:(NSString *)amount forPerformanceId:(NSString *)performanceId forTheatre:(NSString *)rtsTheatreId forAuditorium:(NSString *)auditorium withCardInformation:(NSDictionary *)cardDetails andEmailAddress:(NSString *)email andFilmDetails:(NSDictionary *)filmDetails withTicketNames:(NSArray *)ticketNames
{
  
    NSMutableDictionary *parameters = [NSMutableDictionary new];
 
    /*
    [parameters setObject:@"989656745315" forKey:@"barcode"];
    [parameters setObject:@"656745" forKey:@"transaction_id"];
    return parameters;
    */
      
    
    
   // NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:ticketNames
                   forKey:@"ticket_names"];
    
    [parameters setObject:[filmDetails objectForKey:@"title"]
                   forKey:@"movie_title"];
    [parameters setObject:[filmDetails objectForKey:@"theatreName"]
                   forKey:@"theatre_name"];
    [parameters setObject:[filmDetails objectForKey:@"showDate"]
                   forKey:@"movie_date"];
    [parameters setObject:[filmDetails objectForKey:@"showTime"]
                   forKey:@"movie_time"];
    
    [parameters setObject:tickets forKey:@"tickets"];
    
    [parameters setObject:auditorium
                   forKey:@"auditorium"];
    [parameters setObject:rtsTheatreId
                   forKey:@"rts_id"];
    [parameters setObject:performanceId
                   forKey:@"performance_id"];
    [parameters setObject:email
                   forKey:@"email_address"];
    [parameters setObject:amount
                   forKey:@"payment_info_amount"];
    
    NSString *cardType = [cardDetails objectForKey:@"cardType"];
    [parameters setObject:cardType
                   forKey:@"payment_info_type"];
    
    if ([cardType isEqualToString:@"CreditCard"]) {
        NSString *expDate = [cardDetails objectForKey:@"cardExpirationDate"];
        expDate = [expDate stringByReplacingOccurrencesOfString:@"/" withString:@""];
        [parameters setObject:expDate
                       forKey:@"payment_info_cc_expiration"];
        
        [parameters setObject:[cardDetails objectForKey:@"cardCVC"]
                       forKey:@"payment_info_cc_cid"];
        
        [parameters setObject:[cardDetails objectForKey:@"cardName"]
                       forKey:@"payment_info_cc_name_on_card"];
        
        [parameters setObject:[cardDetails objectForKey:@"cardNumber"]
                       forKey:@"payment_info_cc_number"];
       /*
        [parameters setObject:[cardDetails objectForKey:@"cardZIP"]
                       forKey:@"payment_info_cc_postal"];
       */
    }
    else {
        [parameters setObject:[cardDetails objectForKey:@"cardNumber"]
                       forKey:@"payment_info_gc_number"];
    }

    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters
                                                            options:NSJSONWritingPrettyPrinted
                                                              error:&error];
    //NSData *postString = [postData data]
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/PurchaseMovieTickets",apiServer]]];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    


    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        //success
        id json = [NSJSONSerialization
                   JSONObjectWithData:data
                   options:kNilOptions
                   error:&error];
        
        return json;
    }
    return nil;
}



+(NSDictionary*)getTheatresForZipCode:(NSString *)zipcode withUserLocation:(CLLocation*)userLocation andRadius:(NSInteger)radius
{
    NSString *firstURL = [NSString stringWithFormat:@"%@/getTheatresForZipCode?zip=%@&radius=%ld", apiServer, zipcode, (long) radius];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *fullTheatreList = [self queryURL:urlQuery];
    
    NSDictionary *west = [fullTheatreList objectForKey:@"west"];
    
    NSMutableArray *westCinemas = [NSMutableArray new];
    NSArray *brandingForWest;
    if ([west count] > 0) {
        westCinemas = [west objectForKey:@"theaters"];
        brandingForWest = [west objectForKey:@"branding"];
    }
    
    NSDictionary *arthouse = [fullTheatreList objectForKey:@"arthouse"];
    NSMutableArray *artCinemas= [arthouse objectForKey:@"theaters"];
    NSUInteger index = 0;
    
    for (NSDictionary *artCinema in artCinemas)
        {
            index = [westCinemas indexOfObject:artCinema];
            if (index != NSNotFound) {
                [westCinemas removeObject:artCinema];
            }
        }
    artCinemas = [self sortTheatersByDistance:artCinemas withLocation:userLocation];
    westCinemas = [self sortTheatersByDistance:westCinemas withLocation:userLocation];
    
    NSArray *mergedCinemas = [artCinemas arrayByAddingObjectsFromArray:westCinemas];
    
    if (brandingForWest && [mergedCinemas count] > 0) {
        NSMutableArray *cinemasWithBranding = [[NSMutableArray alloc] initWithArray:mergedCinemas];
        for (id westCinema in mergedCinemas)
        {
            NSUInteger index = [brandingForWest indexOfObjectPassingTest:
                                ^BOOL(NSDictionary *theatre, NSUInteger idx, BOOL *stop)
                                {
                                    return [[theatre objectForKey:@"theatreId"] isEqual:[westCinema objectForKey:@"theatreId"]];
                                }
                                ];
            if (index != NSNotFound)
            {
                NSMutableDictionary *westWithBrand = [NSMutableDictionary dictionaryWithDictionary:westCinema];
                NSUInteger indexofCinema = [cinemasWithBranding indexOfObject:westCinema];
                if (indexofCinema < [cinemasWithBranding count]) {
                    
//                [cinemasWithBranding removeObject:westCinema];
                    [westWithBrand setObject:[[brandingForWest objectAtIndex:index] objectForKey:@"branding"]
                                     forKey:@"branding"];
                    [cinemasWithBranding setObject:westWithBrand
                                atIndexedSubscript:indexofCinema];
                }
                
            }
        }
        mergedCinemas = cinemasWithBranding;
    }
    
    NSDictionary *newResult = [[NSDictionary alloc] initWithObjects:@[mergedCinemas, [NSNumber numberWithInteger:[artCinemas count]]] forKeys:@[@"theatres", @"moreCount"]] ;
    
    return newResult;
}

+(NSDictionary*)getTheatresForCoordinates:(CLLocationCoordinate2D)coordinates withUserLocation:(CLLocation *)userLocation andRadius:(NSInteger)radius
{
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinates.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinates.longitude];;

    NSString *firstURL = [NSString stringWithFormat:@"%@/GetTheatresForLatLng?lat=%@&lng=%@&radius=%ld", apiServer, latitude, longitude, (long) radius];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *fullTheatreList = [self queryURL:urlQuery];
    
    NSDictionary *west = [fullTheatreList objectForKey:@"west"];
    
    NSMutableArray *westCinemas = [NSMutableArray new];
    NSArray *brandingForWest;
    if ([west count] > 0) {
        westCinemas = [west objectForKey:@"theaters"];
        brandingForWest = [west objectForKey:@"branding"];
    }
    
    NSDictionary *arthouse = [fullTheatreList objectForKey:@"arthouse"];
    NSMutableArray *artCinemas= [arthouse objectForKey:@"theaters"];
    NSUInteger index = 0;
    
    for (NSDictionary *artCinema in artCinemas)
    {
        index = [westCinemas indexOfObject:artCinema];
        if (index != NSNotFound) {
            [westCinemas removeObject:artCinema];
        }
    }
    artCinemas = [self sortTheatersByDistance:artCinemas withLocation:userLocation];
    westCinemas = [self sortTheatersByDistance:westCinemas withLocation:userLocation];
    
    NSArray *mergedCinemas = [artCinemas arrayByAddingObjectsFromArray:westCinemas];
    
    if (brandingForWest && [mergedCinemas count] > 0) {
        NSMutableArray *cinemasWithBranding = [[NSMutableArray alloc] initWithArray:mergedCinemas];
        for (id westCinema in mergedCinemas)
        {
            NSUInteger index = [brandingForWest indexOfObjectPassingTest:
                                ^BOOL(NSDictionary *theatre, NSUInteger idx, BOOL *stop)
                                {
                                    return [[theatre objectForKey:@"theatreId"] isEqual:[westCinema objectForKey:@"theatreId"]];
                                }
                                ];
            if (index != NSNotFound)
            {
                NSMutableDictionary *westWithBrand = [NSMutableDictionary dictionaryWithDictionary:westCinema];
                NSUInteger indexofCinema = [cinemasWithBranding indexOfObject:westCinema];
                if (indexofCinema < [cinemasWithBranding count]) {
                    
                    //                [cinemasWithBranding removeObject:westCinema];
                    [westWithBrand setObject:[[brandingForWest objectAtIndex:index] objectForKey:@"branding"]
                                      forKey:@"branding"];
                    [cinemasWithBranding setObject:westWithBrand
                                atIndexedSubscript:indexofCinema];
                }
                
            }
        }
        mergedCinemas = cinemasWithBranding;
    }
    
    NSDictionary *newResult = [[NSDictionary alloc] initWithObjects:@[mergedCinemas, [NSNumber numberWithInteger:[artCinemas count]]] forKeys:@[@"theatres", @"moreCount"]] ;
    
    return newResult;
}



+(NSMutableArray *)sortTheatersByDistance:(NSArray*)theaterList withLocation:(CLLocation*)userLocation
{
    NSMutableArray *sortedTheatres = [NSMutableArray arrayWithArray:theaterList];
    double lat, lon;
    if (userLocation)
    {
        for (NSMutableDictionary *theater in sortedTheatres)
        {
            if (![theater objectForKey:@"distance"]) {
                NSDictionary *geoCode = [theater objectForKey:@"geolocation"];
                NSString *latString =[geoCode objectForKey:@"lat"];
                NSString *lonString =[geoCode objectForKey:@"lon"];
                if (!latString || !lonString) {
                    [theater setObject:@0 forKey:@"distance"];
                }
                else
                {
                    lat = [latString doubleValue];
                    lon = [lonString doubleValue];
                    
                    CLLocation *theatreDistance = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
                    CLLocationDistance currentDistance = [theatreDistance distanceFromLocation:userLocation];
                    currentDistance = currentDistance / 1000 * 0.621371192; // Convert meters into miles
                    
                    [theater setObject:[NSNumber numberWithDouble:currentDistance] forKey:@"distance"];
                }
            }
            else {
                NSNumber *newDist = [NSNumber numberWithFloat:[[theater objectForKey:@"distance"] floatValue]];
                 [theater setObject:newDist
                             forKey:@"distance"];
            }
        }
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        sortedTheatres = [[sortedTheatres sortedArrayUsingDescriptors:@[sorter]] mutableCopy];
    }
    return sortedTheatres;
}

+ (NSString*)getTheatreAddress:(NSDictionary*)theatre
{
    NSDictionary *location = [theatre objectForKey:@"location"];
    NSString *street = [location objectForKey:@"street"];
    NSString * state = [location objectForKey:@"state"];
    NSString * postalCode = [location objectForKey:@"postalCode"];
    NSString * city = [location objectForKey:@"city"];
    NSString *fullAddress = [NSString stringWithFormat:@"%@, %@, %@ %@", street, city, state, postalCode];
    return fullAddress;
}

+(NSDictionary*)getShowtimesForTheater:(NSString*)theaterId andDate:(NSDate*)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *newDate = [dateFormat stringFromDate:date];
    NSString *firstURL = [NSString stringWithFormat:@"%@/getShowtimesForTheaterId?theaterId=", apiServer];
    
    NSString *lastURL =[NSString stringWithFormat:@"%@%@&startDate=%@",firstURL, theaterId, newDate];
    NSURL *urlQuery = [NSURL URLWithString:lastURL];
    NSDictionary *schedule = [self queryURL:urlQuery];
    return schedule;
}

+(NSArray*)getShowtimesForDate:(NSDate*)date forZipcode:(NSString*)zip withPartners:(BOOL)showPartners
{
    NSArray *films = [NSArray new];
    NSString *firstURL = [NSString stringWithFormat:@"%@%@", apiServer, @"/moviesWithShowingsForDate?startDate="];
    
    NSString *lastURL = @"&zip=";
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];
    
    NSString *newZip = [NSString stringWithFormat:@"%@", zip];
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@", firstURL, newDate, lastURL, newZip];
    if (showPartners) {
        url = [url stringByAppendingString:@"&showpartners=true"];
    }
    NSURL *urlQuery = [NSURL URLWithString:url];
    
    NSDictionary *results = [self queryURL:urlQuery];
    
    if (results) {
        
        if (showPartners) {
            NSDictionary *partners = [results objectForKey:@"partners"];
            NSArray *partnerFilms = [partners objectForKey:@"movies"];
            partnerFilms = [[NSSet setWithArray:partnerFilms] allObjects];
            partnerFilms = [self sortFilmsByName:partnerFilms];
            return partnerFilms;
        }
        NSDictionary *arthouse = [results objectForKey:@"arthouses"];
        NSArray *artFilms = [arthouse objectForKey:@"movies"];
        
        NSDictionary *west = [results objectForKey:@"west"];
        NSMutableArray *westFilms = [west objectForKey:@"movies"];
        
        for (NSDictionary *movie in artFilms) {
            NSInteger index = [westFilms indexOfObject:movie];
            if (index != NSNotFound) {
                [westFilms removeObject:movie];
            }
        }
        
        films = [self sortFilmsByName:artFilms];
        NSArray *sortedWestFilms = [self sortFilmsByName:westFilms];
        films = [films arrayByAddingObjectsFromArray:sortedWestFilms];
    }
    return films;
}

+(NSArray *)getShowtimesForDate:(NSDate *)date forCoordinates:(CLLocationCoordinate2D)coordinates withPartners:(BOOL)showPartners
{
    NSArray *films = [NSArray new];
    NSString *firstURL = [NSString stringWithFormat:@"%@%@", apiServer, @"/moviesWithShowingsForDate?startDate="];
    
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinates.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinates.longitude];;

    NSString *lastURL = [NSString stringWithFormat:@"&lat=%@&lng=%@",latitude,longitude];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@", firstURL, newDate, lastURL];
    if (showPartners) {
        url = [url stringByAppendingString:@"&showpartners=true"];
    }

    NSURL *urlQuery = [NSURL URLWithString:url];
    
    NSDictionary *results = [self queryURL:urlQuery];
    
    if (results) {
        
        if (showPartners) {
            NSDictionary *partners = [results objectForKey:@"partners"];
            NSArray *partnerFilms = [partners objectForKey:@"movies"];
            partnerFilms = [self sortFilmsByName:partnerFilms];
            return partnerFilms;
        }
        NSDictionary *arthouse = [results objectForKey:@"arthouses"];
        NSArray *artFilms = [arthouse objectForKey:@"movies"];
        
        NSDictionary *west = [results objectForKey:@"west"];
        NSMutableArray *westFilms = [west objectForKey:@"movies"];
        
        for (NSDictionary *movie in artFilms) {
            NSInteger index = [westFilms indexOfObject:movie];
            if (index != NSNotFound) {
                [westFilms removeObject:movie];
            }
        }
        
        films = [self sortFilmsByName:artFilms];
        NSArray *sortedWestFilms = [self sortFilmsByName:westFilms];
        films = [films arrayByAddingObjectsFromArray:sortedWestFilms];
    }
    return films;
}

//----------------------------------------Sort Movies-------------------------------------------//
+(NSArray*)sortFilmsByName:(NSArray*)movieList
{
    if ([movieList isKindOfClass:[NSNull class]]) {
        return [NSArray new];
    }
    if ([movieList count] < 1) {
        return movieList;
    }
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    return [movieList sortedArrayUsingDescriptors:@[sorter]];
}

+(NSDictionary*)sortFilmsByDate:(NSArray*)movieList
{
    if ([movieList isKindOfClass:[NSNull class]]) {
        return [NSDictionary new];
    }
    if ([movieList count] < 1) {
        return [NSDictionary new];
    }
    NSMutableDictionary *sortedFilms = [NSMutableDictionary new];
    
    for (NSDictionary *movie in movieList)
    {
        NSString *releaseDate = [movie objectForKey:@"release_date"];
        if ([sortedFilms objectForKey:releaseDate]) {
            NSMutableArray *filmsOfThisDate = [sortedFilms objectForKey:releaseDate];
            [filmsOfThisDate addObject:movie];
        }
        else {
            NSMutableArray *filmsOfThisDate = [NSMutableArray new];
            [filmsOfThisDate addObject:movie];
            [sortedFilms setObject:filmsOfThisDate forKey:releaseDate];
        }
    }
    return sortedFilms;
}

+(NSDictionary*)getFutureReleases
{
    NSArray *futureReleases = nil;
    NSDictionary *sortedFutureReleases;
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    int daysToAdd = 1;
    NSDate *tomorrow = [today dateByAddingTimeInterval:60*60*24*daysToAdd];
    
    NSString *newDate = [dateFormat stringFromDate:tomorrow];
    
    NSString *firstURL = [NSString stringWithFormat:@"%@/movieFutureReleases?releaseDate=%@", apiServer, newDate];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    id result = [self queryURL:urlQuery];
    if (result) {
        NSArray *sortedFilms = [self sortFilmsByName:result];
        futureReleases = sortedFilms;
    
    }
    sortedFutureReleases = [self sortFilmsByDate:futureReleases];
    return sortedFutureReleases;
}

+(NSArray*)getFestivalFilms
{
    NSArray *festivalFilms = nil;
    NSString *filmFeed = @"http://indieflix.com/api/v1/films?limit=15&channel=shorts&format=json";
    NSURL *urlQuery = [NSURL URLWithString:filmFeed];
    NSDictionary *result = [self queryURL:urlQuery];
    if (result) {
        NSArray *sortedFilms = [result objectForKey:@"objects"];
        festivalFilms = sortedFilms;
    }
    return festivalFilms;
}

//TODO: add userId
+(NSDictionary*)getShowtimesForFilm:(NSString*)movieId forDate:(NSDate*)date forZipcode:(NSString*)zipcode withUserLocation:(CLLocation *)userLocation andRadius:(NSInteger)radius
{
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];

    NSString *firstURL = [NSString stringWithFormat:@"%@/showingsForMovieId?movieId=%@&startDate=%@&zip=%@&radius=%ld", apiServer, movieId, newDate, zipcode, (long) radius];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    NSDictionary *fullTheatreList = [self queryURL:urlQuery];
    
    
    NSMutableArray *westCinemas = [fullTheatreList objectForKey:@"west"];
    NSMutableArray *artCinemas= [fullTheatreList objectForKey:@"arthouse"];
    NSUInteger index;
    
    for (NSDictionary *artCinema in artCinemas)
    {
        index = [westCinemas indexOfObject:artCinema];
        if (index != NSNotFound) {
            [westCinemas removeObject:artCinema];
        }
    }
    artCinemas = [self sortTheatersByDistance:artCinemas withLocation:userLocation];
    westCinemas = [self sortTheatersByDistance:westCinemas withLocation:userLocation];
    
    NSArray *mergedCinemas = [artCinemas arrayByAddingObjectsFromArray:westCinemas];
    
    NSDictionary *newResult = [[NSDictionary alloc] initWithObjects:@[mergedCinemas, [NSNumber numberWithInteger:[artCinemas count]]] forKeys:@[@"theatres", @"moreCount"]] ;
    
    return newResult;
}


+(NSDictionary *)getShowtimesForFilm:(NSString *)movieId forDate:(NSDate *)date forCoordinates:(CLLocationCoordinate2D)coordinates withUserLocation:(CLLocation *)userLocation andRadius:(NSInteger)radius
{
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];
    
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinates.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinates.longitude];;
    
    NSString *lastURL = [NSString stringWithFormat:@"&lat=%@&lng=%@&radius=%ld",latitude,longitude, (long) radius];
    
    NSString *firstURL = [NSString stringWithFormat:@"%@/showingsForMovieId?movieId=%@&startDate=%@%@", apiServer, movieId, newDate, lastURL];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    NSDictionary *fullTheatreList = [self queryURL:urlQuery];
    
    
    NSMutableArray *westCinemas = [fullTheatreList objectForKey:@"west"];
    NSMutableArray *artCinemas= [fullTheatreList objectForKey:@"arthouse"];
    NSUInteger index;
    
    for (NSDictionary *artCinema in artCinemas)
    {
        index = [westCinemas indexOfObject:artCinema];
        if (index != NSNotFound) {
            [westCinemas removeObject:artCinema];
        }
    }
    artCinemas = [self sortTheatersByDistance:artCinemas withLocation:userLocation];
    westCinemas = [self sortTheatersByDistance:westCinemas withLocation:userLocation];
    
    NSArray *mergedCinemas = [artCinemas arrayByAddingObjectsFromArray:westCinemas];
    
    NSDictionary *newResult = [[NSDictionary alloc] initWithObjects:@[mergedCinemas, [NSNumber numberWithInteger:[artCinemas count]]] forKeys:@[@"theatres", @"moreCount"]] ;
    
    return newResult;
}

+(NSArray*)getRottenTomatoFilms
{
    NSArray *tomatoList = [NSArray new];
    NSString *firstURL = @"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/box_office.json?limit=10&apikey=zkn5h4przr6zjrmddkzx7tes";
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    NSDictionary *fullRTFilmList = [self queryURL:urlQuery];
    
    if (fullRTFilmList) {
        NSArray *films = [fullRTFilmList objectForKey:@"movies"];
        tomatoList = films;
    }
    return tomatoList;
}

+(NSDictionary*)getSundanceAndIndieNews
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *newsFeedFile = [mainBundle pathForResource: @"newsfeed"
                                                  ofType: @"json"];
    
    NSData *newsContent = [NSData dataWithContentsOfFile:newsFeedFile];
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:newsContent
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    
    return json;
}

+(NSDictionary*)searchFilmByTitle:(NSString*)title
{
    NSString* newQuery = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                              (CFStringRef)title,
                                                                                              NULL,
                                                                                              (CFStringRef)@"!*'\"();@&=+$,/?%#[]% ",
                                                                                              CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
    
    NSString *firstURL =[NSString stringWithFormat:@"%@/search?title=%@", apiServer, newQuery];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    NSDictionary *result = [self queryURL:urlQuery];
    
    NSMutableArray *possibleFilms = [NSMutableArray new];
    NSDictionary *bestMatchingFilm = nil;

    if (result) {
        NSArray *matchingFilms = [result objectForKey:@"program"];
        for (NSDictionary* film in matchingFilms)
        {
            if ([title caseInsensitiveCompare:[film objectForKey:@"name"]] == NSOrderedSame) {
                [possibleFilms addObject:film];
                break;
            }
        }
    }
    
    if ([possibleFilms count] > 0) {
        bestMatchingFilm = [possibleFilms firstObject];
    }
    
    return bestMatchingFilm;
}

+(NSArray*)getNewsfeedStoriesForPages:(NSArray*)newsfeedList withPostCount:(NSInteger)postCount
{
    NSString *numOfEntries = [NSString stringWithFormat:@"&num=%ld", (long)postCount];
    NSString *firstLink = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=";
    NSMutableArray *fullEntries = [NSMutableArray new];
    for (NSDictionary *page in newsfeedList)
    {
       NSArray *rssList = [page objectForKey:@"rss"];
        
      
        for (NSString *rss in rssList) {
            NSDictionary *data = [self queryURL:
                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", firstLink, rss, numOfEntries]]];
            
            id responseData = [data objectForKey:@"responseData"];
            
            if (![responseData isKindOfClass:[NSNull class]]) {
                NSDictionary *feed = [responseData objectForKey:@"feed"];
                if (feed) {
                    NSArray *entries = [feed objectForKey:@"entries"];
                    [fullEntries addObjectsFromArray:entries];
                }
            }
        }
    }
    return fullEntries;
}

+(NSString*)getMetascoreForMovie:(NSString*)movieTitle
{
    NSString *criticScore = nil;
    NSString *newMovieTitle;
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            newMovieTitle = movieTitle;
        }
        else {
            newMovieTitle = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        newMovieTitle = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    newMovieTitle = [self stringFilter:newMovieTitle];
   
    NSString *lastChar = [newMovieTitle substringFromIndex:[newMovieTitle length]-1];
    if ([lastChar isEqualToString:@" "]) {
        newMovieTitle = [newMovieTitle substringToIndex:[newMovieTitle length]-1];
    }
    
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"/:.,'"];
    newMovieTitle = [[newMovieTitle componentsSeparatedByCharactersInSet:
                      doNotWant]
                     componentsJoinedByString:@""];

    newMovieTitle = [newMovieTitle stringByReplacingOccurrencesOfString:@" " withString:@"-"];
   
    NSString *query = [NSString stringWithFormat:@"%@/GetMetacritics?title=%@&type=meta", apiServer, newMovieTitle];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    NSDictionary *meta = [result objectForKey:@"Meta"];
    if (meta) {
        NSDictionary *score = [meta objectForKey:@"Score"];
        criticScore = [score objectForKey:@"Total"];
    }
    return criticScore;
}

+(NSDictionary *)getMetaReviewsForMovie:(NSString *)movieTitle
{
    NSString *newMovieTitle;
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            newMovieTitle = movieTitle;
        }
        else {
            newMovieTitle = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        newMovieTitle = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    newMovieTitle = [self stringFilter:newMovieTitle];
    
    NSString *lastChar = [newMovieTitle substringFromIndex:[newMovieTitle length]-1];
    if ([lastChar isEqualToString:@" "]) {
        newMovieTitle = [newMovieTitle substringToIndex:[newMovieTitle length]-1];
    }
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"/:.,'"];
    newMovieTitle = [[newMovieTitle componentsSeparatedByCharactersInSet:
                      doNotWant]
                     componentsJoinedByString:@""];
    
    newMovieTitle = [newMovieTitle stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    
    newMovieTitle = [self stringFilter:newMovieTitle];
    NSString *query = [NSString stringWithFormat:@"%@/GetMetaReviews?title=%@&type=meta", apiServer, newMovieTitle];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    if ([result objectForKey:@"error_msg"]) {
        return nil;
    }
    return result;
}

+ (NSString *)stringFilter:(NSString *)targetString {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString: targetString];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"(" intoString:NULL] ;
        
        [theScanner scanUpToString:@")" intoString:&text] ;
        
        targetString = [targetString stringByReplacingOccurrencesOfString:
                        [NSString stringWithFormat:@"%@)", text]
                                                               withString:@""];
        
    }
    
    return targetString;
    
}


// List of default newsfeed pages for the news screen.
+(NSArray*)getNewsfeedPages
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"laemmlefeed"
                                                         ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data
                                              options:kNilOptions
                                                error:&error];
    NSArray *webs = [json objectForKey:@"webs"];
    return webs;
}

+(NSArray*)getRSSForTheatre:(NSString*)theatreId andLimit:(NSInteger)limit
{
    NSString *url =[NSString stringWithFormat:@"%@/GetRssListForTheater?tms_id=%@", apiServer, theatreId];
    NSURL *urlQuery = [NSURL URLWithString:url];

    
    id availablePages = (NSArray*)[self queryURL:urlQuery];
    if ([availablePages isKindOfClass:[NSDictionary class]]) {
        return [NSArray new];
    }
    
    NSMutableArray *newsfeed = [[NSMutableArray alloc] init];
    NSString *numOfEntries = [NSString stringWithFormat:@"&num=%ld", (long)limit];
    NSString *firstLink = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=";
    for (NSDictionary *feedPage in availablePages) {
        
        NSString *rss = [feedPage objectForKey:@"url"];
        rss =  (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( NULL, (CFStringRef)rss, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
        
        NSDictionary *data = [self queryURL:
                              [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", firstLink, rss, numOfEntries]]];
        
        id responseData = [data objectForKey:@"responseData"];
        
        if (![responseData isKindOfClass:[NSNull class]]) {
            NSDictionary *feed = [responseData objectForKey:@"feed"];
            if (feed) {
                NSString *feedURL = [feed objectForKey:@"feedUrl"];
                NSRange range =[feedURL rangeOfString:@".com"];
                if (range.location != NSNotFound) {
                    feedURL = [feedURL substringToIndex:range.location+4];
                }
                
                NSArray *entries = [feed objectForKey:@"entries"];
                NSMutableDictionary *newEntry;
                for (NSDictionary *entry in entries)
                {
                    newEntry = [NSMutableDictionary dictionaryWithDictionary:entry];
                    [newEntry setObject:[feedPage objectForKey:@"type"] forKey:@"page"];
                    [newEntry setObject:feedURL forKey:@"feedUrl"];
                    [newsfeed addObject:newEntry];
                }
            }
        }
    }
    return newsfeed;
}

+(NSArray*)getRSSForTheatre:(NSString*)theatreId andLimit:(NSInteger)limit forRSSType:(NSString *)type
{
    NSString *url =[NSString stringWithFormat:@"%@/GetRssListForTheater?tms_id=%@", apiServer, theatreId];
    NSURL *urlQuery = [NSURL URLWithString:url];
    
    
    id availablePages = (NSArray*)[self queryURL:urlQuery];
    if ([availablePages isKindOfClass:[NSDictionary class]]) {
        return [NSArray new];
    }
    
    NSMutableArray *newsfeed = [[NSMutableArray alloc] init];
    NSString *numOfEntries = [NSString stringWithFormat:@"&num=%ld", (long)limit];
    NSString *firstLink = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=";
    for (NSDictionary *feedPage in availablePages) {
        
        if ([[feedPage objectForKey:@"type"] isEqualToString:type]) {
            NSString *rss = [feedPage objectForKey:@"url"];
            rss =  (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( NULL, (CFStringRef)rss, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
            
            NSDictionary *data = [self queryURL:
                                  [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", firstLink, rss, numOfEntries]]];
            
            id responseData = [data objectForKey:@"responseData"];
            
            if (![responseData isKindOfClass:[NSNull class]]) {
                NSDictionary *feed = [responseData objectForKey:@"feed"];
                if (feed) {
                    NSString *feedURL = [feed objectForKey:@"feedUrl"];
                    NSRange range =[feedURL rangeOfString:@".com"];
                    if (range.location != NSNotFound) {
                        feedURL = [feedURL substringToIndex:range.location+4];
                    }
                    
                    NSArray *entries = [feed objectForKey:@"entries"];
                    NSMutableDictionary *newEntry;
                    for (NSDictionary *entry in entries)
                    {
                        newEntry = [NSMutableDictionary dictionaryWithDictionary:entry];
                        [newEntry setObject:[feedPage objectForKey:@"type"] forKey:@"page"];
                        [newEntry setObject:feedURL forKey:@"feedUrl"];
                        [newsfeed addObject:newEntry];
                    }
                }
            }
        }
    }
    return newsfeed;
}

+(NSArray *)getIndieWireFeeds
{
    NSString *query = [NSString stringWithFormat:@"%@/GetMyCinelifeRss", apiServer];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    
    NSArray *indieFeeds = nil;
    if ([[result objectForKey:@"status"]  isEqual: @"200"]) {
        indieFeeds = [result objectForKey:@"response"];
    }
    return indieFeeds;
}

+(NSDictionary *)getTicketingExample
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"ticketExample"
                                                         ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    id json = [NSJSONSerialization JSONObjectWithData:data
                                              options:kNilOptions
                                                error:&error];
    return json;
}

+(NSDictionary *)getCineLifeTrivia
{
    NSString *query = [NSString stringWithFormat:@"%@/GetMyCinelifeTrivia", apiServer];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    
    NSDictionary *triviaSections = nil;
    if ([[result objectForKey:@"status"]  isEqual: @"200"]) {
        triviaSections = [result objectForKey:@"response"];
    }
    return triviaSections;
    
}

+(PKPass*)getPassbookData:(NSDictionary *)passbookInformation
{

    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:passbookInformation
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    //NSData *postString = [postData data]
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/GetPassbookCode", apiServer]]];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    
    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        //success
        PKPass *pass = [[PKPass alloc] initWithData:data error:&error];
        
        return pass;
    }
    return nil;
}

+(void)getBaseURLsForTrailerAndPoster
{
    NSString *query = [NSString stringWithFormat:@"%@/GetBaseUrlForTrailer", apiServer];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    if (result) {
        [SPNServerQueries setPosterURL:[result objectForKey:@"poster"]];
        [SPNServerQueries setTrailerURL:[result objectForKey:@"trailer"]];
    }
    else {
        [SPNServerQueries setPosterURL:@"http://www.movienewsletters.net/photos/%MOVIEID%.jpg"];
        [SPNServerQueries setTrailerURL:@"http://media.westworldmedia.com/USSPO/mp4/%MOVIEID%_high.mp4"];
    }
}

+(NSArray *)getFilmsFromPartners
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *stringDate = [dateFormatter stringFromDate:date];
    NSString *query = [NSString stringWithFormat:@"%@/MoviesWithShowingsFromPartners?startDate=%@", apiServer, stringDate];
    NSURL *queryURL = [NSURL URLWithString:query];
    id result  = [self queryURL:queryURL];
    if (result) {
        NSArray *films = [self sortFilmsByName:result];
        return films;
    }
    return nil;
}

+(NSArray *)getCineLifePartners
{
    NSString *query = [NSString stringWithFormat:@"%@/GetPartnerList", apiServer];
    NSURL *queryURL = [NSURL URLWithString:query];
    id result  = [self queryURL:queryURL];
    return result;
}
@end
