//
//  SPNEmailInvitesViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNEmailInvitesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>

@property NSArray *possibleContacts;

-(IBAction)sendMail:(id)sender;

@end
