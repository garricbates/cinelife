//
//  SPNAccountSettingsTableViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAccountSettingsTableViewController.h"
#import "SPNAccountLoginCell.h"
#import "SPNAppDelegate.h"
#import "SPNTheatreSearchTableViewController.h"
#import "SPNTutorialManagerViewController.h"
#import "SPNTermsAndPrivacyViewController.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <TwitterKit/TwitterKit.h>
#import <TwitterCore/TwitterCore.h>

@interface SPNAccountSettingsTableViewController ()
{
    NSArray *signInOptions, *otherAccountOptions;
    ACAccount *selectedTwitterAccount;
    NSString *fbName, *gpName, *twName;
    NSMutableDictionary *gpTokens;
    NSString *badge;
}
@end

@implementation SPNAccountSettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[SPNCinelifeQueries sharedInstance] setDelegate:nil];
    [[GIDSignIn sharedInstance] setDelegate:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[GIDSignIn sharedInstance] setDelegate:self];
    [[GIDSignIn sharedInstance] setUiDelegate:self];
    
    NSString *title = @"Sign up!";
    if ([SPNUser userLoggedIn]) {
        title = @"Log out";
    }
    [self.logOutButton setTitle:title
                       forState:UIControlStateNormal];
    [self.tableView reloadData];
    [self.navigationController.tabBarItem setBadgeValue:nil];
    
    [[SPNCinelifeQueries sharedInstance] getNotificationsBadgeWithCompletion:^(NSString *b) {
        badge = b;
        [self.tableView reloadData];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    gpTokens = [NSMutableDictionary new];
    signInOptions = [NSArray arrayWithObjects:@"Location", @"Signed in as", nil];
    otherAccountOptions = [NSArray arrayWithObjects:@"Notifications", @"My purchases", @"Help & Hints", @"Invite friends", @"About/Contact us", @"Terms & Conditions", @"Privacy", nil];
    [self customizeUI];
    
    [self.logOutButton setTitle:@"Sign up!"
                       forState:UIControlStateNormal];
    
    [self.logOutButton addTarget:self
                          action:@selector(signInPressed:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self initializeLogins];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI customization
-(void)customizeUI
{
    [self.logOutButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if ([SPNUser userLoggedIn]) {
                return [signInOptions count];
            }
            else {
                return 2;
            }
            break;
        default:
            return [otherAccountOptions count];
            break;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* AccountLoginCellIdentifier = @"AccountLoginCell";
    static NSString* SignInOptionsCellIdentifier = @"OptionsCell";
    
    NSArray *accountOptions = otherAccountOptions;
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 1 && ![SPNUser userLoggedIn]) {
                SPNAccountLoginCell *accountCell = [tableView dequeueReusableCellWithIdentifier:AccountLoginCellIdentifier];
                [accountCell initializeCell];
                // Configure the cell...
                
                [accountCell.forgotPasswordButton addTarget:self
                                                     action:@selector(restorePasswordPressed:)
                                           forControlEvents:UIControlEventTouchUpInside];
                
                [accountCell.spotlightButton addTarget:self
                                                action:@selector(loginWithSpotlightPressed:)
                                      forControlEvents:UIControlEventTouchUpInside];

                [accountCell.twitterButton addTarget:self
                                              action:@selector(loginTwitterButtonPressed:)
                                    forControlEvents:UIControlEventTouchUpInside];
                
                [accountCell.facebookButton addTarget:self
                                               action:@selector(loginFacebookButtonPressed:)
                                     forControlEvents:UIControlEventTouchUpInside];
                
                [accountCell.googlePlusButton addTarget:self
                                                 action:@selector(loginGooglePlusButtonPressed:)
                                       forControlEvents:UIControlEventTouchUpInside];
                return accountCell;
            }
            else {
                accountOptions = signInOptions;
            }
            
        default:
            {
                UITableViewCell *optionsCell = [tableView dequeueReusableCellWithIdentifier:SignInOptionsCellIdentifier];
                [optionsCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [optionsCell.textLabel setText:[accountOptions objectAtIndex:indexPath.row]];
                
                
                if (indexPath.section > 0 &&  indexPath.row == 1) {
                    NSArray *purchases = [[LCPDatabaseHelper sharedInstance] fetchAllRecordsForEntity:@"Purchases"];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    if ([prefs objectForKey:@"newPurchase"]) {
                        [optionsCell.textLabel setFont:[UIFont spn_NeutraBoldMedium]];
                    }
                    else {
                        [optionsCell.textLabel setFont:[UIFont spn_NeutraMedium]];
                    }
                    [optionsCell.detailTextLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[purchases count]]];
                    return optionsCell;
                }
                else {
                    [optionsCell.textLabel setFont:[UIFont spn_NeutraMedium]];
                    [optionsCell.detailTextLabel setFont:[UIFont spn_NeutraMedium]];
                    [optionsCell.detailTextLabel setText:@""];
                   
                    if (indexPath.row < 1 && indexPath.section < 1) {
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        NSString* storedZipcode = [prefs objectForKey:@"zip"];
                        if (storedZipcode) {
                            [optionsCell.detailTextLabel setText:storedZipcode];
                        }
                    }
                    else if (indexPath.row == 1 && indexPath.section < 1) {
                        [optionsCell.detailTextLabel setText:[SPNUser getCurrentUserFullName]];
                    } else if (indexPath.section == 1 && indexPath.row == 0) {
                        optionsCell.detailTextLabel.text = [badge integerValue] > 0 ? badge : nil;
                    }
                    

                    return optionsCell;
                }
            }
            break;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 1 && ![SPNUser userLoggedIn]) {
                return kAccountLoginCellHeight;
            }
            else {
                return kAccountOptionsCellHeight;
            }
            break;
        default:
            return kAccountOptionsCellHeight;
            break;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            if (indexPath.row < 1) {
                [self performSegueWithIdentifier:@"EditInformation" sender:self];
            }
            break;
            
        default:
            if (indexPath.row < 1) {
                [self performSegueWithIdentifier:@"NotificationSegue" sender:self];
            }
            else if (indexPath.row == 1) { // My purchases
                [self performSegueWithIdentifier:@"MyPurchases" sender:self];
            }
            else if (indexPath.row == 2) {
                [self initializeTutorial];
            }
            else if (indexPath.row == 3) {
                [self performSegueWithIdentifier:@"InviteFriends" sender:self];
            }
            else if (indexPath.row == 4) {
                [self performSegueWithIdentifier:@"AboutUs" sender:self];
            }
            else if (indexPath.row == 5) {
                [self performSegueWithIdentifier:@"TermsAndConditions" sender:self];
            }
            else if (indexPath.row == 6) {
                 [self performSegueWithIdentifier:@"Privacy" sender:self];
            }
            break;
    }

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"TermsAndConditions"]) {
        SPNTermsAndPrivacyViewController *termsController = [segue destinationViewController];
        [termsController setIsTOS:YES];
    }
}
#pragma mark Login initialization
-(void)initializeLogins
{
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    
}

#pragma mark User related functions
-(void)restorePasswordPressed:(id)sender
{
    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserForgotPassword
                          withDelegate:self
                          forAlertType:UIAlertViewStylePlainTextInput
                       withCancelTitle:@"Cancel"
                       withOtherTitles:@"Recover"];
}

-(void)signInPressed:(id)sender
{
    UIButton *signInButton = (UIButton*)sender;
    if ([signInButton.titleLabel.text isEqualToString:@"Sign up!"]) {
        [self signUp];
    }
    else {
        if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
            [[GIDSignIn sharedInstance] disconnect];
        }
        [SPNUser logOutCurrentUser];
        FBSDKLoginManager *loginManager =[[FBSDKLoginManager alloc] init];
        [loginManager logOut];

        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        NSString *userID = store.session.userID;
        
        [store logOutUserID:userID];

        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
                      withRowAnimation:UITableViewRowAnimationMiddle];
        
        [self.logOutButton setTitle:@"Sign up!"
                           forState:UIControlStateNormal];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserLogout"
                                                            object:nil];
    }
}

-(void)loginWithSpotlightPressed:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] disconnect];
    }
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    
    [store logOutUserID:userID];

    [SPNUser logOutCurrentUser];

    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserLoginAttempt
                          withDelegate:self
                          forAlertType:UIAlertViewStyleLoginAndPasswordInput
                       withCancelTitle:@"Cancel"
                       withOtherTitles:@"Log in"];
}

-(BOOL)attemptLoginWithUsername:(NSString*)username andPassword:(NSString*)password
{
    BOOL didLogin = NO;
    return didLogin;
}

-(BOOL)attemptSignUpUsername:(NSString*)username withEmail:(NSString*)email andPassword:(NSString*)password
{
    BOOL didSignUp = NO;
    return didSignUp;
}

-(void)signUp
{
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}

-(void)sendUserToTheatreFavourites {
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] getFavoritesTheatersAndFestivalsWithCompletion:^(NSArray *theaters, NSArray *festivals, NSError *error) {
        [self favoritesLoadedWithTheaters:theaters festivals:festivals];
    }];
}

#pragma mark Twitter login functions
-(void)loginTwitterButtonPressed:(id)sender
{
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] disconnect];
    }
    [SPNUser logOutCurrentUser];
    //[FBSession.activeSession closeAndClearTokenInformation];

    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [self loginTwitter];
}

-(void)loginTwitter
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[Twitter sharedInstance] logInWithCompletion:^
     (TWTRSession *session, NSError *error) {
         if (session) {
             twName = [session userName];
             NSDictionary *credentials = @{@"twitter_token" : [session authToken], @"twitter_token_secret" :[session authTokenSecret]};
             [[SPNCinelifeQueries sharedInstance] postSignIn:credentials];
         } else {
             [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
             NSLog(@"error: %@", [error localizedDescription]);
             
         }
     }];
}

#pragma mark Facebook login functions
-(void)loginFacebookButtonPressed:(id)sender
{
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] disconnect];
    }
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    [store logOutUserID:userID];
    
    [SPNUser logOutCurrentUser];
    [self loginFacebookAccounts];
}

-(void)onProfileUpdated:(NSNotification*)notification
{
    if ([FBSDKAccessToken currentAccessToken]) {
        fbName = [[FBSDKProfile currentProfile] name];
        NSDictionary *fbDictionary = @{@"facebook_token" : [FBSDKAccessToken currentAccessToken].tokenString};
        [[SPNCinelifeQueries sharedInstance] postSignIn:fbDictionary];
    }
}


-(void)loginFacebookAccounts
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logOut];

    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error connecting to Facebook" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            
        } else if (result.isCancelled) {
            // Handle cancellations
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"public_profile"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {

                    if (![FBSDKProfile currentProfile]) {
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector(onProfileUpdated:)
                                                                     name:FBSDKProfileDidChangeNotification object:nil];
                    }
                    else {
                        fbName = [[FBSDKProfile currentProfile] name];
                        NSDictionary *fbDictionary = @{@"facebook_token" : [FBSDKAccessToken currentAccessToken].tokenString};
                        [[SPNCinelifeQueries sharedInstance] postSignIn:fbDictionary];
                    }
                }
            }
        }
    }];
}

#pragma mark Google+ login functions
-(void)loginGooglePlusButtonPressed:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [SPNUser logOutCurrentUser];
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    [store logOutUserID:userID];
    [self loginGooglePlus];
}

-(void)loginGooglePlus
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];

    [[GIDSignIn sharedInstance] signOut];
  
    [[GIDSignIn sharedInstance] signIn];
    
}

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (!error) {
        NSString *homeAuthCode = user.serverAuthCode;
        [gpTokens removeAllObjects];
        [gpTokens setObject:homeAuthCode
                     forKey:@"google_code"];

        
        gpName = user.profile.name;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                             animated:YES];
        
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        [[SPNCinelifeQueries sharedInstance] postSignIn:gpTokens];
        
    }
    else {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserGooglePlusBadUser];
        
    }

}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}


-(void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    }];
}


#pragma mark Alert view functions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Recover"]) {
        
        UITextField *email = [alertView textFieldAtIndex:0];
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        [[SPNCinelifeQueries sharedInstance] postResetPassword:email.text];
        
        /*
        BOOL succeed = [SPNUser recoverPasswordForUserEmail:email.text];
        
        if (succeed) {
            [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserPasswordSent];
        }
        else {
            [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserBadPasswordRecover];
        }
         */
    }
    else if ([title isEqualToString:@"Log in"]) {
        UITextField *emailTextField = [alertView textFieldAtIndex:0];
        UITextField *passwordTextField = [alertView textFieldAtIndex:1];
        NSString *email = emailTextField.text;
        NSString *password = passwordTextField.text;
        
        NSDictionary *credentials = @{@"email" : email, @"password" : password};
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [[SPNCinelifeQueries sharedInstance] postSignIn:credentials];
        /*
        [self attemptLoginWithUsername:email
                           andPassword:password];
         */
        
    }
    else if ([title isEqualToString:@"Sync"]) {
        UITextField *emailTextField = [alertView textFieldAtIndex:0];
        NSString *email = emailTextField.text;
        
        BOOL didSucceed = [self attemptSignUpUsername:selectedTwitterAccount.username
                                            withEmail:email
                                          andPassword:nil];
        if (didSucceed) {
            [SPNUser setTwitterEmail:email
                             forUser:selectedTwitterAccount.username];
        }
    }
    else {
        [self.view endEditing:YES];
    }

}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if  (alertView.alertViewStyle == UIAlertViewStyleLoginAndPasswordInput) {
        
        NSString *usernameText = [[alertView textFieldAtIndex:0] text];
        NSString *passwordText = [[alertView textFieldAtIndex:1] text];
        
        if ([usernameText length] < 1 || [passwordText length] < 1) {
            return NO;
        }
    }
    return YES;
}

-(void)initializeTutorial
{
    UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"TutorialStoryboard"
                                                                 bundle:nil];
    
    SPNTutorialManagerViewController *spnTutorial = [tutorialStoryboard instantiateInitialViewController];
    [self.tabBarController addChildViewController:spnTutorial];
    spnTutorial.view.frame = [self.tabBarController.view bounds];
    [self.tabBarController.view addSubview:spnTutorial.view];
    [spnTutorial didMoveToParentViewController:self];
    
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery userLoggedIn:(NSDictionary *)user withCredentials:(NSDictionary *)credentials
{
  
    NSMutableDictionary *newUser = [NSMutableDictionary new];
    
    if ([credentials objectForKey:@"email"]) {
        [newUser setObject:[credentials objectForKey:@"email"]
                    forKey:@"username"];
    }
    else if ([credentials objectForKey:@"google_code"]) {
        [newUser setObject:gpName
                    forKey:@"username"];
    }
    else if ([credentials objectForKey:@"facebook_token"]) {
        [newUser setObject:fbName
                    forKey:@"username"];
    }
    else if ([credentials objectForKey:@"twitter_token"]) {
        [newUser setObject:twName
                    forKey:@"username"];
    }
    [newUser setObject:[user objectForKey:@"access_token"]
                forKey:@"access_token"];
    
    [newUser setObject:[user objectForKey:@"id"]
                forKey:@"id"];
    
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    [SPNUser setCurrentUser:newUser];
    
    [SPNUser welcomeUser:[newUser objectForKey:@"username"]
               isNewUser:NO];
    
    [self.tableView beginUpdates];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
                  withRowAnimation:UITableViewRowAnimationMiddle ];
    [self.tableView endUpdates];
    [self.logOutButton setTitle:@"Log out"
                       forState:UIControlStateNormal];
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                         |UIUserNotificationTypeSound
                                                                                         |UIUserNotificationTypeAlert) categories:nil];
    // We update the push notification token to include the user ID
    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];

    }

    [self performSelector:@selector(sendUserToTheatreFavourites)
               withObject:nil
               afterDelay:0.5];
}

- (void)favoritesLoadedWithTheaters:(NSArray *)favoriteTheaters festivals:(NSArray *)favoriteFestivals {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
        UINavigationController *navController = [[self.tabBarController childViewControllers] objectAtIndex:0];
        SPNTheatreSearchTableViewController *spnTheatreSearch = [[navController childViewControllers] objectAtIndex:0];
        self.tabBarController.selectedIndex = 0;
    if ([favoriteTheaters count] > 0) {
        [spnTheatreSearch.theaterSegmentControl setSelectedSegmentIndex:1];
        spnTheatreSearch.myTheaters = [SPNCinelifeQueries sortTheatersByDistance:favoriteTheaters
                                                                    withLocation:[appDelegate myLocation]
                                                               withPartnersFirst:YES];
        spnTheatreSearch.myFestivals = [favoriteFestivals mutableCopy];
        
        spnTheatreSearch.selectedTheaterType = SPNTheatreTypeFavourites;
        [spnTheatreSearch.tableView reloadData];
    }
    else {
        [spnTheatreSearch.theaterSegmentControl setSelectedSegmentIndex:0];
         spnTheatreSearch.selectedTheaterType = SPNTheatreTypeInRegion;
    }

}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery passwordDidReset:(BOOL)didReset
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if (didReset) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserPasswordSent];
    }
    else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserBadPasswordRecover];
    }
}
@end
