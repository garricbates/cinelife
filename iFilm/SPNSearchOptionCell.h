//
//  SPNSearchOptionCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNSearchOptionCell : UITableViewCell

@property () IBOutlet UILabel *dateLabel;
@property () IBOutlet UILabel *whereLabel;

@property (strong, nonatomic) IBOutlet UITextField *datePicker;

@property (weak, nonatomic) IBOutlet UITextField *zipcodePicker;

@end
