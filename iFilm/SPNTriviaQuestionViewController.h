//
//  SPNTriviaQuestionViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SPNTriviaQuestionViewController : GAITrackedViewController

@property NSDictionary *triviaQuestion;
@property NSMutableArray *triviaQuestions;
@property NSString *answer;
@property BOOL isLandingTrivia;


@property IBOutlet UILabel *typeOfTriviaLabel;
@property IBOutlet UILabel *triviaTextLabel;
@property IBOutlet UILabel *triviaOptionsLabel;
@property IBOutlet UIButton *nextTriviaButton;

-(IBAction)quitTrivia:(id)sender;

@end
