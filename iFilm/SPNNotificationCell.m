//
//  SPNNotificationCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//

#import "SPNNotificationCell.h"

@implementation SPNNotificationCell

- (IBAction)actionClosePress:(id)sender {
    [self.delegate didPressClose:self];
}

- (IBAction)actionDeletePress:(id)sender {
    [self.delegate didPressDelete:self];
}
@end
