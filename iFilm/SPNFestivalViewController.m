//
//  SPNFestivalViewController.m
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFestivalViewController.h"
#import "Festival.h"
#import "SPNPartnerButton.h"
#import <MessageUI/MessageUI.h>
#import <SafariServices/SafariServices.h>
#import "WebViewController.h"
#import "SPNPosterViewController.h"
#import "SPNIndieHeaderCell.h"
#import "SPNTheatreNewsfeedCell.h"
#import "Feeds.h"
#import "SPNTheatreNewsViewController.h"

static NSString *ContactCellIdentifier = @"ContactCell";
static NSString *NewsCellIdentifier = @"TheatreNewsCell";

@interface SPNFestivalViewController () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, SPNCinelifeQueriesDelegate> {
    Feeds *feeds;
    NSInteger shouldFetchRSSToo;
    NSArray *_posts;
    NSAttributedString *info;
    
    UIColor *backgroundColor, *infoBackgroundColor, *infoTextColor, *sectionTitleBackgroundColor, *sectionTitleTextColor;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet AsyncImageView *posterView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet SPNPartnerButton *partnerBtn;
@property (nonatomic, weak) IBOutlet UIView *infoBackground;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;

@property (nonatomic, strong) NSArray <NSString *> *contacts;

@end

@implementation SPNFestivalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getBrandingColors];
    
    [self.tableView registerCellClass:[SPNIndieHeaderCell class]
                                       withIdentifier:@"HeaderCell"];
    [self.tableView registerCellClass:[SPNTheatreNewsfeedCell class]
                                       withIdentifier:NewsCellIdentifier];
    
    self.titleLabel.font = self.dateLabel.font = [UIFont spn_NeutraBoldLarge];
    self.addressLabel.font = [UIFont spn_NeutraMedium];
    self.venueNameLabel.font = [UIFont spn_NeutraMedium];
    self.categoriesLabel.font = [UIFont spn_NeutraMedium];
    self.startTimeLabel.font = [UIFont spn_NeutraMedium];
    
    self.partnerBtn.visible = self.festival.isPartner.boolValue;
    
    self.posterView.image = [UIImage imageNamed:@"special-event-poster"];
    [self.posterView setImageURL:[NSURL URLWithString:self.festival.photoUrl]];
    
    self.titleLabel.text = self.festival.summary;
    self.addressLabel.text = self.festival.venueAddress;
    self.venueNameLabel.text = self.festival.venueName;
    self.categoriesLabel.text = [NSString stringWithFormat:@"%@ %@", @"Categories: ", self.festival.categories];
    
    self.startTimeLabel.text = [NSString stringWithFormat:@"Start time: %@", self.festival.startTime];
    self.dateLabel.text = [self.festival formattedPeriod];
    
    info = [[NSAttributedString alloc] initWithData:[self.festival.info dataUsingEncoding:NSUTF8StringEncoding]
                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                      NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                 documentAttributes:nil error:nil];
    
    UIView *headerView = self.tableView.tableHeaderView;
    [headerView layoutIfNeeded];
    CGRect frame = headerView.frame;
    frame.size.height = ceil(CGRectGetMaxY(self.addressLabel.frame)) + 50;
    headerView.frame = frame;
    self.tableView.tableHeaderView = headerView;
    
    NSMutableArray *contacts = [NSMutableArray new];
    if (self.festival.contact.length > 0) {
        [contacts addObject:self.festival.contact];
    }
    if (self.festival.website.length > 0) {
        [contacts addObject:self.festival.website];
    }
    if (self.festival.websiteMain.length > 0 && ![self.festival.websiteMain isEqual:self.festival.website]) {
        [contacts addObject:self.festival.websiteMain];
    }
    if (self.festival.ticketsUrl.length > 0) {
        [contacts addObject:self.festival.ticketsUrl];
    }
    self.contacts = contacts.copy;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getCineLifePostsForFestival:self.festival.festivalId.stringValue];
}

- (void)getBrandingColors {
    NSDictionary *branding = _festival.branding;
    if (branding) {
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            backgroundColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            self.view.backgroundColor = backgroundColor;
        }
        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionTitleBackgroundColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            sectionTitleTextColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
        }
        if ([[branding objectForKey:@"info_text"] length] > 0) {
            infoTextColor = [UIColor colorFromHexString:[branding objectForKey:@"info_text"]];
            _titleLabel.textColor = _addressLabel.textColor = _dateLabel.textColor = _venueNameLabel.textColor = _startTimeLabel.textColor = _categoriesLabel.textColor = infoTextColor;
        }
        if ([[branding objectForKey:@"info_background"] length] > 0) {
            infoBackgroundColor = [UIColor colorFromHexString:[branding objectForKey:@"info_background"]];
            _tableView.tableHeaderView.backgroundColor = infoBackgroundColor;
            _infoBackground.backgroundColor = infoBackgroundColor;
        }
    }
}

- (void)createFeedsWithPosts:(NSArray *)posts {
    dispatch_async(kBgQueue, ^{
        feeds = [[Feeds alloc] initWithFeeds:self.festival.feeds posts:posts];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

- (void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery festivalPosts:(NSArray *)posts {
    _posts = posts;
    [self createFeedsWithPosts:posts];
}

- (void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error {
    [self createFeedsWithPosts:nil];
}

- (IBAction)posterAction {
    [SPNPosterViewController presentWithURL:_festival.photoUrl];
}

- (void)seeAll:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (sender) {
        shouldFetchRSSToo = 2;
    }
    if (btn.tag == 1 || btn == nil) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [feeds storiesWithPosts:_posts
              shouldFetchRSSToo:shouldFetchRSSToo
                     completion:^(NSArray *list) {
                         [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                         [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@NO,
                                                                               @"posts":list}];
                     }];
    } else {
        [feeds postsForSocialsWithCompletion:^(NSArray *posts) {
            [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@YES,
                                                                  @"posts":posts}];
        }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *identifier = segue.identifier;
    if ([identifier isEqual:@"blogNews"]) {
        SPNTheatreNewsViewController *controller = [segue destinationViewController];
        controller.theatreNews = sender[@"posts"];
        controller.brandColors = _festival.branding;
        controller.isRSS = [sender[@"rss"] boolValue];
    }
    if ([segue.destinationViewController.navigationItem.title isEqual:@"Theatre Details"]) {
        segue.destinationViewController.navigationItem.title = self.navigationItem.title;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return info ? 1 : 0;
            
        case 1:
            return self.contacts.count;
            
        case 2:
            return feeds.news.count >= NewsLimit ? NewsLimit : feeds.news.count;
            
        case 3:
            return feeds.socials.count >= NewsLimit ? NewsLimit : feeds.socials.count;
            
        default:
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section > 1 && [self tableView:tableView numberOfRowsInSection:section] > 0) ? 30 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *HeaderIdentifier = @"HeaderCell";
    SPNIndieHeaderCell *header = [tableView dequeueReusableCellWithIdentifier:HeaderIdentifier];
    
    switch (section) {
        case 2:
            [header.titleLabel setText:@"News and Promotions"];
            break;
            
        default:
            [header.titleLabel setText:@"Social News"];
            break;
    }
    
    [header.seeAllButton setTag:section];
    [header.seeAllButton addTarget:self
                            action:@selector(seeAll:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    [header setFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    [header setTag:300+section];
    UIView *view = [[UIView alloc] initWithFrame:header.frame];
    [view addSubview:header];
    [view setClipsToBounds:YES];
    
    [header.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [header.seeAllButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    if (sectionTitleBackgroundColor) {
        header.contentView.backgroundColor = sectionTitleBackgroundColor;
    }
    if (sectionTitleTextColor) {
        [header.titleLabel setTextColor:sectionTitleTextColor];
        [header.seeAllButton setTitleColor:sectionTitleTextColor
                                  forState:UIControlStateNormal];
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return section == 1 ? 10 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.backgroundColor = infoBackgroundColor ? infoBackgroundColor : [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            CGFloat width = CGRectGetWidth(tableView.frame) - 48;
            CGFloat height = [info sizeWithMaxSize:CGSizeMake(width, CGFLOAT_MAX)].height;
            return ceil(height) + 8;
        }
            
        case 1: {
            CGFloat width = CGRectGetWidth(tableView.frame) - 48;
            CGFloat height = [_contacts[indexPath.row] findHeightForWidth:width
                                                                      andFont:[UIFont spn_NeutraBoldLarge]];
            return ceil(height) + 8;
        }
            
        case 2: {
            return [SPNTheatreNewsfeedCell heightForFeed:NO
                                                    feed:feeds.news[indexPath.row]
                                             htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
        }
            
        case 3: {
            return [SPNTheatreNewsfeedCell heightForFeed:YES
                                                    feed:feeds.socials[indexPath.row]
                                             htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
        }
            
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        case 1: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ContactCellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ContactCellIdentifier];
                cell.backgroundColor = [UIColor clearColor];
                cell.contentView.backgroundColor = [UIColor clearColor];
                cell.textLabel.font = [UIFont spn_NeutraBoldLarge];
                cell.textLabel.numberOfLines = 0;
                if (infoBackgroundColor) {
                    cell.backgroundColor = infoBackgroundColor;
                }
                if (infoTextColor) {
                    cell.textLabel.textColor = infoTextColor;
                }
            }
            cell.selectionStyle = indexPath.section == 0 ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault;
            if (indexPath.section == 0) {
                cell.textLabel.attributedText = info;
            } else {
                cell.textLabel.text = self.contacts[indexPath.row];
            }
            return cell;
        }
            
        case 2: {
            SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
            [cell configureForFeed:NO
                              feed:feeds.news[indexPath.row]
                       htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            return cell;
        }
            
        case 3: {
            SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
            [cell configureForFeed:YES
                              feed:feeds.socials[indexPath.row]
                       htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            return cell;
        }
            
        default:
            return [UITableViewCell new];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 1: {
            NSString *contact = _contacts[indexPath.row];
            if ([contact containsString:@"@"]) {
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                controller.mailComposeDelegate = self;
                [controller setToRecipients:@[contact]];
                [controller setSubject:@"Cinelife"];
                [self presentViewController:controller animated:YES completion:nil];
            } else if ([contact containsString:@"http"] || [contact containsString:@"www"] || ![contact isEqual:self.festival.contact]) {
                NSString *urlString;
                if ([contact containsString:@"http"]) {
                    urlString = contact;
                } else {
                    urlString = [@"http://" stringByAppendingString:contact];
                }
                if ([SFSafariViewController class]) {
                    SFSafariViewController *controller = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:urlString]];
                    [self presentViewController:controller animated:YES completion:nil];
                } else {
                    WebViewController *controller = [WebViewController new];
                    controller.urlString = urlString;
                    [self.navigationController pushViewController:controller animated:YES];
                }
            } else {
                NSURL *url = [[NSURL alloc] initWithString:[@"tel://" stringByAppendingString:contact]];
                [[UIApplication sharedApplication] openURL:url];
            }
            break;
        }
            
        case 2: {
            NSDictionary *entry = feeds.news[indexPath.row];
            if ([entry objectForKey:@"summary"]) {
                shouldFetchRSSToo = 0;
            }
            else {
                shouldFetchRSSToo = 1;
            }
            [self seeAll:nil];
            break;
        }
            
        case 3: {
            [feeds postsForItem:feeds.socials[indexPath.row] completion:^(NSArray *posts) {
                [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@YES,
                                                                      @"posts":posts}];
            }];
            break;
        }
            
        default:
            break;
    }
}


#pragma mark - Mail

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
