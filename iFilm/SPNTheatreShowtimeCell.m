//
//  SPNTheatreShowtimeCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreShowtimeCell.h"
#import "ShadowButton.h"

@implementation SPNTheatreShowtimeCell

-(void)initializeTheatreCellContents:(NSDictionary*)theatre forIndexPath:(NSIndexPath*)indexPath
{
    CGFloat distance = [[theatre objectForKey:@"distance"] floatValue];
    if (distance > 50.0) {
        self.distanceLabel.text = [NSString stringWithFormat:@"50+ miles"];
    }
    else {
        self.distanceLabel.text = [NSString stringWithFormat:@"%.1f miles",distance];
    }
    
    self.theatreNameLabel.text = [NSString stringWithFormat:@"%@", [theatre objectForKey:@"name"]];
    
    [self.theatreNameLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.distanceLabel setFont:[UIFont spn_NeutraSmallMedium]];
}

-(NSArray*)getShowsForThisTheatre:(NSIndexPath *)indexPath forSchedule:(NSDictionary *)movieShowtimes forDate:(NSString *)dateString
{
    
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *selectedDate = [showtimeDateFormatter dateFromString:dateString];
    BOOL isFutureDate = NO;
    if ([[selectedDate laterDate:[NSDate date]] isEqualToDate:selectedDate]) {
        isFutureDate = YES;
    }
    
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [showtimeDateFormatter setLocale:usLocale];
    [showtimeDateFormatter setDefaultDate:[NSDate date]];
    
    NSMutableArray *allShows = [NSMutableArray new];
    
    id schedule = [movieShowtimes objectForKey:@"showtime"];
    if ([schedule isKindOfClass:[NSArray class]])
    {
        for (id showtime in schedule)
        {
            if ([showtime isKindOfClass:[NSArray class]]) {
                NSDate *date = [NSDate date];
                for (NSString *show in showtime)
                {
                   
                    [showtimeDateFormatter setDateFormat:@"HH:mm"];
                    date = [showtimeDateFormatter dateFromString:show];
                    if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
                        [showtimeDateFormatter setDateFormat:@"h:mma"];
                        NSString* showDate = [showtimeDateFormatter stringFromDate:date];
                        [allShows addObject:showDate];
                    }
                }
                
            }
            else if ([showtime isKindOfClass:[NSString class]]) {
                [showtimeDateFormatter setDateFormat:@"HH:mm"];
                NSDate *date = [showtimeDateFormatter dateFromString:showtime];
                if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
                    [showtimeDateFormatter setDateFormat:@"h:mma"];
                    NSString* showDate = [showtimeDateFormatter stringFromDate:date];
                    [allShows addObject:showDate];
                }
            }
        }
    }
    else if ([schedule isKindOfClass:[NSString class]]) {
        [showtimeDateFormatter setDateFormat:@"HH:mm"];
        NSDate *date = [showtimeDateFormatter dateFromString:schedule];
        if (isFutureDate || [[date laterDate:[NSDate date]] isEqualToDate:date]) {
            [showtimeDateFormatter setDateFormat:@"h:mma"];
            NSString* showDate = [showtimeDateFormatter stringFromDate:date];
            
            [allShows addObject:showDate];
        }
    }
    
    NSArray *finalShows = [[NSSet setWithArray:allShows] allObjects];
    
    NSArray * sortedDatesArray = [finalShows sortedArrayUsingComparator: ^(id a, id b) {
        NSDate *d1 = [showtimeDateFormatter dateFromString:a];
        NSDate *d2 = [showtimeDateFormatter dateFromString:b];
        return [d1 compare: d2];
    }];
    
    return sortedDatesArray;
}

-(void)setShowForCell:(NSArray*)shows
{
    [[self.showtimesContainerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat maxHeight = 20;
    CGSize size = [UIScreen mainScreen].bounds.size;
    if ([shows count] < 1) {
        UILabel *showLabel = [UILabel new];
        [showLabel setFrame:CGRectMake(0, 0,size.width-40,25)];
        [showLabel setNumberOfLines:2];
        [showLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [showLabel setText:@"No more showtimes for today"];
    
        [showLabel.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [showLabel.layer setBorderWidth:1.0];
        [showLabel setFont:[UIFont spn_NeutraBoldSmall]];
        [showLabel setTextColor:[UIColor darkGrayColor]];
        [showLabel setTextAlignment:NSTextAlignmentCenter];
        
        maxHeight = 40;
        [self.showtimesContainerView addSubview:showLabel];
    }
    
    for (int i = 0; i < [shows count]; i ++)
    {
        int y = (i / 5);

        ShadowButton *showBtn = [ShadowButton buttonWithType:UIButtonTypeCustom];
        [showBtn setFrame:CGRectMake((i % 5)*60, y*25, 55,20)];
        [showBtn setTitle:[shows objectAtIndex:i] forState:UIControlStateNormal];
        [showBtn setBackgroundColor:[UIColor spn_buttonColor]];
        
        showBtn.layer.cornerRadius = 2.5;
        [showBtn.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
        [showBtn.titleLabel setTextColor:[UIColor whiteColor]];
        [showBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        
        [showBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [self.showtimesContainerView addSubview:showBtn];
        [showBtn setTag:i+100];
        showBtn.layer.shadowColor = [UIColor spn_buttonShadowColor].CGColor;
        maxHeight = MAX(maxHeight,y*25+20);
        
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@end
