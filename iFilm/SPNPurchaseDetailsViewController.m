//
//  SPNPurchaseDetailsViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPurchaseDetailsViewController.h"
#import "ZXMultiFormatWriter.h"
#import "ZXImage.h"


@interface SPNPurchaseDetailsViewController ()

@end

@implementation SPNPurchaseDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Purchase Details";
    
    // Do any additional setup after loading the view.
    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.showDateLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.showTimeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.ticketsLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.transactionCodeLabel setFont:[UIFont spn_NeutraBoldGiga]];
    [self.codeTitleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [self initializePurchaseData:self.purchaseInformation];
    [self initializeFilmData:self.movieInformation];
    
 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Film initialization
-(void)initializeFilmData:(NSDictionary *)filmInformation
{
    
    NSString *movieTitle =[filmInformation objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *urlImage = [SPNServerQueries getMovieImageURL:[filmInformation objectForKey:@"movieId"]];
    
    [self.miniPosterImageView setImageURL:[NSURL URLWithString:urlImage]];
    [self.miniPosterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    
    NSString *scale = @"01";
    if (IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        scale = @"h1";
    }
    
    urlImage = [urlImage stringByReplacingOccurrencesOfString:scale withString:@"r1"];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];
    if (urlImage) {
        [self.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
        [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    }
    else {
        [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    }
}

-(void)initializePurchaseData:(Purchases*)purchaseInformation
{
    // Date formatting
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEEdMMMM" options:0
                                                              locale:[NSLocale currentLocale]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:formatString];
    NSDate *selectedDate = purchaseInformation.showDate;
    NSString *todayString = [dateFormatter stringFromDate:selectedDate];
    
    todayString = [@"Date: " stringByAppendingString:todayString];
    NSRange boldFontRange = NSMakeRange(0, 5);
    
    NSMutableAttributedString *dateText = [[NSMutableAttributedString alloc] initWithString:todayString];
    
    [dateText beginEditing];
    [dateText addAttribute:NSFontAttributeName
                     value:[UIFont spn_NeutraBoldMediumLarge]
                     range:boldFontRange];
    [dateText endEditing];
    [self.showDateLabel setAttributedText:dateText];
    
    // Time formatting
    
    // Time formatting
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [showtimeDateFormatter setLocale:usLocale];
    [showtimeDateFormatter setDefaultDate:[NSDate date]];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [showtimeDateFormatter dateFromString:purchaseInformation.showTime];
    [showtimeDateFormatter setDateFormat:@"h:mma"];
    NSString* showDate = [showtimeDateFormatter stringFromDate:date];
    
    NSString *showTimeString = [NSString stringWithFormat:@"Showtime: %@",showDate];
    
    boldFontRange = NSMakeRange(0, 9);
    
    NSMutableAttributedString *showTimeText = [[NSMutableAttributedString alloc] initWithString:showTimeString];
    
    [showTimeText beginEditing];
    [showTimeText addAttribute:NSFontAttributeName
                         value:[UIFont spn_NeutraBoldMediumLarge]
                         range:boldFontRange];
    [showTimeText endEditing];
    [self.showTimeLabel setAttributedText:showTimeText];
    
    // Ticket formatting
    NSString *ticketString = [@"Ticket(s):\r\n" stringByAppendingString:purchaseInformation.tickets];
    boldFontRange = NSMakeRange(0, 10);
    
    NSMutableAttributedString *ticketsText = [[NSMutableAttributedString alloc] initWithString:ticketString];
    
    [ticketsText beginEditing];
    [ticketsText addAttribute:NSFontAttributeName
                        value:[UIFont spn_NeutraBoldMediumLarge]
                        range:boldFontRange];
    [ticketsText endEditing];
    [self.ticketsLabel setAttributedText:ticketsText];
    
    NSError *error = nil;
    ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix* result = [writer encode:purchaseInformation.transactionCode
                                  format:kBarcodeFormatPDF417
                                   width:1250
                                  height:500
                                   error:&error];
    if (result) {
        CGImageRef image = [[ZXImage imageWithMatrix:result] cgimage];
        
        [self.qrCodeImageView setImage:[UIImage imageWithCGImage:image]];
        // This CGImageRef image can be placed in a UIImage, NSImage, or written to a file.
    } else {
        NSString *errorMessage = [error localizedDescription];
        NSLog(@"error: %@", errorMessage);
    }
    
    [self.transactionCodeLabel setText:purchaseInformation.transactionCode];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
