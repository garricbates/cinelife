//
//  SPNFilmDataCache.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPNFilmDataCache : NSObject

@property () NSCache *filmDataCache;
@property () NSCache *metaScoreCache;

@property () NSCache *anonymousReviewCache;

+(SPNFilmDataCache*)sharedInstance;
+(NSCache*)defaultCache;
+(NSCache*)defaultMetaScoreCache;
+(NSCache*)defaultReviewCache;
@end
