//
//  SPNFilmPostReviewViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmPostReviewViewController.h"

@interface SPNFilmPostReviewViewController ()
{
    NSUInteger userRating;
    UIImageView *backgroundView;
}
@end

@implementation SPNFilmPostReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Film Review";
    
    self.starRatingControl.delegate = self;
    [self customizeUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI Customization
-(void)customizeUI
{
    [self.commentLabel setFont:[UIFont spn_NeutraBoldLarge]];
    [self.ratingLabel setFont:[UIFont spn_NeutraBoldLarge]];
    
    [self.postReviewButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.cancelButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    [self.postReviewButton.layer setCornerRadius:2.5];
    [self.postReviewButton.layer setShadowColor:[UIColor spn_aquaShadowColor].CGColor];
    [self.cancelButton.layer setCornerRadius:2.5];
    [self.cancelButton.layer setMasksToBounds:YES];
    
    [self.commentTextView setFont:[UIFont spn_NeutraMedium]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self.postReviewButton addTarget:self
                              action:@selector(postReviewButtonPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.cancelButton addTarget:self
                              action:@selector(cancelButtonPressed:)
                    forControlEvents:UIControlEventTouchUpInside];
    
    if (![SPNUser userLoggedIn]) {
        [self.commentTextView setHidden:YES];
        [self.commentLabel setHidden:YES];
        [self.titleTextField setHidden:YES];
        [self.titleLabel setHidden:YES];
        [self.sliceView setHidden:YES];
        [self.notLoggedMessageLabel setHidden:NO];
        [self.postReviewButton setTitle:@"Add Rating"
                               forState:UIControlStateNormal];
    }
    if (self.previousReview) {
        NSLog(@"review; %@", self.previousReview);
        [self initializeReview:self.previousReview];
    }
}

-(void)initializeReview:(NSDictionary*)review
{
    NSString *comment = [review objectForKey:@"content"];
    NSString *title = [review objectForKey:@"title"];
    NSNumber *rating = [review objectForKey:@"rating"];
    
    if (comment) {
        [self.commentTextView setText:comment];
        self.commentTextView.font = [UIFont spn_NeutraMedium];
        self.commentTextView.textColor = [UIColor blackColor];
    }
    if (title) {
        [self.titleTextField setText:title];
    }
    if (rating && ![rating isKindOfClass:[NSNull class]]) {
        [self.starRatingControl
         setRating:[rating integerValue]];
        userRating = [rating integerValue];
    }

}
#pragma mark Custom Keyboard functions
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(BOOL) textViewShouldReturn:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    textView.font = [UIFont spn_NeutraMedium];
    textView.textColor = [UIColor blackColor];
    
}

#pragma mark Star rating delegate functions
- (void)starRatingControl:(StarRatingControl *)control didUpdateRating:(NSUInteger)rating {
    userRating = rating;
}

- (void)starRatingControl:(StarRatingControl *)control willUpdateRating:(NSUInteger)rating {
    userRating = rating;
}


#pragma mark Review related functions
-(void)cancelButtonPressed:(id)sender
{
    [self.delegate updatePosts:NO
                    withReview:nil];
    [self closeReview];
}

-(void)closeReview
{
    [backgroundView removeFromSuperview];
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

-(void)postReviewButtonPressed:(id)sender
{
    NSString *comment = self.commentTextView.text;
    NSString *title = self.titleTextField.text;
    if (title.length < 1 && [SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewWthTitle:@"Wait" andMessage:@"You need to add a title to your post!"];
    }
    else if ((comment.length < 1 || [comment isEqualToString:@"Write your comments here!"]) && [SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewCommentMissing
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Nope"
                           withOtherTitles:@"I think I will!"];
    }
    else if (userRating < 1) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewRatingMissing
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Nope"
                           withOtherTitles:@"Sure!"];
    }
    else {
        NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
        [self postReviewForMovie:movieId
                     withComment:comment
                      withRating:userRating
                         forUser:[SPNUser getCurrentUserId]
                        andTitle:title];
        /*
        if (succeeded) {
          //  [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewReviewAdded];
            [[[SPNFilmDataCache sharedInstance] anonymousReviewCache]
             setObject:@"done"
             forKey:movieId];
            
            if ([comment length] < 1 || [comment isEqualToString:@"Write your comments here!"]) {
                [self.delegate updatePosts:YES
                                withReview:nil];
                
            }
            else {
                 [self.delegate updatePosts:YES
                                 withReview:comment];
            }
            [self closeReview];
        }
        else {
            [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewReviewNotAdded];
        }
         
        */
    }
}

-(void)postReviewForMovie:(NSString*)movieId withComment:(NSString*)comment withRating:(NSInteger)rating forUser:(NSDictionary*)user andTitle:(NSString*)title
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    NSMutableDictionary *review = [NSMutableDictionary new];
    if (![comment isEqualToString:@"Write your comments here!"] && [comment length] > 0) {
        [review setObject:comment
                   forKey:@"message"];
    }
    if (rating > 0) {
        [review setObject:[NSNumber numberWithInteger:rating]
                   forKey:@"rating"];
    }
    if ([title length] > 0) {
        [review setObject:title
                   forKey:@"title"];
    }
    [[SPNCinelifeQueries sharedInstance] postReview:review
                                         forMovieId:[self.filmInformation objectForKey:@"id"]
                                    withCredentials:[SPNUser getCurrentUserId]];
}

#pragma mark Initialization functions
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil movie:(NSDictionary *)movie
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.filmInformation = movie;
        
    }
    return self;
}

+(void)showWReviewPostForMovie:(NSDictionary *)movie overViewController:(id)viewController withDelegate:(id)viewControllerDelegate
{
    [self showWReviewPostForMovie:movie
               overViewController:viewController
                     withDelegate:viewControllerDelegate
                    withOldReview:nil];
}
+(void)showWReviewPostForMovie:(NSDictionary *)movie overViewController:(id)viewController withDelegate:(id)viewControllerDelegate withOldReview:(NSDictionary *)review
{
    SPNFilmPostReviewViewController *spnReviewController = [[SPNFilmPostReviewViewController alloc]
                                                            initWithNibName:@"PostReview"
                                                            bundle:nil
                                                            movie:movie];

    spnReviewController.delegate = viewControllerDelegate;
    spnReviewController.previousReview = review;
    [viewController addChildViewController:spnReviewController];
    [spnReviewController show];
}

-(void)show
{
    UIViewController *controller = self.parentViewController;
    UIGraphicsBeginImageContextWithOptions(controller.view.frame.size, YES, 0.4);
    
    [controller.view drawViewHierarchyInRect:controller.view.frame
                          afterScreenUpdates:NO];
    
    UIImage *blurImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //Blur the UIImage
    CIImage *imageToBlur = [CIImage imageWithCGImage:blurImg.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
    
    //change number to increase/decrease blur
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat: 3]
                          forKey:@"inputRadius"];
    
    CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    //Place the UIImage in a UIImageView
    backgroundView = [[UIImageView alloc] initWithFrame:controller.view.frame];
    CGImageRef cgImage2 = [context createCGImage:resultImage fromRect:[imageToBlur extent]];
    
    backgroundView.image = [UIImage imageWithCGImage:cgImage2];
    UIView *shieldView = [[UIView alloc] initWithFrame:backgroundView.frame];
    [shieldView setBackgroundColor:[UIColor blackColor]];
    [shieldView setAlpha:0.6];
    [backgroundView addSubview:shieldView];
    
    [self.view setFrame:controller.view.frame];
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.containerView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.view
                                                                       attribute:NSLayoutAttributeWidth
                                                                      multiplier:1
                                                                        constant:0];
    
    [self.view addConstraint:widthConstraint];

    
    [self.view setAlpha:1];
    
    //insert blur UIImageView below transparent view inside the blur image container
    [controller.view addSubview:self.view];
    [controller.view insertSubview:backgroundView belowSubview:self.view];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Nope"] ) {
        NSString *comment = self.commentTextView.text;
        NSString *title  = self.titleTextField.text;
        if (comment.length < 1 || [comment isEqualToString:@"Write your comments here!"]) {
            comment = @"";
        }
        NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
        [self postReviewForMovie:movieId
                                      withComment:comment
                                       withRating:userRating
                                          forUser:[SPNUser getCurrentUserId]
                                         andTitle:title];
        
        /*
        if (succeeded) {
            //  [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewReviewAdded];
            if (comment.length < 1 || [comment isEqualToString:@"Write your comments here!"]) {
                [self.delegate updatePosts:YES
                                withReview:nil];
            }
            else {
                [self.delegate updatePosts:YES
                                withReview:comment];
            }
            [self closeReview];
        }
        else {
            [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeReviewReviewNotAdded];
        }
         */

    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 255) ? NO : YES;
}

#pragma mark Cinelife queries delegate functions
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");

}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery reviewPosted:(NSDictionary *)review
{
    NSLog(@"");
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.delegate updatePosts:YES
                    withReview:[review objectForKey:@"message"]];
    [self closeReview];
    
}
@end
