//
//  SPNPassbookViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/28/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPNShareSocialMediaType)
{
    SPNShareSocialMediaTypeFacebook = 0,
    SPNShareSocialMediaTypeTwitter,
    SPNShareSocialMediaTypeGooglePlus
};


#import <FBSDKShareKit/FBSDKShareKit.h>
#import <TwitterKit/TwitterKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface SPNPassbookViewController : UIViewController <UIAlertViewDelegate, UIActionSheetDelegate, GIDSignInDelegate, GIDSignInUIDelegate, GPPShareDelegate, FBSDKSharingDelegate, SFSafariViewControllerDelegate>

@property NSDictionary *movieInformation;
@property NSDictionary *showtimeInformation;
@property NSDictionary *paymentInformation;
@property NSDictionary *branding;

@property NSDictionary *transactionInformation;

@property PKPass *generatedPass;

@property IBOutlet UIScrollView *scrollView;
@property IBOutlet UILabel *ticketsLabel;
@property IBOutlet UILabel *showDateLabel;
@property IBOutlet UILabel *showTimeLabel;
@property IBOutlet UILabel *transactionCodeLabel;

@property IBOutlet UILabel *confirmationLabel;
@property IBOutlet UILabel *codeTitleLabel;
@property IBOutlet UILabel *codeLabel;

@property IBOutlet UIImageView *miniPosterImageView;
@property IBOutlet UIImageView *qrCodeImageVIew;

@property IBOutlet UIButton *doneButton;


@property IBOutlet UIView *theatreBackgroundView;
@property IBOutlet UIImageView *theatreLogoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollOriginTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandLogoHeightConstraint;


// Movie information outlets
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *metaScoreLabel;
@property IBOutlet UILabel *userRatingLabel;
@property IBOutlet UILabel *genresLabel;
@property IBOutlet UILabel *classificationLabel;
@property IBOutlet UILabel *durationLabel;
@property IBOutlet UIImageView *posterImageView;

@property IBOutlet UIView *codeContainerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreLeadConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaIconWidthConstraint;

//-------------------------//

-(IBAction)addPassbook:(id)sender;
-(IBAction)sharePurchase:(id)sender;
-(IBAction)doneWithPurchase:(id)sender;
@end
