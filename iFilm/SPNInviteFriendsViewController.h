//
//  SPNInviteFriendsViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#import <GoogleSignIn/GoogleSignIn.h>

typedef NS_ENUM(NSInteger, SPNShareSocialMediaType)
{
    SPNShareSocialMediaTypeFacebook = 0,
    SPNShareSocialMediaTypeTwitter,
    SPNShareSocialMediaTypeGooglePlus
};


@interface SPNInviteFriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, GPPShareDelegate, GIDSignInDelegate, GIDSignInUIDelegate, FBSDKSharingDelegate, SFSafariViewControllerDelegate>

@end
