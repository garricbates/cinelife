//
//  SPNPurchaseTotalCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPurchaseTotalCell.h"

@implementation SPNPurchaseTotalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.creditCardButton setBackgroundColor:[UIColor spn_aquaColor]];
    [self.creditCardButton.layer setBorderColor:[UIColor spn_aquaColor].CGColor];
    [self.creditCardButton setTitleColor:[UIColor whiteColor]
                                forState:UIControlStateNormal];
    
    [self.creditCardButton setTitleColor:[UIColor lightGrayColor]
                                forState:UIControlStateDisabled];
    [self.giftCardButton setTitleColor:[UIColor lightGrayColor]
                              forState:UIControlStateDisabled];
    [self.membershipCardButton setTitleColor:[UIColor lightGrayColor]
                                    forState:UIControlStateDisabled];
}

@end
