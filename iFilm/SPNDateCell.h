//
//  SPNDateCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 1/14/15.
//  Copyright (c) 2015 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNDateCell : UICollectionViewCell

@property IBOutlet UILabel *dateTitleLabel;

@end
