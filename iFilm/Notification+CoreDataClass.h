//
//  Notification+CoreDataClass.h
//  iFilm
//
//  Created by Vlad Getman on 19.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Notification : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Notification+CoreDataProperties.h"
