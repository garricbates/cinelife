//
//  SPNNavigationController.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/15/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNNavigationController.h"
#import "SPNFilmTrailerViewController.h"

@interface SPNNavigationController ()

@end

@implementation SPNNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    id currentViewController = self.topViewController;
    
    if ([currentViewController isKindOfClass:[SPNFilmTrailerViewController class]]) {
        return YES;
    }
    return NO;
}
@end
