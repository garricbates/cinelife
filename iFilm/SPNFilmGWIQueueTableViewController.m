//
//  SPNFilmGWIQueueTableViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmGWIQueueTableViewController.h"
#import "SPNFilmGWIQueueCell.h" 
#import "SPNFilmDetailsViewController.h"

@interface SPNFilmGWIQueueTableViewController ()
{
    NSDictionary *selectedGWIFilm;
}
@end

@implementation SPNFilmGWIQueueTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeUI];
}

#pragma mark - UI customization
-(void)customizeUI
{
    [self.tableView setTableFooterView:[UIView new]];
    [self.tableView setBackgroundColor:[UIColor spn_brownColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view functions     

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.queuedFilms count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* GWIQueuedFilmCellIdentifier = @"QueuedFilmCell";
    SPNFilmGWIQueueCell *cell = [tableView dequeueReusableCellWithIdentifier:GWIQueuedFilmCellIdentifier];
    
    NSDictionary *queuedFilm = [self.queuedFilms objectAtIndex:indexPath.row];
    [cell initializeCellForMovie:queuedFilm];
    
    NSString *movieId = [queuedFilm objectForKey:@"movieId"];
    NSString *urlImage;
    if (movieId) {
        urlImage = [SPNServerQueries getMovieImageURL:movieId];
    }
    
    if (urlImage) {
        urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];
        NSRange result = [urlImage rangeOfString:@"default_movie"];
        if (result.length < 1) {
            dispatch_async(kBgQueue, ^{
                [cell.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
                [cell.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    SPNFilmGWIQueueCell *updateCell = (SPNFilmGWIQueueCell*)[tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell) {
                        [cell.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
                    }
                });
            });
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *queuedFilm = [self.queuedFilms objectAtIndex:indexPath.row];
    NSString *title = [queuedFilm objectForKey:@"title"];
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    dispatch_async(kBgQueue, ^{
        selectedGWIFilm = [SPNServerQueries searchFilmByTitle:title];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            [self performSegueWithIdentifier:@"FilmDetails" sender:self];
        });
    });
    
}

#pragma mark - Navigation functions
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FilmDetails"]) {
        SPNFilmDetailsViewController *spnFilmDetails = [segue destinationViewController];
        [spnFilmDetails setFilmInformation:selectedGWIFilm];
        [spnFilmDetails setLastKnownLocation:self.lastKnownLocation];
    }
}
@end
