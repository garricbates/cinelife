//
//  SPNSynopsisCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/27/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNSynopsisCell : UITableViewCell

@property IBOutlet UILabel *titleLabel;
@end
