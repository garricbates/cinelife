//
//  SPNFilterCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNFilterCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *filterTextLabel;

@end
