//
//  SPNCinelifeQueries.h
//  iFilm
//
//  Created by Eduardo Salinas on 8/3/15.
//  Copyright (c) 2015 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SPNCinelifeQueries;
@class Festival;

//static NSString *server = @"https://developmentlcp.com/cinelife/api";

#if DEV
static NSString *server = @"http://ec2-52-24-221-96.us-west-2.compute.amazonaws.com/v2/api";
#else
static NSString *server = @"https://cinelife.com/v2/api";
#endif

@protocol SPNCinelifeQueriesDelegate <NSObject>

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
              theaterList:(NSArray*)theaters;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
             festivalList:(NSArray <Festival *> *)festivals;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
       theaterInformation:(NSDictionary*)theater;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             theaterPosts:(NSArray *)posts;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
            festivalPosts:(NSArray *)posts;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
              movieList:(NSArray*)movies;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
               futureList:(NSArray*)movies;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
         movieInformation:(NSDictionary*)movie;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
           userRegistered:(NSDictionary*)user withParams:(NSDictionary*)userDetails;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
             userLoggedIn:(NSDictionary*)user withCredentials:(NSDictionary*)credentials;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
          myCinelifeTiles:(NSArray*)tiles;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
                  reviews:(NSArray*)reviews;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             reviewPosted:(NSDictionary*)review;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             addedTheater:(NSDictionary*)theater;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             addedMovie:(NSDictionary*)movie;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             removedTheater:(NSDictionary*)theater;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             removedMovie:(NSDictionary*)movie;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
         favoriteTheaters:(NSArray*)favoriteTheaters;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             ticketingURL:(NSString*)url;
@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
         ticketingFromRTS:(NSDictionary*)ticketingInformation;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
         passwordDidReset:(BOOL)didReset;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
         purchasedTickets:(NSDictionary*)purchaseInformation;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
           createdPassbook:(PKPass*)passbook;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
    cinelifeConfiguration:(NSDictionary*)configuration;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
           ticketingError:(NSError*)error;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
           facebookFeed:(NSArray*)facebookFeed;

@optional
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery
             twitterFeed:(NSArray*)twitterFeed;

-(void)spnCinelifeQueries:(SPNCinelifeQueries*)cinelifeQuery
             errorOcurred:(NSError*)error;

@end

@interface SPNCinelifeQueries : NSObject <NSURLSessionDataDelegate, NSURLSessionDelegate>

@property (nonatomic, strong) id <SPNCinelifeQueriesDelegate> delegate;
// The NSURLSession created in the init method, to communicate with the PHP server.
@property  (nonatomic, strong) NSURLSession* serverSession;

+(SPNCinelifeQueries*)sharedInstance;

/**
 * Method to obtain initial information for the app. As of the moment of coding, it obtains the text for the affiliated theater's label, as well as its background color.
 * @author La Casa de los Pixeles
 * @return This method calls the delegate call "spnCinelifeQueries:cinelifeConfiguration:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.

 */
+(NSDictionary*)getCineLifeInitialConfiguration;

/**
 * Method to sort a list of theaters based in a user's location.
 * @author La Casa de los Pixeles
 * @param theaterList The list of theater objects obtained by other queries. This list should be sorted based on the user's location.
 * @param userLocation A CLLocation object that contains the user coordinates. If not specified, then the list will be returned as is.
 * @return The list of theaters sorted by the user's proximity to each theater.
 */
+(NSMutableArray *)sortTheatersByDistance:(NSArray*)theaterList
                             withLocation:(CLLocation*)userLocation withPartnersFirst:(BOOL)partnersFirst;

/**
 * Method to sort a list of theater showtimes based on their show date
 * @author La Casa de los Pixeles
 * @param theaterList The list of theater showtime objects obtained by other queries. This list should be sorted based on their show date
 * @return The list of theatre showtimes grouped as dictionary entries, with each dictionary key being an NSDate object.
 */
+(NSDictionary *)sortShowdatesForTheaters:(NSArray*)theaterList;

/**
 * Method to sort a list of theater showtimes based on their show date
 * @author La Casa de los Pixeles
 * @param theaterList The list of theater showtime objects obtained by other queries. This list should be sorted based on their show date
 * @return The list of theatre showtimes grouped as dictionary entries, with each dictionary key being an NSDate object.
 */
+(NSDictionary *)sortShowdatesForOneTheatre:(NSArray*)movieList;


/**
 * Method to sort a list of movie showtimes based on their show date
 * @author La Casa de los Pixeles
 * @param movieList The list of movie showtime objects obtained by other queries. This list should be sorted based on their show date
 * @return The list of movie showtimes grouped as dictionary entries, with each dictionary key being an NSDate object.
 */
+(NSDictionary *)sortShowdatesForMovies:(NSArray *)movieList;

/**
 * Method to sort a list of movies based in their title
 * @author La Casa de los Pixeles
 * @param movies The list of movie objects obtained by other queries. This list should be sorted based on the movie names.
 * @param orderPartners Boolean to indicate whether partners should take higher priority in the sorting.
 * @return The list of movies sorted by their names.
 */
+(NSArray*)sortMoviesByName:(NSArray*)movies
          withPartnersFirst:(BOOL)orderPartners;

/**
 * Method to get and merge all the theater's address info into one string.
 * @author La Casa de los Pixeles
 * @param theater The theater object containing information about its location, such as street, city, zip code, etc.
 * @return An address string, with format: Street, City, State Abbreviation, Zip Code.
 */
+ (NSString*)getTheatreAddress:(NSDictionary*)theatre;

/**
 * Method to obtain a list of theaters in a specified location.
 * @author La Casa de los Pixeles
 * @param zipcode The zipcode for which to query the server. The zipcode must be a valid U.S. or Canadian code. Example: 90064.
 * @param coordinates A CLLocationCoordinate2D object containing latitude and longitude values.
 * @param radius The radius in kilometers to limit the range of the search. Included by default if not specified (from the server).
 * @return This method calls the delegate call "spnCinelifeQueries:theaterList:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getTheatersForZipcode:(NSString*)zipcode
               orCoordinates:(CLLocation*)location
                  withRadius:(NSInteger)radius
             withCredentials:(NSDictionary*)credentials;

/**
 * Method to obtain a list of festivals in a specified location.
 * @param zipcode The zipcode for which to query the server. The zipcode must be a valid U.S. or Canadian code. Example: 90064.
 * @param coordinates A CLLocationCoordinate2D object containing latitude and longitude values.
 * @param radius The radius in kilometers to limit the range of the search. Included by default if not specified (from the server).
 * @return This method calls the delegate call "spnCinelifeQueries:festivalList:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
- (void)getFestivalsForZipcode:(NSString *)zipcode
                 orCoordinates:(CLLocation *)location
                    withRadius:(NSInteger)radius
                    onlyPinned:(BOOL)onlyPinned
                       filters:(NSDictionary *)filters;

/**
 * Method to obtain a the details and showtime of a particular theater.
 * @author La Casa de los Pixeles
 * @param theaterId The wwm id of the theatre for which to fetch the showtimes.
 * @return This method calls the delegate call "spnCinelifeQueries:theaterList:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getTheaterInformation:(NSString*)theaterId
             withCredentials:(NSDictionary*)credentials;

/**
 * Method to obtain a list of movies playing in affiliated theaters
 * @author La Casa de los Pixeles
 */
-(void)getPartnerMoviesWithCompletion:(void(^)(NSArray *movies, NSError *error))completion;

/**
 * Method to obtain a list of movies that will soon be on theaters.
 * @author La Casa de los Pixeles
 * @return This method calls the delegate call "spnCinelifeQueries:futureList:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getFutureReleases;

/**
 * Method to obtain a list of movies playing in a specified location.
 * @author La Casa de los Pixeles
 * @param zipcode The zipcode for which to query the server. The zipcode must be a valid U.S. or Canadian code. Example: 90064.
 * @param coordinates A CLLocationCoordinate2D object containing latitude and longitude values.
 * @param radius The radius in kilometers to limit the range of the search. Included by default if not specified (from the server).
 * @return This method calls the delegate call "spnCinelifeQueries:movieList:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getMoviesForZipcode:(NSString*)zipcode
             orCoordinates:(CLLocation*)location
                withRadius:(NSInteger)radius;

/**
 * Method to obtain the details of a particular movie playing in a specified location.
 * @author La Casa de los Pixeles
 * @param movieId The movieId of the movie to search.
 * @param zipcode The zipcode for which to query the server. The zipcode must be a valid U.S. or Canadian code. Example: 90064.
 * @param coordinates A CLLocationCoordinate2D object containing latitude and longitude values.
 * @param radius The radius in kilometers to limit the range of the search. Included by default if not specified (from the server).
 * @return This method calls the delegate call "spnCinelifeQueries:movieInformation:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getMovieInformation:(NSString*)movieId
                forZipcode:(NSString*)zipcode
             orCoordinates:(CLLocation*)location
                withRadius:(NSInteger)radius
           withCredentials:(NSDictionary*)credentials;

/**
 * Method to register a new user account into Cinelife.
 * @author La Casa de los Pixeles
 * @param user A dictionary containing the following entries: username, email, password, confirm_password. Each entry is of type string.
 * @return This method calls the delegate call "spnCinelifeQueries:userRegistered:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postSignUpNewUser:(NSDictionary*)user;

/**
 * Method to log in with an existing account into Cinelife.
 * @author La Casa de los Pixeles
 * @param credentials A dictionary containing access tokens, depending on the social media account / cinelife account used to login. FB -> facebook_token, G+ -> google_id_token, google_access_token, google_user_id, Twitter -> twitter_token, twitter_token_secret, twitter_token_secret
 * @return This method calls the delegate call "spnCinelifeQueries:userLoggedIn:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postSignIn:(NSDictionary*)credentials;

/**
 * Method to get the tiles for the My Cinelife section.
 * @author La Casa de los Pixeles
 * @return This method calls the delegate call "spnCinelifeQueries:myCinelifeTiles:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getMyCineLife;

/**
 * Method to post a Cinelife review for a particular movie. Note that registered users can only post one review per movie. If the user tries to add another review for the same movie, then he will be taken to his previous review for him to edit.
 * @author La Casa de los Pixeles
 * @param review A NSDictionary containing the message and rating.
 * @param movieId The movieId string defining the movie for which to post the review.
 * @param credentials A NSDictionary containing the access token for the user. This parameter is optional, but if its not specified, only the rating will be sent to the server.
 * @return This method calls the delegate call "spnCinelifeQueries:reviewPosted:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postReview:(NSDictionary*)review
       forMovieId:(NSString*)movieId
  withCredentials:(NSDictionary*)credentials;

/**
 * Method to fetch the list of Cinelife reviews for a particular movie.
 * @author La Casa de los Pixeles
 * @param movieId The movieId string defining the movie for which to fetch the reviews.
 * @return This method calls the delegate call "spnCinelifeQueries:reviews:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getReviewsForMovieId:(NSString*)movieId;

/**
 * Method to add a theater to the user's favorites. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 * @param theaterId The theater Id for the theater to add.
 * @param credentials The user's access token must be passed in this parameter
 * @return This method calls the delegate call "spnCinelifeQueries:addedTheater:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postAddFavoriteTheater:(NSString*)theaterId
              withCredentials:(NSDictionary*)credentials;

/**
 * Method to add a movie to the user's favorites. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 * @param theaterId The movie Id for the movie to add.
 * @param credentials The user's access token must be passed in this parameter
 * @return This method calls the delegate call "spnCinelifeQueries:addedMovie:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
*/
-(void)postAddFavoriteMovie:(NSString*)movieId
            withCredentials:(NSDictionary*)credentials;

/**
 * Method to remove a theater to the user's favorites. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 * @param theaterId The theater Id for the theater to remove.
 * @param credentials The user's access token must be passed in this parameter
 * @return This method calls the delegate call "spnCinelifeQueries:removedTheater:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postRemoveFavoriteTheater:(NSString*)theaterId
                 withCredentials:(NSDictionary*)credentials;

/**
 * Method to remove a movie to the user's favorites. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 * @param theaterId The movie Id for the movie to remove.
 * @param credentials The user's access token must be passed in this parameter
 * @return This method calls the delegate call "spnCinelifeQueries:removedMovie:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postRemoveFavoriteMovie:(NSString*)movieId
               withCredentials:(NSDictionary*)credentials;

/**
 * Method to fetch the user's favorite theaterss. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 * @param credentials The user's access token must be passed in this parameter
 * @return This method calls the delegate call "spnCinelifeQueries:favoriteTheaters:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getFavoriteTheatersWithCredentials:(NSDictionary*)credentials;

/**
 * Method to fetch the user's favorite theaters and festivals. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 */
-(void)getFavoritesTheatersAndFestivalsWithCompletion:(void(^)(NSArray *theaters, NSArray *festivals, NSError *error))completion;

/**
 * Method to fetch the user's favorite movies and festivals. Requires the user to be logged into the app.
 * @author La Casa de los Pixeles
 */
-(void)getFavoritesMoviesAndFestivalsWithCompletion:(void(^)(NSArray *films, NSArray *festivals, NSError *error))completion;

/**
 * Method to fetch the list of avaiable ticketing information for a particular theater.
 * @author La Casa de los Pixeles
 * @param theaterId The theater Id for which to search the ticketing details.
 * @param movieId The movie Id playing in the specified theater.
 * @param date The date in which the movie is playing. Format is yyyy-MM-dd.
 * @param showtime The hour of the day in which the movie is playing. Format is HH:mm.
 * @return This method calls the delegate call "spnCinelifeQueries:ticketingInformation:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postTicketingForTheater:(NSString*)theaterId
                      forMovie:(NSString*)movieId
                   forShowdate:(NSString*)date
                   andShowtime:(NSString*)showtime;

/**
 * Method to fetch the purchase tickets using RTS for a movie and selected theater.
 * @author La Casa de los Pixeles
 * @param tickets An array of ticket dictionaries containing ticket id, and number of tickets. Tickets are grouped by type (i.e. senior, child, etc.)
 * @param amount The total price of all purchased tickets. Each ticket contains a convenience charge, which is included in this amount.
 * @param performanceId The id identifying the show's date, time and movie. This is unique from RTS.
 * @param rtsTheaterId The RTStheater Id identifying the theater on which to purchase tickets.
 * @param auditorium The auditorium of the theater where the movie is playing.
 * @param cardDetails The gift/credit/premiere card associated with the purchase. This information is not stored on the device nor the server.
 * @param email The buyer's email. This email address is used for the confirmation email. When a user successfully purchases tickets, an email with the purchase code is automatically sent to them.
 * @param filmDetails The details of the movie. The movie title is used to link the WWM movie data with the RTS movie data.
 * @param ticketNames The name of each ticket's type. For example (premier ticket - senior discount, etc.).
 
 * @return This method calls the delegate call "spnCinelifeQueries:purchasedTickets:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */

-(void)postPurchaseTickets:(NSArray*)tickets
                 forAmount:(NSString*)amount
          forPerformanceId:(NSString*)performanceId
                forTheatre:(NSDictionary*)theaterDetails
             forAuditorium:(NSString*)auditorium
             withTicketFee:(NSString*)fee
       withCardInformation:(NSDictionary*)cardDetails
               andCustomer:(NSDictionary*)customer
            andFilmDetails:(NSDictionary*)filmDetails;


/**
 * Method to fetch the entries of an RSS (ATOM) feed. The entries are returned in a json format. This services uses a google api to fetch info (http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http://pageinATOMformatforfeed.com) This method uses a synchronous connection, since we have to query multiple pages, and we cannot determine when each page will finish sending its information.
 * @author La Casa de los Pixeles
 * @param An array of JSON objects, containing one "feed" entry containing the link for the RSS feed in ATOM format.
 * @param postCount The number of maximum entries for each RSS feed.

 * @return This method returns an array of entries from different news feed pages. They will be sorted based on their publication date.
 */
+(NSArray*)getNewsfeedStoriesForPages:(NSArray*)newsfeedList
                        withPostCount:(NSInteger)postCount;


- (void)readAllNotifications;

- (void)getNotificationsWithCompletion:(void(^)(NSArray *notifications, NSError *error))completion;

/**
 * Method to register a device for remote notifications.
 * @author La Casa de los Pixeles
 * @param udid The device unique identifier. This id can change with updates or new installs of the app on the same device.
 * @param token The token obtained by the system to use for the remote notifications.
 * @param deviceName The name of the device.
 * @param deviceModel The model of the current device.
 * @param version The iOS version of the device.
 * @param enableBadge A boolean indicating wether the user has authorized badge notifications.
 * @param enableAlert A boolean indicating wether the user has authorized alert notifications.
 * @param enableSound A boolean indicating wether the user has authorized sound notifications.
 * @param credentials The user's access token can be passed in this parameter. If so, the udid will be linken to the logged user.
 */
-(void)postRegisterDeviceForPushNotificationsWithUDID:(NSString*)udid
                                             andToken:(NSString*)token
                                       withDeviceName:(NSString*)deviceName
                                       andDeviceModel:(NSString*)deviceModel
                                          withVersion:(NSString*)version
                                             forBadge:(BOOL)enableBadge
                                             forAlert:(BOOL)enableAlert
                                             forSound:(BOOL)enableSound
                                      withCredentials:(NSDictionary*)credentials;


/**
 * Method to unregister a device for remote notifications.
 * @author La Casa de los Pixeles
 * @param udid The device unique identifier.
*/
-(void)postUnregisterDeviceForPushNotificationsWithUDID:(NSString*)udid;


/**
 * Method to obtain a list of json objects, each with an entry from a specified RSS feed. This method was constructed using a synchronous call, but hidden behind a dispatch queue. That way, we can be sure that when this method ends, we have the full list of news from the list of news pages.
 * @author La Casa de los Pixeles
 * @param newsfeedList An array of dictionaries containing a "feed" node. This feed node contains the string URL of the newsfeed page for which to fetch the entries.
 * @param postCount The max number of news entries to fetch for each page.
 * @return An array of json posts for each post of the desired news page.
 */
+(NSArray*)getStoriesForTheaterWithFeeds:(NSArray*)newsfeedList withPostCount:(NSInteger)postCount;


/**
 * Method to validate a URL. This method queries the headers of the desired URL and returns NO if an error ocurred.
 * @author La Casa de los Pixeles
 * @param url The URL to validate.
 * @return NO if there was an error with the URL. YES, otherwise.
 */
+(BOOL)isValidURL:(NSURL*)url;

/**
 * Method to get the url of the CineLife Terms of Service.
 * @author La Casa de los Pixeles
 * @return The url of the TOS.
 */
+(NSString*)getCinelifeTermsOfService;

/**
 * Method to get the url of the CineLife Privacy Policy.
 * @author La Casa de los Pixeles
 * @return The url of the Privacy Policy.
 */
+(NSString*)getCinelifePrivacyPolicy;

/**
 * Method to obtain a list of json objects, each with an entry from a specified theater's feed. This feed comes from the CineLife dashboard, and is usually associated with an affiliated theater.
 * @author La Casa de los Pixeles
 * @param theaterId The ID of the theater for which to fetch the list of available posts.
 * @return This method calls the delegate call "spnCinelifeQueries:theaterPosts:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)getCineLifePostsForTheater:(NSString*)theaterId;

/**
 * Method to obtain a list of json objects, each with an entry from a specified festival's feed. This feed comes from the CineLife dashboard, and is usually associated with an affiliated festival.
 * @author La Casa de los Pixeles
 * @param festivalId The ID of the theater for which to fetch the list of available posts.
 * @return This method calls the delegate call "spnCinelifeQueries:festivalPosts:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
- (void)getCineLifePostsForFestival:(NSString *)festivalId;

/**
 * Method to reset the password of a CineLife user's account. Use in case the user forgot his/her password.
 * @author La Casa de los Pixeles
 * @param email The ID of the theater for which to fetch the list of available posts.
 * @return This method calls the delegate call "spnCinelifeQueries:theaterPosts:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postResetPassword:(NSString*)email;

/**
 * Method to obtain the Passbook pass created from a successful purchase.
 * @author La Casa de los Pixeles
 * @param purchaseInformation A dictionary containing the transaction ID and the barcode of the resulting purchase. The server is in charge of generating the full pass (i. e. theater info, movie details, seating, etc.).
 * @return This method calls the delegate call "spnCinelifeQueries:createdPassbook:" if successful. Otherwise, the delegate call "spnCinelifeQueries:errorOcurred:" will be called.
 */
-(void)postCreatePassbookForPurchase:(NSDictionary*)purchaseInformation;

+(NSArray*)getFacebookFeed:(NSString*)facebookFeed withLimit:(NSInteger)limit;

+(NSArray*)getTwitterFeed:(NSString*)twitterFeed withLimit:(NSInteger)limit;

+(NSDictionary*)getCastAndCrewForMovie:(NSDictionary*)movie;

- (void)favoriteFestival:(NSNumber *)festivalId completion:(void(^)(NSError *))completion;
- (void)unfavoriteFestival:(NSNumber *)festivalId completion:(void(^)(NSError *))completion;


- (void)getTheater:(NSString *)theaterId completion:(void(^)(NSDictionary *theaterInfo, NSError *error))completion;

- (void)getFestival:(NSString *)festivalId completion:(void(^)(Festival *festival, NSError *error))completion;

- (void)getMovie:(NSString *)movieId completion:(void(^)(NSDictionary *movieInfo, NSError *error))completion;

- (void)saveLocation:(CLLocation *)location completion:(void(^)(NSError *))completion;

- (void)getNotificationsBadgeWithCompletion:(void (^)(NSString *badge))completion;

- (void)removeNotificationWithId:(NSNumber *)notificationId completion:(void(^)(NSError *))completion;

@end
