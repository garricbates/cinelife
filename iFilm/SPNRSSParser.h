//
//  SPNRSSParser.h
//  iFilm
//
//  Created by Dmytro Gladush on 1/15/17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MWFeedParser/MWFeedParser.h>

@interface SPNRSSParser : NSObject<MWFeedParserDelegate>

@property (nonatomic, retain) NSMutableDictionary *res;
@property (nonatomic, retain) NSMutableArray *entries;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (nonatomic) NSInteger numberOfEntities;

- (NSDictionary *)parseRSS:(NSString *)rssUrl numberOfEntities:(NSInteger)numberOfEntities;

@end
