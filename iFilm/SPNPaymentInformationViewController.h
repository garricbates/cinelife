//
//  SPNPaymentInformationViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPNPaymentMethod) {
    SPNPaymentMethodCreditCard = 0,
    SPNPaymentMethodGiftCard,
    SPNPaymentMethodMembershipCard
};

@interface SPNPaymentInformationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property NSDictionary *movieInformation;
@property NSDictionary *showtimeInformation;
@property NSDictionary *paymentInformation;
@property NSDictionary *ticketingInformation;
@property NSDictionary *branding;

// Movie information outlets
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *metaScoreLabel;
@property IBOutlet UILabel *userRatingLabel;
@property IBOutlet UILabel *genresLabel;
@property IBOutlet UILabel *classificationLabel;
@property IBOutlet UILabel *durationLabel;
@property IBOutlet UIImageView *posterImageView;

//-------------------------//
@property IBOutlet UIButton *nextButton;
@property IBOutlet UITableView *paymentInformationTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreLeadConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaIconWidthConstraint;

@property IBOutlet UIView *theatreBackgroundView;
@property IBOutlet UIImageView *theatreLogoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandLogoHeightConstraint;



@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *emailTextField;

@property IBOutlet UITextField *cardNumberTextField;
@property IBOutlet UITextField *cardCVCTextField;
@property IBOutlet UITextField *cardExpirationDateTextField;
@property IBOutlet UITextField *postalTextField;


@property IBOutlet UIImageView *checkIcon;
@property SPNPaymentMethod selectedPaymentMethod;

-(IBAction)confirmPurchase:(id)sender;
-(IBAction)storePersonalData:(id)sender;
@end
