//
//  Purchases.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Purchases : NSManagedObject

@property (nonatomic, retain) NSString * movieId;
@property (nonatomic, retain) NSDate * showDate;
@property (nonatomic, retain) NSString * showTime;
@property (nonatomic, retain) NSString * tickets;
@property (nonatomic, retain) NSString * transactionCode;
@property (nonatomic, retain) NSString * movieTitle;

@end
