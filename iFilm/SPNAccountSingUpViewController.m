//
//  SPNAccountSingUpViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAccountSingUpViewController.h"
#import "MBProgressHUD.h"

@interface SPNAccountSingUpViewController ()

@end

@implementation SPNAccountSingUpViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Sign Up";
    // Do any additional setup after loading the view.
    [self customizeUI];
    
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI customization
-(void)customizeUI
{
    [self.fillOutFieldsLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    [self.signUpButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.cancelButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];

    self.signUpButton.layer.cornerRadius = 2.5;
    [self.signUpButton.layer setShadowColor:[UIColor spn_aquaShadowColor].CGColor];
    
    self.cancelButton.layer.cornerRadius = 2.5;
    self.cancelButton.layer.masksToBounds = YES;
   

    [self.signUpButton setBackgroundColor:[UIColor spn_aquaColor]];
    [self.cancelButton setBackgroundColor:[UIColor whiteColor]];
}

#pragma mark Custom Keyboard functions
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark Textfield functions

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    }
    else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks*/
}
#pragma mark Sign up functions
-(void)validateAndSignUp
{
    
    if ([self signInValidation]) {
        //NSString *username = self.usernameTextField.text;
        NSString *email = self.emailTextField.text;
        NSString *password = self.passwordTextField.text;
        
        
        NSMutableDictionary *newUser = [NSMutableDictionary new];
        //[newUser setObject:email forKey:@"username"];
        [newUser setObject:email forKey:@"email"];
        [newUser setObject:password forKey:@"password"];
        [newUser setObject:password forKey:@"confirm_password"];
        
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        [[SPNCinelifeQueries sharedInstance] postSignUpNewUser:newUser];
    }
}

-(BOOL)attemptLoginWithUsername:(NSString*)username andPassword:(NSString*)password
{
    BOOL didLogin = NO;
    return didLogin;
}

-(BOOL)signInValidation
{
    BOOL succeeded = NO;
    //NSString *username = self.usernameTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *retypePassword = self.retypePasswordTextField.text;
    
    if ([email length] < 1 || [password length] < 1 || [retypePassword length] < 1) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserSignInFieldsRequired];
    }
    else if (![password isEqualToString:retypePassword]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserSignInPasswordMismatch];
    }
    else {
        succeeded = YES;
    }
    return succeeded;
}

- (IBAction)signUpButtonPressed:(id)sender {
    [self validateAndSignUp];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSLog(@"Error");
    
    NSString *errorText = [error.userInfo objectForKey:@"error_msg"];
    if (!errorText) {
        errorText = error.localizedDescription;
    }
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"Error on sign up"
                                message:errorText
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
    
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery userRegistered:(NSDictionary *)user withParams:(NSDictionary *)userDetails
{
    
    NSMutableDictionary *newUser = [NSMutableDictionary new];
    [newUser setObject:[userDetails objectForKey:@"email"]
                forKey:@"username"];
    [newUser setObject:[user objectForKey:@"access_token"]
                forKey:@"access_token"];
    
    [SPNUser setCurrentUser:newUser];

    
    [SPNUser welcomeUser:[userDetails objectForKey:@"email"]
               isNewUser:YES];
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                         |UIUserNotificationTypeSound
                                                                                         |UIUserNotificationTypeAlert) categories:nil];
    // We update the push notification token to include the user ID
    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    }

    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    NSLog(@"Success");
    [[[UIAlertView alloc] initWithTitle:@"Success!"
                                message:@"User registered succesfully!"
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
    
    
}

@end
