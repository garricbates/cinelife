//
//  SPNFilmSearchViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"  
#import "SPNFilterCell.h"

typedef NS_ENUM(NSInteger, SPNFilmType)
{
    SPNFilmTypeFilmsInRegion = 0,
    SPNFilmTypeFutureReleases
};
@interface SPNFilmSearchViewController : GAITrackedViewController

@end
