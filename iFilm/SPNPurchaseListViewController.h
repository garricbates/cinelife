//
//  SPNPurchaseListViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNPurchaseCell.h" 
#import "GAITrackedViewController.h"

@interface SPNPurchaseListViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property IBOutlet UITableView *purchaseListTableView;

@property (nonatomic) SPNPurchaseCell *prototypeCell;
@end
