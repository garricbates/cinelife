//
//  DataModel.h
//  iFilm
//
//  Created by Vlad Getman on 19.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FastEasyMapping.h"

@interface DataModel : NSObject

+ (NSArray *)fetchNotifications;

+ (NSArray *)mapNotifications:(id)json;

+ (FEMMapping *)notificationMapping;

@end
