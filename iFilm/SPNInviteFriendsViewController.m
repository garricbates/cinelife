//
//  SPNInviteFriendsViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNInviteFriendsViewController.h"
#import "SPNEmailInvitesViewController.h"


#import <TwitterKit/TwitterKit.h>


@interface SPNInviteFriendsViewController ()
{
    NSArray *inviteOptions;
    NSArray *emailContacts;
}
@end

@implementation SPNInviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;

    // Do any additional setup after loading the view.
    inviteOptions = [NSArray arrayWithObjects:@"Facebook", @"Google+", @"Twitter", @"Email", nil];
  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Select the media on which to send the invite";
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView*)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMedium]];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [inviteOptions count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *InviteCellIdentifier = @"InviteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:InviteCellIdentifier];
    [cell.textLabel setText:[inviteOptions objectAtIndex:indexPath.row]];
    [cell.textLabel setFont:[UIFont spn_NeutraMedium]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if  (indexPath.row < 1) {
        [self shareWithFacebook:nil];
    }
    else if (indexPath.row < 2) {
        [self shareWithGoogle:nil];
    }
    else if (indexPath.row < 3) {
        [self shareWithTwitter:nil];
    }
    else if (indexPath.row < 4) {
        emailContacts = [self getAllContacts];
        [self performSegueWithIdentifier:@"EmailInvites" sender:self];
    }
}

-(NSArray *)getAllContacts
{
    
    CFErrorRef *error = nil;
    
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        
#ifdef DEBUG
        NSLog(@"Fetching contact info ----> ");
#endif
        
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        NSMutableArray* items = [NSMutableArray arrayWithCapacity:nPeople];
        
        
        for (int i = 0; i < nPeople; i++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            //get First Name and Last Name
            NSString *firstNames = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            
            NSString *lastNames =  (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
            
            if (!firstNames) {
                firstNames = @"";
            }
            if (!lastNames) {
                lastNames = @"";
            }
            else {
                firstNames = [NSString stringWithFormat:@"%@ %@", firstNames, lastNames];
            }
            
            
            //get Contact email
            NSMutableArray *contactEmails = [NSMutableArray new];
            ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
            
            for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
                CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
                NSString *contactEmail = (__bridge NSString *)contactEmailRef;
                
                [contactEmails addObject:contactEmail];
                // NSLog(@"All emails are:%@", contactEmails);
                
            }

          
            
            if ([contactEmails count] > 0) {
                NSDictionary *contacts = [NSDictionary dictionaryWithObjects:@[firstNames, contactEmails]
                                                                     forKeys:@[@"name", @"emails"]];
                [items addObject:contacts];
            }
        
        }
        return items;
    }
    return nil;
}

#pragma mark - Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EmailInvites"]) {
        SPNEmailInvitesViewController *emailController = [segue destinationViewController];
        [emailController setPossibleContacts:emailContacts];
    }
}


#pragma mark - Invite 
-(void)shareWithTwitter:(id)sender
{
    // Twitter takes the attached file as a link for 23 characters. Max Tweet length is 140, so this leaves 117 possible characters to share, but we also add the link to the app, which takes 22 - 23 characters, leaving us roughly 95 characters for input. Finally, we'll add @Ripple to each post, so we remove another 7 characters, leaving us with 88.
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    
    if ([[store existingUserSessions] count] > 0) {
        [self shareToTwitter];
    }
    else {
        [[Twitter sharedInstance] logInWithCompletion:^
         (TWTRSession *session, NSError *error) {
             if (session) {
                 NSLog(@"signed in as %@", [session userName]);
                 [self shareToTwitter];
             } else {
                 NSLog(@"error: %@", [error localizedDescription]);
             }
         }];
    }
}

-(void)shareToTwitter
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:@"Hey guys! Check out the CineLife app I've been using! With CineLife, you can find films and theatres near you!"];
    [composer setURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/cinelife/id954894382?mt=8"]];
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
        }
        
    }];
    
}

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (!error) {
        [self shareToGoogle:[NSURL URLWithString:@"https://cinelife.com/"]];
    }
    else {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}


-(void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)shareWithGoogle:(id)sender
{
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] signInSilently];
    }
    else {
        [[GIDSignIn sharedInstance] signIn];
    }
}

#pragma mark - GPP Sign in delegate functions



-(void)shareToGoogle:(NSURL*)shareURL
{
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[shareURL absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)shareWithFacebook:(id)sender
{
    [self createShareDialog:nil];
}

-(void)createShareDialog:(NSNotification*)notification
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle = @"CineLife, the new app to find films, festivals and theatres near you!";
    content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/cinelife/id954894382?mt=8"];
    content.contentDescription = @"Hey guys! Check out this new app I've been using! With CineLife, you can find films, theatres and festivals near you, plus you can get the latest news for the independent film community. Why don't you give it a try?";
    
    [FBSDKShareDialog  showFromViewController:self
                                  withContent:content
                                     delegate:self];
}


-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"succeeded");
}

-(void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    
    NSLog(@"cancel");
}

-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Error sharing to Facebook" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

@end
