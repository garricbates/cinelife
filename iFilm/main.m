//
//  main.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SPNAppDelegate class]));
    }
}
