//
//  SPNFilterOptionCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNFilterOptionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *checkPartnersButton;
@property (nonatomic, weak) IBOutlet UIButton *checkNearbyButton;
@property (nonatomic, weak) IBOutlet UIButton *filterButton;

@property (nonatomic, weak) IBOutlet UISearchBar *zipPicker;

@property (nonatomic, weak) IBOutlet UIButton *checkPartnersContainerButton;
@property (nonatomic, weak) IBOutlet UIButton *checkNearbyContainerButton;

@property (nonatomic, weak) IBOutlet UIView *searchBarBackgroundView;

@end
