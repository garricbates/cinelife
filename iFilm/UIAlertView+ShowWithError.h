//
//  UIAlertView+ShowWithError.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SPNAlertViewErrorCode) {
    SPNAlertViewErrorCodeNoTrailer = 0,
    // User Account Codes
    SPNAlertViewErrorCodeUserNotLoggedIn,
    SPNAlertViewErrorCodeUserLoginAttempt,
    SPNAlertViewErrorCodeUserPasswordIncorrect,
    SPNAlertViewErrorCodeUserForgotPassword,
    SPNAlertViewErrorCodeUserPasswordSent,
    SPNAlertViewErrorCodeUserBadPasswordRecover,
    SPNAlertViewErrorCodeUserSignInFieldsRequired,
    SPNAlertViewErrorCodeUserSignInPasswordMismatch,
    SPNAlertViewErrorCodeUserSignInBadRegister,
    SPNAlertViewErrorCodeUserNoFavoriteTheatres,
    //Twitter Error codes
    SPNAlertViewErrorCodeUserTwitterNotFound,
    SPNAlertViewErrorCodeUserTwitterDenied,
    SPNAlertViewErrorCodeUserTwitterSync,
    //Facebook Error Codes
    SPNAlertViewErrorCodeUserFacebookNotFound,
    //Google Error Codes
    SPNAlertViewErrorCodeUserGooglePlusNotFound,
    SPNAlertViewErrorCodeUserGooglePlusBadUser,
    //GoWatchItErrorCodes
    SPNAlertViewErrorCodeGoWatchItGetDetails,
    SPNAlertViewErrorCodeGoWatchItNoQueue,
    SPNAlertViewErrorCodeGoWatchItNoToken,
    SPNAlertViewErrorCodeGoWatchItFailMovieId,
    SPNAlertViewErrorCodeGoWatchItMovieNotAdded,
    SPNAlertViewErrorCodeGoWatchItMovieAdded,
    // Theatre Error Codes
    SPNAlertViewErrorCodeNoSavedLocation,
    SPNAlertViewErrorCodeFavoriteNotAdded,
    SPNAlertViewErrorCodeFavoriteNotDeleted,
    // Review Error Code
    SPNAlertViewErrorCodeReviewCommentMissing,
    SPNAlertViewErrorCodeReviewRatingMissing,
    SPNAlertViewErrorCodeReviewReviewNotAdded,
    SPNAlertViewErrorCodeReviewReviewAdded
};


@interface UIAlertView (ShowWithError)

+(void)showAlertViewForError:(SPNAlertViewErrorCode)errorCode;

+(void)showAlertViewForError:(SPNAlertViewErrorCode)errorCode
                withDelegate:(id)delegate
                forAlertType:(UIAlertViewStyle)style
             withCancelTitle:(NSString*)cancelTitle
             withOtherTitles:(NSString*)otherTitle;

+(void)showAlertViewWthTitle:(NSString*)title
                  andMessage:(NSString*)message;
@end
