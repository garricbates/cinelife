//
//  SPNIndieRSSFullListViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNTheatreNewsfeedCell.h"
#import "GAITrackedViewController.h"

@interface SPNIndieRSSFullListViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property NSArray *postFeed;
@property NSString *feedName;

@property IBOutlet UITableView* rssPostsTableView;
@property (nonatomic) SPNTheatreNewsfeedCell *prototypeCell;
@end
