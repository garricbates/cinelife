//
//  SPNShowMoreCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNShowMoreCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *showMoreButton;

@end
