//
//  SPNPosterViewController.m
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPosterViewController.h"

@interface SPNPosterViewController ()

@property (nonatomic, weak) IBOutlet UIView *backgroundView;
@property (nonatomic, weak) IBOutlet AsyncImageView *imageView;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) NSString *url;

@end

@implementation SPNPosterViewController

+ (void)presentWithURL:(NSString *)url {
    SPNPosterViewController *controller = [[UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SPNPosterViewController"];
    controller.url = url;
    controller.providesPresentationContextTransitionStyle = YES;
    controller.definesPresentationContext = YES;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [[UIViewController top] presentViewController:controller animated:NO completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageView.image = [UIImage imageNamed:@"poster-not-available"];
    [self.imageView setImageURL:[NSURL URLWithString:self.url]];
    
    [self show:NO animted:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self show:YES animted:YES];
}

- (IBAction)closeAction {
    [self show:NO animted:YES];
}

- (void)show:(BOOL)show animted:(BOOL)animated {
    void (^backgroundAnimation)(void) = ^() {
        _backgroundView.alpha = show ? 1 : 0;
    };
    void (^contentAnimation)(void) = ^() {
        _imageView.transform = show ? CGAffineTransformIdentity : CGAffineTransformMakeScale(0.001, 0.001);
        _closeButton.transform = show ? CGAffineTransformIdentity : CGAffineTransformMakeTranslation(0, 100);
    };
    
    if (animated) {
        [UIView animateWithDuration:0.3
                              delay:show ? 0 : 0.2
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:backgroundAnimation
                         completion:^(BOOL finished) {
                             if (!show && animated) {
                                 [self dismissViewControllerAnimated:NO completion:nil];
                             }
                         }];
        [UIView animateWithDuration:0.3
                              delay:show ? 0.3 : 0.0
             usingSpringWithDamping:0.5
              initialSpringVelocity:0.1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:contentAnimation
                         completion:nil];
        
    } else {
        backgroundAnimation();
        contentAnimation();
    }
}

@end
