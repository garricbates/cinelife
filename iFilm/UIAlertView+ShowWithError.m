//
//  UIAlertView+ShowWithError.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "UIAlertView+ShowWithError.h"

@implementation UIAlertView (ShowWithError)

+(void)showAlertViewForError:(SPNAlertViewErrorCode)errorCode
{
    [self showAlertViewForError:errorCode
                   withDelegate:nil
                   forAlertType:UIAlertViewStyleDefault
                withCancelTitle:@"Ok"
                withOtherTitles:nil];
}

+(void)showAlertViewForError:(SPNAlertViewErrorCode)errorCode withDelegate:(id)delegate forAlertType:(UIAlertViewStyle)style withCancelTitle:(NSString *)cancelTitle withOtherTitles:(NSString *)otherTitle
{
    NSString *title;
    NSString *message;
    switch (errorCode) {
        case SPNAlertViewErrorCodeNoTrailer:
            title = @"Trailer not available";
            message = @"No trailers available for this film";
            break;
        case SPNAlertViewErrorCodeNoSavedLocation:
            title = @"Location not available";
            message = @"You have not stored a preferred location yet";
            break;
        case SPNAlertViewErrorCodeFavoriteNotAdded:
            title = @"Favorite not added";
            message = @"There was an error while saving the theatre to your favorites";
            break;
        case SPNAlertViewErrorCodeFavoriteNotDeleted:
            title = @"Favorite not deleted";
            message = @"There was an error while removing the theatre to your favorites";
            break;
        case SPNAlertViewErrorCodeUserForgotPassword:
            title = @"Restore password";
            message = @"Please enter your email";
            break;
        case SPNAlertViewErrorCodeUserPasswordIncorrect:
            title = @"Login error";
            message = @"Wrong email or password";
            break;
        case SPNAlertViewErrorCodeUserPasswordSent:
            title  = @"Email sent";
            message = @"An email with your new password has been sent to your account.";
            break;
        case SPNAlertViewErrorCodeUserBadPasswordRecover:
            title = @"Password recovery error";
            message = @"Something went wrong, please try again later";
            break;
        case SPNAlertViewErrorCodeUserSignInFieldsRequired:
            title = @"Sign up error";
            message = @"All fields are required";
            break;
        case SPNAlertViewErrorCodeUserSignInPasswordMismatch:
            title = @"Sign up error";
            message = @"The passwords do not match";
            break;
        case SPNAlertViewErrorCodeUserSignInBadRegister:
            title = @"Sign up error";
            message = @"Something went wrong, please try again later";
            break;
        case SPNAlertViewErrorCodeUserLoginAttempt:
            title = @"Log in";
            message = @"Please enter your email and password";
            break;
        case SPNAlertViewErrorCodeUserNotLoggedIn:
            title = @"Sign in to CineLife";
            message = @"Sign in to manage your favorite theatres, rate and track movies, and more.";
            break;
        case SPNAlertViewErrorCodeUserTwitterNotFound:
            title = @"Twitter Account error";
            message = @"Twitter account not found";
            break;
        case SPNAlertViewErrorCodeUserTwitterDenied:
            title = @"Twitter Account error";
            message = @"Access to Twitter accounts was denied";
            break;
        case SPNAlertViewErrorCodeUserTwitterSync:
            title = @"Twitter Account validation";
            message = @"Enter an email address to validate your account";
            break;
        case SPNAlertViewErrorCodeUserFacebookNotFound:
            title = @"Facebook Account error";
            message = @"Could not connect to Facebook";
            break;
        case SPNAlertViewErrorCodeUserGooglePlusNotFound:
            title = @"Google+ Account error";
            message = @"Could not connect to Google+";
            break;
        case SPNAlertViewErrorCodeUserGooglePlusBadUser:
            title = @"Google+ Account error";
            message = @"Error while fetching Google+ user's data";
            break;
        case SPNAlertViewErrorCodeGoWatchItGetDetails:
            title = @"Link with GoWatchIt";
            message = @"Please enter your GoWatchIt! email and password";
            break;
        case SPNAlertViewErrorCodeGoWatchItNoQueue:
            title = @"Favorite Films";
            message = @"Keep track of all the films you’re interested in on one list. Click on the heart icon next to the film’s title to add it to the watch list.";
            break;
        case SPNAlertViewErrorCodeGoWatchItNoToken:
            title = @"Error";
            message = @"We could not connect to your account, please try again later";
            break;
        case SPNAlertViewErrorCodeGoWatchItFailMovieId:
            title = @"Error";
            message = @"This movie was not found in the database";
            break;
        case SPNAlertViewErrorCodeGoWatchItMovieNotAdded:
            title = @"Error";
            message = @"We could not add the film to your list, please try again later";
            break;
        case SPNAlertViewErrorCodeGoWatchItMovieAdded:
            title = @"Success";
            message = @"The film was added to your watch list.";
            break;
        case SPNAlertViewErrorCodeReviewCommentMissing:
            title = @"Wait";
            message = @"Would you like to write a review for this film?";
            break;
        case SPNAlertViewErrorCodeReviewRatingMissing:
            title = @"Wait";
            message = @"Would you like to rate this film?";
            break;
        case SPNAlertViewErrorCodeReviewReviewAdded:
            title = @"Review posted";
            message = @"You have added the review, everyone can read your review now";
            break;
        case SPNAlertViewErrorCodeReviewReviewNotAdded:
            title = @"Review not posted";
            message = @"Something went wrong, please try again later";
            break;
        case SPNAlertViewErrorCodeUserNoFavoriteTheatres:
            title = @"Wait";
            message = @"You should add some theatres to your list first. Give it a try!";
            break;
        default:
            break;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:delegate
                                              cancelButtonTitle:cancelTitle
                                              otherButtonTitles:otherTitle, nil];
    [alertView setAlertViewStyle:style];
    [alertView show];
}

+(void)showAlertViewWthTitle:(NSString*)title
                  andMessage:(NSString*)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

@end
