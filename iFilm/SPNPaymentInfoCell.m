//
//  SPNPaymentInfoCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPaymentInfoCell.h"

@implementation SPNPaymentInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIImageView *spacerView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 30, 10)];
    [spacerView setImage:[UIImage imageNamed:@"miniCard"]];
    [spacerView setContentMode:UIViewContentModeScaleAspectFit];
    //[spacerView setBackgroundColor:[UIColor lightGrayColor]];
    
    [_cardNumberTextField setLeftView:spacerView];
    [_cardNumberTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    UIImageView *spacer2View = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 30, 10)];
    [spacer2View setImage:[UIImage imageNamed:@"securityIcon"]];
      [spacer2View setContentMode:UIViewContentModeScaleAspectFit];
  //  [spacer2View setBackgroundColor:[UIColor lightGrayColor]];
    
    [_cardCVCTextField setLeftView:spacer2View];
    [_cardCVCTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    UIImageView *spacer3View = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 30, 10)];
    [spacer3View setImage:[UIImage imageNamed:@"calendarIcon"]];
      [spacer3View setContentMode:UIViewContentModeScaleAspectFit];
  //  [spacer3View setBackgroundColor:[UIColor lightGrayColor]];
    
    [_cardExpirationDateTextField setLeftView:spacer3View];
    [_cardExpirationDateTextField setLeftViewMode:UITextFieldViewModeAlways];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
