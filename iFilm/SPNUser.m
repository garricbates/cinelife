//
//  SPNUser.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNUser.h"

@implementation SPNUser

+(BOOL)recoverPasswordForUserEmail:(NSString*)email
{
    NSString *url= [NSString stringWithFormat:@"%@/passwordRequest?email=%@", apiServer, email];
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *result = [SPNServerQueries queryURL:urlQuery];
    
    NSNumber *success = [result objectForKey:@"success"];
    if (success.intValue < 1) {
        return NO;
    }
    else {
        return YES;
    }
}

+(void)setCurrentUser:(NSDictionary*)userInfoDictionary
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
  /*
    NSDictionary* user = [userInfoDictionary objectForKey:@"user"];
    NSString* uid = [userInfoDictionary objectForKey:@"uid"];
    NSString* email = [userInfoDictionary objectForKey:@"email"];
    
    NSString* lastname = [user objectForKey:@"lastname"];
    NSString* firstname = [user objectForKey:@"firstname"];
    NSString* fullname = firstname;
    
    if (lastname) {
        fullname = [NSString stringWithFormat:@"%@ %@", fullname, lastname];
    }
    
    [preferences setObject:fullname forKey:@"fullname"];
    [preferences setObject:uid forKey:@"currentUser"];
    [preferences setObject:email forKey:@"email"];
   */
    [preferences setObject:userInfoDictionary forKey:@"currentUser"];
    [preferences synchronize];
}

+(void)logOutCurrentUser
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences removeObjectForKey:@"currentUser"];
    [preferences removeObjectForKey:@"fullname"];
    [preferences removeObjectForKey:@"email"];
    [preferences synchronize];
}

+(BOOL)userLoggedIn
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* user =[prefs objectForKey:@"currentUser"];
    return user ? YES : NO;
}

+(NSString*)getCurrentUserFullName
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* user =[[prefs objectForKey:@"currentUser"] objectForKey:@"username"];
    return user;
}

+(NSDictionary*)getCurrentUserId
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary* user = [prefs objectForKey:@"currentUser"];
    return user;
}

+(NSDictionary*)addTheatreToFavourites:(NSDictionary*)theatre forUser:(NSString*)username
{
    NSDictionary *favouriteStatusInformation = nil;
    NSString* theatreId = [theatre objectForKey:@"theatreId"];
    NSString *url= [NSString stringWithFormat:@"%@/addTheatreToFavourites?uid=%@&theaterId=%@", apiServer, username, theatreId];
    
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *result = [SPNServerQueries queryURL:urlQuery];
    NSNumber *success = [result objectForKey:@"success"];
    if (success.intValue < 1) {
        // Show error
        NSDictionary *returning = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"error_msg"], @"error_msg", nil];
        favouriteStatusInformation = returning;
    }
    else {
        favouriteStatusInformation = result;
    }
    return favouriteStatusInformation;
}

+(NSDictionary*)removeFavouriteTheatre:(NSDictionary*)theatre forUser:(NSString*)username
{
    NSDictionary *favouriteStatusInformation = nil;
    NSString *theatreId = [theatre objectForKey:@"theatreId"];
    NSString *url= [NSString stringWithFormat:@"%@/removeTheater?uid=%@&theaterId=%@", apiServer, username, theatreId];
    NSURL *urlQuery = [NSURL URLWithString:url];
    NSDictionary *result = [SPNServerQueries queryURL:urlQuery];
    
    NSNumber *success = [result objectForKey:@"success"];
    if (success.intValue < 1) {
        // Show error
        NSDictionary *returning = [[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"error_msg"], @"error_msg", nil];
        favouriteStatusInformation = returning;
    }
    else {
        favouriteStatusInformation = result;
    }
    return favouriteStatusInformation;
}

+(NSMutableDictionary*)getListOfFavouriteTheatresForUser:(NSString*)username
{
    NSMutableDictionary *favouriteList = nil;
    NSString *firstURL =[NSString stringWithFormat:@"%@/getFavouriteTheatreIdsForUser?uid=%@", apiServer, username];
    NSURL *urlQuery = [NSURL URLWithString:firstURL];
    
    id result = [SPNServerQueries queryURL:urlQuery];
    if (result && [result isKindOfClass:[NSArray class]]) {
        NSMutableDictionary *idsOfTheatres = [NSMutableDictionary new];
        for (NSString* theatreId in result)
        {
            [idsOfTheatres setObject:@"1" forKey:theatreId];
        }
        favouriteList = idsOfTheatres;
    }
    return favouriteList;
}

+(NSMutableArray *)sortTheatersByDistance:(NSArray*)theaterList withLocation:(CLLocation*)userLocation
{
    NSMutableArray *sortedTheatres = [NSMutableArray arrayWithArray:theaterList];
    double lat, lon;
    if (userLocation)
    {
        for (NSMutableDictionary *theater in sortedTheatres)
        {
            if (![theater objectForKey:@"distance"]) {
                NSDictionary *geoCode = [theater objectForKey:@"geolocation"];
                NSString *latString =[geoCode objectForKey:@"lat"];
                NSString *lonString =[geoCode objectForKey:@"lon"];
                if (!latString || !lonString) {
                    [theater setObject:@0 forKey:@"distance"];
                }
                else
                {
                    lat = [latString doubleValue];
                    lon = [lonString doubleValue];
                    
                    CLLocation *theatreDistance = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
                    CLLocationDistance currentDistance = [theatreDistance distanceFromLocation:userLocation];
                    currentDistance = currentDistance / 1000 * 0.621371192; // Convert meters into miles
                    
                    [theater setObject:[NSNumber numberWithDouble:currentDistance] forKey:@"distance"];
                }
            }
            else {
                NSNumber *newDist = [NSNumber numberWithFloat:[[theater objectForKey:@"distance"] floatValue]];
                [theater setObject:newDist
                            forKey:@"distance"];
            }
        }
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        sortedTheatres = [[sortedTheatres sortedArrayUsingDescriptors:@[sorter]] mutableCopy];
    }
    return sortedTheatres;
}

+(NSArray*)getTheatresOfUser:(NSString*)username withUserLocation:(CLLocation *)userLocation

{
    NSArray *favouriteStatusInformation = nil;
    NSString *url= [NSString stringWithFormat:@"%@/getFavouriteTheatresForUserId?uid=%@", apiServer, username];
    NSURL *urlQuery = [NSURL URLWithString:url];
    id result = [SPNServerQueries queryURL:urlQuery];
    
    if (result && [result isKindOfClass:[NSArray class]]) {
        favouriteStatusInformation = result;
        favouriteStatusInformation = [self sortTheatersByDistance:favouriteStatusInformation withLocation:userLocation];
    }
    return favouriteStatusInformation;
    
}

+(BOOL)userHasTwitterEmail:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* user =[prefs objectForKey:username];
    return user ? YES : NO;
}

+(void)setTwitterEmail:(NSString*)email forUser:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:email
              forKey:username];
    
    [prefs synchronize];
}

+(NSString*)getTwitterEmailForUser:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* user =[prefs objectForKey:username];
    return user;
}

+(BOOL)userHasGoWatchItEmail:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary* goWatchItAccount = [prefs objectForKey:username];
    return goWatchItAccount ? YES : NO;
}

+(void)setGoWatchItEmail:(NSString*)email andPassword:(NSString*)password forUser:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary *goWatchItAccount = [NSDictionary dictionaryWithObjects:@[email, password]
                                                                 forKeys:@[@"email", @"password"]];
    [prefs setObject:goWatchItAccount
              forKey:username];
    
    [prefs synchronize];
}

+(NSString*)getGoWatchItEmailForUser:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary* goWatchItAccount = [prefs objectForKey:username];
    NSString *email = [goWatchItAccount objectForKey:@"email"];
    return email;
}

+(NSDictionary*)getGoWatchItDataForUser:(NSString*)username
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDictionary* goWatchItAccount = [prefs objectForKey:username];
    return goWatchItAccount;
}

+(NSString*)getGoWatchItAuthTokenForEmailForUser:(NSString*)username
{
    NSDictionary *goWatchItAccount = [self getGoWatchItDataForUser:username];
    NSString *email = [goWatchItAccount objectForKey:@"email"];
    NSString *password = [goWatchItAccount objectForKey:@"password"];
    NSString* authToken = [self getGoWatchItAuthTokenForEmail:email
                                                  andPassword:password
                                                      forUser:username];
    return authToken;
}

+(NSString*)getGoWatchItAuthTokenForEmail:(NSString*)email andPassword:(NSString*)password forUser:(NSString*)username
{
    NSString* authToken = nil;
    NSString *postParams = [NSString stringWithFormat:@"api_key=b07296a432edba66830b1984&email=%@&password=%@",email, password];
    NSData *postData = [postParams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:@"http://api.gowatchit.com/api/v2/session/with_password"]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSError *error = nil;
    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData:data
                                         options:kNilOptions
                                           error:&error];
        
        if (json) {
            NSDictionary *session = [json objectForKey:@"session"];
            if (session) {
                NSString *message = [session objectForKey:@"message"];
                
                if ([message isEqualToString:@"Wrong username or password"]) {
                    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserPasswordIncorrect];
                    return nil;
                }
                else {
                  // Store GWI data to disk
                    [self setGoWatchItEmail:email
                                andPassword:password
                                    forUser:username];
                }
            }
            
            NSString *preHashedToken = [session objectForKey:@"authentication_token"];
            authToken = [self sha1:preHashedToken];
        }
    }
    return authToken;
}

+ (NSString *)sha1:(NSString*)auth_token
{
    NSData *data = [auth_token dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (int) data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

+(NSArray*)getUserGWIQueue:(NSString*)username withAuthenticationToken:(NSString*)authentication_token
{
    NSArray *films = nil;
    NSString *email = [self getGoWatchItEmailForUser:username];
    
    NSString *postParams = [NSString stringWithFormat:@"api_key=b07296a432edba66830b1984&authentication_token=%@&email=%@",authentication_token, email];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.gowatchit.com/api/v2/user/queue?%@",postParams]]];
    NSError *error = nil;
    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        //success
        id json = [NSJSONSerialization
                   JSONObjectWithData:data
                   options:kNilOptions
                   error:&error];
        
        if (json) {
            NSLog(@"Queue: %@", json);
            NSDictionary *queue = [json objectForKey:@"queue"];
            NSArray *queuedFilms = [queue objectForKey:@"movies"];
            films = queuedFilms;
        }
    }
    return films;
    
}

+(NSString*)getMoiveGWIId:(NSString*)title
{
    NSString *goWatchItId = nil;
    NSString *newTitle = [title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    newTitle = [newTitle stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
    NSString *postParams = [NSString stringWithFormat:@"%@?api_key=b07296a432edba66830b1984",newTitle];
 
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    NSString *stringURL = [NSString stringWithFormat:@"http://api.gowatchit.com/api/v2/search/ranked/%@",postParams];
    NSURL *urlQuery = [NSURL URLWithString:stringURL];
    [request setURL:urlQuery];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        //success
        id possibleFilms = [NSJSONSerialization
                            JSONObjectWithData:data
                                       options:kNilOptions
                                         error:&error];
        
        if (possibleFilms) {
            // In case multiple movies are returned
            if ([possibleFilms isKindOfClass:[NSMutableArray class]]) {
                NSDictionary *chosenFilm = [possibleFilms firstObject];
                NSDictionary *error = [chosenFilm objectForKey:@"error"];
                
                if (error) {
                    NSString *message = [error objectForKey:@"message"];
                    if (message) {
                        //GWI error message
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                        });
                    }
                }
                goWatchItId = [chosenFilm objectForKey:@"id"];
            }
            else {
            // In case one movie is returned
                goWatchItId =  [possibleFilms objectForKey:@"id"];
            }
        }
    }
    return goWatchItId;
}

+(BOOL)addMovietoGWIQueue:(NSString*)username forMovie:(NSString*)movieId withAuthenticationToken:(NSString*)authentication_token
{
    BOOL didAddMovie = NO;
    NSString *email = [self getGoWatchItEmailForUser:username];
    NSString *postParams = [NSString stringWithFormat:@"api_key=b07296a432edba66830b1984&movie_id=%@&authentication_token=%@&email=%@",movieId, authentication_token, email];
    
    NSData *postData = [postParams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:@"http://api.gowatchit.com/api/v2/user/queue"]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSError *error = nil;
    NSURLResponse *response = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSLog(@"Error:%@", error.localizedDescription);
    }
    else {
        //success
        id json = [NSJSONSerialization
                   JSONObjectWithData:data
                   options:kNilOptions
                   error:&error];
        
        didAddMovie = json ? YES : NO;
    }
    return didAddMovie;
}

+(void)welcomeUser:(NSString*)username isNewUser:(BOOL)isNewUser
{
    NSString *message = [NSString stringWithFormat:@"Welcome back, %@", username];
    if (isNewUser) {
        message = [NSString stringWithFormat:@"Welcome to CineLife, %@", username];
    }
    [[[UIAlertView alloc] initWithTitle:@"Welcome"
                                message:message
                               delegate:nil
                      cancelButtonTitle:@"Thanks!"
                      otherButtonTitles:nil] show];
}


@end
