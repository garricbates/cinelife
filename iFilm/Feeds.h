//
//  Feeds.h
//  iFilm
//
//  Created by Vlad Getman on 20.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSInteger const NewsLimit = 2;

@interface Feeds : NSObject

- (instancetype)initWithFeeds:(NSArray *)feeds posts:(NSArray *)posts;

@property (nonatomic, strong) NSArray *socials;
@property (nonatomic, strong) NSArray *news;
@property (nonatomic, strong) NSArray *htmlNews;

- (NSAttributedString *)htmlAtIndex:(NSInteger)index;

- (BOOL)hasRSS;

- (void)update;

- (void)postsForItem:(NSDictionary *)item completion:(void(^)(NSArray *posts))completion;
- (void)postsForSocialsWithCompletion:(void(^)(NSArray *posts))completion;
- (void)storiesWithPosts:(NSArray *)posts
       shouldFetchRSSToo:(NSInteger)shouldFetchRSSToo
              completion:(void(^)(NSArray *posts))completion;

@end
