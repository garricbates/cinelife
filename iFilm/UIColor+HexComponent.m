//
//  UIColor+HexComponent.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "UIColor+HexComponent.h"

@implementation UIColor (HexComponent)

+(UIColor*)colorFromHexString:(NSString*)color
{
    color = [color stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if ([color length] < 6) {
        NSString *pad = [NSString new];
        pad = [pad stringByPaddingToLength:6-[color length] withString:@"0" startingAtIndex:0];
        color = [pad stringByAppendingString:color];
    }
    
    CGFloat red, green, blue, alpha;
    alpha = 1.0f;
    red   = [self colorComponentFrom: color start: 0 length: 2];
    green = [self colorComponentFrom: color start: 2 length: 2];
    blue  = [self colorComponentFrom: color start: 4 length: 2];
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(CGFloat) colorComponentFrom:(NSString *)hexString
                        start:(NSUInteger)start
                       length:(NSUInteger)length
{
    NSString *substring = [hexString substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    NSScanner *scanner = [NSScanner scannerWithString:fullHex];
    [scanner scanHexInt:&hexComponent];
    return hexComponent / 255.0;
}


+ (UIColor*)spn_notSoLightBrownColor {
    return [UIColor colorWithRed:227.0/255.0 green:221.0/255.0 blue:208.0/255.0 alpha:1];
}

+ (UIColor*)spn_lightBrownColor {
    return [UIColor colorWithRed:234.0/255.0 green:228.0/255.0 blue:214.0/255.0 alpha:1];
}

+ (UIColor*)spn_brownColor {
    return [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:178.0/255.0 alpha:1];
}

+ (UIColor*)spn_darkBrownColor {
    return [UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:148.0/255.0 alpha:1];
}

+ (UIColor*)spn_aquaColor {
    return [UIColor colorWithRed:20.0/255.0 green:150.0/255.0 blue:118.0/255.0 alpha:1];
}

+(UIColor *)spn_aquaShadowColor
{
    return [UIColor colorWithRed:32/255.0 green:120/255.0 blue:96/255.0 alpha:1.0];
}

+ (UIColor*)spn_darkGrayColor {
    return [UIColor colorWithRed:31.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1];
}

+(UIColor *)spn_buttonColor
{
    return [UIColor colorWithRed:163.0/255.0 green:177.0/255.0 blue:145.0/255.0 alpha:1];
}

+(UIColor *)spn_buttonShadowColor
{
    return [UIColor colorWithRed:148.0/255.0 green:160.0/255.0 blue:134.0/255.0 alpha:1];
}
+(UIColor *)fb_blueColor
{
    return [UIColor colorWithRed:0.0/255.0 green:93.0/255.0 blue:199.0/255.0 alpha:1];
}
+(UIColor *)tw_blueColor
{
    return [UIColor colorWithRed:145.0/255.0 green:229.0/255.0 blue:251.0/255.0 alpha:1];
}
+(UIColor *)rss_orangeColor
{
    return [UIColor colorWithRed:233.0/255.0 green:159.0/255.0 blue:0.0/255.0 alpha:1];
}

+(UIColor *)mta_greenColor {
    return [UIColor colorWithRed:102.0/255.0 green:204.0/255.0 blue:51.0/255.0 alpha:1];
}
+(UIColor *)mta_yellowColor {
    return [UIColor colorWithRed:255.0/255.0 green:204.0/255.0 blue:51.0/255.0 alpha:1];
}
+(UIColor *)mta_redColor {
    return [UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1];
}

@end
