//
//  Users.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Users : NSManagedObject

@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSSet *myFilms;
@end

@interface Users (CoreDataGeneratedAccessors)

- (void)addMyFilmsObject:(Films *)value;
- (void)removeMyFilmsObject:(Films *)value;
- (void)addMyFilms:(NSSet *)values;
- (void)removeMyFilms:(NSSet *)values;

@end
