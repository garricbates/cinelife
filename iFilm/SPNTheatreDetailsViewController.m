//
//  SPNTheatreDetailsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

//TODO :DATE RESETS AFTER CLOSING/OPENING DATE PICKER CELL
#import "SPNTheatreDetailsViewController.h"
#import "SPNTheatreNewsViewController.h"
#import "SPNFilmDetailsViewController.h"
#import "SPNTicketPurchaseViewController.h"
#import "SPNTicketSelectionViewController.h"
#import "SPNTheatreNewsDetailsViewController.h"

#import "SPNIndieHeaderCell.h"
#import "SPNPickerCell.h"
#import "SPNMovieShowtimeCell.h"
#import "SPNSearchOptionCell.h"
#import "SPNDismissCell.h"
#import "SPNDateCell.h"
#import "Feeds.h"

static NSString *NewsCellIdentifier = @"TheatreNewsCell";
static NSString *CellIdentifier = @"MovieShowtimeCell";

@interface SPNTheatreDetailsViewController ()
{
    UIColor *mainColor, *cellColor, *btnColor, *showColor, *sectionColor, *titleSectionColor, *titleCellColor, *theatreColor, *amenitiesColor, *infoTextColor, *infoTextBackground;
    NSDateFormatter *datePickerFormatter;
    NSDictionary *selectedNewsPostDetails;
    NSDateFormatter *dateFormatter;
    NSString *selectedNewsRSSLink;
    UIButton *changeDateBtn;
    BOOL isFetchingComingSoon;
    NSInteger shouldFetchRSSToo;

    NSMutableDictionary *brandingElements;
    
    NSMutableDictionary *selectedFilmAndShowtimes;
    
    NSDictionary *movieForTicketing, *movieTicketingInformation, *showTicketingInformation;
    
    NSArray *showtimesOfMovieForTicketing, *selectedNewsForTheatre;
    BOOL shouldInitializeKeyboard, shouldShowDismissCell, shouldShowDateCell;
    
    UIDatePicker *datePicking;
    UIToolbar* keyboardDoneButtonViewDate;
    // Prototype cells to create dynamic height
    
    NSInteger selectedDateForPicker;
    NSDateFormatter *formatter;
    
    BOOL isFetchingMovieInfoForTicketing;
    
    Feeds *feeds;
}

@end

@implementation SPNTheatreDetailsViewController

-(SPNMovieShowtimeCell *)prototypeShowCell
{
    if (!_prototypeShowCell) {
        _prototypeShowCell = (SPNMovieShowtimeCell*)[self.theatreShowtimesTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    return _prototypeShowCell;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.theatreShowtimesTableView registerCellClass:[SPNIndieHeaderCell class]
                                       withIdentifier:@"HeaderCell"];
    [self.theatreShowtimesTableView registerCellClass:[SPNTheatreNewsfeedCell class]
                                       withIdentifier:NewsCellIdentifier];
    
    self.showtimeInformation = [SPNCinelifeQueries sortShowdatesForOneTheatre:[self.theatreInformation objectForKey:@"movies"]];
    
    BOOL showErrorMessage = YES;
    for (NSDate *date in [self.showtimeInformation allKeys]) {
        NSArray *showList = [self.showtimeInformation objectForKey:date];
        if ([showList count] > 0) {
            showErrorMessage = NO;
            break;
        }
    }
    if (!showErrorMessage) {
        [self.theatreShowtimesTableView setTableFooterView:[UIView new]];
        [self.theatreShowtimesTableView setBackgroundColor:[UIColor spn_darkBrownColor]];
    }
    else {
        [self.theatreShowtimesTableView setBackgroundColor:[UIColor spn_lightBrownColor]];
    }
    formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [formatter setDateFormat:@"EEEE \r\n MMMM d"];
    
    selectedDateForPicker = 0;
    datePicking = [[UIDatePicker alloc]init];
    datePickerFormatter = [NSDateFormatter new];
    [datePickerFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [datePickerFormatter setDateStyle:NSDateFormatterFullStyle];
    [datePickerFormatter setTimeStyle:NSDateFormatterNoStyle];
    self.screenName = @"Theatre Details";
    [self customizeUI];
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    
    brandingElements = [NSMutableDictionary new];
    
    shouldInitializeKeyboard = YES;
    // Do any additional setup after loading the view.
    [self initializeTheatreData:self.theatreInformation];
    
    selectedNewsForTheatre = [NSArray new];
    
    feeds = [[Feeds alloc] initWithFeeds:[self.theatreInformation objectForKey:@"feeds"]
                                   posts:[self.theatreInformation objectForKey:@"posts"]];
    
    [self initializeKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePostsAndsSchedulesForNotification:)
                                                 name:@"refreshShowtimes"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addedTheaterToFavorites:)
                                                 name:kNotificationAddedTheaterToFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedTheaterFromFavorites:)
                                                 name:kNotificationRemovedTheaterFromFavorites
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDictionary *branding = [self.theatreInformation objectForKey:@"branding"];
    CGFloat spaceForLogo = 0.0;
    
    [self.view layoutIfNeeded];
    CGRect frame = [self.theatreInformationContainerView frame];
    
    UIView *header = self.theatreShowtimesTableView.tableHeaderView;
    CGRect headerFrame           = header.frame;
    if (branding) {
         NSString *logo = [branding objectForKey:@"logo"];
        if (logo) {
            spaceForLogo = 134.0;
        }
        if (mainColor) {
            [header setBackgroundColor:mainColor];
        }
        
    }
    headerFrame.size.height      = frame.size.height +spaceForLogo;
    header.frame            = headerFrame;

    [self.theatreShowtimesTableView setTableHeaderView:header];
    [self.theatreShowtimesTableView beginUpdates];
    [self.theatreShowtimesTableView endUpdates];
    
}

-(void)updatePostsAndsSchedulesForNotification:(id)sender
{
    NSLog(@"");
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] getTheaterInformation:[self.theatreInformation objectForKey:@"id"]
                                               withCredentials:[SPNUser getCurrentUserId]];
    [self initializeTheatreData:self.theatreInformation];
    
    selectedNewsForTheatre = [NSArray new];
    
    [feeds update];
}

#pragma mark - Customize UI 
-(void)customizeUI
{
    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.addressLabel setFont:[UIFont spn_NeutraMedium]];
    [self.phoneTextView setFont:[UIFont spn_NeutraMedium]];
}

#pragma mark - Table View Functions

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"HeaderCell";
    SPNIndieHeaderCell *header = [tableView dequeueReusableCellWithIdentifier:HeaderIdentifier];
    
    switch (section) {
        case SPNTheatreSectionFeed:
            [header.titleLabel setText:@"Social News"];
            break;
        case SPNTheatreSectionNews:
            [header.titleLabel setText:@"News and Promotions"];
            break;
        default:
            [header.titleLabel setText:@"Showtimes"];
            break;
    }
    
    if (section > 1) {
        if ([self.theatreInformation objectForKey:@"branding"]) {
            [header.seeAllButton setTag:section];
            [header.seeAllButton setTitle:@"COMING SOON >" forState:UIControlStateNormal];
            
            [header.seeAllButton addTarget:self
                                    action:@selector(getComingSoonPosts)
                          forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [header.seeAllButton setHidden:YES];
        }
        
    }
    else {
        [header.seeAllButton setTag:section];
        [header.seeAllButton addTarget:self
                                action:@selector(seeAll:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
    [header setFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    [header setTag:300+section];
    UIView *view = [[UIView alloc] initWithFrame:header.frame];
    [view addSubview:header];
    [view setClipsToBounds:YES];
    
    [header.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [header.seeAllButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];

    return view;
}

-(CGFloat)configureShowtimesCell:(SPNMovieShowtimeCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;
{
    
    NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
    NSArray *showList = [self.showtimeInformation objectForKey:show];
    NSDictionary *movieInformation = [showList objectAtIndex:indexPath.row-2];
    
    [cell initializeMovieShowtimeCell:movieInformation
                                      forTableView:self.theatreShowtimesTableView
                                      forIndexPath:[NSIndexPath indexPathForRow:indexPath.row-2 inSection:indexPath.section]];
    
    NSString *chosenDate = [datePickerFormatter stringFromDate:show];
    NSArray *shows = [cell getShowsForThisMovie:indexPath
                                                 forSchedule:movieInformation
                                                     forDate:chosenDate];
    
    [cell setShowForCell:shows
        withBrandColor:showColor];
    
    
    for (int i = 0; i < [shows count]; i++)
    {
        UIButton *buyTicketBtn = (UIButton*) [cell viewWithTag:i+100];
        buyTicketBtn.theatreId = [movieInformation objectForKey:@"id"];
        [buyTicketBtn addTarget:self
                         action:@selector(buyTicketsDirectly:)
               forControlEvents:UIControlEventTouchUpInside];
    }

    [cell.titleLabel setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapForDetails = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadMovieDetails:)];
    [cell.titleLabel addGestureRecognizer:tapForDetails];
    [cell.titleLabel setTag:indexPath.row - 2];
    [cell.loadMovieDataButton setTag:indexPath.row-2];
    [cell.loadMovieDataButton addTarget:self
                                 action:@selector(loadMovieDetails:)
                       forControlEvents:UIControlEventTouchUpInside];
    
    NSString *metascore = [movieInformation objectForKey:@"metascore"];
    [cell.metaScoreLabel assignMetaScore:metascore];
    if ([metascore isEqualToString:@"NR"]) {
        [cell.metaIconImageView setHidden:NO];
        [cell.metaScoreLabel setHidden:NO];
    }
    else {
        [cell.metaIconImageView setHidden:NO];
        [cell.metaScoreLabel setHidden:NO];
    }
    
    
    CGFloat maxHeight = 0;
    for (UIView *view  in [cell.showtimeContainerView subviews]) {
        maxHeight = MAX(maxHeight, view.frame.origin.y+view.frame.size.height);
    }
    return maxHeight;
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    SPNIndieHeaderCell *header = (SPNIndieHeaderCell *) [view viewWithTag:300+section];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
    
    if (sectionColor) {
        header.contentView.backgroundColor = sectionColor;
    }
    if (titleSectionColor) {
        [header.titleLabel setTextColor:titleSectionColor];
        [header.seeAllButton setTitleColor:titleSectionColor
                                  forState:UIControlStateNormal];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case SPNTheatreSectionNews:
            if (feeds.news.count < 1) {
                return 0;
            }
            break;
        case SPNTheatreSectionFeed:
            if (feeds.socials.count < 1) {
                return 0;
            }
            break;
        default: //SPNTheatreSectionShowtimes
            return 30;
            break;
    }
    return 30;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case SPNTheatreSectionNews:
            return feeds.news.count >= NewsLimit ? NewsLimit : feeds.news.count;
            
        case SPNTheatreSectionFeed:
            return feeds.socials.count >= NewsLimit ? NewsLimit : feeds.socials.count;
            
        default: //SPNTheatreSectionShowtimes
        {
            NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
            if (showArray.count == 0) {
                return 0;
            }
            NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
            NSArray *showList = [self.showtimeInformation objectForKey:show];
            
            return  [showList count] > 0 ? 2 + [showList count] : 0;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    switch (indexPath.section) {
        case SPNTheatreSectionNews:
            return [SPNTheatreNewsfeedCell heightForFeed:NO
                                                    feed:feeds.news[indexPath.row]
                                             htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            
        case SPNTheatreSectionFeed:
            return [SPNTheatreNewsfeedCell heightForFeed:YES
                                                    feed:feeds.socials[indexPath.row]
                                             htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            
        default: // SPNTheatreSectionShowtimes
           
            if (indexPath.row < 1) {
                return 52;
            }/*
            else if (indexPath.row < 2) {
                return shouldShowDateCell ? 62 : 0;

            }*/
            if (indexPath.row < 2) {
                return shouldShowDismissCell ? 45 : 0;
            }
            else {
                CGFloat height = [self configureShowtimesCell:self.prototypeShowCell
                                            forRowAtIndexPath:indexPath];
                [self.prototypeShowCell layoutIfNeeded];
                CGSize size = [self.prototypeShowCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return size.height+height+1;
            }
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *DismissCellIdentifier = @"DismissCell";
    static NSString *PickerCellIdentifier = @"PickerCell";
    
    switch (indexPath.section) {
        case SPNTheatreSectionNews:
        {
            SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
            [cell configureForFeed:NO
                              feed:feeds.news[indexPath.row]
                       htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            return cell;
        }
            break;
        case SPNTheatreSectionFeed:
        {
            SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
            [cell configureForFeed:YES
                              feed:feeds.socials[indexPath.row]
                       htmlSnippet:[feeds htmlAtIndex:indexPath.row]];
            return cell;
        }
            break;
        default:
        {
            if (indexPath.row < 1) {
                SPNPickerCell *pickCell = [tableView dequeueReusableCellWithIdentifier:PickerCellIdentifier];
                
                [pickCell.dateContainerView.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
            
                [pickCell.dateLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
                self.dateCollectionView = pickCell.dateCollectionView;
                return pickCell;
            }
            else if (indexPath.row < 2) {
                SPNDismissCell *dismissCell = [tableView dequeueReusableCellWithIdentifier:DismissCellIdentifier];
                [dismissCell.xLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
                [dismissCell.messageLabel setFont:[UIFont spn_NeutraSmallMedium]];
                return dismissCell;
            }
            else {
                SPNMovieShowtimeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (titleCellColor) {
                    [cell.showtimesLabel setTextColor:titleCellColor];
                    [cell.titleLabel setTextColor:titleCellColor];
                }
               
                [self configureShowtimesCell:cell forRowAtIndexPath:indexPath];
                
                
                
                return cell;
            }
            
        }
            break;
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SPNMovieShowtimeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SPNTheatreSectionFeed:
        case SPNTheatreSectionNews:
            break;
        default:
            if (cellColor) {
                cell.backgroundColor = cellColor;
            }
            if (indexPath.row < 2) {
                cell.backgroundColor = [UIColor spn_brownColor];
                if (sectionColor) {
                    cell.backgroundColor = sectionColor;
                }
            }
            else {
                cell.posterImageView.image = [UIImage imageNamed:@"poster-not-available"];
            }
            break;
    }
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(SPNMovieShowtimeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SPNTheatreSectionFeed:
        case SPNTheatreSectionNews:
            break;
        default:
            if (indexPath.row > 2) {
                [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.posterImageView.imageURL];
                cell.posterImageView.image = [UIImage imageNamed:@"poster-not-available"];

                for (UIView *subview in [cell.showtimeContainerView subviews]) {
                    [subview removeFromSuperview];
                }
            }
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == SPNTheatreSectionShowtimes) {
        if (indexPath.row < 1) {
            shouldShowDateCell = !shouldShowDateCell;
            [self.theatreShowtimesTableView reloadData];
            if (shouldShowDateCell) {
                [self.datePicker becomeFirstResponder];
            }
        }
        else if (indexPath.row == 1) {
             shouldShowDismissCell = NO;
            [self.theatreShowtimesTableView beginUpdates];
            [self.theatreShowtimesTableView endUpdates];
        }
        
    }
    else if (indexPath.section == SPNTheatreSectionNews) {
        
        NSDictionary *entry = feeds.news[indexPath.row];
        if ([entry objectForKey:@"summary"]) {
            shouldFetchRSSToo = 0;
        }
        else {
            shouldFetchRSSToo = 1;
        }
        [self seeAll:nil];
        
    }
    else {
        [feeds postsForSocialsWithCompletion:^(NSArray *posts) {
            [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@YES,
                                                                  @"posts":posts}];
        }];
       
        /*
        NSDictionary *newsData;
        if (indexPath.section == SPNTheatreSectionFeed) {
            newsData = [theatreRSS objectAtIndex:indexPath.row];
            NSString *postType = [newsData objectForKey:@"page"];
            if (postType) {
                [self seeAllOfType:postType forFeed:SPNTheatreSectionFeed];
                
            }
        }
        else {
            newsData = [theatreNews objectAtIndex:indexPath.row];
            NSString *postType = [newsData objectForKey:@"page"] ? [newsData objectForKey:@"page"] : @"News";
                [self seeAllOfType:postType forFeed:SPNTheatreSectionNews];
        }
        */
    }
}

#pragma mark - Custom leyboard features
-(void)initializeKeyboard
{

    [datePicking setDate:[NSDate date]];
    [datePicking setDatePickerMode:UIDatePickerModeDate];
    [datePicking setMinimumDate:[NSDate date]];

    NSDateComponents* addOneMonthComponents = [NSDateComponents new] ;
    addOneMonthComponents.day = 7;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];

    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute)
                                                                fromDate:[NSDate date]];
    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents];
    
    NSDate* threeMonthsFromNowWithoutSeconds = [calendar dateByAddingComponents:addOneMonthComponents
                                                                         toDate:nowWithoutSeconds
                                                                        options:0];
    
    [datePicking setMaximumDate:threeMonthsFromNowWithoutSeconds];
    
    [datePicking addTarget:self
                   action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];
   
    
    
    keyboardDoneButtonViewDate = [[UIToolbar alloc] init];
    keyboardDoneButtonViewDate.barStyle = UIBarStyleBlack;
    keyboardDoneButtonViewDate.translucent = YES;
    keyboardDoneButtonViewDate.tintColor = nil;
    [keyboardDoneButtonViewDate sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(cancelClicked:)];
    [keyboardDoneButtonViewDate setItems:[NSArray arrayWithObjects:doneButton, cancelButton, nil]];
    
}

-(void)doneClicked:(id)sender
{
    [self.datePicker resignFirstResponder];
   // [self getTheatreShowtimes:self.theatreInformation];
    
}
-(void)cancelClicked:(id)sender
{
    [self.datePicker resignFirstResponder];
}

-(void)updateTextField:(id)sender
{
    if([self.datePicker isFirstResponder]) {
        UIDatePicker *picker = (UIDatePicker*)self.datePicker.inputView;
        self.datePicker.text = [NSString stringWithFormat:@"%@",[datePickerFormatter stringFromDate:picker.date]];
    }
}

#pragma mark Initialize theatre data
-(void)initializeTheatreData:(NSDictionary*)theatre
{
    self.titleLabel.text = [theatre objectForKey:@"name"];
    [self.titleLabel sizeToFit];
    self.addressLabel.text = [SPNCinelifeQueries getTheatreAddress:theatre];
    self.phoneTextView.text = [theatre objectForKey:@"phone"];
    
    if ([theatre objectForKey:@"is_favorite"] && [[theatre objectForKey:@"is_favorite"] integerValue] > 0) {
        [self.addToFavouriteButton setTitle:@"Favorite"
                                   forState:UIControlStateNormal];
        [self.heartImage setImage:[UIImage imageNamed:@"heartFill"]];
    }
    
    [self getBrandColors:theatre];
    [self assignBrandColors:theatre];
    
 
}

-(void)getBrandColors:(NSDictionary*)theatre
{
    NSDictionary *branding = [theatre objectForKey:@"branding"];
    if (branding) {
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            [brandingElements setObject:mainColor
                                 forKey:@"mainColor"];
        }
        if ([[branding objectForKey:@"colorCell"] length] > 0) {
            //cellColor = [UIColor colorFromHexString:[branding objectForKey:@"colorCell"]];
        }
        if ([[branding objectForKey:@"colorBtn"] length] > 0) {
            //btnColor = [UIColor colorFromHexString:[branding objectForKey:@"colorBtn"]];
        }
        if ([[branding objectForKey:@"colorShow"] length] > 0) {
            //showColor= [UIColor colorFromHexString:[branding objectForKey:@"colorShow"]];
        }
        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
              [brandingElements setObject:sectionColor
                                   forKey:@"sectionColor"];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            titleSectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
            
            [brandingElements setObject:titleSectionColor
                                 forKey:@"titleSectionColor"];
        }
        if ([[branding objectForKey:@"info_text"] length] > 0) {
            infoTextColor = [UIColor colorFromHexString:[branding objectForKey:@"info_text"]];
            //titleCellColor= [UIColor colorFromHexString:[branding objectForKey:@"colorTitleCell"]];
        }
        if ([[branding objectForKey:@"info_background"] length] > 0) {
            infoTextBackground = [UIColor colorFromHexString:[branding objectForKey:@"info_background"]];
          //  theatreColor= [UIColor colorFromHexString:[branding objectForKey:@"colorTheatre"]];
        }
        if ([[branding objectForKey:@"colorAmenities"] length] > 0) {
            amenitiesColor = [UIColor colorFromHexString:[branding objectForKey:@"colorAmenities"]];
        }
    }
}

- (void) tintedImageView:(UIImageView *)imageView color:(UIColor *)color {
    UIImage *image = imageView.image;
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *flippedImage = [UIImage imageWithCGImage:img.CGImage
                                                scale:1.0 orientation: UIImageOrientationDownMirrored];
    
    imageView.image = flippedImage;
}

-(void)assignBrandColors:(NSDictionary*)theatre
{
    
    NSDictionary *branding = [theatre objectForKey:@"branding"];
    if (branding) {
        // Logo
        NSString *logo = [branding objectForKey:@"logo"];
        
        if ([logo length] > 0) {
            
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
            [brandingElements setObject:realLogo
                                 forKey:@"logoURL"];
        }
        //Button colors & text color
        if (mainColor) {
            self.backgroundView.backgroundColor = mainColor;
            [self.theatreShowtimesTableView setBackgroundColor:mainColor];
        }
        if (btnColor) {
            self.addToFavouriteButton.backgroundColor = btnColor;
        }
        if (theatreColor) {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7) {
                [self.phoneTextView setTextColor:theatreColor];
            }
            else {
                [self.phoneTextView setTintColor:theatreColor];
            }
            [self.addressLabel setTextColor:theatreColor];

        }
        if (infoTextBackground) {
            [self.theatreInformationContainerView setBackgroundColor:[infoTextBackground colorWithAlphaComponent:0.7]];
        }
        if (infoTextColor) {
            [self.titleLabel setTextColor:infoTextColor];
            [self.addressLabel setTextColor:infoTextColor];
            [self.phoneTextView setTextColor:infoTextColor];
            [self.phoneTextView setTintColor:infoTextColor];
            [self tintedImageView: self.heartImage color: infoTextColor];
            [self tintedImageView: self.phoneImage color: infoTextColor];
            [self tintedImageView: self.geoImage color: infoTextColor];
        }
    }
}


-(NSArray*)getTheatreNewsfeed:(NSDictionary*)theatre
{
    return [theatre objectForKey:@"posts"];
   // return [SPNServerQueries getTheatreLastPosts:@"455"];
}


-(void)getComingSoonPosts
{
    isFetchingComingSoon = YES;
    selectedNewsPostDetails = nil;
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getCineLifePostsForTheater:[self.theatreInformation objectForKey:@"id"]];
}

-(void)seeAll:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (sender) {
        shouldFetchRSSToo = 2;
    }
    if (btn.tag == SPNTheatreSectionNews) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        [[SPNCinelifeQueries sharedInstance] getCineLifePostsForTheater:[self.theatreInformation objectForKey:@"id"]];
    }
    else {
        [feeds postsForSocialsWithCompletion:^(NSArray *posts) {
            [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@YES,
                                                                  @"posts":posts}];
        }];
    }
}

-(NSArray*)getTheatreRSSfeed:(NSDictionary*)theatre
{
  
    NSString* theatreId = [theatre objectForKey:@"id"];
    return [SPNServerQueries getRSSForTheatre:theatreId andLimit:4];
   
}

#pragma mark Movie data functions

-(void)loadMovieDetails:(id)sender
{
    isFetchingMovieInfoForTicketing = NO;
    UIView *loadMovieDetailsButton;
    if ([sender isKindOfClass:[UITapGestureRecognizer class]]) {
        UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer*)sender;
        loadMovieDetailsButton = tapGesture.view;
    }
    else {
        loadMovieDetailsButton = (UIView*)sender;
    }
    NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
    NSArray *showList = [self.showtimeInformation objectForKey:show];
    NSDictionary *movieInformation = [showList objectAtIndex:loadMovieDetailsButton.tag];
    
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] getMovieInformation:[movieInformation objectForKey:@"id"]
                                                  forZipcode:nil
                                               orCoordinates:nil
                                                  withRadius:0
                                             withCredentials:[SPNUser getCurrentUserId]];
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
   
}
#pragma mark Ticketing functions

-(void)buyTicketsDirectly:(id)sender
{
    UIButton *btn = (UIButton*)sender;

    NSString *theatreId = [self.theatreInformation objectForKey:@"id"];
    NSString *movieId = [btn theatreId];
    
    movieForTicketing = @{@"id" : movieId};
    NSString *show = btn.titleLabel.text;
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    
    [showtimeDateFormatter setDateFormat:@"hh:mma"];
    NSDate *newShow = [showtimeDateFormatter dateFromString:show];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    NSString *finalShow = [showtimeDateFormatter stringFromDate:newShow];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];

    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute)
                                                                fromDate:[NSDate date]];
    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents];
    
    NSDateComponents* addDayComponents = [NSDateComponents new] ;
    addDayComponents.day = selectedDateForPicker;
    NSDate* newDateWithDays = [calendar dateByAddingComponents:addDayComponents
                                                        toDate:nowWithoutSeconds
                                                       options:0];
    
    NSString *chosenDate = [datePickerFormatter stringFromDate:newDateWithDays];
    
    NSDate *date = [showtimeDateFormatter dateFromString:chosenDate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];

    NSDictionary *location = @{@"lat" : [self.theatreInformation objectForKey:@"lat"], @"lng" : [self.theatreInformation objectForKey:@"lng"]};
    showTicketingInformation = [NSDictionary dictionaryWithObjects:@[[self.theatreInformation objectForKey:@"name"], theatreId, date, finalShow, location, [self.theatreInformation objectForKey:@"id"]]
                                                                forKeys:@[@"theatreName", @"theatreId", @"showDate", @"showTime", @"location", @"theaterId"]];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] postTicketingForTheater:theatreId
                                                        forMovie:movieId
                                                     forShowdate:newDate
                                                     andShowtime:finalShow];
}

#pragma mark Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"blogNews"]) {
        SPNTheatreNewsViewController *controller = [segue destinationViewController];
        controller.theatreNews = sender[@"posts"];
        controller.brandColors = self.theatreInformation[@"branding"];
        controller.isRSS = [sender[@"rss"] boolValue];
    }
    else if ([segue.identifier isEqualToString:@"PostDetails"]) {
        SPNTheatreNewsDetailsViewController *spnNewsDetailsController = [segue destinationViewController];
        [spnNewsDetailsController setNewsPostDetails:selectedNewsPostDetails];
        [spnNewsDetailsController setBrandColors:[self.theatreInformation objectForKey:@"branding"]];
    }
    else if ([segue.identifier isEqualToString:@"RSSDetails"]) {
        SPNTheatreNewsDetailsViewController *spnNewsDetailsController = [segue destinationViewController];
        [spnNewsDetailsController setNewsPostDetails:selectedNewsPostDetails];
        [spnNewsDetailsController setNewsLink:selectedNewsRSSLink];
        
    }
}

#pragma mark Get Directions to theatre
- (IBAction)directionsButtonPressed:(id)sender {
    [self getDirectionsForTheatre:self.theatreInformation];
}



-(void)getDirectionsForTheatre:(NSDictionary*)theatre {
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    CLGeocoder *geoCoder = [CLGeocoder new];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
        NSString *theatreAddress = [SPNCinelifeQueries getTheatreAddress:theatre];
        [geoCoder geocodeAddressString:theatreAddress
         
                          completionHandler:^(NSArray *placemarks, NSError *error) {
                              
                              // Convert the CLPlacemark to an MKPlacemark
                              // Note: There's no error checking for a failed geocode
                              CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                              MKPlacemark *placemark = [[MKPlacemark alloc]
                                                        initWithCoordinate:geocodedPlacemark.location.coordinate
                                                        addressDictionary:geocodedPlacemark.addressDictionary];
                              
                              // Create a map item for the geocoded address to pass to Maps app
                              MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                              [mapItem setName:geocodedPlacemark.name];
                              
                              
                              // [self.locationMap addAnnotation:placemark];
                              // Set the directions mode to "Driving"
                              // Can use MKLaunchOptionsDirectionsModeWalking instead
                              NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
                              
                              // Get the "Current User Location" MKMapItem
                              MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
                              
                              // Pass the current location and destination map items to the Maps app
                              // Set the direction mode in the launchOptions dictionary
                              [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
                              
                          }];
    }

}

#pragma mark - User related functions

- (IBAction)addToFavouriteButtonPressed:(id)sender
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString* showTootip = [preferences objectForKey:@"showTheaterInfo"];
    
    if (!showTootip) {
        [SPNToolTipViewController showWithToolTipText:@"Tap on the heart icon to add a theatre to your favorite theatre list. You can remove theatres from your favorites by tapping on the heart icon again."
                                   overViewController:self.tabBarController];
        
        [preferences setObject:@"no"
                        forKey:@"showTheaterInfo"];
        [preferences synchronize];
    }
    else if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not Now"
                           withOtherTitles:@"Sign In"];
    }
    else {
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        
        NSDictionary *theatre = self.theatreInformation;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        if ([theatre objectForKey:@"is_favorite"] && [[theatre objectForKey:@"is_favorite"] integerValue] > 0) {
            [[SPNCinelifeQueries sharedInstance] postRemoveFavoriteTheater:[theatre objectForKey:@"id" ]
                                                           withCredentials:[SPNUser getCurrentUserId]];
        }
        else {
            [[SPNCinelifeQueries sharedInstance] postAddFavoriteTheater:[theatre objectForKey:@"id" ]
                                                        withCredentials:[SPNUser getCurrentUserId]];
        }
        /*
         __weak __typeof__(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        dispatch_async(kBgQueue, ^{
        if  ([[btn titleForState:UIControlStateNormal] isEqualToString:@"Favorite"]) {
            // Theatre is already a favourite, so we remove it.
            BOOL success = [self removeTheatreFromFavourites:theatre
                                                     forUser:userId];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                __typeof__(self) strongSelf = weakSelf;
                [MBProgressHUD hideAllHUDsForView:strongSelf.tabBarController.view animated:YES];

                if (success) {
                    [strongSelf.heartToolButton setImage:[UIImage imageNamed:@"heart-icon"]];
                    [btn setTitle:@"Add to favs"
                         forState:UIControlStateNormal];
                    
                    [UIAlertView showAlertViewWthTitle:@"Success"
                                            andMessage:@"You removed this theatre from your list. You can add it again later if you want."];
                }
                else {
                    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeFavoriteNotDeleted];
                }
            });
        }
        else {
            BOOL success = [self addTheatreToFavourites:theatre
                                                forUser:userId];
             dispatch_sync(dispatch_get_main_queue(), ^{
                 __typeof__(self) strongSelf = weakSelf;
                 [MBProgressHUD hideAllHUDsForView:strongSelf.tabBarController.view animated:YES];

                if (success) {
                    [strongSelf.heartToolButton setImage:[UIImage imageNamed:@"heartFill"]];
                    [btn setTitle:@"Favorite"
                         forState:UIControlStateNormal];
                    NSString *theatreId = [theatre objectForKey:@"id"];
                    NSNumber *numTheatreId = [NSNumber numberWithInteger:[theatreId integerValue]];
                    if ([self.cineLifePartners indexOfObject:numTheatreId] == NSNotFound) {
                        [UIAlertView showAlertViewWthTitle:@"Congratulations"
                                                andMessage:@"This theatre has been added to your list of favorites."];
                    }
                    else {
                        [UIAlertView showAlertViewWthTitle:@"Congratulations"
                                                andMessage:@"You now have access to custom theatre content, special events and invitation-only screenings from your favorite theatres."];
                    }

                }
                else {
                    [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeFavoriteNotAdded];
                }
             });
        }
            
        });
        */
    }
}

-(BOOL)addTheatreToFavourites:(NSDictionary*)theatre forUser:(NSString*)userId
{
    BOOL success = NO;
    NSDictionary *favouriteInformation = [SPNUser addTheatreToFavourites:theatre
                                                                 forUser:userId];
    NSString* errorMessage = [favouriteInformation objectForKey:@"error_msg"];
    
    if (!errorMessage) {
        success = YES;
    }
    return success;
}

-(BOOL)removeTheatreFromFavourites:(NSDictionary*)theatre forUser:(NSString*)userId
{
    BOOL success = NO;
    NSDictionary *favouriteInformation = [SPNUser removeFavouriteTheatre:theatre
                                                                 forUser:userId];
    
    NSString* errorMessage = [favouriteInformation objectForKey:@"error_msg"];
    if (!errorMessage) {
        success = YES;
    }
    return success;
}

#pragma mark Alert view functions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Sign In"]) {
        [alertView dismissWithClickedButtonIndex:buttonIndex
                                        animated:NO];
        self.tabBarController.selectedIndex = 3;
    }
}


#pragma mark - Collection view delegate functions

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *DateCellIdentifier = @"DateCell";
    
    SPNDateCell *cell = (SPNDateCell*)[collectionView dequeueReusableCellWithReuseIdentifier:DateCellIdentifier forIndexPath:indexPath];
    
    if (selectedDateForPicker == indexPath.row) {
        [cell.dateTitleLabel setBackgroundColor:[UIColor spn_aquaColor]];
        [cell.dateTitleLabel setFont:[UIFont spn_NeutraBoldMedium]];
        [cell.dateTitleLabel setTextColor:[UIColor whiteColor]];
        [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_aquaShadowColor].CGColor];
    }
    else {
        [cell.dateTitleLabel setBackgroundColor:[UIColor clearColor]];
        [cell.dateTitleLabel setFont:[UIFont spn_NeutraSmallMedium]];
        [cell.dateTitleLabel setTextColor:[UIColor blackColor]];
        [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
    }
    
    NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSDate *show = [showArray objectAtIndex:indexPath.row];
    
    if ([self isSameDayWithDate1:[NSDate date] date2:show]) {
        [cell.dateTitleLabel setText:@"Today"];
        return cell;
    }
    
    NSString *dateString = [formatter stringFromDate:show];
    [cell.dateTitleLabel setText:dateString];
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *shows = [self.showtimeInformation allKeys];
    return MAX([shows count], 1);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

        for (SPNDateCell *visibleCell in [collectionView visibleCells])
        {
            [visibleCell.dateTitleLabel setBackgroundColor:[UIColor clearColor]];
            [visibleCell.dateTitleLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [visibleCell.dateTitleLabel setTextColor:[UIColor blackColor]];
            [visibleCell.dateTitleLabel.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
        }
        
        SPNDateCell *cell = (SPNDateCell*)[collectionView cellForItemAtIndexPath:indexPath];
        [cell.dateTitleLabel setBackgroundColor:[UIColor spn_aquaColor]];
        [cell.dateTitleLabel setFont:[UIFont spn_NeutraBoldMedium]];
        [cell.dateTitleLabel setTextColor:[UIColor whiteColor]];
        [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_aquaShadowColor].CGColor];
        
        selectedDateForPicker = indexPath.row;
        //
        [self.theatreShowtimesTableView reloadData];
}

#pragma mark - Cinelife queries
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    isFetchingComingSoon = NO;
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieInformation:(NSDictionary *)movie
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    if (isFetchingMovieInfoForTicketing) {
        
        UIStoryboard *ticketingStoryboard = [UIStoryboard storyboardWithName:@"TicketingStoryboard" bundle:nil];
        SPNTicketSelectionViewController *spnTicketingSelection = [ticketingStoryboard instantiateViewControllerWithIdentifier:@"RTSTicketing"];
        [spnTicketingSelection setMovieInformation:movie];
        [spnTicketingSelection setShowtimeInformation:showTicketingInformation];
        [spnTicketingSelection setTicketingInformation:movieTicketingInformation];
        [spnTicketingSelection setBranding:[self.theatreInformation objectForKey:@"branding"]];
        [self.navigationController pushViewController:spnTicketingSelection animated:YES];
    }
    else {
        UIStoryboard *filmsStoryboard = [UIStoryboard storyboardWithName:@"FilmsStoryboard"
                                                                  bundle:nil];
        SPNFilmDetailsViewController *spnFilmDetails = [filmsStoryboard instantiateViewControllerWithIdentifier:@"FilmDetails"];
        [spnFilmDetails setFilmInformation:movie];
    
        [self appendShowtimesForFilm:movie];
        
        spnFilmDetails.theatreShowtimesInRegion = @[selectedFilmAndShowtimes];
        spnFilmDetails.showtimeInformation = [SPNCinelifeQueries sortShowdatesForTheaters:@[selectedFilmAndShowtimes]];
        [self.navigationController pushViewController:spnFilmDetails animated:YES];
    }
}

-(void)appendShowtimesForFilm:(NSDictionary*)movie
{
    
    NSDateFormatter *dateFormatter2 = [NSDateFormatter new];
    [dateFormatter2 setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
    selectedFilmAndShowtimes = [NSMutableDictionary dictionaryWithDictionary:self.theatreInformation];
    [selectedFilmAndShowtimes removeObjectForKey:@"movies"];
    
    NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSDate *date in showArray)
    {
        NSArray *showList = [self.showtimeInformation objectForKey:date];
        for (NSDictionary *showStuff in showList)
        {
            NSString *movieId = [showStuff objectForKey:@"id"];
            if ([movieId isEqualToString:[movie objectForKey:@"id"]]) {
                NSMutableDictionary *showToAdd = [NSMutableDictionary new];
                NSString *stringDate = [dateFormatter2 stringFromDate:date];
                [showToAdd setObject:stringDate forKey:@"showdate"];
                [showToAdd setObject:[showStuff objectForKey:@"showtime"] forKey:@"showtime"];
                
                if (![selectedFilmAndShowtimes objectForKey:@"showtimes"]) {
                    NSMutableArray *showtimes = [NSMutableArray new];
                    [showtimes addObject:showToAdd];
                    [selectedFilmAndShowtimes setObject:showtimes forKey:@"showtimes"];
                }
                else {
                    NSMutableArray *showtimes = [selectedFilmAndShowtimes objectForKey:@"showtimes"];
                    [showtimes addObject:showToAdd];
                    [selectedFilmAndShowtimes setObject:showtimes forKey:@"showtimes"];

                }
            }
        }
    }
}


- (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery theaterInformation:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    self.theatreInformation = theater;
    self.showtimeInformation = [SPNCinelifeQueries sortShowdatesForOneTheatre:[self.theatreInformation objectForKey:@"movies"]];
    
    BOOL showErrorMessage = YES;
    for (NSDate *date in [self.showtimeInformation allKeys]) {
        NSArray *showList = [self.showtimeInformation objectForKey:date];
        if ([showList count] > 0) {
            showErrorMessage = NO;
            break;
        }
    }
    if (!showErrorMessage) {
        [self.theatreShowtimesTableView setTableFooterView:[UIView new]];
        [self.theatreShowtimesTableView setBackgroundColor:[UIColor spn_darkBrownColor]];
    }
    else {
        [self.theatreShowtimesTableView setBackgroundColor:[UIColor spn_lightBrownColor]];
    }
    [self.theatreShowtimesTableView reloadData];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery addedTheater:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];

    if ([self.theatreInformation objectForKey:@"is_partner"] && [[self.theatreInformation objectForKey:@"is_partner"] integerValue] > 0) {
        [UIAlertView showAlertViewWthTitle:@"Congratulations"
                                andMessage:@"You now have access to custom theatre content, special events and invitation-only screenings from your favorite theatres."];
    }
    else {
        [UIAlertView showAlertViewWthTitle:@"Congratulations"
                                andMessage:@"This theatre has been added to your list of favorites."];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery removedTheater:(NSDictionary *)theater
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [UIAlertView showAlertViewWthTitle:@"Success"
                            andMessage:@"You removed this theatre from your list. You can add it again later if you want."];
}

-(void)addedTheaterToFavorites:(NSNotification*)notification
{
    NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:self.theatreInformation];
    [theater setObject:@1 forKey:@"is_favorite"];
    self.theatreInformation = theater;
    [self.heartImage setImage:[UIImage imageNamed:@"heartFill"]];
}

-(void)removedTheaterFromFavorites:(NSNotification*)notification
{
    NSMutableDictionary *theater = [NSMutableDictionary dictionaryWithDictionary:self.theatreInformation];
    [theater setObject:@0 forKey:@"is_favorite"];
    self.theatreInformation = theater;
    
    [self.heartImage setImage:[UIImage imageNamed:@"heart-icon"]];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingURL:(NSString *)url
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    isFetchingMovieInfoForTicketing = YES;
    UIStoryboard *theatreStoryboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
    SPNTicketPurchaseViewController  *spnTicketing = [theatreStoryboard instantiateViewControllerWithIdentifier:@"TicketPurchase"];
    [spnTicketing setUrl:url];
    [self.navigationController pushViewController:spnTicketing
                                         animated:YES];

}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingFromRTS:(NSDictionary *)ticketingInformation
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    movieTicketingInformation = ticketingInformation;
    isFetchingMovieInfoForTicketing = YES;
    
    [[SPNCinelifeQueries sharedInstance] getMovieInformation:[movieForTicketing objectForKey:@"id"]
                                                  forZipcode:nil
                                               orCoordinates:nil
                                                  withRadius:0
                                             withCredentials:[SPNUser getCurrentUserId]];
    
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingError:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery theaterPosts:(NSArray *)posts
{
    if (isFetchingComingSoon) {
        selectedNewsPostDetails = nil;
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        isFetchingComingSoon = NO;
        for (NSDictionary *post in posts)
        {
            if ([[post objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
                selectedNewsPostDetails = post;
                break;
            }
        }
        if (selectedNewsPostDetails) {
            [self performSegueWithIdentifier:@"PostDetails" sender:self];
        }
        else {
            [UIAlertView showAlertViewWthTitle:@"Showtimes not available" andMessage:@"There are no films currently listed for this theatre."];
        }
    }
    else {
        if (feeds.hasRSS) {
            [feeds storiesWithPosts:posts
                  shouldFetchRSSToo:shouldFetchRSSToo
                         completion:^(NSArray *list) {
                             [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                             [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@NO,
                                                                                   @"posts":list}];
                         }];
        } else {
            NSMutableArray *postsWithoutComingSoon = [NSMutableArray arrayWithArray:posts];
            for (NSDictionary *post in posts)
            {
                if ([[post objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
                    [postsWithoutComingSoon removeObject:post];
                }
            }
            
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            [self performSegueWithIdentifier:@"blogNews" sender:@{@"rss":@NO,
                                                                  @"posts":postsWithoutComingSoon}];
        }
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery favoriteTheaters:(NSArray *)favoriteTheaters
{
    
}

@end
