//
//  SPNAccountLoginCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAccountLoginCell.h"

@implementation SPNAccountLoginCell

-(void)initializeCell
{
    [self.facebookButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.twitterButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.googlePlusButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.spotlightButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.forgotPasswordButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.loginLabel setFont:[UIFont spn_NeutraMedium]];
    
    self.facebookButton.layer.cornerRadius = 2.5;
    self.facebookButton.layer.masksToBounds = YES;
    
    self.twitterButton.layer.cornerRadius = 2.5;
    self.twitterButton.layer.masksToBounds = YES;

    self.googlePlusButton.layer.cornerRadius = 2.5;
    self.googlePlusButton.layer.masksToBounds = YES;

    self.spotlightButton.layer.cornerRadius = 2.5;
    self.spotlightButton.layer.masksToBounds = YES;

}
@end
