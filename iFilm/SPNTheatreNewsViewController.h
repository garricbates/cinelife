//
//  SPNTheatreNewsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNTheatreNewsfeedCell.h"
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"
@interface SPNTheatreNewsViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *theatreNews;
@property (nonatomic, strong) NSDictionary *brandColors;

@property (nonatomic) BOOL isRSS;

@end
