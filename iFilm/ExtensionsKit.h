//
//  ExtensionsKit.h
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (ExtensionsKit)

+ (UIImage *)imageWithSize:(CGSize)size andColor:(UIColor *)color;
- (UIImage *)scaledToSize:(CGSize)size;
- (UIImage *)scaledToFitSize:(CGSize)size;

@end

@interface NSDate (ExtensionsKit)

- (BOOL)isTheSameYear:(NSDate *)date;
- (BOOL)isTheSameDay:(NSDate *)date;
+ (NSDate *)tzid_dateFromString:(NSString *)dateString timeString:(NSString *)timeString;
+ (NSDate *)dateFromString:(NSString *)dateString timeString:(NSString *)timeString;

@end

@interface NSString (ExtensionsKit)

- (CGFloat)findHeightForWidth:(CGFloat)widthValue andFont:(UIFont *)font;
- (CGFloat)findWidthForHeight:(CGFloat)heightValue andFont:(UIFont *)font;
- (CGSize)findSizeForMaxSize:(CGSize)textSize andFont:(UIFont *)font;

@end

@interface NSAttributedString (ExtensionsKit)

- (CGSize)sizeWithMaxSize:(CGSize)size;

@end

@interface UITableView (ExtensionsKit)

- (void)registerCellClass:(id)cell;
- (void)registerCellClass:(id)cell withIdentifier:(NSString *)identifier;

@end

@interface UICollectionView (ExtensionsKit)

- (void)registerCellClass:(id)cell;
- (void)registerCellClass:(id)cell withIdentifier:(NSString *)identifier;

@end

@interface UIViewController (ExtensionsKit)

+ (UIViewController *)top;

@end

@interface UIDevice (ExtensionsKit)

+ (UIEdgeInsets)safeAreaInsets;

@end
