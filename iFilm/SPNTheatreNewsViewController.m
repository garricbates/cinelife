//
//  SPNTheatreNewsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreNewsViewController.h"
#import "SPNTheatreNewsDetailsViewController.h"

@interface SPNTheatreNewsViewController () {
    UIColor *sectionColor, *titleSectionColor, *mainColor;
    NSDictionary *selectedNewsPostDetails;
    NSMutableArray *summaryHTMLList;
    NSDateFormatter *dateFormatter;
    NSString *selectedNewsRSSLink;
}

@property (nonatomic, weak) IBOutlet UITableView *theatreNewsTableView;

@property (nonatomic) SPNTheatreNewsfeedCell *prototypeCell, *prototypeRSSCell;

@end

static NSString* CellIdentifier = @"TheatreNewsCell";
static NSString* RSSCellIdentifier = @"TheatreRSSCell";

@implementation SPNTheatreNewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Theatre News";
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [self customizeUI];
    [self getBrandColors:self.brandColors];
    summaryHTMLList = [NSMutableArray new];
    
    [self initializeSnippets:self.theatreNews];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [summaryHTMLList removeAllObjects];
    [self setPrototypeCell:nil];
    [self setTheatreNewsTableView:nil];
    [self setTheatreNews:nil];
    summaryHTMLList = nil;
    dateFormatter = nil;
    selectedNewsPostDetails = nil;
    sectionColor = nil;
    titleSectionColor = nil;
    mainColor = nil;
    selectedNewsRSSLink = nil;
}
#pragma mark UI Customization

-(void)customizeUI
{
    [self.theatreNewsTableView setBackgroundColor:[UIColor spn_darkBrownColor]];
    [self.theatreNewsTableView setTableFooterView:[UIView new]];
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
        }
        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            titleSectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
        }
    }
}


-(void)initializeSnippets:(NSArray*)posts
{
    NSError *parseError = nil;
    NSAttributedString *attributedHTMLText;
    
    if (!summaryHTMLList) {
        summaryHTMLList = [NSMutableArray new];
    }
    for (NSDictionary *post in posts)
    {
        if ([post objectForKey:@"content"]) {
            NSString *normalText = [post objectForKey:@"summary"];
            attributedHTMLText = [[NSAttributedString alloc] initWithData:[normalText dataUsingEncoding:NSUTF8StringEncoding] options: @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:&parseError];
            
            if (!parseError) {
                [summaryHTMLList addObject:attributedHTMLText];
            }
            else {
                [summaryHTMLList addObject:@""];
            }
        }
        else {
            [summaryHTMLList addObject:@""];
        }
    }
}
-(SPNTheatreNewsfeedCell *)prototypeCell
{
    if (!_prototypeCell) {
        _prototypeCell = (SPNTheatreNewsfeedCell*)[self.theatreNewsTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    return _prototypeCell;
}

-(SPNTheatreNewsfeedCell *)prototypeRSSCell
{
    if (!_prototypeRSSCell) {
        _prototypeRSSCell = (SPNTheatreNewsfeedCell*)[self.theatreNewsTableView dequeueReusableCellWithIdentifier:RSSCellIdentifier];
    }
    return _prototypeRSSCell;
}

#pragma mark Table View Functions
-(void)configureCell:(UITableViewCell*)cell ofType:(BOOL)isRSSCell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    SPNTheatreNewsfeedCell *selectedCell = (SPNTheatreNewsfeedCell*)cell;
    NSDictionary *feed = [self.theatreNews objectAtIndex:indexPath.row];
    NSString *snippet;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    [selectedCell.newsTitleLabel setPreferredMaxLayoutWidth:width-30];
    [selectedCell.newsSnippetLabel setPreferredMaxLayoutWidth:width-30];

    [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
    
    [selectedCell.newsTypeBackgroundView setBackgroundColor:[UIColor spn_brownColor]];
    [selectedCell.newsTypeIconImageView setImage:nil];
    [selectedCell.iconWidthConstraint setConstant:10];
    [selectedCell.newsTitleLabel setText:[[NSString stringWithFormat:@"%@ ",[feed objectForKey:@"title"]] stringByDecodingHTMLEntities]];
    if (isRSSCell) {
        snippet = [feed objectForKey:@"content"];
            [selectedCell.newsTypeLabel setText:[feed objectForKey:@"page"]];
            NSString *feedType = [feed objectForKey:@"page"];
            if ([feedType isEqualToString:@"Facebook"]) {
                [selectedCell.newsTypeBackgroundView setBackgroundColor:[UIColor fb_blueColor]];
                [selectedCell.newsTypeIconImageView setImage:[UIImage imageNamed:@"fb-icon"]];
                snippet = [feed objectForKey:@"content"];
                if (![feed objectForKey:@"title"] || [[feed objectForKey:@"title"] length] < 1) {
                     [selectedCell.newsTitleLabel setText:[[feed objectForKey:@"content"] stringByDecodingHTMLEntities]];
                
                }
                [selectedCell.feedNameHeightConstraint setConstant:17];
                [selectedCell.feedNameBottomConstraint setConstant:8];
            }
            else if ([feedType isEqualToString:@"Twitter"]) {
                [selectedCell.newsTypeBackgroundView setBackgroundColor:[UIColor tw_blueColor]];
                [selectedCell.newsTypeIconImageView setImage:[UIImage imageNamed:@"tw-icon"]];
                snippet = [feed objectForKey:@"content"];
                if (![feed objectForKey:@"title"] || [[feed objectForKey:@"title"] length] < 1) {
                    [selectedCell.newsTitleLabel setText:[[feed objectForKey:@"content"] stringByDecodingHTMLEntities]];
                }
                [selectedCell.feedNameHeightConstraint setConstant:17];
                [selectedCell.feedNameBottomConstraint setConstant:8];
            }
            else if ([feedType isEqualToString:@"RSS"]){
                [selectedCell.newsTypeBackgroundView setBackgroundColor:[UIColor rss_orangeColor]];
                [selectedCell.newsTypeIconImageView setImage:[UIImage imageNamed:@"rss-icon"]];
                 [selectedCell.iconWidthConstraint setConstant:0];
                [selectedCell.feedNameHeightConstraint setConstant:0];
                [selectedCell.feedNameBottomConstraint setConstant:0];
            }
        
        }
     else // SPNTheatreSectionNews
        {
            if ([feed objectForKey:@"category"]) {
                [selectedCell.newsTypeLabel setText:[feed objectForKey:@"category"]];
                snippet = [feed objectForKey:@"summary"];
                [selectedCell.iconWidthConstraint setConstant:0];
                [selectedCell.feedNameHeightConstraint setConstant:17];
                [selectedCell.feedNameBottomConstraint setConstant:8];
            }
            else {
                [selectedCell.newsTypeLabel setText:[feed objectForKey:@"page"]];
                snippet = [feed objectForKey:@"contentSnippet"];
                [selectedCell.newsTypeBackgroundView setBackgroundColor:[UIColor rss_orangeColor]];
                [selectedCell.newsTypeIconImageView setImage:[UIImage imageNamed:@"rss-icon"]];
                [selectedCell.iconWidthConstraint setConstant:0];
                [selectedCell.feedNameHeightConstraint setConstant:0];
                [selectedCell.feedNameBottomConstraint setConstant:0];
            }
            
        }
   
    
    [selectedCell.newsSnippetLabel setFont:[UIFont spn_NeutraSmallMedium]];
    
    NSDate *realDate = [feed objectForKey:@"publishedDate"];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *stringDate = [dateFormatter stringFromDate:realDate];
    if (!stringDate) {
        stringDate = [dateFormatter stringFromDate:[NSDate date]];
    };
    NSString *feedType = [feed objectForKey:@"page"];
    [selectedCell.newsDateLabel setText:[NSString stringWithFormat:@"Date: %@", stringDate]];
    
    if (![feedType isEqualToString:@"Twitter"] || ![feedType isEqualToString:@"Facebook"]) {
        [selectedCell.newsDateLabel setText:[NSString stringWithFormat:@"Date: %@", stringDate]];
    }
    snippet = [snippet stringByDecodingHTMLEntities];
    if ([snippet length] > 140) {
        snippet = [snippet substringWithRange:NSMakeRange(0, 140)];
        snippet = [snippet stringByAppendingString:@"..."];
    }
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[snippet dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

    
    if ([feed objectForKey:@"category"] && [[summaryHTMLList objectAtIndex:indexPath.row] isKindOfClass:[NSAttributedString class]]) {
        [selectedCell.newsSnippetLabel setAttributedText:[summaryHTMLList objectAtIndex:indexPath.row]];
    }
    
    else {
        [selectedCell.newsSnippetLabel setAttributedText:attrStr];
    }
    if (isRSSCell) {
        [selectedCell.newsTitleLabel setFont:[UIFont spn_NeutraSmallMedium]];
    }
    else {
        [selectedCell.newsTitleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
    }
    [selectedCell.newsDateLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [selectedCell.newsTypeLabel setFont:[UIFont spn_NeutraBoldSmall]];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isRSS) {
        [self configureCell:self.prototypeRSSCell
                     ofType:self.isRSS
          forRowAtIndexPath:indexPath];
        [self.prototypeRSSCell layoutIfNeeded];
        CGSize size = [self.prototypeRSSCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;

    }
    else {
        [self configureCell:self.prototypeCell
                     ofType:self.isRSS
          forRowAtIndexPath:indexPath];
        [self.prototypeCell layoutIfNeeded];
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.theatreNews count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isRSS) {
        SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:RSSCellIdentifier];
        [self configureCell:cell
                     ofType:self.isRSS
          forRowAtIndexPath:indexPath];
        return cell;
    }
    else {
        SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [self configureCell:cell
                     ofType:self.isRSS
          forRowAtIndexPath:indexPath];
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor spn_lightBrownColor];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
    
    if (sectionColor) {
        header.contentView.backgroundColor = sectionColor;
    }
    if (titleSectionColor) {
        [header.textLabel setTextColor:titleSectionColor];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (!self.isRSS) {
        return @"News and Promotions";
    }
    else {
        return @"Social News";
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *newsData = [self.theatreNews objectAtIndex:indexPath.row];
    if ([newsData objectForKey:@"url"]) {
        selectedNewsPostDetails = newsData;
        selectedNewsRSSLink = [newsData objectForKey:@"url"];
        [self performSegueWithIdentifier:@"RSSDetails" sender:self];
    }
    else {
        NSMutableDictionary *finalPost = [NSMutableDictionary dictionaryWithDictionary:newsData];
        selectedNewsPostDetails = finalPost;
        [self performSegueWithIdentifier:@"NewsDetails" sender:self];
    }
}

#pragma mark - Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"NewsDetails"]) {
        SPNTheatreNewsDetailsViewController *spnTheatreNewsDetails = [segue destinationViewController];
        [spnTheatreNewsDetails setNewsPostDetails:selectedNewsPostDetails];
        [spnTheatreNewsDetails setBrandColors:self.brandColors];
    }
    else if ([segue.identifier isEqualToString:@"RSSDetails"]) {
        SPNTheatreNewsDetailsViewController *spnNewsDetailsController = [segue destinationViewController];
        [spnNewsDetailsController setNewsPostDetails:selectedNewsPostDetails];
        [spnNewsDetailsController setNewsLink:selectedNewsRSSLink];
    }
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery favoriteTheaters:(NSArray *)favoriteTheaters
{
    
}

@end
