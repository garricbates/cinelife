//
//  Festival.m
//  iFilm
//
//  Created by Vlad Getman on 18.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "Festival.h"
#import "SPNAppDelegate.h"

@implementation Festival

- (instancetype)initWithJSON:(NSDictionary *)json {
    self = [super init];
    if (self) {
        self.festivalId = @([json[@"id"] integerValue]);
        self.type = json[@"typeFestival"];
        self.summary = json[@"summary"];
        self.dtStart = json[@"dtStart"];
        self.startDate = json[@"startDate"];
        self.startTime = json[@"startTime"];
        self.endDate = json[@"endDate"];
        self.dtEnd = json[@"dTend"];
        self.location = json[@"location"];
        self.lat = json[@"lat"];
        self.lng = json[@"lng"];
        self.zip = json[@"zip"];
        self.contact = json[@"contact"];
        self.venueName = json[@"venueName"];
        self.venueAddress = json[@"venueAddress"];
        self.website = json[@"website"];
        self.websiteMain = json[@"websiteMain"];
        self.ticketsUrl = json[@"xTicketsUrl"];
        self.info = json[@"description"];
        self.calcEndDate = json[@"calcEndDate"];
        self.calcEndTime = json[@"calcEndTime"];
        self.defaultEndTime = json[@"defaultEndTime"];
        self.concertEndTime = json[@"concertEndTime"];
        self.tags = json[@"xTags"];
        self.categories = json[@"categories"];
        self.promoStartDate = json[@"promoStartDate"];
        self.promoEndDate = json[@"promoEndDate"];
        self.isFavorited = json[@"is_favorite"];
        self.isPartner = json[@"is_partner"];
        self.photoUrl = json[@"picture"];
        self.feeds = json[@"feeds"];
        self.pinned = json[@"pinned"];
        self.branding = json[@"branding"];
        
        unsigned int varCount;
        
        Ivar *vars = class_copyIvarList([Festival class], &varCount);
        
        for (int i = 0; i < varCount; i++) {
            Ivar var = vars[i];
            const char* name = ivar_getName(var);
            NSString *key = [NSString stringWithUTF8String:name];
            id value = [self valueForKey:key];
            if ([value isKindOfClass:[NSNull class]]) {
                [self setValue:nil forKey:key];
            }
        }
        
        free(vars);
        
        self.photoUrl = [self.photoUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.website = [self.website stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.websiteMain = [self.websiteMain stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        self.ticketsUrl = [self.ticketsUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    return self;
}

+ (NSArray <Festival *> *)festivalsFromJSON:(id)json {
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *info in json[@"festivals"]) {
        [array addObject:[[Festival alloc] initWithJSON:info]];
    }
    return array;
}

- (NSNumber *)distance {
    CLLocation *location = [[CLLocation alloc] initWithLatitude:_lat.doubleValue longitude:_lng.doubleValue];
    CLLocationDistance currentDistance = [location distanceFromLocation:[appDelegate myLocation]];
    currentDistance = currentDistance / 1000 * 0.621371192; // Convert meters into miles
    
    return @(currentDistance);
}

- (NSString *)formattedPeriod {
    NSDate *startDate = [NSDate dateFromString:self.startDate timeString:nil]; //[NSDate tzid_dateFromString:self.dtStart timeString:self.startTime];
    NSDate *endDate = [NSDate dateFromString:self.endDate timeString:nil]; //[NSDate tzid_dateFromString:self.dtEnd timeString:self.calcEndTime];
    
    BOOL sameYear = [startDate isTheSameYear:endDate];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = sameYear ? @"MMMM d" : @"MMMM d, YYYY";
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    NSMutableString *period = [NSMutableString new];
    if (startDate) {
        [period appendString:[dateFormatter stringFromDate:startDate]];
        [period appendString:@" - "];
    }
    if (sameYear) {
        dateFormatter.dateFormat = @"MMMM d, YYYY";
    }
    if (endDate) {
        [period appendString:[dateFormatter stringFromDate:endDate]];
    }
    return period.copy;
}

@end
