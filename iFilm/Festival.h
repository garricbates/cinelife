//
//  Festival.h
//  iFilm
//
//  Created by Vlad Getman on 18.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Festival : NSObject

@property (nonatomic, strong) NSNumber *festivalId;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *dtStart;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *dtEnd;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lng;
@property (nonatomic, readonly) NSNumber *distance;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *venueName;
@property (nonatomic, strong) NSString *venueAddress;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, strong) NSString *websiteMain;
@property (nonatomic, strong) NSString *ticketsUrl;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSString *calcEndDate;
@property (nonatomic, strong) NSString *calcEndTime;
@property (nonatomic, strong) NSString *defaultEndTime;
@property (nonatomic, strong) NSString *concertEndTime;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, strong) NSString *categories;
@property (nonatomic, strong) NSString *promoStartDate;
@property (nonatomic, strong) NSString *promoEndDate;
@property (nonatomic, strong) NSNumber *isFavorited;
@property (nonatomic, strong) NSNumber *isPartner;
@property (nonatomic, strong) NSString *photoUrl;
@property (nonatomic, strong) NSArray *feeds;
@property (nonatomic, strong) NSNumber *pinned;
@property (nonatomic, strong) NSDictionary *branding;

- (instancetype)initWithJSON:(NSDictionary *)json;

+ (NSArray <Festival *> *)festivalsFromJSON:(id)json;

- (NSString *)formattedPeriod;

@end
