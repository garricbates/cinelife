//
//  SPNPurchaseTotalCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNPurchaseTotalCell : UITableViewCell

@property IBOutlet UILabel* totalConvenienceChargeLabel;
@property IBOutlet UILabel* convenienceChargeLabel;

@property IBOutlet UILabel* totalPurchaseLabel;
@property IBOutlet UILabel* totalLabel;

@property IBOutlet UIButton *creditCardButton;
@property IBOutlet UIButton *giftCardButton;
@property IBOutlet UIButton *membershipCardButton;
@property IBOutlet UIButton *nextButton;
@end
