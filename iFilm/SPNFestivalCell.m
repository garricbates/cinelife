//
//  SPNFestivalCell.m
//  iFilm
//
//  Created by Vlad Getman on 21.03.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFestivalCell.h"
#import "Festival.h"
#import "SPNPartnerButton.h"

@interface SPNFestivalCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UIButton *favoriteBtn;
@property (nonatomic, weak) IBOutlet AsyncImageView *logoView;
@property (nonatomic, weak) IBOutlet UILabel *milesAwayLabel;
@property (nonatomic, weak) IBOutlet UIButton *moreInfoButton;
@property (nonatomic, weak) IBOutlet SPNPartnerButton *partnerBtn;

@end

@implementation SPNFestivalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor spn_lightBrownColor];
    self.titleLabel.font = self.dateLabel.font = self.moreInfoButton.titleLabel.font = [UIFont spn_NeutraBoldLarge];
    self.addressLabel.font = [UIFont spn_NeutraMedium];
    self.milesAwayLabel.font = [UIFont spn_NeutraBoldTiny];
    self.logoView.crossfadeDuration = 0;
    
    self.partnerBtn.visible = NO;
    
    NSAttributedString *moreTitle = [[NSAttributedString alloc] initWithString:[_moreInfoButton titleForState:UIControlStateNormal]
                                                                    attributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
    [_moreInfoButton setAttributedTitle:moreTitle forState:UIControlStateNormal];
    
    [self layoutIfNeeded];
}

- (void)configureForFestival:(Festival *)festival {
    self.titleLabel.text = festival.summary;
    self.addressLabel.text = festival.venueAddress;
    
    NSString *faveImage;
    if ([SPNUser userLoggedIn] && festival.isFavorited.boolValue) {
        faveImage = @"heart-filled";
    }
    else {
        faveImage = @"heart-icon";
    }
    [self.favoriteBtn setImage:[UIImage imageNamed:faveImage]
                      forState:UIControlStateNormal];
    
    CGFloat distance = [festival.distance floatValue];
    
    NSString *distanceString = @"";
    if (distance > 50.0) {
        distanceString = [NSString stringWithFormat:@"50+\r\nmiles"];
    } else {
        distanceString = [NSString stringWithFormat:@"%.1f\r\nmiles", distance];
    }
    
    NSMutableAttributedString *distanceAttributeString = [[NSMutableAttributedString alloc] initWithString:distanceString];
    NSRange boldFontRange = NSMakeRange(0, 4);
    [distanceAttributeString beginEditing];
    [distanceAttributeString addAttribute:NSFontAttributeName
                                    value:[UIFont spn_NeutraBoldMedium]
                                    range:boldFontRange];
    [distanceAttributeString endEditing];
    self.milesAwayLabel.attributedText = distanceAttributeString;
    
    self.dateLabel.text = [festival formattedPeriod];
    
    self.partnerBtn.visible = festival.isPartner.boolValue;
    
    self.logoView.image = [UIImage imageNamed:@"special-event-poster"];
    [self.logoView setImageURL:[NSURL URLWithString:festival.photoUrl]];
}

- (IBAction)favoriteAction {
    [self.delegate didPressOnFavoriteInFestivalCell:self];
}

+ (CGFloat)heightForFestival:(Festival *)festival withWidth:(CGFloat)width {
    CGFloat contentWidth = width - 168;
    CGFloat height = 20;
    UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    UIFont *lightFont;
    if (@available(iOS 8.2, *)) {
        lightFont = [UIFont systemFontOfSize:13 weight:UIFontWeightLight];
    } else {
        lightFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
    }
    
    height += [festival.summary findHeightForWidth:contentWidth andFont:boldFont];
    height += 24;
    
    height += [[festival formattedPeriod] findHeightForWidth:contentWidth andFont:boldFont];
    height += 6;
    
    height += [festival.venueAddress findHeightForWidth:contentWidth andFont:lightFont];
    height += 45;
    
    return MAX(212, height);
}

@end
