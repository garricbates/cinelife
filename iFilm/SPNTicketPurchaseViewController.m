//
//  SPNTicketPurchaseViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTicketPurchaseViewController.h"

@interface SPNTicketPurchaseViewController ()

@end

@implementation SPNTicketPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.ticketingWebView loadRequest:request];
    [self.loadingLabel setHidden:YES];
    [self.loadingLabel setFont:[UIFont spn_NeutraMedium]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self.ticketingWebView setDelegate:nil];
    [self.ticketingWebView removeFromSuperview];
    self.ticketingWebView = nil;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.spinner startAnimating];
    [self.loadingLabel setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.spinner stopAnimating];
    [self.loadingLabel setHidden:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
