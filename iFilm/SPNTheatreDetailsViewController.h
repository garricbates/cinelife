//
//  SPNTheatreDetailsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"

#import "SPNTheatreNewsfeedCell.h"

#import "SPNMovieShowtimeCell.h"

#import "SPNCinelifeQueries.h"

typedef NS_ENUM(NSInteger, SPNTheatreSection) {
    SPNTheatreSectionNews = 0,
    SPNTheatreSectionFeed,
    SPNTheatreSectionShowtimes
};
@interface SPNTheatreDetailsViewController : GAITrackedViewController <UITextFieldDelegate, UIAlertViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, SPNCinelifeQueriesDelegate>

@property (nonatomic) SPNMovieShowtimeCell *prototypeShowCell;


@property NSDictionary *showtimeInformation;


@property (strong) UITextField *datePicker;
@property NSDictionary *theatreInformation;
@property NSArray *theatreSchedule, *cineLifePartners;
@property (nonatomic, strong) NSString *lastKnownLocation;
@property (nonatomic) BOOL isFavourite;

@property IBOutlet UITableView *theatreShowtimesTableView;

// Theatre data
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *addressLabel;
@property IBOutlet UITextView *phoneTextView;
@property IBOutlet UIView *theatreInformationContainerView;

@property IBOutlet UIImageView *theatreLogoImageView;

@property IBOutlet UIButton *addToFavouriteButton;
@property IBOutlet UIView *backgroundView;

- (IBAction)directionsButtonPressed:(id)sender;
- (IBAction)addToFavouriteButtonPressed:(id)sender;

@property IBOutlet UIImageView *heartImage;
@property IBOutlet UIBarButtonItem *heartToolButton;
@property IBOutlet UIImageView *phoneImage;
@property IBOutlet UIImageView *geoImage;

@property UICollectionView *dateCollectionView;
@end
