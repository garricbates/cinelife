//
//  SPNBaseTheaterViewController.h
//  iFilm
//
//  Created by Vlad Getman on 03.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

typedef NS_ENUM(NSInteger, SPNTheatreType)
{
    SPNTheatreTypeInRegion = 0,
    SPNTheatreTypeFavourites,
    SPNTheatreTypeFestival
};

@interface SPNBaseTheaterViewController : GAITrackedViewController <UIAlertViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, weak) IBOutlet UISearchBar *zipcodePicker;

@property (nonatomic, retain) NSMutableArray *theatersNearby;
@property (nonatomic, retain) NSMutableArray *myTheaters;
@property (nonatomic, retain) NSMutableArray *myFestivals;
@property (nonatomic, retain) NSMutableArray *festivals;

@property (nonatomic, retain) NSDictionary *filters;

@property (nonatomic) SPNTheatreType selectedTheaterType;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLGeocoder *geoCoder;

- (void)reloadData;
- (BOOL)isTable;

- (void)setCityAndStateToSearchBar:(NSDictionary *)placeDictionary;

- (void)doneClicked;
- (void)activateGPSLocation;
- (void)savedLocation;

- (void)getTheatersWithZip:(NSString*)zipcode orCoordinates:(CLLocation*)coordinates;
- (void)getFestivalsWithZip:(NSString *)zipcode orCoordinates:(CLLocation *)coordinates;
- (void)decodeAddress:(NSString *)address forFesivals:(BOOL)forFestivals;
- (void)favoritesNotFound;

@end
