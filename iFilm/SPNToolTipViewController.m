//
//  SPNToolTipViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNToolTipViewController.h"

@interface SPNToolTipViewController ()
{
    UIImageView *tooltipView;
}
@end

@implementation SPNToolTipViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil toolTipText:(NSString*)toolTipText
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.toolTipText = toolTipText;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customizeUI];
    
    
    self.toolTipLabel.text = self.toolTipText;

    // Do any additional setup after loading the view.
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI Customization
-(void)customizeUI
{
    [self.toolTipLabel setFont:[UIFont spn_NeutraBoldLarge]];
    
    [self.dismissButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.dismissButton setBackgroundColor:[UIColor spn_aquaColor]];
    [self.dismissButton.layer setCornerRadius:2.5];
    self.dismissButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
}

- (IBAction)dismissButtonPressed:(id)sender {
    [tooltipView removeFromSuperview];
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];

}

#pragma mark Tooltip functions
+(void)showWithToolTipText:(NSString*)toolTipText overViewController:(id)viewController
{
    SPNToolTipViewController *toolTipViewController = [[SPNToolTipViewController alloc]
                                                       initWithNibName:@"Tooltip"
                                                       bundle:nil
                                                       toolTipText:toolTipText];
    [viewController addChildViewController:toolTipViewController];
    [toolTipViewController show];
}

-(void)show
{
    NSString* toolTipText = self.toolTipText;
    if (toolTipText) {
       [self showWithToolTipText:toolTipText];
    }
    else {
        [self showWithToolTipText:@""];
    }
}

-(void)showWithToolTipText:(NSString*)toolTipText
{
    self.toolTipText = toolTipText;
    UIViewController *controller = self.parentViewController;
    UIGraphicsBeginImageContextWithOptions(controller.view.frame.size, YES, 0.4);
    
    [controller.view drawViewHierarchyInRect:controller.view.frame
                          afterScreenUpdates:NO];
  
    UIImage *blurImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //Blur the UIImage
    CIImage *imageToBlur = [CIImage imageWithCGImage:blurImg.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];

    //change number to increase/decrease blur
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat:3]
                          forKey:@"inputRadius"];
    
    CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    //Place the UIImage in a UIImageView
    tooltipView = [[UIImageView alloc] initWithFrame:controller.view.frame];
    CGImageRef cgImage2 = [context createCGImage:resultImage
                                        fromRect:[imageToBlur extent]];
    
    tooltipView.image = [UIImage imageWithCGImage:cgImage2];
    UIView *shieldView = [[UIView alloc] initWithFrame:tooltipView.frame];
    [shieldView setBackgroundColor:[UIColor blackColor]];
    [shieldView setAlpha:0.5];
    
    [tooltipView addSubview:shieldView];
    [self.view setFrame:controller.view.frame];
    [self.view setAlpha:1];
    
    //insert blur UIImageView below transparent view inside the blur image container
    [controller.view addSubview:self.view];
    [controller.view insertSubview:tooltipView belowSubview:self.view];
}

@end
