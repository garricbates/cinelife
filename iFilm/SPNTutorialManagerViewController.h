//
//  SPNTutorialManagerViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@protocol SPNTutorialManagerViewControllerDelegate <NSObject, UIAlertViewDelegate>
-(void)spnTutorialManagerDidClose;
@end


@interface SPNTutorialManagerViewController : GAITrackedViewController <UIPageViewControllerDataSource>

@property (nonatomic, strong) UIPageViewController *tutorialController;

@property (nonatomic, assign) id <SPNTutorialManagerViewControllerDelegate> delegate;


@property () IBOutlet UILabel *remindMeLabel;
@property () IBOutlet UIButton *skipButton;
@property () IBOutlet UIView *statusBarBackgroundView;

- (IBAction)skipButtonPressed:(id)sender;
- (IBAction)remindMeSwitchValueChanged:(id)sender;

@end
