//
//  SPNPosterViewController.h
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNPosterViewController : UIViewController

+ (void)presentWithURL:(NSString *)url;

@end
