//
//  WebViewController.h
//  iFilm
//
//  Created by Vlad Getman on 16.12.16.
//  Copyright © 2016 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *urlString;

@end
