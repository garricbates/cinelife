//
//  SPNShowMoreCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNShowMoreCell.h"

@implementation SPNShowMoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initializeCustomUIForCell];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initializeCustomUIForCell];
}

-(void)initializeCustomUIForCell {
    self.showMoreButton.layer.cornerRadius = 2.5;
    self.showMoreButton.layer.masksToBounds = YES;
    [self.showMoreButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
}

@end
