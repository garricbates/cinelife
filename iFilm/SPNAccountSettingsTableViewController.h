//
//  SPNAccountSettingsTableViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPNCinelifeQueries.h"

#import <GoogleSignIn/GoogleSignIn.h>
//@"909685742375-top2olp3p98vs2but0o5qf5ioenj9n56.apps.googleusercontent.com";
@interface SPNAccountSettingsTableViewController : UITableViewController <UIAlertViewDelegate, SPNCinelifeQueriesDelegate, GIDSignInDelegate,GIDSignInUIDelegate>

@property IBOutlet UIButton *logOutButton;

@end
