//
//  LCPDatabaseHelper.m
//  Universades
//
//  Created by La Casa de los Pixeles on 7/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "LCPDatabaseHelper.h"
#import "Films.h"
#import "Users.h"

@implementation LCPDatabaseHelper

static LCPDatabaseHelper *sharedInstance;

-(id)initWithManagedObjectContext:(NSManagedObjectContext*)context
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.managedObjectContext = context;
    }
    return self;
}
-(id)init
{
    return [self initWithManagedObjectContext:nil];
}

-(id)initWithContext:(NSManagedObjectContext *)context
{
    return [self initWithManagedObjectContext:context];
}

+(LCPDatabaseHelper *) sharedInstance
{
    if (!sharedInstance) {
        sharedInstance = [[LCPDatabaseHelper alloc] init];
    }
    return sharedInstance;
}

-(BOOL)addRecord:(NSDictionary *)newRecordInformation forEntity:(NSString*)entityName
{
    BOOL succeeded = NO;
    NSError *error = nil;
    NSEntityDescription *newEntity = (NSEntityDescription*)[NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                   inManagedObjectContext:self.managedObjectContext];
    
    for (NSString *element in [newRecordInformation allKeys])
    {
        [newEntity setValue:[newRecordInformation objectForKey:element]
                     forKey:element];
    }
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error while saving the comment to the database: %@", [error localizedDescription]);
    }
    else {
        succeeded = YES;
    }
    return succeeded;
}

-(BOOL)deleteRecordWithAttribute:(id)attribute forAttributeKey:(NSString*)attributeKey forEntity:(NSString*)entityName
{
    BOOL succeeded = NO;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                           entityForName:entityName
                                  inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(%K = %@)", attributeKey, attribute];
    [fetchRequest setPredicate:searchFilter];
    
    NSError *error = nil;
    NSArray *filteredEntitiesArray = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                              error:&error];
    for (NSManagedObject *filteredEntity in filteredEntitiesArray)
    {
        [self.managedObjectContext deleteObject:filteredEntity];
    }
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error while deleting objects in the database: %@", [error localizedDescription]);
    }
    else {
        succeeded = YES;
    }
    return succeeded;
}

-(BOOL)deleteRecord:(NSManagedObject*)recordToDelete
{
    BOOL succeeded = NO;
    NSError *error = nil;
    [self.managedObjectContext deleteObject:recordToDelete];
    if ([self.managedObjectContext save:&error]) {
        succeeded = YES;
    }
    return succeeded;
}

-(BOOL)updateRecordWithAttribute:(id)attribute withNewAttribute:(id)newAttribute forAttributeKey:(NSString*)attributeKey forEntity:(NSString*)entityName
{
    BOOL succeeded = NO;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName
                          inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(%K = %@)", attributeKey, attribute];
    [fetchRequest setPredicate:searchFilter];

    NSError *error = nil;
    NSArray *filteredEntitiesArray = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                              error:&error];
    
    if ([filteredEntitiesArray count] < 1) {
        succeeded = NO;
    }
    else {
        for (NSManagedObject *filteredEntity in filteredEntitiesArray)
        {
            [filteredEntity setValue:newAttribute
                              forKey:attributeKey];
        }
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Error while updating objects in the database): %@", [error localizedDescription]);
        }
        else {
            succeeded = YES;
        }
    }
    return succeeded;
}

-(BOOL)updateRecord:(NSManagedObject *)recordToUpdate withNewAttribute:(id)newAttribute forAttributeKey:(NSString *)attributeKey
{
    NSError *error = nil;
    BOOL succeeded = NO;
    [recordToUpdate setValue:newAttribute
                      forKey:attributeKey];
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error while updating objects in the database): %@", [error localizedDescription]);
    }
    else {
        succeeded = YES;
    }
    return succeeded;
}

-(NSArray *)fetchAllRecordsForEntity:(NSString *)entityName
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName
                                   inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *recordList = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                   error:&error];
    return recordList;
}

-(NSArray *)fetchRecordsWithAttribute:(id)attribute forAttributeKey:(NSString *)attributeKey forEntity:(NSString *)entityName
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName
                                   inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(%K = %@)", attributeKey, attribute];
    [fetchRequest setPredicate:searchFilter];
    
    NSError *error = nil;
    NSArray *filteredEntitiesArray = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                              error:&error];
    return filteredEntitiesArray;
}

-(NSManagedObject *)fetchFirstRecordWithAttribute:(id)attribute forAttributeKey:(NSString *)attributeKey forEntity:(NSString *)entityName
{
    NSManagedObject *fetchedEntity = nil;
    NSArray *filteredEntitiesArray = [self fetchRecordsWithAttribute:attribute
                                                     forAttributeKey:attributeKey
                                                           forEntity:entityName];
    if ([filteredEntitiesArray count] > 0) {
        fetchedEntity = [filteredEntitiesArray firstObject];
    }
    return fetchedEntity;
}

-(BOOL)recordExistsOnDatabase:(NSManagedObject *)record forEntity:(NSString*)entityName
{
    BOOL succeeded = NO;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity =  [NSEntityDescription entityForName:entityName
                                               inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self == %@", record];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *filteredRecordsArray = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                             error:&error];
    if (filteredRecordsArray != nil) {
        NSUInteger count = [filteredRecordsArray count]; // May be 0 if the object has been deleted.
        succeeded = count > 0 ? YES : NO;
    }
    return succeeded;
}

#pragma mark - Entities with relationship functions
-(BOOL)addMovieToFavourites:(NSDictionary*)movie forUser:(NSString*)username
{
    if (!movie) {
        return NO;
    }
    NSArray *existingFilms = [self fetchRecordsWithAttribute:[movie objectForKey:@"movieId"]
                                             forAttributeKey:@"movieId"
                                                   forEntity:@"Films"];
    
    Films *newFilm;
    if([existingFilms count] < 1) {
        newFilm = [NSEntityDescription  insertNewObjectForEntityForName:@"Films"
                                                 inManagedObjectContext:self.managedObjectContext];
        [newFilm setValue:[movie objectForKey:@"title"] forKey:@"title"];
        [newFilm setValue:[movie objectForKey:@"movieId"] forKey:@"movieId"];
        
    }
    else {
        newFilm = [existingFilms firstObject];
    }

    NSError *error;
  
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error while saving the movie to the database: %@", [error localizedDescription]);
        return NO;
    }
    else
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *user = [NSEntityDescription
                                     entityForName:@"Users" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:user];
        fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[user propertiesByName] objectForKey:@"userId"]];
        NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(userId = %@)", username];
        [fetchRequest setPredicate:searchFilter];
        NSError *error;
        NSArray *userList = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        Users *postingUser = [userList firstObject];
        [postingUser addMyFilmsObject:newFilm];
        
        if(![self.managedObjectContext save:&error]) {
            NSLog(@"Error while saving the relationship (movieUser-user): %@", [error localizedDescription]);
            return NO;
        }
        else {
            NSLog(@"Step Three Complete - Relationship Stored");
        }
    }
    return YES;
    
}

-(NSArray *)getAllMoviesForUser:(NSString*)username
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *Films = [NSEntityDescription
                                   entityForName:@"Films" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:Films];
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(user.userId = %@)", username];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setPredicate:searchFilter];
    NSError *error;
    NSArray *filmList = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return filmList;
}

-(NSArray *)getMovieIdsForUser:(NSString*)username
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *Films = [NSEntityDescription
                                  entityForName:@"Films" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:Films];
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[Films propertiesByName] objectForKey:@"movieId"]];
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(user.userId = %@)", username];
    [fetchRequest setResultType:NSDictionaryResultType];
    [fetchRequest setPredicate:searchFilter];
    NSError *error;
    NSArray *filmList = [self.managedObjectContext executeFetchRequest:fetchRequest
                                                                 error:&error];
    NSMutableArray *newFilmList = [NSMutableArray new];
    for (NSDictionary *film in filmList) {
        [newFilmList addObject:[film objectForKey:@"movieId"]];
    }
    filmList = [NSArray arrayWithArray:newFilmList];
    return filmList;
}

-(BOOL)deleteMovie:(NSString*)movieId ofUser:(NSString*)username
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favouriteEntity = [NSEntityDescription
                                            entityForName:@"Films"
                                            inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:favouriteEntity];
    NSPredicate *searchFilter = [NSPredicate predicateWithFormat:@"(user.userId = %@) AND (movieId = %@)", username, movieId];
    [fetchRequest setPredicate:searchFilter];
    NSError *error;
    NSArray *movieList = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Films *removedFilm = [movieList firstObject];
    [removedFilm.user removeMyFilmsObject:removedFilm];
    
    if(![self.managedObjectContext save:&error]) {
        return NO;
    }
    return YES;
}

-(BOOL)addCreditCardToDatabase:(NSDictionary*)creditCard
{
    /*
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *card = [NSEntityDescription
                                 entityForName:@"CreditCard" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:card];
    NSError *error;
    NSArray *cardList = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([cardList count] < 1) {
        CreditCard *newCard = [NSEntityDescription insertNewObjectForEntityForName:@"CreditCard"
                                                            inManagedObjectContext:self.managedObjectContext];
        
        [newCard setValue:[creditCard objectForKey:@"name"] forKey:@"name"];
        [newCard setValue:[creditCard objectForKey:@"cardNumber"] forKey:@"cardNumber"];
        [newCard setValue:[creditCard objectForKey:@"cardType"] forKey:@"cardType"];
        [newCard setValue:[creditCard objectForKey:@"expirationDate"] forKey:@"expirationDate"];
        NSError *error;
        
        if(![self.managedObjectContext save:&error]) {
            NSLog(@"Error while saving the card to the database: %@", [error localizedDescription]);
            return NO;
        }
        else {
            NSLog(@"Successfully stored the card");
        }
        return YES;
    }
    else {
        for (NSManagedObject * card in cardList) {
            [self.managedObjectContext deleteObject:card];
        }
        NSError *saveError = nil;
        [self.managedObjectContext save:&saveError];
        if(![self.managedObjectContext save:&error]) {
            NSLog(@"Error while deleting existing cards in the database: %@", [error localizedDescription]);
            return NO;
        }
        else {
            NSLog(@"Successfully deleted the cards");
        }
        
        CreditCard *newCard = [NSEntityDescription insertNewObjectForEntityForName:@"CreditCard"
                                                            inManagedObjectContext:self.managedObjectContext];
        
        [newCard setValue:[creditCard objectForKey:@"name"] forKey:@"name"];
        [newCard setValue:[creditCard objectForKey:@"cardNumber"] forKey:@"cardNumber"];
        [newCard setValue:[creditCard objectForKey:@"cardType"] forKey:@"cardType"];
        [newCard setValue:[creditCard objectForKey:@"expirationDate"] forKey:@"expirationDate"];
        NSError *error;
        
        if(![self.managedObjectContext save:&error]) {
            NSLog(@"Error while saving the card to the database: %@", [error localizedDescription]);
            return NO;
        }
        else {
            NSLog(@"Successfully stored the card");
        }
        return YES;

    }*/
    return YES;
}

-(NSDictionary *)getCardInformation
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *card = [NSEntityDescription
                                 entityForName:@"CreditCard" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:card];
    [fetchRequest setResultType:NSDictionaryResultType];
    NSError *error;
    NSDictionary *cardInformation = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] firstObject];
    
    return cardInformation;
}

-(BOOL)deleteCardsInDatabase
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *card = [NSEntityDescription
                                 entityForName:@"CreditCard" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:card];
    NSError *error;
    NSArray *cardList = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject * card in cardList) {
        [self.managedObjectContext deleteObject:card];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    if(![self.managedObjectContext save:&error]) {
        NSLog(@"Error while deleting existing cards in the database: %@", [error localizedDescription]);
        return NO;
    }
    else {
        NSLog(@"Successfully deleted the cards");
        return YES;
    }
}
@end
