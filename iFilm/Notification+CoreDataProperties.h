//
//  Notification+CoreDataProperties.h
//  iFilm
//
//  Created by Vlad Getman on 19.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "Notification+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *notificationId;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSNumber *typeId;
@property (nullable, nonatomic, copy) NSNumber *theaterId;
@property (nullable, nonatomic, copy) NSNumber *festivalId;
@property (nullable, nonatomic, copy) NSNumber *filmId;
@property (nullable, nonatomic, copy) NSDate *date;

@end

NS_ASSUME_NONNULL_END
