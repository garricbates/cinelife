//
//  SPNFilmDataCache.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmDataCache.h"

@implementation SPNFilmDataCache

static SPNFilmDataCache *sharedInstance;

+(SPNFilmDataCache *) sharedInstance
{
    if (!sharedInstance) {
        sharedInstance = [SPNFilmDataCache new];
    }
    return sharedInstance;
}

+ (NSCache *)defaultMetaScoreCache
{
    static NSCache *sharedMetaCache = nil;
    if (sharedMetaCache == nil)
    {
        sharedMetaCache = [[NSCache alloc] init];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(__unused NSNotification *note) {
            
            [sharedMetaCache removeAllObjects];
        }];
    }
    return sharedMetaCache;
}

+ (NSCache *)defaultCache
{
    static NSCache *sharedCache = nil;
	if (sharedCache == nil)
	{
		sharedCache = [[NSCache alloc] init];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(__unused NSNotification *note) {
            
            [sharedCache removeAllObjects];
        }];
	}
	return sharedCache;
}

+ (NSCache *)defaultReviewCache
{
    static NSCache *sharedReviewCache = nil;
    if (sharedReviewCache == nil)
    {
        sharedReviewCache = [[NSCache alloc] init];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(__unused NSNotification *note) {
            
            [sharedReviewCache removeAllObjects];
        }];
    }
    return sharedReviewCache;
}


- (SPNFilmDataCache *)init
{
	if ((self = [super init]))
	{
        self.filmDataCache = [[self class] defaultCache];
        self.metaScoreCache = [[self class] defaultMetaScoreCache];
        self.anonymousReviewCache = [[self class] defaultReviewCache];
    }
	return self;
}
@end
