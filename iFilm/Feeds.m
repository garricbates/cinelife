//
//  Feeds.m
//  iFilm
//
//  Created by Vlad Getman on 20.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "Feeds.h"
#import "SPNCinelifeQueries.h"

@interface Feeds () {
    NSDictionary *_rssFeed;
    NSString *_facebookFeed;
    NSString *_twitterFeed;
    
    NSDateFormatter *dateFormatter;
}

@property (nonatomic, strong) NSArray *feeds;
@property (nonatomic, strong) NSArray *posts;

@end

@implementation Feeds

- (instancetype)initWithFeeds:(NSArray *)feeds posts:(NSArray *)posts {
    self = [super init];
    if (self) {
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        
        self.feeds = feeds;
        self.posts = posts;
        for (NSDictionary *feed in feeds) {
            NSString *type = [feed objectForKey:@"type"];
            if ([type isEqualToString:@"RSS"]) {
                _rssFeed = feed;
            } else if ([type isEqual:@"Twitter"]) {
                _twitterFeed = feed[@"source"];
            } else if ([type isEqual:@"Facebook"]) {
                _facebookFeed = feed[@"source"];
            }
        }
        [self update];
    }
    return self;
}

- (BOOL)hasRSS {
    return _rssFeed != nil;
}

- (void)update {
    [self getDashboardPosts];
    [self getSocials];
}

- (NSAttributedString *)htmlAtIndex:(NSInteger)index {
    if (_htmlNews.count > 0 && [_htmlNews[index] isKindOfClass:[NSAttributedString class]]) {
        return _htmlNews[index];
    }
    return nil;
}

- (void)getDashboardPosts {
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    
    self.news = self.posts;
    
    NSArray *theaterRSSFeed;
    if (_rssFeed) {
        theaterRSSFeed = [SPNCinelifeQueries getStoriesForTheaterWithFeeds:@[_rssFeed] withPostCount:NewsLimit];
    }
    
    NSMutableArray *postsWithoutComingSoon = [NSMutableArray arrayWithArray:self.posts];
    for (NSDictionary *post in self.posts) {
        if ([[post objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
            [postsWithoutComingSoon removeObject:post];
        }
    }
    self.news = postsWithoutComingSoon;
    
    NSMutableArray *sortedNews = [NSMutableArray new];
    for (NSDictionary *entry in theaterRSSFeed) {
        NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"publishedDate"]];
        if (!realDate) {
            realDate = [NSDate date];
        }
        NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
        [entryWithDate setObject:realDate
                          forKey:@"publishedDate"];
        
        if ([[entry objectForKey:@"page"] isEqualToString:@"RSS"]) {
            if (sortedNews.count < NewsLimit) {
                [sortedNews addObject:entryWithDate];
            }
        }
    }
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
    if (!self.news) {
        self.news = sortedNews;
    }
    else if (sortedNews.count > 0) {
        if (sortedNews.count <= NewsLimit) {
            self.news = [self.news arrayByAddingObjectsFromArray:[sortedNews sortedArrayUsingDescriptors:@[sortDescriptor]]];
        }
        else {
            self.news = [[self.news subarrayWithRange:NSMakeRange(0, 2)] arrayByAddingObjectsFromArray:[sortedNews sortedArrayUsingDescriptors:@[sortDescriptor]]];
        }
    }
    
    NSError *parseError = nil;
    NSAttributedString *attributedHTMLText;
    
    NSMutableArray *htmlList = [NSMutableArray new];
    
    for (NSDictionary *post in self.news) {
        if ([post objectForKey:@"summary"]) {
            NSString *normalText = [post objectForKey:@"summary"];
            attributedHTMLText = [[NSAttributedString alloc] initWithData:[normalText dataUsingEncoding:NSUTF8StringEncoding]
                                                                  options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                       documentAttributes:nil
                                                                    error:&parseError];
            if (!parseError) {
                [htmlList addObject:attributedHTMLText];
            }
            else {
                [htmlList addObject:@""];
            }
        }
        else {
            [htmlList addObject:@""];
        }
    }
    self.htmlNews = htmlList;
}

- (void)getSocials {
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssxxxx"];
    NSArray *fbFeed, *twFeed;
    NSMutableArray *bothFeeds = [NSMutableArray new];
    
    if (_facebookFeed.length > 0) {
        fbFeed = [SPNCinelifeQueries getFacebookFeed:_facebookFeed
                                           withLimit:NewsLimit];
        
    }
    if (_twitterFeed.length > 0) {
        twFeed = [SPNCinelifeQueries getTwitterFeed:_twitterFeed
                                          withLimit:NewsLimit];
    }
    NSMutableArray *newFbFeed = [NSMutableArray new];
    NSMutableArray *newTwFeed = [NSMutableArray new];
    
    if (fbFeed && [fbFeed isKindOfClass:[NSArray class]]) {
        for (NSDictionary *entry in fbFeed)
        {
            // Format: 2015-09-30T19:43:05+0000
            NSString *createdDate = [entry objectForKey:@"datetime"];
            NSDate *realDate = [dateFormatter dateFromString:createdDate];
            if (!realDate) {
                realDate = [NSDate date];
            }
            NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
            [entryWithDate setObject:realDate
                              forKey:@"publishedDate"];
            [entryWithDate setObject:@"Facebook"
                              forKey:@"page"];
            
            [newFbFeed addObject:entryWithDate];
        }
    }
    if (twFeed && [twFeed isKindOfClass:[NSArray class]]) {
        for (NSDictionary *entry in twFeed)
        {
            // Format: 2015-09-30T19:43:05+0000
            NSString *createdDate = [entry objectForKey:@"datetime"];
            NSDate *realDate = [dateFormatter dateFromString:createdDate];
            if (!realDate) {
                realDate = [NSDate date];
            }
            NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
            [entryWithDate setObject:realDate
                              forKey:@"publishedDate"];
            [entryWithDate setObject:@"Twitter"
                              forKey:@"page"];
            
            [newTwFeed addObject:entryWithDate];
        }
    }
    if ([newTwFeed count] > 0 && [newFbFeed count] > 0) {
        [bothFeeds addObject: [newTwFeed firstObject]];
        [bothFeeds addObject: [newFbFeed firstObject]];
    }
    else if ([newTwFeed count] > 0) {
        [bothFeeds addObjectsFromArray:newTwFeed];
    }
    else if ([newFbFeed count] > 0) {
        [bothFeeds addObjectsFromArray:newFbFeed];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
    self.socials = [bothFeeds sortedArrayUsingDescriptors:@[sortDescriptor]];
}

- (void)postsForItem:(NSDictionary *)item completion:(void(^)(NSArray *posts))completion {
    UIView *loadingView = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD showHUDAddedTo:loadingView animated:YES];
    dispatch_async(kBgQueue, ^{
        NSString *page = item[@"page"];
        BOOL facebook = [page isEqual:@"Facebook"];
        NSString *feed = facebook ? _facebookFeed : _twitterFeed;
    
        NSArray *posts;
        if (facebook) {
            posts = [SPNCinelifeQueries getFacebookFeed:feed withLimit:50];
        } else {
            posts = [SPNCinelifeQueries getTwitterFeed:feed withLimit:50];
        }
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
        NSMutableArray *postsWithDate = [NSMutableArray new];
        for (NSDictionary *entry in posts)
        {
            NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"publishedDate"]];
            if (!realDate) {
                realDate = [NSDate date];
            }
            NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
            [entryWithDate setObject:realDate
                              forKey:@"publishedDate"];
            [entryWithDate setObject:page forKey:@"page"];
            [postsWithDate addObject:entryWithDate];
        }
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
        NSArray *list = [postsWithDate sortedArrayUsingDescriptors:@[sortDescriptor]];
        if (!list) {
            list = @[];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:loadingView animated:YES];
            completion(list);
        });
    });
}

- (void)postsForSocialsWithCompletion:(void(^)(NSArray *posts))completion {
    UIView *loadingView = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD showHUDAddedTo:loadingView animated:YES];
    
    dispatch_async(kBgQueue, ^{
        
        NSMutableArray *fbPosts = [NSMutableArray new];
        
        NSMutableArray *twPosts = [NSMutableArray new];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssxxxx"];
        if (_facebookFeed) {
            NSArray *facebookPosts = [SPNCinelifeQueries getFacebookFeed:_facebookFeed withLimit:50];
            
            for (NSDictionary *entry in facebookPosts)
            {
                NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"datetime"]];
                if (!realDate) {
                    realDate = [NSDate date];
                }
                NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
                [entryWithDate setObject:realDate
                                  forKey:@"publishedDate"];
                [entryWithDate setObject:@"Facebook" forKey:@"page"];
                [fbPosts addObject:entryWithDate];
            }
        }
        if (_twitterFeed) {
            NSArray *twitterPosts = [SPNCinelifeQueries getTwitterFeed:_twitterFeed withLimit:50];
            for (NSDictionary *entry in twitterPosts)
            {
                NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"datetime"]];
                if (!realDate) {
                    realDate = [NSDate date];
                }
                NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
                [entryWithDate setObject:realDate
                                  forKey:@"publishedDate"];
                [entryWithDate setObject:@"Twitter" forKey:@"page"];
                [twPosts addObject:entryWithDate];
            }
        }
        
        NSArray *posts = [fbPosts arrayByAddingObjectsFromArray:twPosts];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
        NSArray *list = [posts sortedArrayUsingDescriptors:@[sortDescriptor]];
        if (!list) {
            list = @[];
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:loadingView animated:YES];
            completion(list);
        });
    });
}

- (void)storiesWithPosts:(NSArray *)posts
       shouldFetchRSSToo:(NSInteger)shouldFetchRSSToo
              completion:(void(^)(NSArray *posts))completion {
    if (_rssFeed) {
        dispatch_async(kBgQueue, ^{
            NSArray *feedPosts = [SPNCinelifeQueries getStoriesForTheaterWithFeeds:@[_rssFeed]
                                                                     withPostCount:10];
            [dateFormatter setDateFormat:@"EEE, d MMM yyyy HH:mm:ss Z"];
            NSMutableArray *postsWithDate = [NSMutableArray new];
            for (NSDictionary *entry in feedPosts)
            {
                NSDate *realDate = [dateFormatter dateFromString:[entry objectForKey:@"publishedDate"]];
                if (!realDate) {
                    realDate = [NSDate date];
                }
                NSMutableDictionary *entryWithDate = [NSMutableDictionary dictionaryWithDictionary:entry];
                [entryWithDate setObject:realDate
                                  forKey:@"publishedDate"];
                [postsWithDate addObject:entryWithDate];
            }
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"publishedDate" ascending: NO];
            NSArray *postList = [postsWithDate sortedArrayUsingDescriptors:@[sortDescriptor]];
            
            NSMutableArray *postsWithoutComingSoon = [NSMutableArray arrayWithArray:posts];
            for (NSDictionary *post in posts)
            {
                if ([[post objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
                    [postsWithoutComingSoon removeObject:post];
                }
            }
            
            if (shouldFetchRSSToo == 2) {
                postList = [postsWithoutComingSoon arrayByAddingObjectsFromArray:postList];
            }
            else if (shouldFetchRSSToo == 0) {
                postList = postsWithoutComingSoon;
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(postList);
            });
        });
    } else {
        completion(posts);
    }
}

@end
