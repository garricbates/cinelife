//
//  Films.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Users;

@interface Films : NSManagedObject

@property (nonatomic, retain) NSString * movieId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Users *user;

@end
