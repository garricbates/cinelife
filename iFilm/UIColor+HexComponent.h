//
//  UIColor+HexComponent.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/13/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexComponent)
+(UIColor*)colorFromHexString:(NSString*)color;
+(CGFloat) colorComponentFrom:(NSString *)hexString
                        start:(NSUInteger)start
                       length:(NSUInteger)length;

+(UIColor*)spn_notSoLightBrownColor;
+(UIColor*)spn_lightBrownColor;
+(UIColor*)spn_brownColor;
+(UIColor*)spn_darkBrownColor;

+(UIColor*)spn_darkGrayColor;

+(UIColor*)spn_buttonColor;
+(UIColor*)spn_buttonShadowColor;

+(UIColor*)spn_aquaColor;
+(UIColor*)spn_aquaShadowColor;

+(UIColor*)fb_blueColor;
+(UIColor*)tw_blueColor;
+(UIColor*)rss_orangeColor;

+(UIColor*)mta_greenColor;
+(UIColor*)mta_yellowColor;
+(UIColor*)mta_redColor;

@end
