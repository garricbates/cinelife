//
//  SPNEmailInvitesViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNEmailInvitesViewController.h"
#import "SPNAppDelegate.h"


@interface SPNEmailInvitesViewController ()
{
    NSMutableDictionary *selectedContacts;
}
@end

@implementation SPNEmailInvitesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectedContacts = [NSMutableDictionary new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.possibleContacts count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *contact = [self.possibleContacts objectAtIndex:section];
    NSString *fullName = [contact objectForKey:@"name"];
    return fullName;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *contact = [self.possibleContacts objectAtIndex:section];
    return [[contact objectForKey:@"emails"] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *InviteCellIdentifier = @"ContactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:InviteCellIdentifier];
    NSDictionary *contact = [self.possibleContacts objectAtIndex:indexPath.section];
    NSArray *emails = [contact objectForKey:@"emails"];
    NSString *email = [emails objectAtIndex:indexPath.row];
    [cell.textLabel setText:email];
    
    if ([selectedContacts objectForKey:indexPath]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *contact = [self.possibleContacts objectAtIndex:indexPath.section];
    NSArray *emails = [contact objectForKey:@"emails"];
    NSString *email = [emails objectAtIndex:indexPath.row];
    
    if ([selectedContacts objectForKey:indexPath]) {
        [selectedContacts removeObjectForKey:indexPath];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [selectedContacts setObject:email
                             forKey:indexPath];
    }
}

-(IBAction)sendMail:(id)sender
{
    [self createEmailMessageForContacts:selectedContacts];
}

-(void)createEmailMessageForContacts:(NSDictionary*)contacts
{
    if  ([MFMailComposeViewController canSendMail]) {
        
       
        // Email Subject
        NSString *emailTitle = @"Try out CineLife!";
        // Email Content
        NSString *messageBody = @"CineLife features theatres screening the best in art and independent film as well as all of the Hollywood favorites. CineLife is the one mobile source connecting you to your favorite theatres, quality film, exclusive events and screenings, festivals and industry news. Give it a try!";
        // To address
        
        NSArray *toRecipents = [contacts allValues];
        
        APP.globalMailComposer.mailComposeDelegate = self;
        [APP.globalMailComposer setSubject:emailTitle];
        [APP.globalMailComposer setMessageBody:messageBody isHTML:NO];
        [APP.globalMailComposer setToRecipients:toRecipents];
        // Present mail view controller on screen
        
        [self presentViewController:APP.globalMailComposer animated:YES completion:nil];
    }
    else   {
        [UIAlertView showAlertViewWthTitle:@"Did you configure your mail?"
                                andMessage:@"We could not mail anything. Maybe there's no email account configured on this device?"];
        [APP cycleTheGlobalMailComposer];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:^{
        [APP cycleTheGlobalMailComposer];
    }];
}
@end
