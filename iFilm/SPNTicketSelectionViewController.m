//
//  SPNTicketSelectionViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTicketSelectionViewController.h"
#import "SPNPaymentInformationViewController.h"

#import "SPNDismissCell.h"
#import "SPNPurchaseTotalCell.h"
#import "SPNTicketCell.h"
#import "SPNTicketsTitleCell.h"


#define kTitleCellHeight 20
#define kTicketCellHeight 73
#define kPurchaseCellHeight 278

@interface SPNTicketSelectionViewController ()
{
    NSArray *ticketTypes;
    NSMutableDictionary *selectedTickets;
    SPNPaymentMethod selectedPaymentMethod;
    NSMutableDictionary *paymentDetails;
    UIColor *sectionColor, *titleSectionColor, *mainColor;
    BOOL shouldShowDismissCell, shouldAddBackground, shouldShowChargeCell;
    CGFloat convenienceCharge;

}
@end

@implementation SPNTicketSelectionViewController

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(abortPurchase:)
                                                 name:@"cancelPurchase"
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!shouldAddBackground) {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.ticketSelectionTableView.contentSize.height - 70, self.ticketSelectionTableView.bounds.size.width, 600)];
        [bottomView setBackgroundColor:[UIColor spn_brownColor]];
        if (mainColor) {
            [bottomView setBackgroundColor:mainColor];
        }
        [self.ticketSelectionTableView addSubview:bottomView];
        [self.ticketSelectionTableView sendSubviewToBack:bottomView];
        shouldAddBackground = YES;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    convenienceCharge = 0;
    if ([self.ticketingInformation objectForKey:@"ticket_fee"]) {
        convenienceCharge = [[self.ticketingInformation objectForKey:@"ticket_fee"] floatValue];
    }
    
    ticketTypes = [self.ticketingInformation objectForKey:@"tickets_available"];
    selectedTickets = [NSMutableDictionary new];
    selectedPaymentMethod = SPNPaymentMethodCreditCard;
    
  //  [self.navigationController setTitle:@"Buy Tickets"];
    [self initializeFilmData:self.movieInformation];
    
    paymentDetails = [NSMutableDictionary new];
    
    shouldShowDismissCell = YES;
    shouldShowChargeCell = YES;
    [self customizeUI];
    [self getBrandColors:self.branding];
    

}

-(void)abortPurchase:(id)sender {
 
    [UIAlertView showAlertViewWthTitle:@"Showtime not available" andMessage:@"The showtime you selected is not available anymore. Please select a new showtime to purchase tickets!"];

    NSArray *controllers = [self.navigationController childViewControllers];
    [self.navigationController popToViewController:[controllers objectAtIndex:1] animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI customization
-(void)customizeUI
{

    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.genresLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.userRatingLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.view setBackgroundColor:[UIColor spn_darkBrownColor]];
    [self.theatreBackgroundView setBackgroundColor:[UIColor spn_darkBrownColor]];
    
    
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        
        NSString *logo = [branding objectForKey:@"logo"];
        
        if ([logo length] > 0) {
            
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
        }
        else {
            
            UIView *header = self.ticketSelectionTableView.tableHeaderView;
            CGRect headerFrame = header.frame;
            CGFloat spaceForLogo = 125;
            headerFrame.size.height  = spaceForLogo;
            header.frame  = headerFrame;
            
            [self.ticketSelectionTableView setTableHeaderView:header];
            [self.ticketSelectionTableView beginUpdates];
            [self.ticketSelectionTableView endUpdates];
            

        }
        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            titleSectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
        }
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            [self.view setBackgroundColor:mainColor];
            [self.theatreBackgroundView setBackgroundColor:mainColor];
        }
    }
    else {
        [self.brandLogoHeightConstraint setConstant:0];
        UIView *header = self.ticketSelectionTableView.tableHeaderView;
        CGRect headerFrame = header.frame;
        CGFloat spaceForLogo = 125;
        headerFrame.size.height  = spaceForLogo;
        header.frame  = headerFrame;
        
        [self.ticketSelectionTableView setTableHeaderView:header];
        [self.ticketSelectionTableView beginUpdates];
        [self.ticketSelectionTableView endUpdates];
    }
}


#pragma mark - Film initialization
-(void)initializeFilmData:(NSDictionary*)film
{
    NSString *movieTitle =[film objectForKey:@"name"] ? [film objectForKey:@"name"] : [film objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *urlImage;
    NSDictionary *posters = [film objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];
    self.posterImageView.imageURL = [NSURL URLWithString:urlImage];

    
    id genres =[film objectForKey:@"genres"];
    
    if ([genres isKindOfClass:[NSArray class]]) {
        self.genresLabel.text = [genres componentsJoinedByString:@" | "];
    }
    else if ([genres isKindOfClass:[NSString class]]) {
        self.genresLabel.text = genres;
    }
    else {
        self.genresLabel.text = @"No genre information available";
    }
    NSString *rating = [film objectForKey:@"rating"];
    
    if ([rating length] > 0) {
        self.classificationLabel.text = [NSString stringWithFormat:@"%@", rating];
    }
    else {
        self.classificationLabel.text = @"";
    }
    
    NSString *runTime = [film objectForKey:@"runtime"];
    if (!runTime || [runTime length] < 1) {
        runTime = @"0";
    }
    NSString *duration = [NSString stringWithFormat:@"%@ min", runTime];
    self.durationLabel.text = duration;
    
    
    NSDictionary *metacritic = [film objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        [self.userRatingLabel setText:[NSString stringWithFormat:@"%g", [userScore floatValue]]];
    }
    else {
        [self.userRatingLabel setText:@"No votes"];
    }
}


#pragma mark - Table View functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TicketCell";
    static NSString *TitleCellIdentifier = @"TitleCell";
    static NSString *PurchaseCellIdentifier = @"PurchaseCell";
    static NSString *DismissCellIdentifier = @"DismissCell";
    static NSString *ConvenienceChargeCellIdentifier = @"ConvenienceChargeCell";
    if (indexPath.section < 1) {
        if (indexPath.row < 1) {
            SPNDismissCell *dismissCell = [tableView dequeueReusableCellWithIdentifier:ConvenienceChargeCellIdentifier];
            [dismissCell.xLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
            [dismissCell.messageLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [dismissCell.messageLabel setText:[NSString stringWithFormat:@"Ticket convenience charge: $%.2f per ticket", convenienceCharge]];
            return dismissCell;
        }
        else if (indexPath.row < 2) {
            SPNTicketsTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:TitleCellIdentifier];
            [cell.ticketsAmountLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
            [cell.ticketsPriceLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
            [cell.ticketsNameLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];

            return cell;
        }
        else {
            SPNTicketCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            NSDictionary *ticketInformation = [ticketTypes objectAtIndex:indexPath.row-2];
            NSString *ticketName = [ticketInformation objectForKey:@"ticket_name"];
            
            if (![ticketName isKindOfClass:[NSNull class]]) {
                [cell.ticketNameLabel setText:ticketName];
            }
            else {
                [cell.ticketNameLabel setText:@"Ticket"];
            }
            CGFloat totalPrice = [[ticketInformation objectForKey:@"ticket_price"] floatValue] + [[ticketInformation objectForKey:@"ticket_tax"] floatValue];
            [cell.priceLabel setText:[NSString stringWithFormat:@"$%.2f", totalPrice]];
            NSString *availableSeats = [self.ticketingInformation objectForKey:@"remaining_seats"];
        
            [cell.addTicketButton setBackgroundColor:[UIColor spn_buttonColor]];
            [cell.addTicketButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
        
            [cell.substractTicketButton setBackgroundColor:[UIColor spn_buttonColor]];
            [cell.substractTicketButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
           

            [cell.addTicketButton setTag:indexPath.row-2];
            [cell.addTicketButton addTarget:self
                                     action:@selector(addTicket:)
                           forControlEvents:UIControlEventTouchUpInside];
            
            
            [cell.substractTicketButton setTag:indexPath.row-2];
            [cell.substractTicketButton addTarget:self
                                           action:@selector(removeTicket:)
                                 forControlEvents:UIControlEventTouchUpInside];
            
            NSInteger maxSeats =[availableSeats integerValue];
            cell.maxTicketsPossible = maxSeats;
            [cell.ticketNameLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [cell.priceLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [cell.totalTicketsLabel setFont:[UIFont spn_NeutraBoldMedium]];

        
            return cell;
        }
    }
    else {
        if (indexPath.row < 1) {
            SPNDismissCell *dismissCell = [tableView dequeueReusableCellWithIdentifier:DismissCellIdentifier];
            [dismissCell.xLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
            [dismissCell.messageLabel setFont:[UIFont spn_NeutraSmallMedium]];
            return dismissCell;
        }
        SPNPurchaseTotalCell *cell = [tableView dequeueReusableCellWithIdentifier:PurchaseCellIdentifier];
        
        [cell.creditCardButton.titleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
        [cell.giftCardButton.titleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
        [cell.membershipCardButton.titleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];

        [cell.totalLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
        [cell.totalPurchaseLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
        
        [cell.convenienceChargeLabel setFont:[UIFont spn_NeutraMediumLarge]];
        [cell.totalConvenienceChargeLabel setFont:[UIFont spn_NeutraMediumLarge]];
        
        self.creditCardButton = cell.creditCardButton;
        self.giftCardButton = cell.giftCardButton;
        self.membershipCardButton = cell.membershipCardButton;
        
        self.totalAmountLabel = cell.totalPurchaseLabel;
        self.totalConvenienceChargeLabel = cell.totalConvenienceChargeLabel;
        
        [cell.creditCardButton addTarget:self
                                  action:@selector(changePaymentMethod:)
                        forControlEvents:UIControlEventTouchUpInside];
        [cell.giftCardButton addTarget:self
                                action:@selector(changePaymentMethod:)
                      forControlEvents:UIControlEventTouchUpInside];
        [cell.membershipCardButton addTarget:self
                                      action:@selector(changePaymentMethod:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [cell.nextButton.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];

        [cell.nextButton setBackgroundColor:[UIColor spn_buttonColor]];
        [cell.nextButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
        [cell.nextButton addTarget:self
                            action:@selector(buyTickets:)
                  forControlEvents:UIControlEventTouchUpInside];
    
        [self getTotalAmountForTickets];
        [self checkIfPremiereTicketsSelected:selectedTickets];
    
        return cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < 1) {
        return [ticketTypes count]+2;
    }
    else {
        return 2;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section < 1) {
        return @"Select your tickets";
    }
    else {
        return @"Select your payment method";
    }
    /*
    return [NSString stringWithFormat:@"Available seats: %@", [ticketingInformation objectForKey:@"availableSeats"]];*/
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 1) {
        if (indexPath.row < 1) {
            return shouldShowChargeCell ? 35 : 0;
        }
        else if (indexPath.row < 2) {
            return kTitleCellHeight;
        }
        else {
            return kTicketCellHeight;
        }
    }
    else {
        if (indexPath.row < 1) {
            return shouldShowDismissCell ? 35 : 0;
        }
        else {
            return kPurchaseCellHeight;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 1 ) {
        if (indexPath.row == 1) {
            cell.backgroundColor = [UIColor spn_notSoLightBrownColor];
            return;
        }
    }
     cell.backgroundColor = [UIColor spn_lightBrownColor];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
    
    
    if (sectionColor) {
        header.contentView.backgroundColor = sectionColor;
    }
    if (titleSectionColor) {
        [header.textLabel setTextColor:titleSectionColor];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 1 && indexPath.row < 1) {
        shouldShowChargeCell = NO;
    }
    if (indexPath.section > 0 && indexPath.row < 1) {
        shouldShowDismissCell = NO;
    }
    [self.ticketSelectionTableView beginUpdates];
    [self.ticketSelectionTableView endUpdates];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    scrollView.bounces = (scrollView.contentOffset.y < self.view.frame.size.height/3);
}

#pragma mark - Ticket selection option
-(void)addTicket:(UIButton*)button
{
    NSIndexPath *buttonPath = [NSIndexPath indexPathForRow:button.tag+2 inSection:0];
    SPNTicketCell *cell = (SPNTicketCell*)[self.ticketSelectionTableView cellForRowAtIndexPath:buttonPath];
    
    if (cell.maxTicketsPossible > cell.ticketsPurchased) {
        cell.ticketsPurchased +=1;
    }
    [cell.totalTicketsLabel setText:[NSString stringWithFormat:@"%ld", (long)cell.ticketsPurchased]];

    NSDictionary *ticketInformation = [ticketTypes objectAtIndex:button.tag];
    
    NSString *ticketPrice = [ticketInformation objectForKey:@"ticket_price"];
    NSString *ticketTax = [ticketInformation objectForKey:@"ticket_tax"];

    CGFloat totalCost = cell.ticketsPurchased * ([ticketPrice floatValue] + [ticketTax floatValue]);
    
    NSDictionary *ticketTotalDetails = [NSDictionary dictionaryWithObjects:@[[NSNumber numberWithFloat:totalCost], [NSNumber numberWithInteger:cell.ticketsPurchased], [ticketInformation objectForKey:@"type"], [ticketInformation objectForKey:@"ticket_name"]]
                                                                   forKeys:@[@"total", @"amount", @"ticket_type", @"name"]];

    [selectedTickets setObject:ticketTotalDetails
                        forKey:[ticketInformation objectForKey:@"ticket_name"]];
    
    [self getTotalAmountForTickets];
    [self checkIfPremiereTicketsSelected:selectedTickets];

}

-(void)removeTicket:(UIButton*)button
{
    NSIndexPath *buttonPath = [NSIndexPath indexPathForRow:button.tag+2 inSection:0];
    SPNTicketCell *cell = (SPNTicketCell*)[self.ticketSelectionTableView cellForRowAtIndexPath:buttonPath];
    
    if (cell.ticketsPurchased > 0) {
        cell.ticketsPurchased -=1;
    }
    [cell.totalTicketsLabel setText:[NSString stringWithFormat:@"%ld", (long)cell.ticketsPurchased]];
    
    NSDictionary *ticketInformation = [ticketTypes objectAtIndex:button.tag];
    
    NSString *ticketPrice = [ticketInformation objectForKey:@"ticket_price"];
    NSString *ticketTax = [ticketInformation objectForKey:@"ticket_tax"];
    
    CGFloat totalCost = cell.ticketsPurchased * ([ticketPrice floatValue] + [ticketTax floatValue]);
    
    NSDictionary *ticketTotalDetails = [NSDictionary dictionaryWithObjects:@[[NSNumber numberWithFloat:totalCost], [NSNumber numberWithInteger:cell.ticketsPurchased], [ticketInformation objectForKey:@"type"]]
                                                                   forKeys:@[@"total", @"amount", @"ticket_type"]];
    
    if (cell.ticketsPurchased < 1) {
        [selectedTickets removeObjectForKey:[ticketInformation objectForKey:@"ticket_name"]];
    }
    else {
        [selectedTickets setObject:ticketTotalDetails
                            forKey:[ticketInformation objectForKey:@"ticket_name"]];
    }
    [self getTotalAmountForTickets];
    [self checkIfPremiereTicketsSelected:selectedTickets];
}
-(void)getTotalAmountForTickets
{
    CGFloat sum = 0;
    CGFloat cchargeTotal = 0;
    for (NSString *ticketType in [selectedTickets allKeys])
    {
        sum+= [[[selectedTickets objectForKey:ticketType] objectForKey:@"total"] floatValue];
        cchargeTotal += convenienceCharge*[[[selectedTickets objectForKey:ticketType] objectForKey:@"amount"] floatValue];
    }

    [self.totalConvenienceChargeLabel setText:[NSString stringWithFormat:@"$%.2f", cchargeTotal]];
    [self.totalAmountLabel setText:[NSString stringWithFormat:@"$%.2f", sum+cchargeTotal]];
    [paymentDetails setObject:[NSNumber numberWithFloat:sum] forKey:@"total"];
    [paymentDetails setObject:[NSNumber numberWithFloat:cchargeTotal] forKey:@"ccharge"];
}

-(void)checkIfPremiereTicketsSelected:(NSDictionary *)tickets
{
    BOOL selectedPremiereTickets = NO;
    NSArray *ticketNames = [tickets allKeys];
    for (NSString *ticketName in ticketNames) {
        if ([ticketName rangeOfString:@"Premiere"].location != NSNotFound) {
            selectedPremiereTickets = YES;
            break;
        }
    }
    [self.creditCardButton setEnabled:!selectedPremiereTickets];
    [self.giftCardButton setEnabled:!selectedPremiereTickets];

    if (selectedPremiereTickets) {
        [self changePaymentMethod:self.membershipCardButton];
        [self.creditCardButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.giftCardButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    }
    else {
        [self.creditCardButton.layer setBorderColor:[UIColor blackColor].CGColor];
        [self.giftCardButton.layer setBorderColor:[UIColor blackColor].CGColor];
    }
    
}

-(void)changePaymentMethod:(id)sender
{
    UIButton *payBtn = (UIButton*)sender;
    
    [self.creditCardButton setBackgroundColor:[UIColor clearColor]];
    [self.creditCardButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.creditCardButton setTitleColor:[UIColor blackColor]
                                forState:UIControlStateNormal];
    [self.giftCardButton setBackgroundColor:[UIColor clearColor]];
    [self.giftCardButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.giftCardButton setTitleColor:[UIColor blackColor]
                              forState:UIControlStateNormal];
    [self.membershipCardButton setBackgroundColor:[UIColor clearColor]];
    [self.membershipCardButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.membershipCardButton setTitleColor:[UIColor blackColor]
                                    forState:UIControlStateNormal];
    
    if (payBtn == self.creditCardButton) {
        selectedPaymentMethod = SPNPaymentMethodCreditCard;
    }
    else if (payBtn == self.giftCardButton) {
        selectedPaymentMethod = SPNPaymentMethodGiftCard;
    }
    else {
        selectedPaymentMethod = SPNPaymentMethodMembershipCard;
    }
    [self getTotalAmountForTickets];
    
    [payBtn setBackgroundColor:[UIColor spn_aquaColor]];
    [payBtn.layer setBorderColor:[UIColor spn_aquaColor].CGColor];
    [payBtn setTitleColor:[UIColor whiteColor]
                 forState:UIControlStateNormal];
}

-(void)buyTickets:(id)sender
{
    if ([selectedTickets count] < 1) {
        [UIAlertView showAlertViewWthTitle:@"Did you forget something?"
                                andMessage:@"You haven't added any tickets to your cart yet."];
    }
    else {
        [paymentDetails setObject:selectedTickets
                           forKey:@"tickets"];
        [self performSegueWithIdentifier:@"PurchaseTickets"
                                  sender:self];
    }
}

#pragma mark - Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PurchaseTickets"]) {
        SPNPaymentInformationViewController *paymentController = [segue destinationViewController];
        [paymentController setSelectedPaymentMethod:selectedPaymentMethod];
        [paymentController setMovieInformation:self.movieInformation];
        [paymentController setShowtimeInformation:self.showtimeInformation];
        [paymentController setPaymentInformation:paymentDetails];
        [paymentController setTicketingInformation:self.ticketingInformation];
        [paymentController setBranding:self.branding];
        
    }
}
@end
