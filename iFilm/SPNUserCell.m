//
//  SPNUserCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNUserCell.h"

@implementation SPNUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIImageView *spacerView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 30, 10)];
    [spacerView setImage:[UIImage imageNamed:@"miniCard"]];
    [spacerView setContentMode:UIViewContentModeScaleAspectFit];
    
    [_nameTextField setLeftView:spacerView];
    [_nameTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    UIImageView *spacer2View = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 30, 10)];
    [spacer2View setImage:[UIImage imageNamed:@"mailIcon"]];
    [spacer2View setContentMode:UIViewContentModeScaleAspectFit];

    [_emailTextField setLeftView:spacer2View];
    [_emailTextField setLeftViewMode:UITextFieldViewModeAlways];
}

@end
