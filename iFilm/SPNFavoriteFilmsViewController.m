//
//  SPNFavoriteFilmsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFavoriteFilmsViewController.h"
#import "SPNMovieCollectionCell.h"
#import "SPNFilmDetailsViewController.h"
#import "SPNFestivalCell.h"
#import "SPNFilmSearchCell.h"
#import "SPNFestivalViewController.h"
#import "Festival.h"

static NSString* FestivalCellIdentifier = @"FestivalCell";
static NSString* FilmCellIdentifier = @"FilmCell";

@interface SPNFavoriteFilmsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SPNCinelifeQueriesDelegate, UITableViewDataSource, UITableViewDelegate, SPNFestivalCellDelegate> {
    NSDictionary *selectedFilmForDetails;
    BOOL active;
}

@property (nonatomic, retain) NSString *lastKnownLocation;
@property (nonatomic, weak) IBOutlet UITableView *tableView;


@end

@implementation SPNFavoriteFilmsViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Film Favorites";
    [self.tableView registerCellClass:[SPNFestivalCell class] withIdentifier:FestivalCellIdentifier];
    [self.tableView registerCellClass:[SPNFilmSearchCell class] withIdentifier:FilmCellIdentifier];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedFestivalFromFavorites:)
                                                 name:kNotificationRemovedFestivalFromFavorites
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedMovieFromFavorites:)
                                                 name:kNotificationRemovedMovieFromFavorites
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [self closeIfEmpty];
    active = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    active = NO;
}

- (void)closeIfEmpty {
    if (self.movies.count == 0 && self.festivals.count == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)removedFestivalFromFavorites:(NSNotification *)notification {
    NSNumber *festivalId = [[notification userInfo] objectForKey:@"id"];
    Festival *festival = [_festivals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"festivalId == %@", festivalId]].firstObject;
    if (!festival) {
        return;
    }
    NSMutableArray *festivals = [_festivals mutableCopy];
    [festivals removeObject:festival];
    _festivals = festivals.copy;
    [self.tableView reloadData];
    if (active) {
        [self closeIfEmpty];
    }
}

-(void)removedMovieFromFavorites:(NSNotification *)notification {
    NSNumber *movieId = [[notification userInfo] objectForKey:@"id"];
    NSDictionary *movie = [_movies filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %@", movieId]].firstObject;
    if (!movie) {
        return;
    }
    NSMutableArray *movies = [_movies mutableCopy];
    [movies removeObject:movie];
    _movies = movies.copy;
    [self.tableView reloadData];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        NSInteger rowCount = self.movies.count / 3;
        if (self.movies.count % 3 != 0) {
            rowCount +=1;
        }
        return rowCount;
    } else {
        return self.festivals.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 1 ? 212 : 179;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        SPNFilmSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:FilmCellIdentifier];
        cell.backgroundColor = [UIColor spn_lightBrownColor];
        [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
        cell.separatorInset = UIEdgeInsetsMake(0, CGRectGetWidth(tableView.frame), 0, 0);
        return cell;
    } else {
        SPNFestivalCell *cell = [tableView dequeueReusableCellWithIdentifier:FestivalCellIdentifier];
        cell.delegate = self;
        [cell configureForFestival:_festivals[indexPath.row]];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
        SPNFestivalViewController *controller = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SPNFestivalViewController class])];
        controller.festival = _festivals[indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)didPressOnFavoriteInFestivalCell:(SPNFestivalCell *)cell {
    if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not Now"
                           withOtherTitles:@"Sign In"];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Festival *festival = _festivals[indexPath.row];
    [[SPNCinelifeQueries sharedInstance] unfavoriteFestival:festival.festivalId
                                                 completion:^(NSError *error) {
                                                     [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                                                 }];
}

#pragma mark - UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.movies count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* FilmCellIdentifier = @"MovieCell";
    SPNMovieCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FilmCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *filmInformation = [self.movies objectAtIndex:indexPath.row];
    if (cell) {
        [cell initializeMovieSchedule:filmInformation
                    forCollectionView:collectionView
                         forIndexPath:indexPath];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *filmInformation = [self.movies objectAtIndex:indexPath.row];
    NSString *movieId = [filmInformation objectForKey:@"id"];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] getMovieInformation:movieId
                                                  forZipcode:nil
                                               orCoordinates:nil
                                                  withRadius:15
                                             withCredentials:[SPNUser getCurrentUserId]];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width*0.3, 168);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (section == 0) {
        if (IS_IPHONE_6_PLUS) {
            return UIEdgeInsetsMake(5, 30, 0, 0);
        } else if (IS_IPHONE_6) {
            return UIEdgeInsetsMake(5, 20, 0, 5);
        } else {
            return UIEdgeInsetsMake(5, 5, 0, 5);
        }
    } else {
        return UIEdgeInsetsZero;
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FilmDetails"]) {
        SPNFilmDetailsViewController *spnFilmDetails = [segue destinationViewController];
        [spnFilmDetails setFilmInformation:selectedFilmForDetails];
        [spnFilmDetails setLastKnownLocation:self.lastKnownLocation];
        [spnFilmDetails setShouldBeReviewable:NO];
    }
}

#pragma mark cinelife queries delegate functions
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieInformation:(NSDictionary *)movie
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                             animated:YES];
    selectedFilmForDetails = movie;
    [self performSegueWithIdentifier:@"FilmDetails" sender:self];
}
@end
