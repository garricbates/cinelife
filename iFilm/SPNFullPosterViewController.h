//
//  SPNFullPosterViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SPNFullPosterViewController : GAITrackedViewController

@property BOOL isFestivalFilm;
@property NSString *posterURL;
@property IBOutlet UIImageView *posterImageView;
@end
