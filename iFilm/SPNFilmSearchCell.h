//
//  SPNFilmSearchCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNShowtimesCollectionView.h"

@interface SPNFilmSearchCell : UITableViewCell

@property (nonatomic, weak) IBOutlet SPNShowtimesCollectionView *horizontalCollectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

@end
