//
//  SPNPurchaseListViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPurchaseListViewController.h"
#import "SPNPurchaseDetailsViewController.h"

@interface SPNPurchaseListViewController ()
{
    NSMutableArray *purchases;
    Purchases *selectedPurchaseForDetails;
    NSDictionary *selectedFilmForDetails;
    NSDateFormatter *showtimeDateFormatter;
    NSLocale *usLocale;
}
@end


@implementation SPNPurchaseListViewController


-(SPNPurchaseCell*)prototypeCell{
    if (!_prototypeCell) {
        _prototypeCell = (SPNPurchaseCell*)[self.purchaseListTableView dequeueReusableCellWithIdentifier:@"PurchaseCell"];
    }
    return _prototypeCell;
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"newPurchase"];
    [prefs synchronize];
    [self.purchaseListTableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Purchase List";
    // Do any additional setup after loading the view.
    purchases = [[[LCPDatabaseHelper sharedInstance] fetchAllRecordsForEntity:@"Purchases"] mutableCopy];
    
    showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [showtimeDateFormatter setLocale:usLocale];
    [showtimeDateFormatter setDefaultDate:[NSDate date]];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    
    [self.purchaseListTableView setBackgroundView:[UIView new]];
    [self.purchaseListTableView setBackgroundColor:[UIColor spn_brownColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view functions
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.prototypeCell initializePurchaseCell:[purchases objectAtIndex:indexPath.row]
                             forRowAtIndexPath:indexPath
                                  forTableView:tableView
                             withDateFormatter:showtimeDateFormatter];
    
    [_prototypeCell layoutIfNeeded];
    CGSize size = [_prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height+1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *PurchaseCellIdentifier = @"PurchaseCell";
    SPNPurchaseCell *cell = (SPNPurchaseCell*)[tableView dequeueReusableCellWithIdentifier:PurchaseCellIdentifier];
    [cell initializePurchaseCell:[purchases objectAtIndex:indexPath.row]
               forRowAtIndexPath:indexPath
                    forTableView:tableView
               withDateFormatter:showtimeDateFormatter];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [purchases count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    selectedPurchaseForDetails = [purchases objectAtIndex:indexPath.row];
    
    NSString *movieId = selectedPurchaseForDetails.movieId;
    selectedFilmForDetails = @{@"movieId" : movieId, @"title" : selectedPurchaseForDetails.movieTitle};
    [self performSegueWithIdentifier:@"PurchaseDetails"
                                      sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PurchaseDetails"]) {
        SPNPurchaseDetailsViewController *spnPurchaseController = [segue destinationViewController];
        [spnPurchaseController setPurchaseInformation:selectedPurchaseForDetails];
        [spnPurchaseController setMovieInformation:selectedFilmForDetails];
    }
}

#pragma mark - Table view functions
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        id purchase = [purchases objectAtIndex:indexPath.row];
        BOOL succeeded = [[LCPDatabaseHelper sharedInstance] deleteRecord:purchase];
        if (succeeded) {
            [purchases removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }
}
@end
