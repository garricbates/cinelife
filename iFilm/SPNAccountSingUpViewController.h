//
//  SPNAccountSingUpViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
#import "SPNCinelifeQueries.h"

@interface SPNAccountSingUpViewController : GAITrackedViewController <SPNCinelifeQueriesDelegate>
@property IBOutlet UILabel *fillOutFieldsLabel;

@property IBOutlet UIView *fieldsContainerView;

@property IBOutlet UITextField *usernameTextField;
@property IBOutlet UITextField *emailTextField;
@property IBOutlet UITextField *passwordTextField;
@property IBOutlet UITextField *retypePasswordTextField;

@property IBOutlet UIButton *signUpButton;
@property IBOutlet UIButton *cancelButton;

- (IBAction)signUpButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@end
