//
//  SPNTheatreNewsDetailsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreNewsDetailsViewController.h"


@interface SPNTheatreNewsDetailsViewController ()
{
    UIColor *mainColor;
    NSDateFormatter *dateFormatter;
}
@end

@implementation SPNTheatreNewsDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
        }
        NSString *logo = [branding objectForKey:@"logo"];
        if ([logo length] > 0) {
            
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
        }
    }
    else if (self.theatreBackgroundView) {
        [self.view layoutIfNeeded];
    
        CGRect frame = [self.newsContainerView  frame];
        NSLayoutConstraint *heightC = [NSLayoutConstraint constraintWithItem:self.theatreBackgroundView
                                                                   attribute:NSLayoutAttributeHeight
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.theatreBackgroundView
                                                                   attribute:NSLayoutAttributeHeight
                                                                  multiplier:0
                                                                    constant:frame.size.height];
        [heightC setPriority:UILayoutPriorityRequired];
        [self.view addConstraint:heightC];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.screenName = @"Theatre News Details";
    // Do any additional setup after loading the view.
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [self customizeUI];
    
    [self initializePostTitle:self.newsPostDetails];
   
    
    [self getBrandColors:self.brandColors];
    if (self.newsLink) {
        [self initializeWebPost:self.newsLink];
    }
    else {
        [self initializePostData:self.newsPostDetails];
    }
}


-(void)dealloc
{
    [self setBrandColors:nil];
    [self setNewsLink:nil];
    [self setNewsPostDetails:nil];
    [self setNewsTitleLabel:nil];
    [self setNewsDateLabel:nil];
    [self setNewsContainerView:nil];
    [self setNewsContentTextView:nil];
    [self setNewsWebView:nil];
    [self setTheatreLogoImageView:nil];
    [self setTheatreBackgroundView:nil];
    dateFormatter = nil;
    mainColor = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}
#pragma mark Customize UI
-(void)customizeUI
{
    [self.newsTitleLabel setFont:[UIFont spn_NeutraBoldLarge]];
    [self.newsDateLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.newsContentTextView setFont:[UIFont spn_NeutraSmallMedium]];
}
#pragma mark Iniitialize Post data
-(void)initializePostTitle:(NSDictionary*)postDetails
{
    NSString *title = [postDetails objectForKey:@"title"];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *realDate = [dateFormatter dateFromString:[postDetails objectForKey:@"start_date"]];
    
    NSDate *endDate = [dateFormatter dateFromString:[postDetails objectForKey:@"end_date"]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *stringDate = [dateFormatter stringFromDate:realDate];
    if (!stringDate) {
        stringDate = [dateFormatter stringFromDate:[NSDate date]];
    }
    NSString *stringEndDate;
    if (endDate) {
        stringEndDate = [dateFormatter stringFromDate:endDate];
    }
    NSString *dateRange = [NSString stringWithFormat:@"Date: %@ - %@", stringDate, stringEndDate];
    
    if (stringEndDate) {
        [self.newsDateLabel setText:dateRange];
    }
    else {
        [self.newsDateLabel setText:[NSString stringWithFormat:@"Date: %@", stringDate]];
    }
    [self.newsTitleLabel setText:title];
    if ([[postDetails objectForKey:@"category"] isEqualToString:@"Coming Soon"]) {
//        [self.newsDateLabel removeFromSuperview];
        [self.newsDateLabel setFont:[UIFont systemFontOfSize:0]];
    }
}
-(void)initializeWebPost:(NSString*)newsLink
{
    NSURL *url = [NSURL URLWithString:newsLink];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [self.newsWebView loadRequest:request];

}
-(void)initializePostData:(NSDictionary*)postDetails;
{
    
    NSString *fulltext = [postDetails objectForKey:@"content"];
    
    fulltext = [NSString stringWithFormat:
                        @"<html> \n" "<head> \n"
                        "<style type=\"text/css\"> \n"
                        "body {font-family: \"%@\"; font-size: %@;}\n"
                        "</style> \n"
                        "</head> \n"
                        "<body>%@</body> \n"
                        "</html>",@"HelveticaNeue-Light",[NSNumber numberWithInt:12],
                        fulltext];
    
    [self.newsWebView setOpaque:NO];
    [self.newsWebView setBackgroundColor:[UIColor clearColor]];
    [self.newsWebView setDelegate:self];
   
    [self.newsWebView loadHTMLString:fulltext baseURL:nil];

    [self.newsWebView.scrollView setScrollEnabled:NO];
    [self.newsWebView setUserInteractionEnabled:YES];
    [self.newsWebView setScalesPageToFit:YES];
    [self.scrollView setScrollEnabled:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
  
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",
                          300];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    webView.frame = frame;
    
   CGSize bounds = webView.scrollView.contentSize;
    [webView setFrame:CGRectMake(webView.frame.origin.x, webView.frame.origin.y, bounds.width, bounds.height+self.theatreBackgroundView.frame.size.height)];
    [self.scrollView setContentSize:webView.frame.size];

    NSLayoutConstraint *conts = [NSLayoutConstraint constraintWithItem:self.webContainerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeHeight multiplier:0 constant:bounds.height+self.theatreBackgroundView.frame.size.height];
    
    [self.view addConstraint:conts];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}


@end
