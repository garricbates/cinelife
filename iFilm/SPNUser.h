//
//  SPNUser.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface SPNUser : NSObject

/**
 * Method to recover a los password for a given user. The server sends an email with the instructions to reset the password for the given user.
 * @author La Casa de los Pixeles
 * @param email The email of the user.
 * @return YES if the server sent an email with the new password, NO if the email does not exist, or the server encountered an error.
 */
+(BOOL)recoverPasswordForUserEmail:(NSString*)email;

/**
 * Method to set the current user's data for the app. This method stores the user Id, name and email in the device. This way, we can determine if a user is logged in.
 * @author La Casa de los Pixeles
 * @param userInfoDictionary The dictionary with the user's details obtained by the signUp or login function.
 */
+(void)setCurrentUser:(NSDictionary*)userInfoDictionary;

/**
 * Method to log out a previously logged user. This method removes the information stored in the device, and this way, we can determine that no user is currently logged in.
 * @author La Casa de los Pixeles
 */
+(void)logOutCurrentUser;

/**
 * Method to determine if a user is logged in.
 * @author La Casa de los Pixeles
 * @return YES, if there is a user currently logged in. NO, otherwise.
 */
+(BOOL)userLoggedIn;

/**
 * Method to obtain the full name of the current user.
 * @author La Casa de los Pixeles
 * @return The full name of the logged user, or nil if there is no user logged in.
 */
+(NSString*)getCurrentUserFullName;

/**
 * Method to obtain the Id of the current user.
 * @author La Casa de los Pixeles
 * @return The ID of the logged user, or nil if there is no user logged in.
 */
+(NSDictionary*)getCurrentUserId;

/**
 * Method to add a theatre to the user's favourites. Requires the user to be logged in.
 * @author La Casa de los Pixeles
 * @param theatre A theatre object obtained by any of the theatre related functions.
 * @param username The user ID of the registered user.
 * @return A dictionary with the user's theatre information in case the update was successful, or a dictionary with the error message in case something went wrong.
 */
+(NSDictionary*)addTheatreToFavourites:(NSDictionary*)theatre
                               forUser:(NSString*)username;

/**
 * Method to remove a theatre to the user's favourites. Requires the user to be logged in.
 * @author La Casa de los Pixeles
 * @param theatre A theatre object obtained by any of the theatre related functions.
 * @param username The user ID of the registered user.
 * @return A dictionary with the user's theatre information in case the update was successful, or a dictionary with the error message in case something went wrong.
 */
+(NSDictionary*)removeFavouriteTheatre:(NSDictionary*)theatre
                               forUser:(NSString*)username;

/**
 * Method obtain the list of favorite theatre ids of a given user. Requires the user to be logged in.
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @return A dictionary with the theatre id's of the user's favourite list, or nil in case the user does not have any theatres stored.
 */
+(NSMutableDictionary*)getListOfFavouriteTheatresForUser:(NSString*)username;

/**
 * Method to add get the list of favortie theatre objects for a given user. Requires the user to be logged in.
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @return An array of favorite theatres for the user, or nil in case the user does not have any theatres stored.
 */
+(NSArray*)getTheatresOfUser:(NSString*)username withUserLocation:(CLLocation *)userLocation;

/**
 * Method to determine if a use has an email linked to his twitter account.
 * @author La Casa de los Pixeles
 * @param username The user's twitter display name.
 * @return YES, if the user has an email linked to his account. NO, otherwise.
 */
+(BOOL)userHasTwitterEmail:(NSString*)username;

/**
 * Method to store an email for the user, linking it to their twitter user name. This method stores the information locally.
 * @author La Casa de los Pixeles
 * @param email The user's provided email.
 * @param username The user's twitter display name.
 */
+(void)setTwitterEmail:(NSString*)email
               forUser:(NSString*)username;

/**
 * Method to retrieve the user's twitter email, in case the user has an email linked to their twitter account.
 * @author La Casa de los Pixeles
 * @param username The user's twitter display name.
 * @return The user's email, if the user has one. Nil, otherwise.
 */
+(NSString*)getTwitterEmailForUser:(NSString*)username;

/**
 * Method to determine if a user has linked their account with their GoWatchItAccount.
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @return YES, if the user has a GoWatchIt account linked to his account. NO, otherwise.
 */
+(BOOL)userHasGoWatchItEmail:(NSString*)username;

/**
 * Method to store a GoWatchIt account for the user. This method stores the information locally, and is only used to get an authentication token when retrieveing their GoWatchIt queue.
 * @author La Casa de los Pixeles
 * @param email The user's GoWatchIt email.
 * @param password The user's GoWatchIt password.
 * @param username The user ID of the registered user.
 */
+(void)setGoWatchItEmail:(NSString*)email
             andPassword:(NSString*)password
                 forUser:(NSString*)username;

/**
 * Method to retrieve the user's GoWatchIt email, in case the user has a GoWatchIt account linked to their account.
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @return The user's GoWatchIt email, if the user has one. Nil, otherwise.
 */
+(NSString*)getGoWatchItEmailForUser:(NSString*)username;

/**
 * Method to retrieve an authtentication token from GoWatchIt!, for a previously registered user.
 * @author La Casa de los Pixeles
 * @param email The user's GoWatchIt email. (Must be previously registered at http://gowatchit.com/
 * @param password The user's GoWatchIt password.
 * @param username The user ID of the registered user.
 * @return An authentication token uses for future GoWatchIt queries.
 */
+(NSString*)getGoWatchItAuthTokenForEmailForUser:(NSString*)username;

+(NSString*)getGoWatchItAuthTokenForEmail:(NSString*)email
                              andPassword:(NSString*)password
                                  forUser:(NSString*)username;

/**
 * Method to retrieve an authtentication token from GoWatchIt!. (Must be previously registered at http://gowatchit.com/
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @param authentication_token An authentication token previously obtained from GoWatchIt.
 * @return An array of movie objects from GoWatchIt.
 */
+(NSArray*)getUserGWIQueue:(NSString*)username
   withAuthenticationToken:(NSString*)authentication_token;

/**
 * Method to retrieve the GoWatchIt ID from a movie title. This method may return multiple films, or none, depending on the given title. This method is used to add movies to the user's GoWatchIt queue.
 * @author La Casa de los Pixeles
 * @param title The title of a movie for which to search its GoWatchIt ID.
 * @return The movie's GoWatchIt ID, or nil, in case no match was found.
 */
+(NSString*)getMoiveGWIId:(NSString*)title;

/**
 * Method to add a movie to a user's GoWatchIt. The user must have a GoWatchIt account linked previously
 * @author La Casa de los Pixeles
 * @param username The user ID of the registered user.
 * @param movieId The movie's GoWatchIt ID.
 * @param authentication_token An authentication token previously obtained from GoWatchIt.
 * @return YES, if the movie was added to the user's queue. NO, otherwise.
 */
+(BOOL)addMovietoGWIQueue:(NSString*)username
                 forMovie:(NSString*)movieId
  withAuthenticationToken:(NSString*)authentication_token;

+(void)welcomeUser:(NSString*)username
         isNewUser:(BOOL)isNewUser;

@end
