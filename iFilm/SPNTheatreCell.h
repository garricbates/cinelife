//
//  TheatreCell.h
//  Spotlight
//
//  Created by La Casa de los Pixeles on 11/19/13.
//  Copyright (c) 2013 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNShowtimesCollectionView.h"
#import "SPNCinelifeQueries.h"

@class SPNTheatreCell;

@protocol SPNTheatreCellDelegate <NSObject>

- (void)didPressOnFavoriteInTheaterCell:(SPNTheatreCell *)cell;

@end

@interface SPNTheatreCell : UITableViewCell
{
    SPNShowtimesCollectionView *_horizontalCollectionView;
}

@property (nonatomic) IBOutlet SPNShowtimesCollectionView *horizontalCollectionView;
@property (nonatomic, weak) id <SPNTheatreCellDelegate> delegate;
@property (nonatomic, strong) NSArray *movies;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteBtn;
@property (weak, nonatomic) IBOutlet UILabel *milesAwayLabel;

@property (weak, nonatomic) IBOutlet UILabel *noShowsLabel;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

-(void)initializeTheatreCellContents:(NSDictionary*)theatre forIndexPath:(NSIndexPath*)indexPath;

- (void)configureForTheater:(NSDictionary *)theater;

@end
