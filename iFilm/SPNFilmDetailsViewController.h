//
//  SPNFilmDetailsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNFilmPostReviewViewController.h"
#import "SPNFilmTrailerViewController.h"
#import "GAITrackedViewController.h"
#import "SPNCinelifeQueries.h"

#import "SPNCriticReviewCell.h"
#import "SPNSynopsisCell.h"
#import "SPNFilmReviewCell.h"

#import "SPNTheatreShowtimeCell.h"

#import <FBSDKShareKit/FBSDKShareKit.h>
#import <TwitterKit/TwitterKit.h>

#import <GoogleSignIn/GoogleSignIn.h>

typedef NS_ENUM(NSInteger, SPNFilmDetailsType)
{
    SPNFilmDetailsTypeShowtimes = 0,
    SPNFilmDetailsTypeCastAndCrew,
    SPNFilmDetailsTypeReviews
};

typedef NS_ENUM(NSInteger, SPNFilmReviewType)
{
    SPNFilmReviewTypeUserReviews = 0,
    SPNFilmReviewTypeCriticReviews
};

typedef NS_ENUM(NSInteger, SPNShareSocialMediaType)
{
    SPNShareSocialMediaTypeFacebook = 0,
    SPNShareSocialMediaTypeTwitter,
    SPNShareSocialMediaTypeGooglePlus
};

typedef NS_ENUM(NSInteger, SPNFilmSectionType)
{
    SPNFilmSectionTypeDetails = 0,
    SPNFilmSectionTypePhotos,
    SPNFilmSectionTypeTrailer
};
@interface SPNFilmDetailsViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate, SPNFilmPostReviewViewControllerDelegate, UIActionSheetDelegate, GPPShareDelegate, UIAlertViewDelegate, SPNFilmTrailerViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, SPNCinelifeQueriesDelegate, FBSDKSharingDelegate, GIDSignInDelegate, GIDSignInUIDelegate, SFSafariViewControllerDelegate>


@property (nonatomic) BOOL shouldBeReviewable;

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLGeocoder *geoCoder;

@property (nonatomic) SPNFilmReviewCell* prototypeReviewCell;
@property (nonatomic) SPNCriticReviewCell *prototypeCriticCell;
@property (nonatomic) SPNSynopsisCell *prototypeCastCell;
@property (nonatomic) SPNTheatreShowtimeCell *prototypeShowtimeCell;

@property CLLocation *currentUserLocation;

@property NSDictionary *filmInformation;

@property NSArray *theatreShowtimesInRegion;

@property NSDictionary *showtimeInformation;

@property NSString *lastKnownLocation;

@property UITextField *datePicker;
@property UITextField *zipcodePicker;

@property UICollectionView *dateCollectionView;

@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *genreLabel;
@property IBOutlet UILabel *metaScoreLabel;
@property IBOutlet UILabel *userScoreLabel;
@property IBOutlet UILabel *durationLabel;
@property IBOutlet UILabel *ratingLabel;


@property IBOutlet UIImageView *heartIconImageView;
@property IBOutlet UIImageView *posterImageView;

@property IBOutlet NSLayoutConstraint *movingBarLeadConstraint;
@property IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;
@property IBOutlet NSLayoutConstraint *metaScoreLeadConstraint;
@property IBOutlet NSLayoutConstraint *metaIconWidthConstraint;
@property IBOutlet NSLayoutConstraint *heartWidthConstraint;


@property IBOutlet UITableView *filmDetailsTableView;

@property IBOutlet UICollectionView *movieStillsCollectionView;

@property IBOutlet UIView *barForButtonsView;
@property IBOutlet UIView *fullPosterView;
@property IBOutlet UIView *barContainerView;

@property IBOutlet UIButton *detailsButton;
@property IBOutlet UIButton *photosButton;
@property IBOutlet UIButton *viewTrailerButton;

@property IBOutlet UIButton *queueItButton;
@property IBOutlet UIButton *shareButton;

@property IBOutlet UIButton *castButton;
@property IBOutlet UIButton *showtimesButton;
@property IBOutlet UIButton *reviewsButton;




- (IBAction)shareButtonPressed:(id)sender;

-(void)buyTicketsDirectly:(id)sender;
@end
