//
//  SPNTheatreNewsfeedCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/16/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTheatreNewsfeedCell : UITableViewCell

@property () IBOutlet UILabel *newsTitleLabel;
@property () IBOutlet UILabel *newsSnippetLabel;
@property () IBOutlet UILabel *newsDateLabel;
@property () IBOutlet UILabel *newsTypeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedNameHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedNameBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *feedTitleHeightConstraint;

@property () IBOutlet UIView *newsTypeBackgroundView;

@property IBOutlet UIImageView *newsTypeIconImageView;

- (void)configureForFeed:(BOOL)forFeed feed:(NSDictionary *)feed htmlSnippet:(NSAttributedString *)htmlSnippet;
+ (CGFloat)heightForFeed:(BOOL)forFeed feed:(NSDictionary *)feed htmlSnippet:(NSAttributedString *)htmlSnippet;

@end
