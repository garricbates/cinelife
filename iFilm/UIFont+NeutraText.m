//
//  UIFont+NeutraText.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "UIFont+NeutraText.h"

@implementation UIFont (NeutraText)


// Regular Font
+(UIFont *)spn_NeutraSmall
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:10.0];
}

+(UIFont *)spn_NeutraSmallMedium
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
}

+(UIFont *)spn_NeutraMedium
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:13.5];
}

+(UIFont *)spn_NeutraMediumLarge
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
}

+(UIFont *)spn_NeutraLarge
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
}

+(UIFont *)spn_NeutraHuge
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0];
}

// Large Font
+(UIFont *)spn_NeutraBoldTiny
{
   return [UIFont fontWithName:@"HelveticaNeue-Bold" size:8.0];
}

+(UIFont *)spn_NeutraBoldSmall
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0];
}

+(UIFont *)spn_NeutraBoldSmallMedium
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0];
}

+(UIFont *)spn_NeutraBoldMedium
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.5];
}

+(UIFont *)spn_NeutraBoldMediumLarge
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0];
}

+(UIFont *)spn_NeutraBoldLarge
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0];
}

+(UIFont *)spn_NeutraBoldHuge
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0];
}

+(UIFont *)spn_NeutraBoldMega
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:21.0];
}

+(UIFont *)spn_NeutraBoldGiga
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:25.0];
}


+(UIFont *)spn_NeutraBoldTera
{
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.0];
}

// Button fonts
+(UIFont *)spn_NeutraButtonMedium
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
}

@end
