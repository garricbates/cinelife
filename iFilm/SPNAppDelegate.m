//
//  SPNAppDelegate.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAppDelegate.h"
#import "SPNNavigationController.h"
#import "SPNNotificationTableViewController.h"
#import "SPNFilmDetailsViewController.h"
#import "SPNTheatreDetailsViewController.h"
#import "SPNFestivalViewController.h"
#import "SPNTutorialManagerViewController.h"

#import <GooglePlus/GPPURLHandler.h>
#import "GAI.h"

#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "SPNCinelifeQueries.h"  

#import <Crashlytics/Crashlytics.h>
//#import <IntensifyUp/IntensifyUp.h>

#import "PartnerConfiguration.h"

#import "LNNotificationsUI.h"

//#import "Localytics.h"
#import <Localytics/Localytics.h>
@import UserNotifications;
#import <LUKeychainAccess.h>

//static NSString *kClientId = @"909685742375-top2olp3p98vs2but0o5qf5ioenj9n56.apps.googleusercontent.com";

@interface SPNAppDelegate () <SPNTutorialManagerViewControllerDelegate, UNUserNotificationCenterDelegate> {
    BOOL locationSaving;
}

@end

@implementation SPNAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Crashlytics
    [Fabric with:@[[Crashlytics class], [Twitter class]]];

    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-57849729-1"];
    
    //Right, that is the point
    if ([UNUserNotificationCenter class] != nil) {
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions options = UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    } else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)
                                           categories:nil]];
    }
    
    // Override point for customization after application launch.

    [self customizeUI];
    [self initializeGoogleLogin];
    
    [[LCPDatabaseHelper sharedInstance] setManagedObjectContext:self.managedObjectContext];
    [[UINavigationBar appearance] setBarTintColor:[UIColor spn_darkGrayColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor spn_aquaColor]];
    
  
    //[[Twitter sharedInstance] startWithConsumerKey:@"S94tVlfrBGlTTObRHIWgr1g58"
//consumerSecret:@"71MCIGexNrnvPbhEP7BNOfzNZCNv9S5DODBnmKHs5ocXPaFnPX"];
    [[Twitter sharedInstance] startWithConsumerKey:@"PoXy9EYE5O7PLfmkYNHCdd0k5"
                                    consumerSecret:@"moEF6l7sx6sIMb6Ih6T4HxFN5gRZCy6evNL58D1SqyjlQnVAzH"];
    [Fabric with:@[[Twitter sharedInstance]]];

    [FBSDKAppEvents activateApp];
    
    [self cycleTheGlobalMailComposer];
    
    [SPNServerQueries getBaseURLsForTrailerAndPoster];
       [self initializeTabBarItems];
    
   
    NSString *remind = [[NSUserDefaults standardUserDefaults] objectForKey:@"tutorialOnStart"];
    if (!remind || [remind isEqualToString:@"remind"]) {
        [self initializeTutorial];
    }
    else {
        [self initializeTabBarItems];
    }
    
    [Localytics autoIntegrate:@"4b3b0816b5dd0f89a66c9f9-43732868-8994-11e5-7fc0-00736b041834"
                launchOptions:launchOptions];
  
    [self initializeGoogleLogin];
    
    //Intensify-UP framework init
//    [Intesify setImplementPushNotification:NO];
//    [Intesify setUseBeaconLocationServices:NO];
//    [Intesify initWithBaseUrl:@"https://cinemaup.intensify-solutions.com" launchOptions:launchOptions viewOptions:nil];
    
    [[PartnerConfiguration current] load];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    if ([UNUserNotificationCenter class] == nil) {
        UIImage *appIcon = [UIImage imageNamed:@"AppIcon60x60"];
        [[LNNotificationCenter defaultCenter] registerApplicationWithIdentifier:@"CineLife" name:@"CineLife" icon:appIcon defaultSettings:[LNNotificationAppSettings defaultNotificationAppSettings]];
    }
    
    NSDictionary *launchNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (launchOptions) {
        [self processNotification:launchNotification];
    }
    
    if ([UNUserNotificationCenter class]) {
        [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
            for (UNNotification *notification in notifications) {
                NSDictionary *notificationJSON = notification.request.content.userInfo;
                if (notificationJSON) {
                    [self saveNotification:notificationJSON];
                }
            }
            [[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
        }];
    }
    
    [[SPNCinelifeQueries sharedInstance] readAllNotifications];
    
    return YES;
}

-(void)initializeTutorial
{
    UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"TutorialStoryboard"
                                                                 bundle:nil];
    SPNTutorialManagerViewController *controller = [tutorialStoryboard instantiateInitialViewController];
    controller.delegate = self;
    
    self.window.rootViewController = controller;
}
    
-(void)cycleTheGlobalMailComposer
{


    
    // we are cycling the damned GlobalMailComposer... due to horrible iOS issue
    self.globalMailComposer = nil;
    self.globalMailComposer = [[MFMailComposeViewController alloc] init];
    [[self.globalMailComposer navigationBar] setTintColor:[UIColor spn_aquaColor]];
    [[self.globalMailComposer navigationBar] setBarTintColor:[UIColor spn_darkGrayColor]];
    
    
    
}
    
-(void)spnTutorialManagerDidClose {
    [self initializeTabBarItems];
}

-(void)initializeGoogleLogin
{
    [GIDSignIn sharedInstance].clientID = kClientId;
//    [GIDSignIn sharedInstance].allowsSignInWithBrowser = NO;
    [GIDSignIn sharedInstance].serverClientID = kServerId;
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
    [GIDSignIn sharedInstance].scopes = [NSArray arrayWithObjects:
                                         kGTLAuthScopePlusLogin, kGTLAuthScopePlusUserinfoEmail, kGTLAuthScopePlusUserinfoProfile, kGTLAuthScopePlusMe,
                                         nil];

}
#pragma mark UI customization
-(void)customizeUI
{
    // Customize the segment controls
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraMedium], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setTintColor:[UIColor spn_aquaColor]];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraMedium], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    // Customize textfields
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont spn_NeutraMedium]];
    [[UITextField appearance] setFont:[UIFont spn_NeutraMedium]];
    
    // Customize bar buttons
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraBoldMedium], NSFontAttributeName, [UIColor spn_aquaColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    // Customize navigation bar
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraBoldLarge], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor spn_aquaColor]];
    // Customize tab bars
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraMedium], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont spn_NeutraMedium], NSFontAttributeName, nil] forState:UIControlStateSelected];
    
    [[UITabBar appearance] setBarTintColor:[UIColor spn_darkGrayColor]];
    
    [[UITabBar appearance] setTintColor:[UIColor spn_aquaColor]];

}

+ (NSString *)uuid {
    NSString *uuid = [[LUKeychainAccess standardKeychainAccess] stringForKey:@"uuid"];
    if (uuid.length > 0) {
        return uuid;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    uuid = [defaults objectForKey:@"deviceUuid"];
    if (uuid) {
        [defaults removeObjectForKey:@"deviceUuid"];
        [defaults synchronize];
    } else {
        CFStringRef cfUuid = CFUUIDCreateString(NULL, CFUUIDCreate(NULL));
        uuid = (__bridge NSString *)cfUuid;
        CFRelease(cfUuid);
    }
    [[LUKeychainAccess standardKeychainAccess] setString:uuid forKey:@"uuid"];
    return uuid;
}

#pragma mark Location

- (void)setMyLocation:(CLLocation *)myLocation {
    _myLocation = myLocation;
    if (locationSaving) {
        return;
    }
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.timeStyle = NSDateFormatterMediumStyle;
    
    NSUserDefaults *defauls = [NSUserDefaults standardUserDefaults];
    
    NSDate *date = [dateFormatter dateFromString:[defauls objectForKey:@"location_update"]];
    if ([date isTheSameDay:[NSDate date]]) {
        return;
    }
    
    locationSaving = YES;
    
    [[SPNCinelifeQueries sharedInstance] saveLocation:myLocation completion:^(NSError *error) {
        if (error) {
            NSLog(@"failed to save location: %@", error.localizedDescription);
        } else {
            [defauls setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:@"location_update"];
            [defauls synchronize];
        }
        locationSaving = NO;
    }];
}

#pragma mark Tab bar and view controller initialization
-(void)initializeTabBarItems
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tabBarController = [mainStoryboard instantiateInitialViewController];
    
    UIStoryboard *theatreStoryboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
    SPNNavigationController *theatreNavigationController = [theatreStoryboard instantiateInitialViewController];
    
    UIStoryboard *filmsStoryboard = [UIStoryboard storyboardWithName:@"FilmsStoryboard" bundle:nil];
    SPNNavigationController *filmNavigationController = [filmsStoryboard instantiateInitialViewController];
    
    UIStoryboard *whatsNextStoryboard = [UIStoryboard storyboardWithName:@"WhatsNextStoryboard" bundle:nil];
    SPNNavigationController *whatsNetxtNavigationController = [whatsNextStoryboard instantiateInitialViewController];
    
    UIStoryboard *accountStoryboard = [UIStoryboard storyboardWithName:@"AccountStoryboard" bundle:nil];
    SPNNavigationController *accountNavigationController = [accountStoryboard instantiateInitialViewController];
    
    [theatreNavigationController.navigationBar setBarTintColor:[UIColor spn_darkGrayColor]];
    [filmNavigationController.navigationBar setBarTintColor:[UIColor spn_darkGrayColor]];
    [whatsNetxtNavigationController.navigationBar setBarTintColor:[UIColor spn_darkGrayColor]];
    [accountNavigationController.navigationBar setBarTintColor:[UIColor spn_darkGrayColor]];
    
    
    NSMutableArray *tabBarViewControllers = [NSMutableArray arrayWithArray:[tabBarController viewControllers]];
    [tabBarViewControllers addObjectsFromArray:@[theatreNavigationController, filmNavigationController, whatsNetxtNavigationController, accountNavigationController]];
    
    [tabBarController setViewControllers: tabBarViewControllers];
    NSArray *tabList =[[tabBarController tabBar] items];
    UITabBarItem *tab1 = [tabList firstObject];
    [tab1 setTitle:@"Theatres"];
    [tab1 setImage:[UIImage imageNamed:@"theatre-icon"]];
    [tab1 setSelectedImage:[UIImage imageNamed:@"theatre-icon-active"]];
    
    UITabBarItem *tab2 = [tabList objectAtIndex:1];
    [tab2 setTitle:@"Films"];
    [tab2 setImage:[UIImage imageNamed:@"films-icon"]];
    [tab2 setSelectedImage:[UIImage imageNamed:@"films-icon"]];
    
    UITabBarItem *tab3 = [tabList objectAtIndex:2];
    [tab3 setTitle:@"My CineLife"];
    [tab3 setImage:[UIImage imageNamed:@"cineLifeLogo"]];
    [tab3 setSelectedImage:[UIImage imageNamed:@"cineLifeLogo"]];
    
    UITabBarItem *tab4 = [tabList lastObject];
    [tab4 setTitle:@"Account"];
    [tab4 setImage:[UIImage imageNamed:@"config-icon"]];
    [tab4 setSelectedImage:[UIImage imageNamed:@"config-icon"]];
  //  tab4.imageInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    self.window.rootViewController = tabBarController;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // [UIApplication sharedApplication].applicationIconBadgeNumber = 5;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    [userPrefs setObject:[NSDate date] forKey:@"lastDate"];
    [userPrefs synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[SPNCinelifeQueries sharedInstance] readAllNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if ([UIApplication sharedApplication].applicationIconBadgeNumber > 0) {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
   
    if ([tabBarController isKindOfClass:[UITabBarController class]]) {
        [MBProgressHUD hideAllHUDsForView:tabBarController.view animated:YES];
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
        [[[tabBarController viewControllers] objectAtIndex:3] tabBarItem].badgeValue = 0;
        if ([UIApplication sharedApplication].applicationIconBadgeNumber > 0) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
      //  NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
      //  NSDate *currDate = [NSDate date];
      //  NSDate *lastDate =[userPrefs objectForKey:@"lastDate"];
        
      //  NSInteger days = [self daysBetweenDate:currDate andDate:lastDate];
        //if ( days <1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPurchase" object:self userInfo:nil];
        
        [self performSelector:@selector(sendNotifications) withObject:nil afterDelay:1];
        
    }
    
   // }
}

-(void)sendNotifications
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshShowtimes" object:self userInfo:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshFilmShowtimes" object:self userInfo:nil];
    

}
- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    if (!fromDateTime || !toDateTime) {
        return 0;
    }
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return ABS([difference day]);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark Google plus login handler
- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation {
    
   // if ([[url scheme] isEqualToString:@"fb1381433738821756"]) {
    //if ([[url scheme] isEqualToString:@"fb418844628318200"]) {

    if ([[url scheme] isEqualToString:@"fb269325429936788"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
    }
    else {
        return [[GIDSignIn sharedInstance] handleURL:url
                      sourceApplication:sourceApplication
                             annotation:annotation];
    }
}


- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FavoriteFilms" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FavoriteFilms.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *migrationOptions =@{ NSMigratePersistentStoresAutomaticallyOption : @YES, NSInferMappingModelAutomaticallyOption : @YES};
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:migrationOptions error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Push notification service
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSLog(@"notifs");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationsEnabled"
                                                        object:self
                                                      userInfo:@{@"gotToken" : @YES}];
     #if !TARGET_IPHONE_SIMULATOR
     
     // Get Bundle Info for Remote Registration (handy if you have more than one app)
    // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    // NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
     
     // Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
    NSUInteger rntypes;
    #ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        rntypes = [settings types];
    #else
        rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    #endif
     // Set the defaults to disabled unless we find otherwise...
     BOOL pushBadge = (rntypes & UIUserNotificationTypeBadge) ? YES : NO;
     BOOL pushAlert = (rntypes & UIUserNotificationTypeAlert) ? YES : NO;
     BOOL pushSound = (rntypes & UIUserNotificationTypeSound) ? YES : NO;
     
     // Get the users Device Model, Display Name, Unique ID, Token & Version Number
     UIDevice *dev = [UIDevice currentDevice];
     NSString *deviceUuid = [SPNAppDelegate uuid];
     NSString *deviceName = dev.name;
     NSString *deviceModel = dev.model;
     NSString *deviceSystemVersion = dev.systemVersion;
     
     // Prepare the Device Token for Registration (remove spaces and < >)
     NSString *devToken = [[[[deviceToken description]
     stringByReplacingOccurrencesOfString:@"<"withString:@""]
     stringByReplacingOccurrencesOfString:@">" withString:@""]
     stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    if (pushAlert || pushBadge || pushSound) {
        [[SPNCinelifeQueries sharedInstance] postRegisterDeviceForPushNotificationsWithUDID:deviceUuid
                                                                                   andToken:devToken
                                                                             withDeviceName:deviceName
                                                                             andDeviceModel:deviceModel
                                                                                withVersion:deviceSystemVersion
                                                                                   forBadge:pushBadge
                                                                                   forAlert:pushAlert
                                                                                   forSound:pushSound
                                                                            withCredentials:[SPNUser getCurrentUserId]];
        
    }
   
    else {
        [[SPNCinelifeQueries sharedInstance] postUnregisterDeviceForPushNotificationsWithUDID:deviceUuid];
    }
//    NSDictionary *user = [SPNUser getCurrentUserId];
//    NSString *userId = [user objectForKey:@"id"];
//    if(userId){
//        [Intesify registerUser:@"https://cinemaup.intensify-solutions.com" userId:userId deviceToken:devToken language:@"" completion:^(IUUser * user) {
//            [Intesify deliverNotificationFromLaunch];
//            
//        }];
//    }
     #endif

}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@",  error);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationsEnabled"
                                                        object:self
                                                      userInfo:@{@"gotToken" : @NO, @"error" : error.localizedDescription}];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Remote notification: %@", userInfo);
    [self didReceiveRemoteNotification:userInfo];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Remote notification 2: %@", userInfo);
//    if([Intesify isIntensifyUPNotificationWithUserInfo:userInfo]){
//        NSDictionary *user = [SPNUser getCurrentUserId];
//        NSString *userId = [user objectForKey:@"id"];
//        if(userId){
//            [Intesify handleIntensifyUpRemoteNotifications:application userInfo:userInfo completionHandler:completionHandler];
//        }
//    } else {
        [self didReceiveRemoteNotification:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);
//    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *notificationJSON = notification.request.content.userInfo;
    if (notificationJSON) {
        [self saveNotification:notificationJSON];
        [self updateBadge];
        completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
    } else {
        completionHandler(0);
    }
}

- (void)updateBadge {
    if ([UIApplication sharedApplication].applicationIconBadgeNumber > 0) {
        UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
        if (![tabBarController isKindOfClass:[UITabBarController class]]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self updateBadge];
            });
            return;
        }
        [[[tabBarController viewControllers] objectAtIndex:3] tabBarItem].badgeValue = [NSString stringWithFormat:@"%ld", (long) [UIApplication sharedApplication].applicationIconBadgeNumber];
    }
}

- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self updateBadge];
    if ([UNUserNotificationCenter class] != nil) {
        if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive) {
            [self saveNotification:userInfo];
        }
        [self processNotification:userInfo withAlert:NO];
    } else {
        [self saveNotification:userInfo];
        [self processNotification:userInfo withAlert:[[UIApplication sharedApplication] applicationState] == UIApplicationStateActive];
    }
}


- (void)saveNotification:(NSDictionary *)userInfo {
    NSDictionary *notificationContent = [userInfo objectForKey:@"aps"];
    NSString *alert = [notificationContent objectForKey:@"alert"];
    NSMutableDictionary *newNotification = [NSMutableDictionary new];
    
    [newNotification setObject:alert ? alert : @""
                        forKey:@"notification_body"];
    [newNotification setObject:@"Title"
                        forKey:@"notification_title"];
    [newNotification setObject:[NSDate date]
                        forKey:@"notification_date"];
    [newNotification setObject:notificationContent
                        forKey:@"info"];
    
    [[LCPDatabaseHelper sharedInstance] addRecord:newNotification
                                        forEntity:@"Notifications"];
}

- (NotificationType)notificationTypeFromPushType:(NSInteger)type {
    switch (type) {
        case 1:
            return NotificationTypeTheater;
            
        case 2:
            return NotificationTypeFilm;
        
        case 3:
            return NotificationTypeFestival;
            
        case 4:
            return NotificationTypeMarketing;
            
        default:
            return NotificationTypeGeneral;
    }
}

- (void)processNotification:(NSDictionary *)notification withAlert:(BOOL)alert {
    if (!notification) return;
    
    if (alert) {
        void (^notificationBlock)(NSString *title, NSDictionary *notification) = ^(NSString *title, NSDictionary *notification) {
            LNNotification *alert = [LNNotification notificationWithMessage:[notification valueForKeyPath:@"aps.alert"]];
            alert.title = title;
            alert.defaultAction = [LNNotificationAction actionWithTitle:@"View" handler:^(LNNotificationAction *action) {
                [self processNotification:notification];
            }];
            [[LNNotificationCenter defaultCenter] presentNotification:alert forApplicationIdentifier:@"CineLife"];
        };
        notificationBlock(@"New Notification", notification);
    } else {
        [self processNotification:notification];
    }
}

- (void)processNotification:(NSDictionary *)userInfo {
    NotificationType type = NotificationTypeGeneral;
    NSString *objectId;
    
    if (userInfo) {
        NSDictionary *notificationContent = [userInfo objectForKey:@"aps"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PushNotificationReceived" object:nil];
        
        type = [NotificationsHelper notificationTypeFromPushType:[notificationContent[@"type"] integerValue]];
        objectId = notificationContent[@"id"];
    }
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    if (![tabBarController isKindOfClass:[UITabBarController class]]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self didReceiveRemoteNotification:userInfo];
        });
        return;
    }
    
    [self updateBadge];
    
    [self handleNotification:type objectId:objectId manual:NO];
}

- (void)handleNotification:(NotificationType)type objectId:(NSString *)objectId manual:(BOOL)manual {
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    switch (type) {
        case NotificationTypeGeneral: {
            if (!manual) {
                tabBarController.selectedIndex = 3;
                UINavigationController *navigationController = tabBarController.viewControllers[3];
                for (UIViewController *controller in navigationController.viewControllers) {
                    if ([controller isKindOfClass:[SPNNotificationTableViewController class]]) {
                        [navigationController popToViewController:controller animated:YES];
                        return;
                    }
                }
                [navigationController.viewControllers.firstObject performSegueWithIdentifier:@"NotificationSegue" sender:nil];
            } else {
                tabBarController.selectedIndex = 0;
                UINavigationController *navigationController = tabBarController.viewControllers[3];
                [navigationController popViewControllerAnimated:YES];
            }
            break;
        }
            
        case NotificationTypeTheater:
            if (!manual) {
                tabBarController.selectedIndex = 0;
            }
            if (objectId) {
                [MBProgressHUD showHUDAddedTo:self.window animated:YES];
                [[SPNCinelifeQueries sharedInstance] getTheater:objectId completion:^(NSDictionary *theaterInfo, NSError *error) {
                    if (theaterInfo) {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
                        SPNTheatreDetailsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SPNTheatreDetailsViewController class])];
                        controller.theatreInformation = theaterInfo;
                        UINavigationController *navigationController = tabBarController.selectedViewController;
                        [navigationController pushViewController:controller animated:YES];
                    }
                    if (error) {
                        NSLog(@"%s getTheater: %@", __PRETTY_FUNCTION__, error.localizedDescription);
                    }
                    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
                }];
            }
            break;
            
        case NotificationTypeFestival:
            if (!manual) {
                tabBarController.selectedIndex = 0;
            }
            if (objectId) {
                [MBProgressHUD showHUDAddedTo:self.window animated:YES];
                [[SPNCinelifeQueries sharedInstance] getFestival:objectId completion:^(Festival *festival, NSError *error) {
                    if (festival) {
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
                        SPNFestivalViewController *controller = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SPNFestivalViewController class])];
                        controller.festival = festival;
                        UINavigationController *navigationController = tabBarController.selectedViewController;
                        [navigationController pushViewController:controller animated:YES];
                    }
                    if (error) {
                        NSLog(@"%s getFestival: %@", __PRETTY_FUNCTION__, error.localizedDescription);
                    }
                    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
                }];
            }
            break;
            
        case NotificationTypeFilm:
            if (!manual) {
                tabBarController.selectedIndex = 0;
            }
            if (objectId) {
                [MBProgressHUD showHUDAddedTo:self.window animated:YES];
                [[SPNCinelifeQueries sharedInstance] getMovie:objectId completion:^(NSDictionary *movieInfo, NSError *error) {
                    if (movieInfo) {
                        UIStoryboard *filmsStoryboard = [UIStoryboard storyboardWithName:@"FilmsStoryboard"
                                                                                  bundle:nil];
                        SPNFilmDetailsViewController *controller = [filmsStoryboard instantiateViewControllerWithIdentifier:@"FilmDetails"];
                        controller.filmInformation = movieInfo;
                        controller.currentUserLocation = [appDelegate myLocation];
                        controller.shouldBeReviewable = YES;
                        
                        UINavigationController *navigationController = tabBarController.selectedViewController;
                        [navigationController pushViewController:controller animated:YES];
                    }
                    if (error) {
                        NSLog(@"%s getMovie: %@", __PRETTY_FUNCTION__, error.localizedDescription);
                    }
                    [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
                }];
            }
            break;
            
        case NotificationTypeMarketing:
            if (!manual) {
                tabBarController.selectedIndex = 2;
            } else {
                tabBarController.selectedIndex = 2;
                UINavigationController *navigationController = tabBarController.viewControllers[3];
                [navigationController popViewControllerAnimated:YES];
            }
            break;
    }
}

@end

