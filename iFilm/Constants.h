//
//  Constants.h
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

static NSString * const kNotificationAddedTheaterToFavorites = @"addedTheaterToFavorites";
static NSString * const kNotificationRemovedTheaterFromFavorites = @"removedTheaterFromFavorites";

static NSString * const kNotificationAddedMovieToFavorites = @"addedMovieToFavorites";
static NSString * const kNotificationRemovedMovieFromFavorites = @"removedMovieFromFavorites";

static NSString * const kNotificationAddedFestivalToFavorites = @"addedFestivalToFavorites";
static NSString * const kNotificationRemovedFestivalFromFavorites = @"removedFestivalFromFavorites";
