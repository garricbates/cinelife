//
//  SPNIndiePostDetailsViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNIndiePostDetailsViewController.h"

@interface SPNIndiePostDetailsViewController ()

@end

@implementation SPNIndiePostDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"CineLife RSS Details";
    // Do any additional setup after loading the view.
    NSURL *url = [NSURL URLWithString:[self.postURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.postWebView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
