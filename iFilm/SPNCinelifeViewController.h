//
//  SPNCinelifeViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

#import "SPNCinelifeQueries.h"
@interface SPNCinelifeViewController : GAITrackedViewController   <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIAlertViewDelegate, SPNCinelifeQueriesDelegate>

@property (strong, nonatomic) IBOutlet UIButton *promotionsButton;
@property IBOutlet UICollectionView *indieCollectionView;
@end

