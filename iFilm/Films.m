//
//  Films.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "Films.h"
#import "Users.h"


@implementation Films

@dynamic movieId;
@dynamic title;
@dynamic user;

@end
