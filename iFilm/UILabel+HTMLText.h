//
//  UILabel+HTMLText.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTMLText)

-(void)setHTMLText:(NSString*)htmlText;

-(void)assignMetaScore:(NSString*)metaScore;
@end
