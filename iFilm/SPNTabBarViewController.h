//
//  SPNTabBarViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/15/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTabBarViewController : UITabBarController

@end
