//
//  SPNPaymentInformationViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPaymentInformationViewController.h"
#import "SPNConfirmPurchaseViewController.h"

#import "SPNDismissCell.h"
#import "SPNUserCell.h"
#import "SPNPaymentInfoCell.h"

#define kPersonalInfoRowHeight 225
#define kCardInfoRowHeight 93

@interface SPNPaymentInformationViewController ()
{
    NSArray *monthsArray;
    UIPickerView *monthYearPicker;
    NSInteger maxMonths;
    BOOL shouldStoreData, shouldShowDismissCell, shouldAddBackground;
    NSDictionary *cardInfo;
    NSString *email, *name;
    NSString *cardType;
    UIColor *sectionColor, *titleSectionColor, *mainColor;
}
@end

@implementation SPNPaymentInformationViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(abortPurchase:)
                                                 name:@"cancelPurchase"
                                               object:nil];
    
    if (!shouldAddBackground) {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.paymentInformationTableView.contentSize.height - 44, self.paymentInformationTableView.bounds.size.width+300, 600)];
        [bottomView setBackgroundColor:[UIColor spn_brownColor]];
        if (mainColor) {
            [bottomView setBackgroundColor:mainColor];
        }
        [bottomView setTag:1011];
        [self.paymentInformationTableView addSubview:bottomView];
        [self.paymentInformationTableView sendSubviewToBack:bottomView];
        shouldAddBackground = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    UIView *bottomView = [self.paymentInformationTableView viewWithTag:1011];
    [bottomView setFrame:CGRectMake(0, self.paymentInformationTableView.contentSize.height - 44, self.paymentInformationTableView.bounds.size.width+300, 600)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shouldStoreData = YES;
    
    
   // [self.navigationController setTitle:@"Buy Tickets"];
    [self initializeFilmData:self.movieInformation];
    [self.nextButton setBackgroundColor:[UIColor spn_buttonColor]];
    [self.nextButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
    [self.nextButton.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    shouldShowDismissCell = YES;
    
    NSString *movieTitle = [self.movieInformation objectForKey:@"title"];
    NSString *movieId = [self.movieInformation objectForKey:@"movieId"];
    
    if (![[[SPNFilmDataCache sharedInstance] metaScoreCache] objectForKey:movieId]) {
        NSString *metascore = [SPNServerQueries getMetascoreForMovie:movieTitle];
        if (metascore && ![metascore isEqualToString:@"0"]) {
            [[[SPNFilmDataCache sharedInstance] metaScoreCache] setObject:metascore
                                                                   forKey:movieId];
        }
        else {
            [[[SPNFilmDataCache sharedInstance] metaScoreCache] setObject:@"NR"
                                                                   forKey:movieId];
        }
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:[[[SPNFilmDataCache sharedInstance]
                                               metaScoreCache]
                                              objectForKey:movieId]];
    }
    [self customizeUI];
    [self getBrandColors:self.branding];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)abortPurchase:(id)sender {
    
    [UIAlertView showAlertViewWthTitle:@"Showtime not available" andMessage:@"The showtime you selected is not available anymore. Please select a new showtime to purchase tickets!"];

    NSArray *controllers = [self.navigationController childViewControllers];
    [self.navigationController popToViewController:[controllers objectAtIndex:1] animated:YES];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UI customization
-(void)customizeUI
{
    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.genresLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.userRatingLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.view setBackgroundColor:[UIColor spn_darkBrownColor]];
    [self.theatreBackgroundView setBackgroundColor:[UIColor spn_darkBrownColor]];
    
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        NSString *logo = [branding objectForKey:@"logo"];
        if ([logo length] > 0) {
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
        }
        else {
            [self.brandLogoHeightConstraint setConstant:0];
            UIView *header = self.paymentInformationTableView.tableHeaderView;
            CGRect headerFrame = header.frame;
            CGFloat spaceForLogo = 125;
            headerFrame.size.height  = spaceForLogo;
            header.frame  = headerFrame;
            
            [self.paymentInformationTableView setTableHeaderView:header];
            [self.paymentInformationTableView beginUpdates];
            [self.paymentInformationTableView endUpdates];

        }
        
        if ([[branding objectForKey:@"section_title_background"] length] > 0) {
            sectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_background"]];
        }
        if ([[branding objectForKey:@"section_title_text"] length] > 0) {
            titleSectionColor = [UIColor colorFromHexString:[branding objectForKey:@"section_title_text"]];
        }
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            [self.view setBackgroundColor:mainColor];
            [self.theatreBackgroundView setBackgroundColor:mainColor];
        }
    }
    else {
        [self.brandLogoHeightConstraint setConstant:0];
        UIView *header = self.paymentInformationTableView.tableHeaderView;
        CGRect headerFrame = header.frame;
        CGFloat spaceForLogo = 125;
        headerFrame.size.height  = spaceForLogo;
        header.frame  = headerFrame;
        
        [self.paymentInformationTableView setTableHeaderView:header];
        [self.paymentInformationTableView beginUpdates];
        [self.paymentInformationTableView endUpdates];
        
    }

}



#pragma mark - Film initialization
-(void)initializeFilmData:(NSDictionary*)film
{
    NSString *movieTitle =[film objectForKey:@"name"] ? [film objectForKey:@"name"] : [film objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *urlImage;
    NSDictionary *posters = [film objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];

    self.posterImageView.imageURL = [NSURL URLWithString:urlImage];
    
    
    id genres =[film objectForKey:@"genres"];
    
    if ([genres isKindOfClass:[NSArray class]]) {
        self.genresLabel.text = [genres componentsJoinedByString:@" | "];
    }
    else if ([genres isKindOfClass:[NSString class]]) {
        self.genresLabel.text = genres;
    }
    else {
        self.genresLabel.text = @"No genre information available";
    }
    NSString *rating = [film objectForKey:@"rating"];
    
    if ([rating length] > 0) {
        self.classificationLabel.text = [NSString stringWithFormat:@"%@", rating];
    }
    else {
        self.classificationLabel.text = @"";
    }
    
    NSString *runTime = [film objectForKey:@"runtime"];
    if (!runTime || [runTime length] < 1) {
        runTime = @"0";
    }
    NSString *duration = [NSString stringWithFormat:@"%@ min", runTime];
    self.durationLabel.text = duration;
    
    NSDictionary *metacritic = [film objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        [self.userRatingLabel setText:[NSString stringWithFormat:@"%g", [userScore floatValue]]];
    }
    else {
        [self.userRatingLabel setText:@"No votes"];
    }    
}



#pragma mark- Customization of text fields
-(void)configureTextFields
{
    maxMonths = 12;
    monthsArray = @[@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December"];
    
    monthYearPicker = [[UIPickerView alloc] init];
    monthYearPicker.delegate = self;
    monthYearPicker.dataSource = self;
    [monthYearPicker selectRow:5000 inComponent:0 animated:NO];
    [monthYearPicker selectRow:2550 inComponent:1 animated:NO];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleBlack;
    keyboardDoneButtonView.translucent = YES;
    keyboardDoneButtonView.tintColor = nil;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(cancelClicked:)];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:cancelButton, nil]];
    self.cardExpirationDateTextField.inputAccessoryView = keyboardDoneButtonView;
    [self.cardExpirationDateTextField setInputView:monthYearPicker];
    
    
    UIToolbar* keyboardCVCView = [[UIToolbar alloc] init];
    keyboardCVCView.barStyle = UIBarStyleBlack;
    keyboardCVCView.translucent = YES;
    keyboardCVCView.tintColor = nil;
    [keyboardCVCView sizeToFit];
    
    UIBarButtonItem* doneCVCButton = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                      style:UIBarButtonItemStylePlain target:self
                                                                     action:@selector(nextClicked:)];
    
    UIBarButtonItem* cancelCVCButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                        style:UIBarButtonItemStylePlain target:self
                                                                       action:@selector(cancelClicked:)];
    [keyboardCVCView setItems:[NSArray arrayWithObjects:doneCVCButton, cancelCVCButton, nil]];
    
    
    self.cardCVCTextField.inputAccessoryView = keyboardCVCView;

    
    UIToolbar* keyboardCCNView = [[UIToolbar alloc] init];
    keyboardCCNView.barStyle = UIBarStyleBlack;
    keyboardCCNView.translucent = YES;
    keyboardCCNView.tintColor = nil;
    [keyboardCCNView sizeToFit];
    
    UIBarButtonItem* doneCCNButton = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                      style:UIBarButtonItemStylePlain target:self
                                                                     action:@selector(nextClicked:)];
    
    UIBarButtonItem* cancelCCNButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                        style:UIBarButtonItemStylePlain target:self
                                                                       action:@selector(cancelClicked:)];
    [keyboardCCNView setItems:[NSArray arrayWithObjects:doneCCNButton, cancelCCNButton, nil]];
    self.cardNumberTextField.inputAccessoryView = keyboardCCNView;
    
   
}

#pragma mark - Table view functions

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 1) {
        return shouldShowDismissCell ? 44 : 0;
    }
    else if (indexPath.row < 2 || self.selectedPaymentMethod == SPNPaymentMethodCreditCard) {
        return kPersonalInfoRowHeight;
    }
    else {
        return kCardInfoRowHeight;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PersonalInformationCell";
    static NSString *CreditCardCellIdentifier = @"PaymentInformationCell";
    static NSString *OtherCardCellIdentifier = @"CardInformationCell";
    static NSString *DismissCellIdentifier  = @"DismissCell";
    
    if (indexPath.row < 1) {
        SPNDismissCell *dismissCell = [tableView dequeueReusableCellWithIdentifier:DismissCellIdentifier];
        
        [dismissCell.xLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
        [dismissCell.messageLabel setFont:[UIFont spn_NeutraSmallMedium]];
        [dismissCell.messageTwoLabel setFont:[UIFont spn_NeutraSmallMedium]];
        return dismissCell;
    }
    else if (indexPath.row < 2) {
        SPNUserCell *cell = (SPNUserCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        self.nameTextField = cell.nameTextField;
        self.emailTextField = cell.emailTextField;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSDictionary *userData = [prefs objectForKey:@"personalInformation"];
        if (userData) {
            [self.nameTextField setText:[userData objectForKey:@"name"]];
            [self.emailTextField setText:[userData objectForKey:@"email"]];
            [cell.checkIcon setImage:[UIImage imageNamed:@"check-icon"]];
        }
        self.checkIcon = cell.checkIcon;
        UITapGestureRecognizer *check = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(storePersonalData:)];
        
        [cell.storePersonalDataContainerView addGestureRecognizer:check];
        [cell.userTitleLabel setFont:[UIFont spn_NeutraMediumLarge]];
        [cell.storeDataMessageLabel setFont:[UIFont spn_NeutraMediumLarge]];
        return cell;

    }
    else {
        switch (self.selectedPaymentMethod) {
            case SPNPaymentMethodMembershipCard:
            case SPNPaymentMethodGiftCard:
            {
                SPNPaymentInfoCell *cell =  (SPNPaymentInfoCell*)[tableView dequeueReusableCellWithIdentifier:OtherCardCellIdentifier];
                self.cardNumberTextField = cell.cardNumberTextField;
                self.cardCVCTextField = cell.cardCVCTextField;
                self.cardExpirationDateTextField = cell.cardExpirationDateTextField;
                self.postalTextField = cell.postalTextField;
                [self configureTextFields];
                [cell.paymentTitleLabel setFont:[UIFont spn_NeutraMediumLarge]];

                return cell;
            }
                break;
                
            default:
            {
                SPNPaymentInfoCell *cell =  (SPNPaymentInfoCell*)[tableView dequeueReusableCellWithIdentifier:CreditCardCellIdentifier];
                self.cardNumberTextField = cell.cardNumberTextField;
                self.cardCVCTextField = cell.cardCVCTextField;
                self.cardExpirationDateTextField = cell.cardExpirationDateTextField;
                self.postalTextField = cell.postalTextField;
                [self configureTextFields];
                [cell.paymentTitleLabel setFont:[UIFont spn_NeutraMediumLarge]];

                return cell;
            }
                break;
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
    
    if (sectionColor) {
        header.contentView.backgroundColor = sectionColor;
    }
    if (titleSectionColor) {
        [header.textLabel setTextColor:titleSectionColor];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (self.selectedPaymentMethod) {
        case SPNPaymentMethodMembershipCard:
            return @"Laemmle Premiere card";
            break;
        case SPNPaymentMethodGiftCard:
            return @"Gift card";
            break;
        default:
            return @"Credit card";
            break;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor spn_lightBrownColor];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 1) {
        shouldShowDismissCell = NO;
    }
    [self.paymentInformationTableView beginUpdates];
    [self.paymentInformationTableView endUpdates];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  //  scrollView.bounces = (scrollView.contentOffset.y < 70);
}
#pragma mark - Navigation functions
-(void)confirmPurchase:(id)sender
{
    
    BOOL shouldProceed = YES;
    if ([self.emailTextField.text length] < 1) {
        shouldProceed = NO;
    }
    switch (self.selectedPaymentMethod) {
        case SPNPaymentMethodCreditCard:
        {
            cardType = @"CreditCard";
            if ([self.cardCVCTextField.text length] < 1 || [self.cardExpirationDateTextField.text length] < 1 || [self.postalTextField.text length] < 1) {
                shouldProceed = NO;
            }
        }
        default:
            cardType = @"GiftCard";
            if ([self.cardNumberTextField.text length] < 1) {
                shouldProceed = NO;
            }
            break;
    }

    if (!shouldProceed) {
        [UIAlertView showAlertViewWthTitle:@"Wait"
                                andMessage:@"Please fill in the required fields"];
    }
    else {
        if (shouldStoreData) {
            [self addPersonalInformationToUser:self.nameTextField.text
                                     withEmail:self.emailTextField.text];
        }
        else {
            [self deleteData];
        }
        NSString *cardCVC = self.cardCVCTextField.text;
        NSString *cardNumber = self.cardNumberTextField.text;
        NSString *cardExpDate = self.cardExpirationDateTextField.text;
        NSString *postalCode = self.postalTextField.text;
        email = self.emailTextField.text;
        name = self.nameTextField.text;
        if (self.selectedPaymentMethod == SPNPaymentMethodCreditCard) {
            cardInfo = [NSDictionary dictionaryWithObjects:@[email, cardNumber, cardExpDate, cardCVC, postalCode, @"CreditCard"]
                                                   forKeys:@[@"cardName", @"cardNumber", @"cardExpirationDate", @"cardCVC", @"cardPostal", @"cardType"]];
        }
        else {
            cardInfo = [NSDictionary dictionaryWithObjects:@[cardNumber, @"GiftCard"]
                                                   forKeys:@[@"cardNumber", @"cardType"]];
        }
        [self performSegueWithIdentifier:@"ConfirmPurchase" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ConfirmPurchase"]) {
        SPNConfirmPurchaseViewController *confirmController = [segue destinationViewController];
        [confirmController setShowtimeInformation:self.showtimeInformation];
        [confirmController setMovieInformation:self.movieInformation];
        [confirmController setPaymentInformation:self.paymentInformation];
        [confirmController setCardInformation:cardInfo];
        [confirmController setEmail:email];
        [confirmController setName:name];
        [confirmController setTicketingInformation:self.ticketingInformation];
        [confirmController setBranding:self.branding];
    }
}

#pragma mark - Textfield delegate functions
-(void)nextClicked:(id)sender
{
    NSInteger nextTag;
    if ([self.cardNumberTextField isFirstResponder]) {
        nextTag = self.cardNumberTextField.tag +1;
    }
    else if ([self.cardCVCTextField isFirstResponder]) {
        nextTag = self.cardCVCTextField.tag + 1;
    } else {
        nextTag = self.postalTextField.tag + 1;
    }
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self dismissKeyboard];
    }
}


-(void)doneClicked:(id)sender
{
    [self dismissKeyboard];
}

-(void)cancelClicked:(id)sender
{
    [self dismissKeyboard];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }

    return NO;
    
}

#pragma mark - Picker View delegate and data source methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [monthsArray count]*10000;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component < 1) {
        return [monthsArray objectAtIndex:(row % maxMonths)];
    }
    else {
        return [NSString stringWithFormat:@"%ld", (long) (row % 500) + 1970];
    }
}

-(void)updateTextField:(id)sender
{
    if([self.cardExpirationDateTextField isFirstResponder]) {
        UIPickerView *picker = (UIPickerView*)self.cardExpirationDateTextField.inputView;
        self.cardExpirationDateTextField.text =   [NSString stringWithFormat:@"%02ld/%02ld",
                                                (long) ([picker selectedRowInComponent:0] % maxMonths) + 1, (long) (([picker selectedRowInComponent:1] % 500) + 1970) % 100];
        
    }
    
    else if ([self.cardNumberTextField isFirstResponder]) {
        if ([[self.cardNumberTextField.text substringToIndex:1] isEqualToString:@"3"]) {
           // self.creditCardType.text = @"American Express";
        }
        else if ([[self.cardNumberTextField.text substringToIndex:1] isEqualToString:@"4"]) {
           // self.creditCardType.text = @"VISA";
        }
        else if ([[self.cardNumberTextField.text substringToIndex:1] isEqualToString:@"5"]) {
          //  self.creditCardType.text = @"MasterCard";
        }
        else {
          //  self.creditCardType.text = @"Invalid card";
        }
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    self.cardExpirationDateTextField.text =   [NSString stringWithFormat:@"%02ld/%02ld",
                                            (long) ([pickerView selectedRowInComponent:0]  % maxMonths) + 1, (long) (([pickerView selectedRowInComponent:1] % 500) + 1970) % 100];
}

#pragma mark - DB Storage functions
-(IBAction)storePersonalData:(id)sender
{
    if (!shouldStoreData) {
        [self.checkIcon setImage:[UIImage imageNamed:@"check-icon"]];
        //[self.storeDataButton setBackgroundColor:[UIColor spn_aquaColor]];
    }
    else {
        [self.checkIcon setImage:[UIImage imageNamed:@"uncheck-icon"]];
     //   [self.checkBoxButton setBackgroundColor:[UIColor whiteColor]];
    }
    shouldStoreData = !shouldStoreData;
}


-(void)addPersonalInformationToUser:(NSString*)userName withEmail:(NSString*)userEmail
{
    NSDictionary *personalData = [[NSDictionary alloc] initWithObjects:@[userName, userEmail]
                                                             forKeys:@[@"name", @"email"]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:personalData forKey:@"personalInformation"];
    [prefs synchronize];
    NSString *hiddenCardNumber = [NSString new];
    for (int i=1; i < 13; i++) {
        hiddenCardNumber = [hiddenCardNumber stringByAppendingString:@"\u25CF"];
        if (i % 4 == 0)
            hiddenCardNumber = [hiddenCardNumber stringByAppendingString:@" "];
    }
    hiddenCardNumber = [self.cardNumberTextField.text stringByReplacingCharactersInRange:NSMakeRange(0, [self.cardNumberTextField.text length])
                                                                              withString:hiddenCardNumber];
    [self.cardNumberTextField setSecureTextEntry:NO];
  //  self.cardNumberTextField.text = hiddenCardNumber;

}

-(void)deleteData
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"personalInformation"];
    [prefs synchronize];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:self.cardExpirationDateTextField]) {
        textField.text =   [NSString stringWithFormat:@"%02ld/%02ld",
                                                  (long) ([monthYearPicker selectedRowInComponent:0]  % maxMonths) + 1, (long) (([monthYearPicker selectedRowInComponent:1] % 500) + 1970) % 100];
    }
}

@end
