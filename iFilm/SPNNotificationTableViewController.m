//
//  SPNNotificationTableViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//

#import "SPNNotificationTableViewController.h"
#import "Notification+CoreDataProperties.h"
#import "NotificationsHelper.h"
#import "SPNAppDelegate.h"
#import "SPNCinelifeQueries.h"
#import "DataModel.h"

@interface SPNNotificationTableViewController () <SPNNotificationCellDelegate>

@property (nonatomic, retain) NSMutableArray *notifications;

@end

@implementation SPNNotificationTableViewController

static NSString *NotificationCellIdentifier = @"NotificationCell";

NSIndexPath *selectedIndexPath;
bool isClose;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setBackgroundView:[UIView new]];
    [self.tableView setBackgroundColor:[UIColor spn_brownColor]];
    
    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        [self.notificationSwitch setOn:YES];
    }
    else {
        [self.notificationSwitch setOn:NO];
    }
    
    [self.notificationSwitch addTarget:self action:@selector(changeNotificationSettings:) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadNotifications)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadNotifications)
                                                 name:@"PushNotificationReceived"
                                               object:nil];

    selectedIndexPath = NULL;
    isClose = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadNotifications];
}

- (void)reloadNotifications {
    [[SPNCinelifeQueries sharedInstance] getNotificationsWithCompletion:^(NSArray *notifications, NSError *error) {
        self.notifications = notifications.mutableCopy;
        if (!error) {
            [[SPNCinelifeQueries sharedInstance] readAllNotifications];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}

-(void)changeNotificationSettings:(id)sender {
    UISwitch *notifSwitch = (UISwitch*)sender;
    // User turned on push notifs
    if ([notifSwitch isOn]) {
        if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
            UIUserNotificationType types = UIUserNotificationTypeBadge |
            UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
            
            UIUserNotificationSettings *mySettings =
            [UIUserNotificationSettings settingsForTypes:types
                                              categories:nil];
            
            [[UIApplication sharedApplication]  registerUserNotificationSettings:mySettings];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotToken:) name:@"notificationsEnabled" object:nil];
            
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Wait"
                                    message:@"If you turn off notifications, you will not be able to receive updates on last minute schedule changes and exclusive special events from your favorite theatres."
                                   delegate:self
                          cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
        
    }
}

-(void)gotToken:(NSNotification*)notification
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL didGetToken = [[[notification userInfo] objectForKey:@"gotToken"] boolValue];
    if (!didGetToken) {
        NSString *errorText =[[notification userInfo] objectForKey:@"error"];
        [[[UIAlertView alloc] initWithTitle:@"Failed to activate notifications"
                                    message:errorText
                                   delegate:nil
                          cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        [self.notificationSwitch setOn:NO animated:YES];
    }
}

#pragma mark - Table view data source
-(void)configureNotificationCell:(SPNNotificationCell*)cell forIndexPath:(NSIndexPath*)path
{
    Notification *notification = _notifications[path.row];
    cell.titleLabel.text = notification.message;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Recent Notifications";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _notifications.count;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SPNNotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:NotificationCellIdentifier];
    [self configureNotificationCell:cell
                       forIndexPath:indexPath];
    
    cell.delegate = self;
    
    if (selectedIndexPath != NULL && indexPath.row == selectedIndexPath.row && !isClose) {
        cell.closeButton.hidden = NO;
        cell.deleteButton.hidden = NO;
    } else {
        cell.closeButton.hidden = YES;
        cell.deleteButton.hidden = YES;
    }

    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat rowHeight;
    if (selectedIndexPath != NULL && indexPath.row == selectedIndexPath.row && !isClose) {
        Notification *notification = _notifications[indexPath.row];
        UIFont *font = [UIFont systemFontOfSize:14];
        CGFloat width = CGRectGetWidth(tableView.frame) - 75;
        CGFloat height = [notification.message findHeightForWidth:width andFont:font] + 24;
        rowHeight = MAX(height, 70);
    } else {
        rowHeight = 41;
    }
    return rowHeight;
}

#pragma mark - Table view functions
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)deleteAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = _notifications[indexPath.row];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] removeNotificationWithId:notification.notificationId completion:^(NSError *error) {
        if (!error) {
            [self.tableView beginUpdates];
            if (selectedIndexPath != nil) {
                SPNNotificationCell *cell = (id)[self.tableView cellForRowAtIndexPath:selectedIndexPath];
                cell.titleLabel.numberOfLines = 1;
                cell.closeButton.hidden = YES;
                cell.deleteButton.hidden = YES;
                selectedIndexPath = nil;
            }
            [_notifications removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                  withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView endUpdates];
            [[LCPDatabaseHelper sharedInstance] deleteRecord:notification];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                        message:error.localizedDescription
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (selectedIndexPath != NULL) {
        SPNNotificationCell *cell = [tableView cellForRowAtIndexPath:selectedIndexPath];
        cell.closeButton.hidden = YES;
        cell.deleteButton.hidden = YES;
        cell.titleLabel.numberOfLines = 1;
    }

    if (selectedIndexPath == NULL || selectedIndexPath.row != indexPath.row|| isClose) {
        selectedIndexPath = indexPath;
        isClose = NO;
        
        SPNNotificationCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.closeButton.hidden = NO;
        cell.deleteButton.hidden = NO;
        cell.titleLabel.numberOfLines = 0;
        
        [tableView beginUpdates];
        [tableView endUpdates];
    } else {
    Notification *notification = _notifications[indexPath.row];
    NotificationType type = [NotificationsHelper notificationTypeFromPushType:notification.typeId.integerValue];
        NSString *objectId;
        if (type == NotificationTypeTheater) {
            objectId = notification.theaterId.stringValue;
        } else if (type == NotificationTypeFestival) {
            objectId = notification.festivalId.stringValue;
        } else {
            objectId = notification.filmId.stringValue;
        }
            [appDelegate handleNotification:type objectId:objectId manual:YES];
            return;
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Yes"]) {
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
    else if ([title isEqualToString:@"No"]) {
        [self.notificationSwitch setOn:YES animated:YES];
    }
}

- (void)didPressClose:(SPNNotificationCell *)cell {
    isClose = YES;
    selectedIndexPath = [self.tableView indexPathForCell:cell];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    cell.titleLabel.numberOfLines = 1;
    cell.closeButton.hidden = YES;
    cell.deleteButton.hidden = YES;
}

- (void)didPressDelete:(SPNNotificationCell *)cell {
    isClose = YES;
    selectedIndexPath = [self.tableView indexPathForCell:cell];
    [self.tableView beginUpdates];
    cell.titleLabel.numberOfLines = 1;
    cell.closeButton.hidden = YES;
    cell.deleteButton.hidden = YES;
    [self.tableView endUpdates];
    [self deleteAtIndexPath:selectedIndexPath];
}
@end
