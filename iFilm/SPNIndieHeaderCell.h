//
//  SPNIndieHeaderCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNIndieHeaderCell : UITableViewCell

@property IBOutlet UILabel *titleLabel;
@property IBOutlet UIButton *seeAllButton;
@end
