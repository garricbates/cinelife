//
//  SPNPartnerButton.m
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPartnerButton.h"
#import "PartnerConfiguration.h"

@interface SPNPartnerButton () {
    BOOL partnerInitiated;
}

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightConstriant;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthConstriant;

@end

@implementation SPNPartnerButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    _visible = YES;
    [self addTarget:self
             action:@selector(explainPartnership)
   forControlEvents:UIControlEventTouchUpInside];
}

- (void)setVisible:(BOOL)visible {
    _visible = visible;
    
    if (visible && !partnerInitiated) {
        PartnerConfiguration *config = [PartnerConfiguration current];
        self.widthConstriant.constant = config.size.width;
        
        [self setTitle:config.text forState:UIControlStateNormal];
        self.backgroundColor = config.color;
        
        partnerInitiated = YES;
    }
    self.heightConstriant.constant = visible ? [PartnerConfiguration current].size.height : 0;
    self.hidden = !visible;
}

- (void)explainPartnership {
    [[PartnerConfiguration current] explain];
}


@end
