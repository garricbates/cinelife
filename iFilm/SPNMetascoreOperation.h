//
//  SPNMetascoreOperation.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/14/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPNMetascoreOperation : NSBlockOperation

@property NSString *movieId;
-(void)cancelOperationWithMovieId:(NSString*)movieId;

@end
