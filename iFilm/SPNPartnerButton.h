//
//  SPNPartnerButton.h
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNPartnerButton : UIButton

@property (nonatomic) BOOL visible;

@end
