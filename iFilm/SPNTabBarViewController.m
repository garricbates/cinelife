//
//  SPNTabBarViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/15/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTabBarViewController.h"
#import "SPNFilmTrailerViewController.h"    

@interface SPNTabBarViewController ()

@end

@implementation SPNTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)shouldAutorotate
{
   /* NSInteger selectedIndex =  self.selectedIndex;
    UINavigationController *currentNavViewController = [[self childViewControllers] objectAtIndex:selectedIndex];
    id currentController = [currentNavViewController topViewController];
    if ([currentController isKindOfClass:[SPNFilmTrailerViewController class]]) {
        return YES;
    }*/
    return NO;
}
@end
