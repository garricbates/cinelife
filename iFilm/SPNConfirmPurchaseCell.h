//
//  SPNConfirmPurchaseCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/4/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNConfirmPurchaseCell : UITableViewCell

@property IBOutlet UILabel *dateLabel;
@property IBOutlet UILabel *showLabel;
@property IBOutlet UILabel *ticketsLabel;
@property IBOutlet UILabel *dateTitleLabel;
@property IBOutlet UILabel *showTitleLabel;
@property IBOutlet UILabel *ticketsTitleLabel;
@property IBOutlet UILabel *totalTitleLabel;
@property IBOutlet UILabel *totalAmountLabel;

@end
