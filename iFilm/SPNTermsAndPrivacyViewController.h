//
//  SPNTermsAndPrivacyViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 12/2/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPNCinelifeQueries.h"

@interface SPNTermsAndPrivacyViewController : UIViewController

@property BOOL isTOS;

@property IBOutlet UIWebView *webView;

-(IBAction)doneButtonPressed:(id)sender;
@end
