//
//  UIFont+NeutraText.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (NeutraText)

+(UIFont*)spn_NeutraSmall; // 10px
+(UIFont*)spn_NeutraSmallMedium; // 12px
+(UIFont*)spn_NeutraMedium; // 13.5px
+(UIFont*)spn_NeutraMediumLarge; // 15 px
+(UIFont*)spn_NeutraLarge; // 16px
+(UIFont*)spn_NeutraHuge; // 17px

+(UIFont*)spn_NeutraBoldTiny; // 8px
+(UIFont*)spn_NeutraBoldSmall; // 10px
+(UIFont*)spn_NeutraBoldSmallMedium; // 12px
+(UIFont*)spn_NeutraBoldMedium; // 13.5px
+(UIFont*)spn_NeutraBoldMediumLarge; // 15 px
+(UIFont*)spn_NeutraBoldLarge; // 16 px
+(UIFont*)spn_NeutraBoldHuge; // 17 px
+(UIFont*)spn_NeutraBoldMega; // 21 px
+(UIFont*)spn_NeutraBoldGiga; // 25 px
+(UIFont*)spn_NeutraBoldTera; // 30 px

+(UIFont*)spn_NeutraButtonMedium;
@end
