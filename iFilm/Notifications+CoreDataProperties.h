//
//  Notifications+CoreDataProperties.h
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Notifications.h"

NS_ASSUME_NONNULL_BEGIN

@interface Notifications (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *notification_title;
@property (nullable, nonatomic, retain) NSString *notification_body;
@property (nullable, nonatomic, retain) NSDate *notification_date;
@property (nullable, nonatomic, retain) NSDictionary *info;

@end

NS_ASSUME_NONNULL_END
