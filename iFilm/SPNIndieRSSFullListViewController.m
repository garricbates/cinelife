//
//  SPNIndieRSSFullListViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNIndieRSSFullListViewController.h"

#import "SPNShowtimesCollectionView.h"
#import "SPNIndiePostDetailsViewController.h"



@interface SPNIndieRSSFullListViewController ()
{
    NSString *selectedPostURL;
//    NSDateFormatter *dateFormatter;
    
}
@end

@implementation SPNIndieRSSFullListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"CineLife RSS List";
//    dateFormatter = [[NSDateFormatter alloc] init];
//     dateFormatter = [NSDateFormatter new];
    [self.rssPostsTableView setBackgroundColor:[UIColor spn_brownColor]];
    
    [self.rssPostsTableView setTableFooterView:[UIView new]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(SPNTheatreNewsfeedCell *)prototypeCell
{
    if (!_prototypeCell) {
        _prototypeCell = (SPNTheatreNewsfeedCell*)[self.rssPostsTableView dequeueReusableCellWithIdentifier:@"IndieNewsCell"];
    }
    return _prototypeCell;
}
#pragma mark - Table view functions
-(void)configureCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    SPNTheatreNewsfeedCell *selectedCell = (SPNTheatreNewsfeedCell*)cell;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    [selectedCell.newsTitleLabel setPreferredMaxLayoutWidth:width-16];
    [selectedCell.newsSnippetLabel setPreferredMaxLayoutWidth:width-16];
    
    
    NSDictionary *feed = [self.postFeed objectAtIndex:indexPath.row];
    NSString *snippet = [feed objectForKey:@"content"];
    
    NSString *title = [feed objectForKey:@"title"];
    [selectedCell.newsTitleLabel setText:[title stringByDecodingHTMLEntities]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss Z"];
    
    NSDate *realDate = [feed objectForKey:@"publishedDate"];
    
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];

    NSString *stringDate = [dateFormatter stringFromDate:realDate];
    
    if (!stringDate) {
        stringDate = [dateFormatter stringFromDate:[NSDate date]];
    };
    [selectedCell.newsDateLabel setText:[NSString stringWithFormat:@"Date: %@", stringDate]];
    if ([snippet length] > 140) {
        
        snippet = [snippet substringWithRange:NSMakeRange(0, 140)];
        snippet = [snippet stringByAppendingString:@"..."];
    }
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[snippet dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [selectedCell.newsSnippetLabel setAttributedText: attrStr];
    [selectedCell.newsTitleLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
    [selectedCell.newsSnippetLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [selectedCell.newsDateLabel setFont:[UIFont spn_NeutraSmallMedium]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IndieNewsCell";
    SPNTheatreNewsfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.postFeed count];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SPNTheatreNewsfeedCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor spn_lightBrownColor];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.feedName;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell
      forRowAtIndexPath:indexPath];
    [self.prototypeCell layoutIfNeeded];
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height+1;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *newsData = [self.postFeed objectAtIndex:indexPath.row];
    selectedPostURL = [newsData objectForKey:@"link"];
    [self performSegueWithIdentifier:@"PostDetails" sender:self];

}

#pragma mark - Navigation functions
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PostDetails"]) {
        SPNIndiePostDetailsViewController *postController = [segue destinationViewController];
        [postController setPostURL:selectedPostURL];
    }
}
@end
