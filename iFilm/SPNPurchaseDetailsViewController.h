//
//  SPNPurchaseDetailsViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Purchases.h"
#import "GAITrackedViewController.h"

@interface SPNPurchaseDetailsViewController : GAITrackedViewController

@property Purchases *purchaseInformation;
@property NSDictionary *movieInformation;

@property IBOutlet UILabel *ticketsLabel;
@property IBOutlet UILabel *showDateLabel;
@property IBOutlet UILabel *showTimeLabel;
@property IBOutlet UILabel *transactionCodeLabel;
@property IBOutlet UILabel *codeTitleLabel;

@property IBOutlet UIImageView *qrCodeImageView;
// Movie information outlets
@property IBOutlet UILabel *titleLabel;

@property IBOutlet UIImageView *posterImageView;
@property IBOutlet UIImageView *miniPosterImageView;
@end
