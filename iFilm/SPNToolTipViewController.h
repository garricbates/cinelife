//
//  SPNToolTipViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNToolTipViewController : UIViewController

@property () NSString *toolTipText;

@property () IBOutlet UILabel *toolTipLabel;
@property () IBOutlet UIButton *dismissButton;

+ (void)showWithToolTipText:(NSString*)toolTipText
         overViewController:(id)viewController;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          toolTipText:(NSString*)toolTipText;

-(void)show;
-(void)showWithToolTipText:(NSString*)toolTipText;

- (IBAction)dismissButtonPressed:(id)sender;

@end
