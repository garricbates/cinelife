//
//  SPNTutorialStepsViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SPNTutorialStepsViewController : GAITrackedViewController
@property (nonatomic, assign) NSInteger index;

@property () IBOutlet UILabel *screenTitleLabel;
@property () IBOutlet UILabel *screenDescriptionLabel;
@property () IBOutlet UIImageView *screenBackgroundImageView;

@end
