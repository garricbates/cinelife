//
//  SPNCheckboxButton.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/28/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNCheckboxButton : UIButton

@property BOOL isChecked;
@property UIImage *checkedImage;
@property UIColor *uncheckedBackgroundColor;
@property UIColor *checkedBackgroundColor;

-(id)initWithFrame:(CGRect)frame andCheckedImage:(UIImage*)checkedImage;

@end
