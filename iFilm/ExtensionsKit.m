//
//  ExtensionsKit.m
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "ExtensionsKit.h"

@implementation UIImage (ExtensionsKit)

+ (UIImage *)imageWithSize:(CGSize)size andColor:(UIColor *)color {
    
    if (size.width == 0 || size.height == 0) {
        return nil;
    }
    
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)scaledToFitSize:(CGSize)size {
    //calculate rect
    CGFloat aspect = self.size.width / self.size.height;
    if (size.width / aspect <= size.height) {
        return [self scaledToSize:CGSizeMake(size.width, floor(size.width / aspect))];
    } else {
        return [self scaledToSize:CGSizeMake(floor(size.height * aspect), size.height)];
    }
}

- (UIImage *)scaledToSize:(CGSize)size {
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    //draw
    [self drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *res = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return res;
}


@end

@implementation NSDate (ExtensionsKit)

- (BOOL)isTheSameYear:(NSDate *)date {
    NSCalendarUnit units = NSCalendarUnitEra | NSCalendarUnitYear;
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:units fromDate:self];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:units fromDate:date];
    if ([today year] == [otherDay year] &&
        [today era] == [otherDay era]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isTheSameDay:(NSDate *)date {
    NSCalendarUnit units = NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:units fromDate:self];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:units fromDate:date];
    if ([today day] == [otherDay day] &&
        [today month] == [otherDay month] &&
        [today year] == [otherDay year] &&
        [today era] == [otherDay era]) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSDate *)tzid_dateFromString:(NSString *)dateString timeString:(NSString *)timeString {
    if (dateString.length == 0) {
        return [NSDate date];
    }
    NSArray *components = [dateString componentsSeparatedByString:@":"];
    NSString *timeZoneString = [[components firstObject] substringFromIndex:5];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:timeZoneString];
    
    NSString *date;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.timeZone = timeZone;
    if (timeString) {
        dateFormatter.dateFormat = @"YYYYMMdd HH:mm:ss";
        date = [NSString stringWithFormat:@"%@ %@", components.lastObject, timeString];
    } else {
        dateFormatter.dateFormat = @"YYYYMMdd";
        date = components.lastObject;
    }
    return [dateFormatter dateFromString:date];
}

+ (NSDate *)dateFromString:(NSString *)dateString timeString:(NSString *)timeString {
    if (dateString.length == 0) {
        return [NSDate date];
    }
    
    NSString *date;
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"America/Los_Angeles"];
    if (timeString) {
        dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
        date = [NSString stringWithFormat:@"%@ %@", dateString, timeString];
    } else {
        dateFormatter.dateFormat = @"YYYY-MM-dd";
        date = dateString;
    }
    return [dateFormatter dateFromString:date];
}

@end

@implementation NSString (ExtensionsKit)

- (CGFloat)findHeightForWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGFloat result = font.lineHeight;
    if (self) {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        CGRect frame = [self boundingRectWithSize:textSize
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height);
        
        result = MAX(size.height, result); //At least one row
    }
    return ceilf(result);
}

- (CGFloat)findWidthForHeight:(CGFloat)heightValue andFont:(UIFont *)font {
    CGFloat result = 0;
    if (self) {
        CGSize textSize = { CGFLOAT_MAX, heightValue };
        CGSize size;
        CGRect frame = [self boundingRectWithSize:textSize
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height);
        
        result = MAX(size.width, result);
    }
    return ceilf(result);
}

- (CGSize)findSizeForMaxSize:(CGSize)textSize andFont:(UIFont *)font {
    CGSize result = CGSizeMake(0, font.lineHeight);
    if (self) {
        CGRect frame = [self boundingRectWithSize:textSize
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        result = CGSizeMake(frame.size.width, frame.size.height);
    }
    return result;
}

@end

@implementation NSAttributedString (ExtensionsKit)

- (CGSize)sizeWithMaxSize:(CGSize)size; {
    return [self boundingRectWithSize:size
                              options:(NSStringDrawingUsesLineFragmentOrigin |
                                       NSStringDrawingUsesFontLeading)
                              context:nil].size;
}

@end

@implementation UITableView (ExtensionsKit)

- (void)registerCellClass:(id)cell {
    [self registerCellClass:cell withIdentifier:NSStringFromClass(cell)];
}

- (void)registerCellClass:(id)cell withIdentifier:(NSString *)identifier {
    [self registerClass:cell forCellReuseIdentifier:identifier];
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(cell) bundle:nil] forCellReuseIdentifier:identifier];
}

@end

@implementation UICollectionView (UIKitExtensions)

- (void)registerCellClass:(id)cell {
    [self registerCellClass:cell withIdentifier:NSStringFromClass(cell)];
}

- (void)registerCellClass:(id)cell withIdentifier:(NSString *)identifier {
    [self registerClass:cell forCellWithReuseIdentifier:identifier];
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(cell) bundle:nil] forCellWithReuseIdentifier:identifier];
}

@end

@implementation UIViewController (ExtensionsKit)

+ (UIViewController *)top {
    UIViewController *topVc = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while(topVc.presentedViewController != nil) {
        topVc = topVc.presentedViewController;
    }
    return topVc;
}

@end


@implementation UIDevice (ExtensionsKit)

+ (UIEdgeInsets)safeAreaInsets {
#ifdef __IPHONE_11_0
    if (@available(iOS 11.0, *)) {
        return [[UIApplication sharedApplication] keyWindow].safeAreaInsets;
    }
#endif
    return UIEdgeInsetsZero;
}

@end
