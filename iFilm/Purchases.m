//
//  Purchases.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "Purchases.h"


@implementation Purchases

@dynamic movieId;
@dynamic showDate;
@dynamic showTime;
@dynamic tickets;
@dynamic transactionCode;
@dynamic movieTitle;

@end
