//
//  SPNFilmDetailsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmDetailsViewController.h"

#import "SPNFilmPostReviewViewController.h"
#import "SPNTicketPurchaseViewController.h"
#import "SPNFullPosterViewController.h"
#import "SPNTicketSelectionViewController.h"
#import "SPNTheatreNewsDetailsViewController.h"

#import "SPNSearchOptionCell.h"


#import "SPNPickerCell.h"
#import "SPNShowMoreCell.h"
#import "SPNReviewOptionCell.h"
#import "SPNAddRevieCell.h"
#import "SPNMovieStillCell.h"
#import "SPNDismissCell.h"

#import "SPNDateCell.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

static NSString *metaCriticServer =  @"http://www.metacritic.com";

static NSString *ReviewCellIdentifier = @"ReviewCell";
static NSString *CriticReviewCellIdentifier = @"CriticReviewCell";
static NSString *SynopsisCellIdentifier = @"SynopsisCell";
static NSString *TheatreShowtimeCellIdentifier = @"TheatreShowtimeCell";

#define kNumberOfCellsToAdd 10
#define INITIAL_RADIUS 10
#define RADIUS_ENHANCE 10
#define MAX_RADIUS 30

@interface SPNFilmDetailsViewController ()
{
    
    NSLayoutConstraint *barC;
    NSDateFormatter *formatter;
    NSString *synopsis;
    NSArray *sortedCastTypes;
    NSDictionary *filmCastAndCrew;

    NSArray *criticReviews;
    NSArray *userReviews;
    
    BOOL shouldDismissCell, shouldShowDateCell, shouldShowLocationCell;
    BOOL onlyWantReviews;
    
    BOOL shouldInitializeKeyboard, shouldInitializeLocationKeyboard;
    NSString *selectedDate;
    SPNFilmReviewType selectedReviewType;
    NSArray *movieStills;
    NSString *trailerURL;
    UIButton *locationBtn, *dateBtn;
    NSString *posterURL;
    
    NSString *userReview, *criticReviewLink;
    NSDateFormatter *datePickerFormatter;
    
    NSInteger cellsToDisplay;
    SPNFilmDetailsType selectedFilmDetailsType;
   
    CGFloat globalUserRating;
    
    NSString *averageUserScoring;
    
    BOOL shouldFetchThearesNearby, shouldFetchSavedLocationTheatres;
    
    NSInteger radiusAmplifier;
    SPNFilmSectionType selectedFilmSection;
    
    NSInteger selectedDateForPicker;
    
    NSDictionary *showTicketingInformation;
}

@end

@implementation SPNFilmDetailsViewController

-(SPNTheatreShowtimeCell *)prototypeShowtimeCell
{
    if (!_prototypeShowtimeCell) {
        _prototypeShowtimeCell = (SPNTheatreShowtimeCell*)[self.filmDetailsTableView dequeueReusableCellWithIdentifier:TheatreShowtimeCellIdentifier];
    }
    return _prototypeShowtimeCell;
}
-(SPNSynopsisCell *)prototypeCastCell
{
    if (!_prototypeCastCell) {
        _prototypeCastCell = (SPNSynopsisCell*)[self.filmDetailsTableView dequeueReusableCellWithIdentifier:SynopsisCellIdentifier];
    }
    return _prototypeCastCell;
}

-(SPNCriticReviewCell *)prototypeCriticCell
{
    if (!_prototypeCriticCell) {
        _prototypeCriticCell = (SPNCriticReviewCell*)[self.filmDetailsTableView dequeueReusableCellWithIdentifier:CriticReviewCellIdentifier];
    }
    return _prototypeCriticCell;
}

-(SPNFilmReviewCell *)prototypeReviewCell
{
    if (!_prototypeReviewCell) {
        _prototypeReviewCell = [self.filmDetailsTableView dequeueReusableCellWithIdentifier:ReviewCellIdentifier];
    }
    return _prototypeReviewCell;
}
-(CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
#ifdef __IPHONE_8_0
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [_locationManager requestWhenInUseAuthorization];
        }
#endif
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    return _locationManager;
}

-(CLGeocoder *)geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];

    /*
    if ([SPNUser userLoggedIn])  {
        NSString *username = [NSString stringWithFormat:@"%@", [SPNUser getCurrentUserId]];
        NSArray *movieIdList = [[LCPDatabaseHelper sharedInstance] getMovieIdsForUser:username];
        NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
        if ([movieIdList indexOfObject:movieId] != NSNotFound) {
            [self.queueItButton setTitle:@"Favorite" forState:UIControlStateNormal];
            [self.heartIconImageView setImage:[UIImage imageNamed:@"heartFill"]];
        }
        else {
            [self.queueItButton setTitle:@"Like It" forState:UIControlStateNormal];
            [self.heartIconImageView setImage:[UIImage imageNamed:@"heartIcon"]];

        }
    }
     */
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;

    UINib* reviewCell = [UINib nibWithNibName:@"UserReviewCell" bundle:nil];
    [self.filmDetailsTableView registerNib:reviewCell
                    forCellReuseIdentifier:ReviewCellIdentifier];
    
    self.screenName = @"Film Details";
    selectedDateForPicker = 0;
    selectedFilmSection = SPNFilmSectionTypeDetails;
    radiusAmplifier = 0;
    self.zipcodePicker = [UITextField new];
    self.datePicker = [UITextField new];
    
    datePickerFormatter = [[NSDateFormatter alloc] init];
    [datePickerFormatter setDateStyle:NSDateFormatterFullStyle];
    [datePickerFormatter setTimeStyle:NSDateFormatterNoStyle];
    [datePickerFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    
    formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [formatter setDateFormat:@"EEEE \r\n MMMM d"];
    // Do any additional setup after loading the view.
    shouldDismissCell = NO;
    shouldShowDateCell = NO;
    shouldShowLocationCell = NO;
    shouldInitializeLocationKeyboard = YES;
    
    [self customizeUI];
    [self assignActionsToButtons];
    
    [self initializeMovieData:self.filmInformation];
 
    shouldInitializeKeyboard = YES;
    
    cellsToDisplay = kNumberOfCellsToAdd;
    
    selectedFilmDetailsType = SPNFilmDetailsTypeCastAndCrew;
    
    filmCastAndCrew = [SPNCinelifeQueries getCastAndCrewForMovie:self.filmInformation];
    sortedCastTypes = [[filmCastAndCrew allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    selectedReviewType = SPNFilmReviewTypeUserReviews;
    
    [self.filmDetailsTableView setTableFooterView:[UIView new]];
    
    averageUserScoring = @"No votes";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshShowtimes:)
                                                 name:@"refreshFilmShowtimes"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addedMovieToFavorites:)
                                                 name:kNotificationAddedMovieToFavorites
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removedMovieFromFavorites:)
                                                 name:kNotificationRemovedMovieFromFavorites
                                               object:nil];
}

-(void)addedMovieToFavorites:(NSNotification*)notification
{
    NSMutableDictionary *newFilm = [NSMutableDictionary dictionaryWithDictionary:self.filmInformation];
    [newFilm setObject:@1 forKey:@"is_favorite"];
    self.filmInformation = newFilm;
    [self.heartIconImageView setImage:[UIImage imageNamed:@"heartFill"]];
    
}


-(void)removedMovieFromFavorites:(NSNotification*)notification
{
    [self.heartIconImageView setImage:[UIImage imageNamed:@"heartIcon"]];
    
    NSMutableDictionary *newFilm = [NSMutableDictionary dictionaryWithDictionary:self.filmInformation];
    [newFilm setObject:@0 forKey:@"is_favorite"];
    self.filmInformation = newFilm;
}

- (void)didReceiveMemowryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}

#pragma mark UI customization
-(void)customizeUI
{
   [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    
    [self.genreLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.userScoreLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.ratingLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.ratingLabel.layer setBorderColor:[UIColor whiteColor].CGColor];

    [self.shareButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.castButton.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [self.reviewsButton.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    [self.showtimesButton.titleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];

    [self.filmDetailsTableView setTableFooterView:[UIView new]];
    
    
}

-(void)assignActionsToButtons
{
    [self.viewTrailerButton addTarget:self
                               action:@selector(showTrailers)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [self.queueItButton addTarget:self
                           action:@selector(queueItButtonPressed:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.detailsButton addTarget:self
                           action:@selector(moveBar:)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.photosButton addTarget:self
                          action:@selector(moveBar:)
                forControlEvents:UIControlEventTouchUpInside];
    
    [self.viewTrailerButton addTarget:self
                               action:@selector(moveBar:)
                     forControlEvents:UIControlEventTouchUpInside];

    [self.castButton addTarget:self
                           action:@selector(changeFilmSelection:)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.showtimesButton addTarget:self
                          action:@selector(changeFilmSelection:)
                forControlEvents:UIControlEventTouchUpInside];
    [self.reviewsButton addTarget:self
                               action:@selector(changeFilmSelection:)
                     forControlEvents:UIControlEventTouchUpInside];
}

-(void)moveBar:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (btn == self.photosButton) {
        selectedFilmSection = SPNFilmSectionTypePhotos;
        [self.filmDetailsTableView setContentOffset:CGPointZero animated:YES];
        [self showMovieStills:sender];
    }
    else if (btn == self.detailsButton) {
        selectedFilmSection = SPNFilmSectionTypeDetails;
        [self returnToDetails:sender];
    }
    
    else {
        
    }
}

-(void)changeFilmSelection:(id)sender
{
    [self.castButton setBackgroundColor:[UIColor spn_darkBrownColor]];
    [self.showtimesButton setBackgroundColor:[UIColor spn_darkBrownColor]];
    [self.reviewsButton setBackgroundColor:[UIColor spn_darkBrownColor]];
    
    [self.castButton setTitleColor:[UIColor darkTextColor]
                             forState:UIControlStateNormal];
    [self.showtimesButton setTitleColor:[UIColor darkTextColor]
                             forState:UIControlStateNormal];
    [self.reviewsButton setTitleColor:[UIColor darkTextColor]
                             forState:UIControlStateNormal];
    
    UIButton *btn = (UIButton*)sender;
    [btn setBackgroundColor:[UIColor spn_aquaColor]];
    
    [btn setTitleColor:[UIColor whiteColor]
              forState:UIControlStateNormal];
    
      [self.filmDetailsTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    if (btn == self.castButton) {
        selectedFilmDetailsType = SPNFilmDetailsTypeCastAndCrew;
        [self.filmDetailsTableView reloadData];
    }
    else if (btn == self.showtimesButton) {
        if ([self.theatreShowtimesInRegion count] < 1) {
            if (!self.lastKnownLocation || [self.lastKnownLocation length] < 1) {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSString* storedZipcode = [prefs objectForKey:@"zip"];
                if (storedZipcode) {
                    [self getLocationWithUserInput:storedZipcode];
                    shouldFetchThearesNearby = NO;
                }
                else {
                    shouldFetchThearesNearby = YES;
                }
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                [self.locationManager startUpdatingLocation];
                selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
                [self.filmDetailsTableView reloadData];
            }
            else {
                [self getLocationWithUserInput:self.lastKnownLocation];
            }
        }
        else {
            selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
            [self.filmDetailsTableView reloadData];
        }
    }
    else {
        [self.filmDetailsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self getFilmReviews];
    }
}

-(void)returnToDetails:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    UIView *header = self.filmDetailsTableView.tableHeaderView;
    CGRect headerFrame           = header.frame;
    headerFrame.size.height      = 323;
    header.frame            = headerFrame;
    [self.filmDetailsTableView setTableHeaderView:header];
    
    [self.filmDetailsTableView setScrollEnabled:YES];
    [self.movieStillsCollectionView setHidden:YES];
    
    [self.movingBarLeadConstraint setConstant:btn.frame.origin.x];
    [UIView animateWithDuration:.15 animations:^{
        [self.barContainerView layoutIfNeeded];
    }];


}

-(void)showFullPoster
{
    if (posterURL) {
        [self performSegueWithIdentifier:@"FullPoster" sender:self];
    }
    
}

-(void)showMovieStills:(id)sender
{
    
    UIView *header = self.filmDetailsTableView.tableHeaderView;
    CGRect headerFrame           = header.frame;
    headerFrame.size.height      = self.view.frame.size.height;
    header.frame            = headerFrame;
    [self.filmDetailsTableView setTableHeaderView:header];
    
    
    [self.filmDetailsTableView setScrollEnabled:NO];
 
    [self.movieStillsCollectionView reloadData];
    [self.movieStillsCollectionView setHidden:NO];
    [self performSelector:@selector(shiftBar:) withObject:sender afterDelay:0.05];
}

-(void)showStills
{
  //  [self.movieStillsCollectionView reloadData];
    
}

-(void)shiftBar:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    [self.movingBarLeadConstraint setConstant:btn.frame.origin.x];
    [UIView animateWithDuration:.15 animations:^{
        [self.barContainerView layoutIfNeeded];
    }];
}

#pragma mark Initialize Movie Data
-(void)initializeMovieData:(NSDictionary*)film
{
    
    if ([film objectForKey:@"is_favorite"] && [[film objectForKey:@"is_favorite"] integerValue] > 0) {
        [self.heartIconImageView setImage:[UIImage imageNamed:@"heartFill"]];
    }
    NSString *movieTitle =[film objectForKey:@"name"] ? [film objectForKey:@"name"] : [film objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    synopsis = [film objectForKey:@"synopsis"];
    if ([synopsis length] < 1) {
        synopsis = @"No synopsis available";
    }
    NSString *urlImage;
    NSDictionary *posters = [film objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
 
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];

    self.posterImageView.imageURL = [NSURL URLWithString:urlImage];
    if (urlImage) {
        posterURL = urlImage;
        UITapGestureRecognizer *tapToFullPoster = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullPoster)];
        [self.fullPosterView addGestureRecognizer:tapToFullPoster];
    }

    id genres =[film objectForKey:@"genres"];
    
    if ([genres isKindOfClass:[NSArray class]]) {
        self.genreLabel.text = [genres componentsJoinedByString:@" | "];
    }
    else if ([genres isKindOfClass:[NSString class]]) {
        self.genreLabel.text = genres;
    }
    else {
        self.genreLabel.text = @"No genre information available";
    }
    NSString *rating = [film objectForKey:@"rating"];
    
    if ([rating length] > 0) {
        self.ratingLabel.text = [NSString stringWithFormat:@"%@", rating];
    }
    else {
        self.ratingLabel.text = @"";
    }
    
    NSString *runTime = [film objectForKey:@"runtime"];
    if (!runTime || [runTime length] < 1) {
        runTime = @"0";
    }
    NSString *duration = [NSString stringWithFormat:@"%@ min", runTime];
    self.durationLabel.text = duration;
    
    NSDictionary *metacritic = [film objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        globalUserRating = [userScore floatValue];
        [self.userScoreLabel setText:[NSString stringWithFormat:@"%g", globalUserRating]];
    }
    else {
        [self.userScoreLabel setText:@"No votes"];
    }
    
    movieStills = nil;
    NSDictionary *photos = [film objectForKey:@"photos"];
    
    NSArray *stills = [photos objectForKey:@"high"];
    [self.photosButton setEnabled:NO];

    dispatch_async(kBgQueue, ^{
        NSMutableArray *validStills = [NSMutableArray arrayWithArray:stills];
        for (NSString* still in stills)
        {
            NSURL *stillURL = [NSURL URLWithString:still];
            if (![SPNCinelifeQueries isValidURL:stillURL]) {
                [validStills removeObject:still];
            }
        }
        movieStills = validStills;
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([movieStills count] > 0) {
                [self.photosButton setEnabled:YES];
            }
        });
    });
    
    
    
    NSDictionary *trailers = [film objectForKey:@"trailer"];
    trailerURL = [trailers objectForKey:@"high"];

    if (!trailerURL) {
        [self.viewTrailerButton setEnabled:NO];
    }
    else if (![SPNCinelifeQueries isValidURL:[NSURL URLWithString:trailerURL]]) {
          [self.viewTrailerButton setEnabled:NO];
    }

    
    if (self.shouldBeReviewable) {
        [self.showtimesButton setEnabled:NO];
    }
}


#pragma mark Table View functions

-(void)configureCriticPostCell:(SPNCriticReviewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;
{
    NSDictionary *filmReview = [criticReviews  objectAtIndex:indexPath.row];
    [cell.criticSnippetLabel setText:[filmReview objectForKey:@"quote"]];
    
    NSDictionary *critic = [filmReview objectForKey:@"critic"];
    if (!critic) {
        critic = [filmReview objectForKey:@"publication"];
        [cell.criticSiteLabel setText:[critic objectForKey:@"name"]];
    }
    NSString *criticName = [critic objectForKey:@"name"];
    [cell.criticNameLabel setText:criticName];
    NSString *score = [filmReview objectForKey:@"score"];
    [cell.criticScoreLabel assignMetaScore:score];

}

-(void)configureCastCell:(SPNSynopsisCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    [cell.titleLabel setPreferredMaxLayoutWidth:width - 30];
    [cell.titleLabel setFont:[UIFont spn_NeutraSmallMedium]];
    
    if (indexPath.section < 1) {
        [cell.titleLabel setText:synopsis];
    }
    else {
        NSArray *castMembers = [filmCastAndCrew objectForKey:[sortedCastTypes objectAtIndex:indexPath.row]];
        NSRange boldFontRange = NSMakeRange(0, [[sortedCastTypes objectAtIndex:indexPath.row] length]+1);
        
        NSString *castList = [NSString stringWithFormat:@"%@: %@", [[sortedCastTypes objectAtIndex:indexPath.row] capitalizedString], [castMembers componentsJoinedByString:@", "]];
        NSMutableAttributedString *castWithName = [[NSMutableAttributedString alloc] initWithString:castList];
        
        [castWithName beginEditing];
        [castWithName addAttribute:NSFontAttributeName
                             value:[UIFont spn_NeutraBoldSmallMedium]
                             range:boldFontRange];
        [castWithName endEditing];
        [cell.titleLabel setAttributedText:castWithName];
    }

}

-(void)configurePostCell:(SPNFilmReviewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;
{
    NSDictionary *filmReview = [userReviews objectAtIndex:indexPath.row];
    [cell initializeReviewCell:filmReview];
}

-(CGFloat)configureShowtimesCell:(SPNTheatreShowtimeCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;
{
   
    NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
    NSArray *showList = [self.showtimeInformation objectForKey:show];
    NSDictionary *theatreInformation = [showList objectAtIndex:indexPath.row];
    
    [cell initializeTheatreCellContents:theatreInformation
                           forIndexPath:indexPath];
    
   
    NSString *chosenDate = [datePickerFormatter stringFromDate:show];
    NSArray *shows = [cell getShowsForThisTheatre:indexPath
                                                   forSchedule:theatreInformation
                                                       forDate:chosenDate];
    
    [cell setShowForCell:shows];
    CGFloat maxHeight = 0;
    for (UIView *view  in [cell.showtimesContainerView subviews]) {
        maxHeight = MAX(maxHeight, view.frame.origin.y+20);
    }
    return maxHeight;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch (selectedFilmDetailsType) {
        case SPNFilmDetailsTypeCastAndCrew:
            return 2;
            break;
        case SPNFilmDetailsTypeReviews:
            return 3; // Synopsis and cast
            break;
        default:
            if (cellsToDisplay < [self.theatreShowtimesInRegion count]) {
                return 3;
            }
            else if (radiusAmplifier > MAX_RADIUS) {
                return 2;
            }
            else if ([self.theatreShowtimesInRegion count] > 0){
                return 3;
            }
            else {
                return 2;
            }
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *PickerCellIdentifier = @"PickerCell";
    static NSString *LocationCellIdentifier = @"LocationCell";
    static NSString *DateCellIdentifier = @"DatePickerCell";
    static NSString *DismissCellIdentifier = @"DismissCell";
    static NSString *ReviewOptionCellIdentifier =@"ReviewOptionCell";
    static NSString *ReviewFilmCellIdentifier =@"ReviewFilmCell";
    static NSString *SearchMoreCellIdentifier = @"SearchMoreCell";
    static NSString *ShowCellIdentifier = @"MoreCell";
    static NSString *ShowMoreReviewsCellIdentifier = @"MoreReviewsCell";
    
    if (selectedFilmDetailsType == SPNFilmDetailsTypeCastAndCrew) {
        SPNSynopsisCell *cell = (SPNSynopsisCell*) [tableView dequeueReusableCellWithIdentifier:SynopsisCellIdentifier];
        [cell.titleLabel setFont:[UIFont spn_NeutraSmallMedium]];
        
        if (indexPath.section < 1) {
            [cell.titleLabel setText:synopsis];
        }
        else {
            NSArray *castMembers = [filmCastAndCrew objectForKey:[sortedCastTypes objectAtIndex:indexPath.row]];
            NSRange boldFontRange = NSMakeRange(0, [[sortedCastTypes objectAtIndex:indexPath.row] length]+1);
            
            NSString *castList = [NSString stringWithFormat:@"%@: %@", [[sortedCastTypes objectAtIndex:indexPath.row] capitalizedString], [castMembers componentsJoinedByString:@", "]];
            NSMutableAttributedString *castWithName = [[NSMutableAttributedString alloc] initWithString:castList];

            [castWithName beginEditing];
            [castWithName addAttribute:NSFontAttributeName
                                 value:[UIFont spn_NeutraBoldSmallMedium]
                                 range:boldFontRange];
            [castWithName endEditing];
            [cell.titleLabel setAttributedText:castWithName];
        }
        return cell;
    }
    else if (selectedFilmDetailsType == SPNFilmDetailsTypeShowtimes) {
        
        if (indexPath.section < 1) {
            if (indexPath.row < 1) {
                SPNPickerCell *pickCell = [tableView dequeueReusableCellWithIdentifier:PickerCellIdentifier];
              
                [pickCell.changeLocationBtn addTarget:self
                                               action:@selector(showZipPicker:)
                                     forControlEvents:UIControlEventTouchUpInside];
                
                locationBtn = pickCell.changeLocationBtn;
                
              
                if ([self.zipcodePicker.text length] > 0) {
                    [pickCell.changeLocationBtn setTitle:self.zipcodePicker.text
                                                forState:UIControlStateNormal];
                }
                else if (self.lastKnownLocation && [self.lastKnownLocation length] > 1) {
                    [pickCell.changeLocationBtn setTitle:self.lastKnownLocation
                                                forState:UIControlStateNormal];
                }
                               [pickCell.dateLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
                [pickCell.zipLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
                
                [pickCell.changeDateBtn.titleLabel setFont:[UIFont spn_NeutraSmallMedium]];
                [pickCell.changeLocationBtn.titleLabel setFont:[UIFont spn_NeutraSmallMedium]];

                return pickCell;
            }
            else if (indexPath.row < 2) {
                SPNSearchOptionCell *searchCell = [tableView dequeueReusableCellWithIdentifier:LocationCellIdentifier];
                [searchCell.whereLabel setFont:[UIFont spn_NeutraMedium]];
                
                searchCell.zipcodePicker.delegate = self;
                if (shouldInitializeLocationKeyboard) {
                    shouldInitializeLocationKeyboard = NO;
                    [self initializeLocationKeyboard:searchCell];
                }
                return searchCell;
            }
            else if (indexPath.row < 3) {
                
                SPNPickerCell *datePickCell = [tableView dequeueReusableCellWithIdentifier:DateCellIdentifier];
                [datePickCell.dateLabel setFont:[UIFont spn_NeutraMedium]];
               
                [datePickCell.dateContainerView.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
                self.dateCollectionView = datePickCell.dateCollectionView;

                return datePickCell;
            }

            else {
                SPNDismissCell *dismissCell = [tableView dequeueReusableCellWithIdentifier:DismissCellIdentifier];
                [dismissCell.xLabel setFont:[UIFont spn_NeutraBoldSmallMedium]];
                [dismissCell.messageLabel setFont:[UIFont spn_NeutraSmallMedium]];
                return dismissCell;
            }
        }
        else if (indexPath.section < 2){
            SPNTheatreShowtimeCell *theatreCell = [tableView dequeueReusableCellWithIdentifier:TheatreShowtimeCellIdentifier];
            
            NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
            NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
            NSArray *showList = [self.showtimeInformation objectForKey:show];
            NSDictionary *theatreInformation = [showList objectAtIndex:indexPath.row];
            
            [theatreCell initializeTheatreCellContents:theatreInformation
                                          forIndexPath:indexPath];
          
            NSString *chosenDate = [datePickerFormatter stringFromDate:show];
            
            NSArray *shows = [theatreCell getShowsForThisTheatre:indexPath
                                                     forSchedule:theatreInformation
                                                         forDate:chosenDate];
            
            [theatreCell setShowForCell:shows];
            
            
            for (int i = 0; i < [shows count]; i++)
            {
                UIButton *buyTicketBtn = (UIButton*) [theatreCell viewWithTag:i+100];
                buyTicketBtn.theatreId = [theatreInformation objectForKey:@"id"];
                [buyTicketBtn addTarget:self
                                 action:@selector(buyTicketsDirectly:)
                       forControlEvents:UIControlEventTouchUpInside];
            }
            return theatreCell;
        }
        else {
            if (cellsToDisplay < [self.theatreShowtimesInRegion count]) {
                SPNShowMoreCell *showCell = (SPNShowMoreCell*)[tableView dequeueReusableCellWithIdentifier:ShowCellIdentifier];
                showCell.showMoreButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
                [showCell.showMoreButton.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
                return showCell;
            }
            else {
                SPNShowMoreCell *searchMoreCell = (SPNShowMoreCell*)[tableView dequeueReusableCellWithIdentifier:SearchMoreCellIdentifier];
                searchMoreCell.showMoreButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
                [searchMoreCell.showMoreButton.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
                return searchMoreCell;
            }

        }
    }
    else {
        if (indexPath.section < 1) {
            if (indexPath.row < 1) {
                SPNReviewOptionCell *optionCell = [tableView dequeueReusableCellWithIdentifier:ReviewOptionCellIdentifier];
                [optionCell.userReviewsButton addTarget:self action:@selector(showUserReviews:) forControlEvents:UIControlEventTouchUpInside];
                [optionCell.criticReviewsButton addTarget:self action:@selector(showCriticReviews:) forControlEvents:UIControlEventTouchUpInside];

                [optionCell.criticReviewsButton.layer setBorderWidth:0.5];
                [optionCell.userReviewsButton.layer setBorderWidth:0.5];
                
                
                NSDictionary *cinelifeReviews =  [self.filmInformation objectForKey:@"cinelife_reviews"];
                
                if (cinelifeReviews) {
                    NSString *rating = [cinelifeReviews objectForKey:@"rating"];
                    NSString *totalReviews = [cinelifeReviews objectForKey:@"reviews"];
                    CGFloat score = [rating floatValue];
                    NSString *formattedScore = [NSString stringWithFormat:@"%.1f", score];
                    NSRange userFontRange = NSMakeRange(0, [formattedScore length]);
                    NSMutableAttributedString *userRating = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.1f/5", score]];
                    
                    [userRating beginEditing];
                    [userRating addAttribute:NSFontAttributeName
                                       value:[UIFont boldSystemFontOfSize:14]
                                       range:NSMakeRange(0, [userRating length])];
                    [userRating addAttribute:NSForegroundColorAttributeName
                                       value:[UIColor darkGrayColor]
                                       range:NSMakeRange(0, [userRating length])];
                    
                    [userRating addAttribute:NSFontAttributeName
                                       value:[UIFont boldSystemFontOfSize:17]
                                       range:userFontRange];
                    [userRating addAttribute:NSForegroundColorAttributeName
                                       value:[UIColor darkTextColor]
                                       range:userFontRange];
                    [userRating endEditing];
                    [optionCell.userScoreLabel setAttributedText:userRating];
                    
                    if (totalReviews) {
                        [optionCell.userReviewAmountLabel setText:[NSString stringWithFormat:@"%@ user reviews", totalReviews]];
                    }
                    else {
                        [optionCell.userReviewAmountLabel setText:@"No user reviews yet"];
                    }
                }
                else {
                    [optionCell.userScoreLabel setText:@"No votes"];
                    [optionCell.userReviewAmountLabel setText:@"No user reviews yet"];
                }

                NSDictionary *metacritic = [self.filmInformation objectForKey:@"metacritic"];
                
                if ([metacritic count] >0) {
                    NSInteger criticRatingCount = [[metacritic objectForKey:@"review_count"] integerValue];
                    if (criticRatingCount > 0) {
                        [optionCell.criticReviewAmountLabel setText:[NSString stringWithFormat:@"%ld critic reviews", (long) criticRatingCount]];
                    }
                    else {
                        [optionCell.criticReviewAmountLabel setText:@"No critic reviews yet"];
                    }
                    
                    NSString *metascore = [metacritic objectForKey:@"score"];
                    if (!metascore || [metascore isEqualToString:@"NR"]) {
                        [optionCell.metaScoreWidthConstraint setConstant:0];
                    }
                    else {
                        [optionCell.criticScoreLabel assignMetaScore:metascore];
                    }
                }
                else {
                     [optionCell.criticReviewAmountLabel setText:@"No critic reviews yet"];
                     [optionCell.metaScoreWidthConstraint setConstant:0];
                    
                }
                if (selectedReviewType == SPNFilmReviewTypeUserReviews) {
                    [optionCell.userReviewsButton setBackgroundColor:[UIColor clearColor]];
                    [optionCell.criticReviewsButton setBackgroundColor:[UIColor spn_brownColor]];
                    [optionCell.criticReviewsButton.layer setBorderColor:[UIColor spn_darkBrownColor].CGColor];
                    [optionCell.userReviewsButton.layer setBorderColor:[UIColor clearColor].CGColor];

                }
                else {
                    [optionCell.criticReviewsButton setBackgroundColor:[UIColor clearColor]];
                    [optionCell.userReviewsButton setBackgroundColor:[UIColor spn_brownColor]];
                    [optionCell.userReviewsButton.layer setBorderColor:[UIColor spn_darkBrownColor].CGColor];
                    [optionCell.criticReviewsButton.layer setBorderColor:[UIColor clearColor].CGColor];
                }
                
                return optionCell;
            }
            else {
                SPNAddRevieCell *cell = [tableView dequeueReusableCellWithIdentifier:ReviewFilmCellIdentifier];
                [cell.reviewButton addTarget:self action:@selector(reviewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.reviewButton.layer.shadowColor = [UIColor spn_buttonShadowColor].CGColor;
                return cell;
            }
        }
        else if (indexPath.section < 2) {
            if (selectedReviewType == SPNFilmReviewTypeUserReviews) {
                SPNFilmReviewCell *reviewCell = [tableView dequeueReusableCellWithIdentifier:ReviewCellIdentifier];
                NSDictionary *filmReview = [userReviews objectAtIndex:indexPath.row];
                [reviewCell initializeReviewCell:filmReview];
                return reviewCell;
            }
            else {
                SPNCriticReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:CriticReviewCellIdentifier];
                NSDictionary *criticReview = [criticReviews objectAtIndex:indexPath.row];
                
                [cell.criticSnippetLabel setText:[criticReview objectForKey:@"quote"]];
                NSDictionary *critic = [criticReview objectForKey:@"critic"];
                if (!critic) {
                    critic = [criticReview objectForKey:@"publication"];
                    [cell.criticSiteLabel setText:[critic objectForKey:@"name"]];
                    
                }
                NSString *criticName = [critic objectForKey:@"name"];
                [cell.criticNameLabel setText:criticName];
               
                NSString *score = [criticReview objectForKey:@"score"];
                [cell.criticScoreLabel assignMetaScore:score];
                [cell.readFullReviewButton setBackgroundColor:[UIColor spn_buttonColor]];
                cell.readFullReviewButton.layer.shadowColor = [UIColor spn_buttonShadowColor].CGColor;
                [cell.readFullReviewButton setTag:indexPath.row];
                [cell.readFullReviewButton addTarget:self action:@selector(readFullReview:) forControlEvents:UIControlEventTouchUpInside];
                return cell;
            }
        }
        else {
            SPNAddRevieCell *showCell = (SPNAddRevieCell*)[tableView dequeueReusableCellWithIdentifier:ShowMoreReviewsCellIdentifier];
            [showCell.reviewButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
            [showCell.reviewButton addTarget:self action:@selector(loadMetaReviews:) forControlEvents:UIControlEventTouchUpInside];
            return showCell;
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (selectedFilmDetailsType) {
        case SPNFilmDetailsTypeCastAndCrew:
            if (section < 1) {
                return 1;
            }
            else {
                return [filmCastAndCrew count];
            }
            break;
        case SPNFilmDetailsTypeReviews:
            if (section < 1) {
                if (!self.shouldBeReviewable) {
                    return selectedReviewType == SPNFilmReviewTypeUserReviews ? 2 : 1;
                }
                else {
                    return 1;
                }
            }
            else if (section < 2){
                if (selectedReviewType == SPNFilmReviewTypeUserReviews) {
                    return [userReviews count];
                }
                else {
                      return [criticReviews count];
                }
            }
            else {
                if (selectedReviewType == SPNFilmReviewTypeUserReviews) {
                    return 0;
                }
                NSDictionary *metacritic = [self.filmInformation objectForKey:@"metacritic"];
                if ([metacritic count] > 0) {
                    NSInteger totalReviews = [[metacritic objectForKey:@"review_count"] integerValue];
                    if (totalReviews > 3) {
                        return 1;
                    }
                }
                return 0;
            }
            break;
        default:
            
            if (section < 1) {
                return 4;
            }
            else if (section < 2){
                NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
                if ([showArray count] < 1) {
                    return 0;
                }
                NSDate *show = [showArray objectAtIndex:selectedDateForPicker];
                NSArray *showList = [self.showtimeInformation objectForKey:show];
                return MIN(cellsToDisplay, [showList count]);
            }
            else {
                return 1;
            }
            break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (selectedFilmDetailsType) {
        case SPNFilmDetailsTypeShowtimes:
        case SPNFilmDetailsTypeReviews:
            return 0;
            break;
        default:
            return 30;
            break;
    }
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (selectedFilmDetailsType) {
        case SPNFilmDetailsTypeCastAndCrew:
            if (section < 1) {
                return @"Synopsis";
            }
            else {
                return @"Cast";
            }
            break;
        case SPNFilmDetailsTypeShowtimes:
            return @"";
             break;
        default:
            return @"Reviews";
            break;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
    
}
/*
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedFilmDetailsType == SPNFilmDetailsTypeShowtimes) {
        if (indexPath.section < 1) {
            if (indexPath.row < 1) {
                return 45;
            }
            else if (indexPath.row == 1) {
                return shouldShowLocationCell ? 62: 0;
            }
            else if (indexPath.row == 2) {
                return 52;
            }
            else {
                return !shouldDismissCell ? 45 : 0;
            }
        }
        else if (indexPath.section < 2) {
            CGFloat height = [self configureShowtimesCell:self.prototypeShowtimeCell
                                        forRowAtIndexPath:indexPath];
            [self.prototypeShowtimeCell layoutIfNeeded];
            CGSize size = [self.prototypeShowtimeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            return size.height+height+1;
        }
        else {
            return kShowMoreCellHeight;
        }
    }
    else if (selectedFilmDetailsType == SPNFilmDetailsTypeReviews) {
        
        if (indexPath.section < 1) {
            if (indexPath.row < 1) {
                return 50;
            }
            else {
                return 91;
            }
        }
        else if (indexPath.section < 2){
            
            if (selectedReviewType == SPNFilmReviewTypeUserReviews) {
                [self configurePostCell:self.prototypeReviewCell forRowAtIndexPath:indexPath];
                CGSize size = [self.prototypeReviewCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return size.height+1;
            }
            else {
                [self configureCriticPostCell:self.prototypeCriticCell forRowAtIndexPath:indexPath];
                CGSize size = [self.prototypeCriticCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return size.height+1;
            }
        }
        else {
            return 64;
        }
    }
    else {
        [self configureCastCell:self.prototypeCastCell forRowAtIndexPath:indexPath];
         CGSize size = [self.prototypeCastCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedFilmDetailsType == SPNFilmDetailsTypeShowtimes) {
        if (indexPath.row == 3) {
                shouldDismissCell = YES;
            [tableView beginUpdates];
            [tableView endUpdates];
        }
        if (indexPath.section == 2) {
            if (cellsToDisplay < [self.theatreShowtimesInRegion count]) {
                NSUInteger i, totalNumberOfItems = [self.theatreShowtimesInRegion count];
                NSUInteger bumberOfItemsToDisplay = MIN(totalNumberOfItems, cellsToDisplay + kNumberOfCellsToAdd);
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                
                for (i=cellsToDisplay; i<bumberOfItemsToDisplay; i++)
                {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                }
                
                cellsToDisplay = bumberOfItemsToDisplay;
                
                [tableView beginUpdates];
                [tableView insertRowsAtIndexPaths:indexPaths
                                 withRowAnimation:UITableViewRowAnimationTop];
                
                if (cellsToDisplay == [self.theatreShowtimesInRegion count] && radiusAmplifier > MAX_RADIUS) {
                    [tableView deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationTop];
                }
                [tableView endUpdates];
            }
            else {
                radiusAmplifier += RADIUS_ENHANCE;
                [self getLocationWithUserInput:self.zipcodePicker.text];
            }
        }
    }
    
}

#pragma mark Custom keyboard functions
-(void)initializeLocationKeyboard:(SPNSearchOptionCell*)cell
{
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleBlack;
    keyboardDoneButtonView.translucent = YES;
    keyboardDoneButtonView.tintColor = nil;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    
    UIBarButtonItem* getZipButton = [[UIBarButtonItem alloc] initWithTitle:@"Nearby"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(activateGPSLocation:)];
    
    UIBarButtonItem* useSavedLocButton = [[UIBarButtonItem alloc] initWithTitle:@"Saved"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(savedLocation:)];
    
    UIBarButtonItem* cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(cancelClicked:)];
    
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, getZipButton, useSavedLocButton, cancelButton1, nil]];
    
    cell.zipcodePicker.inputAccessoryView = keyboardDoneButtonView;
    [cell.zipcodePicker setKeyboardType:UIKeyboardTypeDefault];
    
       if (self.lastKnownLocation) {
        [cell.zipcodePicker setText:self.lastKnownLocation];
    }
    
    self.zipcodePicker = cell.zipcodePicker;
    self.zipcodePicker.delegate = self;
}

-(void)initializeKeyboard:(SPNSearchOptionCell*)cell
{
      // Date picker: minimum date today, max date 90 days from now (as per TMS API)
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    
    [datePicker setDate:[NSDate date]];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setMinimumDate:[NSDate date]];

    NSDateComponents* addOneMonthComponents = [NSDateComponents new] ;
    addOneMonthComponents.month = 3 ;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute) fromDate:[NSDate date]] ;
    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents] ;
    
    NSDate* threeMonthsFromNowWithoutSeconds = [calendar dateByAddingComponents:addOneMonthComponents toDate:nowWithoutSeconds options:0] ;
    [datePicker setMaximumDate:threeMonthsFromNowWithoutSeconds];
    
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
  /*  UIPickerView *newDatePicker = [UIPickerView new];

    [newDatePicker setDelegate:self];
    [newDatePicker setDataSource:self];*/
    [cell.datePicker setInputView:datePicker];
    UIToolbar* keyboardDoneButtonViewDate = [[UIToolbar alloc] init];
    keyboardDoneButtonViewDate.barStyle = UIBarStyleBlack;
    keyboardDoneButtonViewDate.translucent = YES;
    keyboardDoneButtonViewDate.tintColor = nil;
    [keyboardDoneButtonViewDate sizeToFit];
    
    UIBarButtonItem* doneDateButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(doneClicked:)];
    
    UIBarButtonItem* cancelDateButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(cancelClicked:)];
    [keyboardDoneButtonViewDate setItems:[NSArray arrayWithObjects:doneDateButton, cancelDateButton, nil]];
    
    // Plug the keyboardDoneButtonView into the text field...
  
    
    __weak  NSString *formattedDateString = [datePickerFormatter stringFromDate:[NSDate date]];

    cell.datePicker.inputAccessoryView = keyboardDoneButtonViewDate;
    [cell.datePicker setText:formattedDateString];

    self.datePicker = cell.datePicker;
    self.datePicker.delegate = self;
}

-(void)showDatePicker:(id)sender
{
    shouldShowDateCell = !shouldShowDateCell;
    shouldShowLocationCell = NO;
    [self.filmDetailsTableView reloadData];
}

-(void)showZipPicker:(id)sender
{
    shouldShowLocationCell = !shouldShowLocationCell;
    shouldShowDateCell = NO;
    [self.filmDetailsTableView reloadData];
}

-(void)doneClicked:(id)sender
{
    [self.datePicker resignFirstResponder];
    [self.zipcodePicker resignFirstResponder];
    [dateBtn setTitle:self.datePicker.text
             forState:UIControlStateNormal];
    
   
    /*
     
    
    if (!self.currentUserLocation) {
        [locationBtn setTitle:self.zipcodePicker.text
                     forState:UIControlStateNormal];
        shouldFetchThearesNearby = NO;
        shouldFetchSavedLocationTheatres = NO;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [self.locationManager startUpdatingLocation];
    }
     */
    [self getLocationWithUserInput:self.zipcodePicker.text];
    
}

-(void)activateGPSLocation:(id)sender
{
    shouldFetchThearesNearby = YES;
    shouldFetchSavedLocationTheatres = NO;
    [self.zipcodePicker resignFirstResponder];
    [self.locationManager startUpdatingLocation];
}

-(void)savedLocation:(id)sender
{
  
    [self.zipcodePicker resignFirstResponder];
    if (!self.currentUserLocation) {
        shouldFetchThearesNearby = NO;
        shouldFetchSavedLocationTheatres = YES;
        [self.locationManager startUpdatingLocation];
    }
    else {
        [self getLastSavedLocation];
    }
}

-(void)cancelClicked:(id)sender
{
    [self.zipcodePicker resignFirstResponder];
    [self.datePicker resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    [self getLocationWithUserInput:self.zipcodePicker.text];
}

-(void)updateTextField:(id)sender
{
    if([self.datePicker isFirstResponder]) {
        UIDatePicker *picker = (UIDatePicker*)self.datePicker.inputView;
        self.datePicker.text = [NSString stringWithFormat:@"%@",[datePickerFormatter stringFromDate:picker.date]];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.filmDetailsTableView setContentOffset:CGPointZero animated:YES];
    CGRect frame = self.view.frame;
    frame.origin.y = frame.origin.y-150;
    [self.view setFrame:frame];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frame = self.view.frame;
    frame.origin.y = frame.origin.y+150;
    [self.view setFrame:frame];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.zipcodePicker) {
        [textField resignFirstResponder];
        [self getLocationWithUserInput:textField.text];
        return NO;
    }
    return YES;
}
#pragma mark - Location Manager functions

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *userLocation = [locations lastObject];
    self.currentUserLocation = userLocation;
    [self.locationManager stopUpdatingLocation];
    
    if (shouldFetchThearesNearby) {
        [self.geoCoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark *place = [placemarks firstObject];
            
            NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
            if (zip) {
                [self.zipcodePicker setText:zip];
                [self setCityAndStateToSearchBar:place.addressDictionary];
            }

            [[SPNCinelifeQueries sharedInstance] getMovieInformation:[self.filmInformation objectForKey:@"id"]
                                                          forZipcode:nil
                                                       orCoordinates:userLocation
                                                          withRadius:INITIAL_RADIUS+radiusAmplifier withCredentials:[SPNUser getCurrentUserId]];
        }];
    }
    else if (shouldFetchSavedLocationTheatres) {
        [self getLastSavedLocation];
/*        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* storedZipcode = [prefs objectForKey:@"zip"];
        [self getLocationWithUserInput:storedZipcode];
 */
    }
    else {
        [self getLocationWithUserInput:self.zipcodePicker.text];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if (shouldFetchSavedLocationTheatres) {
        [self getLastSavedLocation];
    }
    else {
        [self getLocationWithUserInput:self.zipcodePicker.text];
    }
}

-(void)refreshShowtimes:(id)sender
{
    selectedDateForPicker = 0;
    [self getLastLocation:sender];
}
-(void)getLastLocation:(id)sender
{
    NSString *address = self.zipcodePicker.text;
    if ([address length] < 1) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        address = [prefs objectForKey:@"zip"];
    }
    if  (address) {
        [self getLocationWithUserInput:address];
    }
}
-(void)getLocationWithUserInput:(NSString*)address
{
    shouldShowLocationCell = NO;
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self.geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if(placemarks.count > 0) {
            CLPlacemark *usPlacemark = [placemarks firstObject];
            for (CLPlacemark* placemark in placemarks) {
                if ([placemark.ISOcountryCode isEqualToString:@"US"]) {
                    usPlacemark = placemark;
                }
            }
            [self.geoCoder reverseGeocodeLocation:usPlacemark.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                if (zip) {
                
                    [self.zipcodePicker setText:zip];
                    [locationBtn setTitle:zip forState:UIControlStateNormal];
                    NSCalendar* calendar =[NSCalendar currentCalendar];
                    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
                    
                    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute)
                                                                                fromDate:[NSDate date]];
                    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents];
                    
                    NSDateComponents* addDayComponents = [NSDateComponents new] ;
                    addDayComponents.day = selectedDateForPicker;
                    NSDate* newDateWithDays = [calendar dateByAddingComponents:addDayComponents
                                                                        toDate:nowWithoutSeconds
                                                                       options:0];
                    [self getFilmShowtimesWithDate:newDateWithDays];
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                }
            }];
        }
        else if (error.domain == kCLErrorDomain) {
            switch (error.code)
            {
                case kCLErrorDenied:
                    self.zipcodePicker.text = @"Location Services Denied by User";
                    break;
                case kCLErrorNetwork:
                    self.zipcodePicker.text = @"No Network";
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    self.zipcodePicker.text = @"Could not get your location";
                    break;
                default:
                    self.zipcodePicker.text = error.localizedDescription;
                    break;
            }
            selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
            [self.filmDetailsTableView reloadData];
            [self.dateCollectionView reloadData];
             [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
        else {
            selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
            [self.filmDetailsTableView reloadData];
            [self.dateCollectionView reloadData];

             [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
        
    }];
}

-(void)setCityAndStateToSearchBar:(NSDictionary*)placeDictionary
{
    NSString *country = [placeDictionary objectForKey:@"CountryCode"];
    NSString *zipcode = [placeDictionary objectForKey:@"ZIP"];
    
    if ([country isEqualToString:@"US"]) {
        NSString *city = [placeDictionary objectForKey:@"City"];
        NSString *state = [placeDictionary objectForKey:@"State"];
        NSString *location = [NSString stringWithFormat:@"%@, %@ (%@)", city, state, zipcode];
        [self.zipcodePicker setText:location];
        [locationBtn setTitle:location forState:UIControlStateNormal];
    }
    else {
        [self.zipcodePicker setText:zipcode];
        [locationBtn setTitle:zipcode forState:UIControlStateNormal];

    }
}

#pragma mark Film information and queries
-(void)getFilmShowtimesWithDate:(NSDate*)date
{

    NSNumberFormatter *possibleZip = [[NSNumberFormatter alloc] init];
    [possibleZip setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *zip = [possibleZip numberFromString:self.zipcodePicker.text];
    if (zip) {
        NSString *zipcodeString = self.zipcodePicker.text;
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        NSString *movieId = [self.filmInformation objectForKey:@"id"];
        [[SPNCinelifeQueries sharedInstance] getMovieInformation:movieId
                                                      forZipcode:zipcodeString
                                                   orCoordinates:nil
                                                      withRadius:INITIAL_RADIUS+radiusAmplifier withCredentials:[SPNUser getCurrentUserId]];
        
        /*
        dispatch_async(kBgQueue, ^{
            __block BOOL showAlert = YES;
            
            if (radiusAmplifier > 0) {
                NSMutableArray *newTheatresToAppend =  [[self queryServerForFilmShowtimes:[self.filmInformation objectForKey:@"movieId"]
                                                        
                                                                               forZipcode:zipcodeString
                                                                                 forDate:date
                                                                            withLocation:self.currentUserLocation] mutableCopy];
                
                
                if ([newTheatresToAppend count] < 1) {
                    // ADD MESSAGE AND REQUERY
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                        
                        if (radiusAmplifier <= MAX_RADIUS) {
                            
                            [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                                        message:@"We did not find other theatres nearby. You can try again and we will search on a larger radius."
                                                       delegate:self
                                              cancelButtonTitle:@"It's ok"
                                              otherButtonTitles:@"Search again!", nil]
                             show];
                            showAlert = NO;
                        }
                        else {
                            [self.filmDetailsTableView beginUpdates];
                            [self.filmDetailsTableView deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationTop];
                            [self.filmDetailsTableView endUpdates];
                        }
                        
                    });
                    
                }
                NSMutableArray *theareIdList = [NSMutableArray new];
                for (NSDictionary *theatre in self.theatreShowtimesInRegion)
                {
                    [theareIdList addObject:[theatre objectForKey:@"theatreId"]];
                }
                
                NSIndexSet *theatresToRemove = [newTheatresToAppend indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                    NSString *theatreId = [obj objectForKey:@"theatreId"];
                    return [theareIdList indexOfObject:theatreId] != NSNotFound;
                }];
                [newTheatresToAppend removeObjectsAtIndexes:theatresToRemove];
 
                NSUInteger i = 0;
                cellsToDisplay = [self.theatreShowtimesInRegion count];
                self.theatreShowtimesInRegion = [self.theatreShowtimesInRegion arrayByAddingObjectsFromArray:newTheatresToAppend];
                
                if (cellsToDisplay < [self.theatreShowtimesInRegion count]) {
                    NSUInteger totalNumberOfItems = [self.theatreShowtimesInRegion count];
                    NSUInteger bumberOfItemsToDisplay = MIN(totalNumberOfItems, cellsToDisplay + kNumberOfCellsToAdd);
                    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                    
                    for (i=cellsToDisplay; i<bumberOfItemsToDisplay; i++)
                    {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                    }
                     
                    cellsToDisplay = bumberOfItemsToDisplay;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                        
                        [self.filmDetailsTableView beginUpdates];
                        [self.filmDetailsTableView insertRowsAtIndexPaths:indexPaths
                                                     withRowAnimation:UITableViewRowAnimationTop];
                        [self.filmDetailsTableView endUpdates];
                    });
                    
                }
                
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                        
                          if (radiusAmplifier <= MAX_RADIUS) {
                              
                              if (showAlert) {
                                  [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                                              message:@"We did not find other theatres nearby. You can try again and we will search on a larger radius."
                                                             delegate:self
                                                    cancelButtonTitle:@"It's ok"
                                                    otherButtonTitles:@"Search again!", nil]
                                   show];
                              }
                          }
                    });
                }

            }

            else {
                self.theatreShowtimesInRegion = [self queryServerForFilmShowtimes:[self.filmInformation objectForKey:@"movieId"]
                                                 
                                                                       forZipcode:zipcodeString
                                                                          forDate:date
                                                                     withLocation:self.currentUserLocation];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
                    [self.filmDetailsTableView reloadData];
                    [self.dateCollectionView reloadData];
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                                             animated:YES];
                    
                });

            }
        });
        */
    }
    else {
        selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
        [self.filmDetailsTableView reloadData];
    }
}



-(void)getFilmReviews
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    [[SPNCinelifeQueries sharedInstance] getReviewsForMovieId:[self.filmInformation objectForKey:@"id"]];
}



-(void)showUserReviews:(id)sender
{
    selectedReviewType = SPNFilmReviewTypeUserReviews;
    [self.filmDetailsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.filmDetailsTableView reloadData];
    
}

-(void)showCriticReviews:(id)sender
{
    selectedReviewType = SPNFilmReviewTypeCriticReviews;
    [self.filmDetailsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    // TODO: Fetch critic reviews
    [self.filmDetailsTableView reloadData];
}

-(void)loadMetaReviews:(id)sender
{
    NSDictionary *metacritic = [self.filmInformation objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        criticReviewLink = [metacritic objectForKey:@"url"];
        [self performSegueWithIdentifier:@"FullReview" sender:self];
    }
    
}

-(void)readFullReview:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    NSDictionary *review = [criticReviews objectAtIndex:btn.tag];
    criticReviewLink = [review objectForKey:@"url"];
    [self performSegueWithIdentifier:@"FullReview" sender:self];
}

-(void)getLastSavedLocation
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* storedZipcode = [prefs objectForKey:@"zip"];
    if (storedZipcode) {
        [self getLocationWithUserInput:storedZipcode];
    }
    else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeNoSavedLocation];
    }
}

#pragma mark Buy tickets functions
-(void)buyTickets:(id)sender
{
    UIButton *buyButton = (UIButton*)sender;
    NSDictionary *theatre = [self.theatreShowtimesInRegion objectAtIndex:buyButton.tag];
    [self loadTicketingViewForMovie:self.filmInformation forTheatre:theatre];
}

-(void)buyTicketsDirectly:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    NSString *theatreId = [btn theatreId];
    NSDictionary *selectedTheatre;
    for (NSDictionary *theatre in self.theatreShowtimesInRegion) {
        if ([[theatre objectForKey:@"id"] isEqualToString:theatreId]) {
            selectedTheatre = theatre;
            break;
        }
    }
    NSString *movieId = [self.filmInformation objectForKey:@"id"];
    
   // movieForTicketing = [[[SPNFilmDataCache sharedInstance] filmDataCache] objectForKey:movieId];

    NSString *show = btn.titleLabel.text;
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    
    [showtimeDateFormatter setDateFormat:@"hh:mma"];
    NSDate *newShow = [showtimeDateFormatter dateFromString:show];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    NSString *finalShow = [showtimeDateFormatter stringFromDate:newShow];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    
    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute)
                                                                fromDate:[NSDate date]];
    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents];
    
    NSDateComponents* addDayComponents = [NSDateComponents new] ;
    addDayComponents.day = selectedDateForPicker;
    NSDate* newDateWithDays = [calendar dateByAddingComponents:addDayComponents
                                                        toDate:nowWithoutSeconds
                                                       options:0];
    
    NSString *chosenDate = [datePickerFormatter stringFromDate:newDateWithDays];
    
    NSDate *date = [showtimeDateFormatter dateFromString:chosenDate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *newDate = [dateFormat stringFromDate:date];
    /*
    NSDictionary *location = @{@"lat" : [selectedTheatre objectForKey:@"lat"], @"lng" : [selectedTheatre objectForKey:@"lng"]};
     */
    showTicketingInformation = [NSDictionary dictionaryWithObjects:@[[selectedTheatre objectForKey:@"name"], theatreId, date, finalShow, [selectedTheatre objectForKey:@"id"]]
                                                           forKeys:@[@"theatreName", @"theatreId", @"showDate", @"showTime", @"theaterId"]];
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
    [[SPNCinelifeQueries sharedInstance] postTicketingForTheater:theatreId
                                                        forMovie:movieId
                                                     forShowdate:newDate
                                                     andShowtime:finalShow];
    
    
}

#pragma mark Navigation functions

-(void)loadTicketingViewForMovie:(NSDictionary*)film forTheatre:(NSDictionary*)theatre
{
    
/*
    SPNTheatreShowtimeCell *cell = [SPNTheatreShowtimeCell new];
    
    NSArray *showtimesOfMovieForTicketing = [cell getShowsForThisTheatre:nil
                                                             forSchedule:theatre
                                                                 forDate:self.datePicker.text];

    UIStoryboard *theatreStoryboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
    SPNTicketingViewController  *spnTicketing = [theatreStoryboard instantiateViewControllerWithIdentifier:@"BuyTickets"];
    [spnTicketing setTheatre:theatre];
    [spnTicketing setMovie:film];
    [spnTicketing setDate:self.datePicker.text];
    [spnTicketing setShowtimes:showtimesOfMovieForTicketing];
    
    [self.navigationController pushViewController:spnTicketing animated:YES];*/
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"WatchTrailer"]) {
        SPNFilmTrailerViewController *spnFilmTrailer = [segue destinationViewController];
        spnFilmTrailer.delegate = self;
        [spnFilmTrailer setTrailerURL:trailerURL];
    }
    else if ([segue.identifier isEqualToString:@"FullPoster"]) {
        SPNFullPosterViewController *spnFullPoster = [segue destinationViewController];
        [spnFullPoster setPosterURL:posterURL];
        [spnFullPoster setIsFestivalFilm:NO];
    }
    else if ([segue.identifier isEqualToString:@"FullReview"]) {
        SPNTheatreNewsDetailsViewController *spnReviewcontroller = [segue destinationViewController];
        [spnReviewcontroller setNewsLink:criticReviewLink];
    }
}
#pragma mark Trailer functions
-(void)showTrailers {

    [self.movingBarLeadConstraint setConstant:self.viewTrailerButton.frame.origin.x];
    [UIView animateWithDuration:0.15 animations:^{
        [self.barContainerView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self performSegueWithIdentifier:@"WatchTrailer" sender:self];
    }];
    
}

#pragma mark User related functions
-(void)queueItButtonPressed:(id)sender
{
    if (![SPNUser userLoggedIn]) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeUserNotLoggedIn
                              withDelegate:self
                              forAlertType:UIAlertViewStyleDefault
                           withCancelTitle:@"Not now"
                           withOtherTitles:@"Sign In"];
    }
    else {
        if ([self.filmInformation objectForKey:@"is_favorite"] && [[self.filmInformation objectForKey:@"is_favorite"] integerValue] < 1) {
            
            [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                                 animated:YES];

            [[SPNCinelifeQueries sharedInstance] postAddFavoriteMovie:[self.filmInformation objectForKey:@"id"]
                                                      withCredentials:[SPNUser getCurrentUserId]];
        }
        else {
            [MBProgressHUD showHUDAddedTo:self.tabBarController.view
                                 animated:YES];
            
            [[SPNCinelifeQueries sharedInstance] postRemoveFavoriteMovie:[self.filmInformation objectForKey:@"id"]
                                                         withCredentials:[SPNUser getCurrentUserId]];
        }
    }
    /*
    else if ([[self.queueItButton titleForState:UIControlStateNormal] isEqualToString:@"Favorite"]){
        [self removeFilmFromFavorites];
    }
    else {
        [self addFilmToFavorites];
    }
     */
}

-(void)addFilmToFavorites
{
    NSString *username = [NSString stringWithFormat:@"%@",[SPNUser getCurrentUserId]];
    
    NSDictionary *user = [NSDictionary dictionaryWithObject:username
                                                     forKey:@"userId"];
    
    NSArray *userList = [[LCPDatabaseHelper sharedInstance] fetchRecordsWithAttribute:username
                                                                      forAttributeKey:@"userId"
                                                                            forEntity:@"Users"];
    if ([userList count] < 1) {
        [[LCPDatabaseHelper sharedInstance] addRecord:user
                                            forEntity:@"Users"];
        NSLog(@"df");
    }
    NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
    NSString *title = [self.filmInformation objectForKey:@"title"];
    
    NSDictionary *newFilmForUser = [NSDictionary dictionaryWithObjects:@[movieId, title]
                                                               forKeys:@[@"movieId", @"title"]];
    
    
    BOOL succeeded = [[LCPDatabaseHelper sharedInstance] addMovieToFavourites:newFilmForUser
                                                                      forUser:username];
    /*
     NSString* user = [SPNUser getCurrentUserId];
     BOOL succeeded = [self addFilmToGoWatchItQueue:self.filmInformation forUser:user];*/
    if (!succeeded) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeGoWatchItMovieNotAdded];
    }
    else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeGoWatchItMovieAdded];
        [self.queueItButton setTitle:@"Favorite"
                            forState:UIControlStateNormal];
        [self.heartIconImageView setImage:[UIImage imageNamed:@"heartFill"]];
    }
}

-(void)removeFilmFromFavorites
{
    NSString *username = [NSString stringWithFormat:@"%@",[SPNUser getCurrentUserId]];
    NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
    BOOL succeeded = [[LCPDatabaseHelper sharedInstance] deleteMovie:movieId
                                                              ofUser:username];
    if (succeeded) {
        [self.queueItButton setTitle:@"Like It" forState:UIControlStateNormal];
        [self.heartIconImageView setImage:[UIImage imageNamed:@"heartIcon"]];

        [[[UIAlertView alloc] initWithTitle:@"Done"
                                    message:@"This film is not on your list anymore, but you can always add it again."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];

    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"Something went wrong. We could not delete this film from your list. Try again later please."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
    }
}

-(BOOL)addFilmToGoWatchItQueue:(NSDictionary*)film forUser:(NSString*)username
{
    BOOL succeed = NO;
    NSString *title = [film objectForKey:@"title"];
    NSString *goWatchItTitle = [SPNUser getMoiveGWIId:title];
    NSString *authToken = [SPNUser getGoWatchItAuthTokenForEmailForUser:username];
    
    if (!goWatchItTitle) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeGoWatchItFailMovieId];
    }
    else if (!authToken) {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeGoWatchItNoToken];
    }
    else {
        succeed = [SPNUser addMovietoGWIQueue:username
                                     forMovie:goWatchItTitle
                      withAuthenticationToken:authToken];
    }
    return succeed;
}

-(void)reviewButtonPressed:(id)sender
{
    
    if ([SPNUser userLoggedIn]) {
        dispatch_async(kBgQueue, ^{
            NSDictionary *previousReview = [SPNServerQueries getCommentOfUser:[[SPNUser getCurrentUserId] objectForKey:@"name"]
                                                                     forMovie:[self.filmInformation objectForKey:@"movieId"]];
            
         
            dispatch_sync(dispatch_get_main_queue(), ^{
                [SPNFilmPostReviewViewController showWReviewPostForMovie:self.filmInformation
                                                      overViewController:self.tabBarController
                                                            withDelegate:self
                                                           withOldReview:previousReview];
            
            });
        });
    }
    else {
        NSString *movieId = [self.filmInformation objectForKey:@"movieId"];
       if ([[[SPNFilmDataCache sharedInstance] anonymousReviewCache]
           objectForKey:movieId]) {
           [UIAlertView showAlertViewWthTitle:@"Wait a minute..." andMessage:@"You already rated this movie. Sign in to edit your review!"];
       }
       else {
        [SPNFilmPostReviewViewController showWReviewPostForMovie:self.filmInformation
                                              overViewController:self.tabBarController
                                                    withDelegate:self];
       }
   //
    }
        
}

#pragma mark PostReview delegate functions

-(void)updatePosts:(BOOL)reload withReview:(NSString *)review
{
    userReview = nil;
    if (reload && selectedFilmDetailsType == SPNFilmDetailsTypeReviews) {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        onlyWantReviews = YES;
        [[SPNCinelifeQueries sharedInstance] setDelegate:self];
        [[SPNCinelifeQueries sharedInstance] getMovieInformation:[self.filmInformation objectForKey:@"id"]
                                                      forZipcode:nil
                                                   orCoordinates:nil
                                                      withRadius:1
                                                 withCredentials:[SPNUser getCurrentUserId]];
        [self getFilmReviews];
    }
    if (reload && review) {
        userReview = review;
        [[[UIAlertView alloc]  initWithTitle:@"Share Your Review..."
                                     message:@"Would you like other members beyond the CineLife Community to benefit from your review?"
                                    delegate:self
                           cancelButtonTitle:@"Not Now"
                           otherButtonTitles:@"Yes, Please", nil]
         show];
    }

}

#pragma mark Action sheet functions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeFacebook
                             withReview:userReview];
            break;
        case 1:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeTwitter withReview:userReview];
            break;
        case 2:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeGooglePlus withReview:userReview];
            break;
        default:
            break;
    }

}
#pragma mark Sharing functions
-(void)shareToMediaWithReview:(NSString*)review
{
    NSString *title = review ? @"Post your review to your social media!" : @"Share your thoughts about this film!";
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:title
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Facebook", @"Twitter", @"Google+", nil];
    
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:[self.view window]];
}

- (IBAction)shareButtonPressed:(id)sender
{
    userReview = nil;
    [self shareToMediaWithReview:nil];
}

-(void)checkInForSocialMedia:(SPNShareSocialMediaType)socialMedia withReview:(NSString*)review
{
    switch (socialMedia) {
        case SPNShareSocialMediaTypeFacebook:
            [self shareWithFacebook:nil];
            break;
        case SPNShareSocialMediaTypeTwitter:
            [self shareWithTwitter:nil];
            break;
        default: //SPNShareSocialMediaTypeGooglePlus
            [self shareWithGoogle:nil];
            break;
    }
}

-(void)shareWithFacebook:(id)sender
{
    [self createShareDialog:nil];
}

-(void)createShareDialog:(NSNotification*)notification
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    NSString *postTitle = self.titleLabel.text;
    content.contentTitle = postTitle;
    
    NSString *urlImage;
    NSDictionary *posters = [self.filmInformation objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
 
    content.imageURL = [NSURL URLWithString:urlImage];
    content.contentDescription = [NSString stringWithFormat:@"Check out %@ via @My_Cinelife", self.titleLabel.text];
    if (userReview) {
        content.contentTitle = [NSString stringWithFormat:@"I wrote a review for %@!", self.titleLabel.text];
        content.contentDescription = [NSString stringWithFormat:@"%@", userReview];
    }
    
    [FBSDKShareDialog  showFromViewController:self withContent:content delegate:self];
}


-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"succeeded");
}

-(void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    
    NSLog(@"cancel");
}

-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Error sharing to Facebook" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

-(void)shareWithTwitter:(id)sender
{
    // Twitter takes the attached file as a link for 23 characters. Max Tweet length is 140, so this leaves 117 possible characters to share, but we also add the link to the app, which takes 22 - 23 characters, leaving us roughly 95 characters for input. Finally, we'll add @Ripple to each post, so we remove another 7 characters, leaving us with 88.
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];

    if ([[store existingUserSessions] count] > 0) {
         [self shareToTwitter];
    }
    else {
        [[Twitter sharedInstance] logInWithCompletion:^
         (TWTRSession *session, NSError *error) {
             if (session) {
                 NSLog(@"signed in as %@", [session userName]);
                 [self shareToTwitter];
             } else {
                 NSLog(@"error: %@", [error localizedDescription]);
             }
         }];
    }
}

-(void)shareToTwitter
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:[NSString stringWithFormat:@"Check out %@ via @My_Cinelife", self.titleLabel.text]];
    [composer setImage:self.posterImageView.image];
    
    if (userReview) {
        [composer setText:[NSString stringWithFormat:@"I wrote a review for %@!\r\n%@", self.titleLabel.text, userReview]];
    }

    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
        }
        
    }];

}

-(void)shareWithGoogle:(id)sender
{
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] signInSilently];
    }
    else {
        [[GIDSignIn sharedInstance] signIn];
    }
}

#pragma mark - GPP Sign in delegate functions
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (!error) {
        [self shareToGoogle:[NSURL URLWithString:@"https://cinelife.com/"]];
    }
    else {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}


-(void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)safariViewControllerDidFinish:(SFSafariViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)shareToGoogle:(NSURL*)shareURL
{
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[shareURL absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
}



#pragma mark Alert view functions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Sign In"]) {
        [alertView dismissWithClickedButtonIndex:buttonIndex
                                        animated:NO];
        self.tabBarController.selectedIndex = 3;
    }
    else if ([title isEqualToString:@"Yes, Please"]) {
        [self shareToMediaWithReview:userReview];
    }
    else if ([title isEqualToString:@"Not Now"]) {
        userReview = nil;
    }
    else if ([title isEqualToString:@"Search again!"]) {
        radiusAmplifier+=RADIUS_ENHANCE;
        [self getLocationWithUserInput:self.zipcodePicker.text];
    }
}

#pragma mark - trailer delegate functions
-(void)spnFilmTrailerDidFinish
{
    if (selectedFilmSection == SPNFilmSectionTypePhotos) {
        [self.movingBarLeadConstraint setConstant:self.photosButton.frame.origin.x];
    }
    else {
         [self.movingBarLeadConstraint setConstant:self.detailsButton.frame.origin.x];
    }
    [UIView animateWithDuration:0.15 animations:^{
        [self.barContainerView layoutIfNeeded];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }];
}

#pragma mark - collection view functions (For movie stills)
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.movieStillsCollectionView) {
        return [movieStills count];
    }
    else {
        NSArray *shows = [self.showtimeInformation allKeys];
        return MAX([shows count], 1);
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *photoIdentifier = @"PhotoCell";
    static NSString *DateCellIdentifier = @"DateCell";
    
    if (collectionView == self.movieStillsCollectionView) {
        SPNMovieStillCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:photoIdentifier
                                                                             forIndexPath:indexPath];
        NSURL *imageURL = [NSURL URLWithString:[movieStills objectAtIndex:indexPath.row]];
        [cell.photoImageView setShowActivityIndicator:YES];
        [cell.photoImageView setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        
        cell.photoImageView.imageURL = imageURL;
        return cell;
    }
    else {
        SPNDateCell *cell = (SPNDateCell*)[collectionView dequeueReusableCellWithReuseIdentifier:DateCellIdentifier forIndexPath:indexPath];
        
        if (selectedDateForPicker == indexPath.row) {
            [cell.dateTitleLabel setBackgroundColor:[UIColor spn_aquaColor]];
            [cell.dateTitleLabel setFont:[UIFont spn_NeutraBoldMedium]];
            [cell.dateTitleLabel setTextColor:[UIColor whiteColor]];
            [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_aquaShadowColor].CGColor];
        }
        else {
            [cell.dateTitleLabel setBackgroundColor:[UIColor clearColor]];
            [cell.dateTitleLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [cell.dateTitleLabel setTextColor:[UIColor blackColor]];
            [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
        }
        
        NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
        
        if ([showArray count] < 1) {
            [cell.dateTitleLabel setText:@"Today"];
            return cell;
        }
        NSDate *show = [showArray objectAtIndex:indexPath.row];
        
        if ([self isSameDayWithDate1:[NSDate date] date2:show]) {
            [cell.dateTitleLabel setText:@"Today"];
            return cell;
        }

        NSString *dateString = [formatter stringFromDate:show];
        if (!dateString) {
            [cell.dateTitleLabel setText:@"Today"];
        }
        else {
            [cell.dateTitleLabel setText:dateString];
        }
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(SPNMovieStillCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.movieStillsCollectionView) {
        [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.photoImageView.imageURL];
        cell.photoImageView.image = nil;
    }
}

-(void)collectionView:(UICollectionView*)collectionView willDisplayItem:(SPNMovieStillCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.movieStillsCollectionView) {
        cell.photoImageView.image = nil;
    }
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.movieStillsCollectionView) {
        return CGSizeMake(self.view.frame.size.width-20, collectionView.frame.size.height);
    }
    else {
        return CGSizeMake(89, 52);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView != self.movieStillsCollectionView) {
        for (SPNDateCell *visibleCell in [collectionView visibleCells])
        {
            [visibleCell.dateTitleLabel setBackgroundColor:[UIColor clearColor]];
            [visibleCell.dateTitleLabel setFont:[UIFont spn_NeutraSmallMedium]];
            [visibleCell.dateTitleLabel setTextColor:[UIColor blackColor]];
            [visibleCell.dateTitleLabel.layer setBorderColor:[UIColor spn_buttonShadowColor].CGColor];
        }
        
        SPNDateCell *cell = (SPNDateCell*)[collectionView cellForItemAtIndexPath:indexPath];
        [cell.dateTitleLabel setBackgroundColor:[UIColor spn_aquaColor]];
        [cell.dateTitleLabel setFont:[UIFont spn_NeutraBoldMedium]];
        [cell.dateTitleLabel setTextColor:[UIColor whiteColor]];
        [cell.dateTitleLabel.layer setBorderColor:[UIColor spn_aquaShadowColor].CGColor];
        
        selectedDateForPicker = indexPath.row;
        [self.filmDetailsTableView reloadData];
    }
}

#pragma mark - Cinelife queries
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery reviews:(NSArray *)reviews
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    NSDictionary *metacritic = [self.filmInformation objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        criticReviews = [metacritic objectForKey:@"metacritic_reviews"];
    }
    userReviews =reviews;

    selectedFilmDetailsType = SPNFilmDetailsTypeReviews;
    
    selectedReviewType = SPNFilmReviewTypeCriticReviews;
    [self.filmDetailsTableView reloadData];

}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSString *errorText = @"";
    onlyWantReviews = NO;
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}

-(void)updateReviewInformation:(NSDictionary*)film
{
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        globalUserRating = [userScore floatValue];
        [self.userScoreLabel setText:[NSString stringWithFormat:@"%g", globalUserRating]];
    }
    else {
        [self.userScoreLabel setText:@"No votes"];
    }

}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieInformation:(NSDictionary *)movie
{
    if (onlyWantReviews) {
        onlyWantReviews = NO;
        NSMutableDictionary *updatedFilmInfo = [NSMutableDictionary dictionaryWithDictionary:self.filmInformation];
        [updatedFilmInfo setObject:[movie objectForKey:@"rating"]
                            forKey:@"rating"];
        [updatedFilmInfo setObject:[movie objectForKey:@"cinelife_reviews"]
                            forKey:@"cinelife_reviews"];
        self.filmInformation = updatedFilmInfo;
        [self updateReviewInformation:self.filmInformation];
        return;
    }
    // This means that the user selected to search more theaters
    NSDictionary *newTheatres = [SPNCinelifeQueries sortShowdatesForTheaters:[movie objectForKey:@"theaters"]];
    
    selectedFilmDetailsType = SPNFilmDetailsTypeShowtimes;
    
    if (radiusAmplifier > 0) {
        if ([newTheatres count] <= [self.showtimeInformation count]) {
            // No new theatres found
            if (radiusAmplifier+INITIAL_RADIUS > MAX_RADIUS) {
                [self.filmDetailsTableView beginUpdates];
                [self.filmDetailsTableView deleteSections:[NSIndexSet indexSetWithIndex:1]
                              withRowAnimation:UITableViewRowAnimationTop];
                [self.filmDetailsTableView endUpdates];
                [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                            message:@"We did not find other theatres nearby. Please try using a different location."
                                           delegate:self
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil]
                 show];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:@"Sorry..."
                                            message:@"We did not find other theatres nearby. You can try again and we will search on a larger radius."
                                           delegate:self
                                  cancelButtonTitle:@"It's ok"
                                  otherButtonTitles:@"Search again!", nil]
                 show];
            }
        }
        else {
            // We have to append new theaters
            // We get the list of ids for all theaters we currently have
            NSMutableDictionary *foundTheatres = [NSMutableDictionary dictionaryWithDictionary:newTheatres];
            
            NSArray *showArray = [[self.showtimeInformation allKeys] sortedArrayUsingSelector:@selector(compare:)];
            NSDate *show = [showArray objectAtIndex:selectedDateForPicker];

            
            NSMutableArray *oldTheatreList = [self.showtimeInformation objectForKey:show];
            NSMutableArray *newTheatreList = [NSMutableArray arrayWithArray:[foundTheatres objectForKey:show]];
            
            NSIndexSet *theatresToRemove = [newTheatreList indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                NSString *theatreId = [obj objectForKey:@"id"];
                return [oldTheatreList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                    NSString *theatre2Id = [obj objectForKey:@"id"];
                    return [theatre2Id isEqualToString:theatreId];
                }] != NSNotFound;
            }];
            
            
            [newTheatreList removeObjectsAtIndexes:theatresToRemove];
        
            NSUInteger i = 0;
            NSMutableArray *shows = [self.showtimeInformation objectForKey:show];
            cellsToDisplay = [shows count];
            NSArray *newShows = [shows arrayByAddingObjectsFromArray:newTheatreList];
            
            if (cellsToDisplay < [newShows count]) {
                NSLog(@"");
                
                NSUInteger totalNumberOfItems = [newShows count];
                NSUInteger numberOfItemsToDisplay = MIN(totalNumberOfItems, cellsToDisplay + kNumberOfCellsToAdd);
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                
                for (i=cellsToDisplay; i<numberOfItemsToDisplay; i++)
                {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:1]];
                }
                
                cellsToDisplay = numberOfItemsToDisplay;
                
                self.showtimeInformation = newTheatres;
                
                self.theatreShowtimesInRegion = [movie objectForKey:@"theaters"];
                
                [self.filmDetailsTableView beginUpdates];
                [self.filmDetailsTableView insertRowsAtIndexPaths:indexPaths
                                                 withRowAnimation:UITableViewRowAnimationTop];
                [self.filmDetailsTableView endUpdates];
                [self.dateCollectionView reloadData];
            }
        }

    }
    else {
        self.theatreShowtimesInRegion = [movie objectForKey:@"theaters"];
        NSInteger affiliatedCount = 0;
        for (NSDictionary *theatre in self.theatreShowtimesInRegion) {
            if ([[theatre objectForKey:@"is_partner"] boolValue]) {
                affiliatedCount+=1;
            }
        }
        if (affiliatedCount < 1) {
            cellsToDisplay = kNumberOfCellsToAdd;
        }
        else {
            cellsToDisplay = MIN(affiliatedCount, kNumberOfCellsToAdd);
        }
        self.showtimeInformation = newTheatres;
        [self.filmDetailsTableView reloadData];
        [self.dateCollectionView reloadData];
    }
    
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                             animated:YES];
    
 
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery addedMovie:(NSDictionary *)movie
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddedMovieToFavorites object:movie userInfo:movie];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery removedMovie:(NSDictionary *)movie
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRemovedMovieFromFavorites object:self userInfo:movie];
}

- (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingURL:(NSString *)url
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    UIStoryboard *theatreStoryboard = [UIStoryboard storyboardWithName:@"TheatresStoryboard" bundle:nil];
    SPNTicketPurchaseViewController  *spnTicketing = [theatreStoryboard instantiateViewControllerWithIdentifier:@"TicketPurchase"];
    [spnTicketing setUrl:url];
    [self.navigationController pushViewController:spnTicketing
                                         animated:YES];
    
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingFromRTS:(NSDictionary *)ticketingInformation
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    UIStoryboard *ticketingStoryboard = [UIStoryboard storyboardWithName:@"TicketingStoryboard" bundle:nil];
    SPNTicketSelectionViewController *spnTicketingSelection = [ticketingStoryboard instantiateViewControllerWithIdentifier:@"RTSTicketing"];
    [spnTicketingSelection setMovieInformation:self.filmInformation];
    [spnTicketingSelection setShowtimeInformation:showTicketingInformation];
    [spnTicketingSelection setTicketingInformation:ticketingInformation];
    [self.navigationController pushViewController:spnTicketingSelection animated:YES];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery ticketingError:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery favoriteTheaters:(NSArray *)favoriteTheaters
{
    
}

@end
