//
//  SPNMovieCollectionCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/12/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//
#import "SPNShowtimesCollectionView.h"
#import "SPNMovieCollectionCell.h"

@implementation SPNMovieCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(NSString*)reuseIdentifier
{
    return @"MovieCell";
}

-(void)initializeMovieSchedule:(NSDictionary*)schedule
             forCollectionView:(SPNShowtimesCollectionView*)collectionView
                  forIndexPath:(NSIndexPath*)indexPath
{
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    NSString *movieTitle = [schedule objectForKey:@"name"] ? [schedule objectForKey:@"name"] : [schedule objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *movieId = [schedule objectForKey:@"id"] ? [schedule objectForKey:@"id"] : [schedule objectForKey:@"movieId"];
    NSString *urlImage;
    BOOL isFestivalFilm = YES;
    if (movieId) {
        isFestivalFilm = NO;
        NSDictionary *posters = [schedule objectForKey:@"poster"];
        if ([posters count] > 0) {
            urlImage = [posters objectForKey:@"thumb"];
        }
    }
    else if (!urlImage) {
        urlImage = [schedule objectForKey:@"image_16_9_url"];
    }
    [self.thumbnail setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];
    [self.thumbnail setImageURL:[NSURL URLWithString:urlImage]];
    
    [self.titleLabel setFont:[UIFont spn_NeutraBoldSmall]];
}

@end
