//
//  PartnerConfiguration.m
//  iFilm
//
//  Created by Vlad Getman on 18.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "PartnerConfiguration.h"
#import "SPNCinelifeQueries.h"

@implementation PartnerConfiguration

+ (PartnerConfiguration *)current {
    static PartnerConfiguration *config = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        config = [[self alloc] init];
    });
    return config;
}

- (void)load {
    NSDictionary *partnerConfiguration = [SPNCinelifeQueries getCineLifeInitialConfiguration];
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 152;
    
    if (partnerConfiguration) {
        
        NSString *cinelifeText = [partnerConfiguration objectForKey:@"PartnerLabelText"];
        
        self.text = ([cinelifeText isKindOfClass:[NSString class]] && [cinelifeText length] > 0) ? cinelifeText : @"CineLife Partner";
        
        NSString *cinelifeColor = [partnerConfiguration objectForKey:@"PartnerLabelColor"];
        
        UIColor *bgColor;
        if ([cinelifeColor isKindOfClass:[NSString class]]) {
            bgColor = [UIColor colorFromHexString:cinelifeColor];
        }
        if (bgColor) {
            self.color = bgColor;
        }
    }
    else {
        self.text = @"CineLife Partner";
        self.color = [UIColor spn_brownColor];
    }
    
    CGRect rect = [self.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName : [UIFont spn_NeutraSmallMedium]}
                                          context:nil];
    self.size = CGSizeMake(rect.size.width + 10, rect.size.height);
    
    NSString *labelDescription = partnerConfiguration[@"PartnerLabelDescription"];
    self.info = labelDescription ? labelDescription : @"Affiliated theaters to our network offer promotions, exclusive reviews, showings and much more!";
}

- (void)explain {
    [[[UIAlertView alloc] initWithTitle:@"CineLife Partners"
                                message:self.info
                               delegate:nil cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
}

@end
