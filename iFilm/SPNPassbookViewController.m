//
//  SPNPassbookViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/28/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPassbookViewController.h"
#import "ZXMultiFormatWriter.h"
#import "ZXImage.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
@interface SPNPassbookViewController ()
{
    BOOL storedPurchase, shouldAddBackground;
    UIColor *sectionColor, *titleSectionColor, *mainColor;
}
@end

@implementation SPNPassbookViewController

-(void)viewWillAppear:(BOOL)animated
{
    if (!shouldAddBackground) {
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.scrollView.bounds.size.height-112, self.scrollView.bounds.size.width+150, 600)];
        [bottomView setBackgroundColor:[UIColor spn_brownColor]];
        if (mainColor) {
            [bottomView setBackgroundColor:mainColor];
        }
        [self.scrollView addSubview:bottomView];
        [self.scrollView sendSubviewToBack:bottomView];
        shouldAddBackground = YES;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    PKAddPassButton *passBtn = [[PKAddPassButton alloc] initWithAddPassButtonStyle:PKAddPassButtonStyleBlack];
    [self.codeContainerView addSubview:passBtn];
    
    [passBtn addTarget:self action:@selector(addToPassbook) forControlEvents:UIControlEventTouchUpInside];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [self.view setClipsToBounds:YES];
    // Do any additional setup after loading the view.
    [self customizeUI];
    NSMutableDictionary *paymentInfo = [NSMutableDictionary dictionaryWithDictionary:self.showtimeInformation];
    [paymentInfo setObject:[self.paymentInformation objectForKey:@"tickets"]
                    forKey:@"tickets"];
    [self initializeFilmData:self.movieInformation];
    [self initializePurchaseData:paymentInfo];
    
    

    NSString *barcode = [self.transactionInformation objectForKey:@"barcode"];
    NSError *error = nil;
    ZXMultiFormatWriter *writer = [ZXMultiFormatWriter writer];
    ZXBitMatrix* result = [writer encode:barcode
                                  format:kBarcodeFormatPDF417
                                   width:1250
                                  height:500
                                   error:&error];
    if (result) {
        CGImageRef image = [[ZXImage imageWithMatrix:result] cgimage];
        
        [self.qrCodeImageVIew setImage:[UIImage imageWithCGImage:image]];
        // This CGImageRef image can be placed in a UIImage, NSImage, or written to a file.
    } else {
        NSString *errorMessage = [error localizedDescription];
        NSLog(@"error: %@", errorMessage);
    }
    [self getBrandColors:self.branding];
}


-(void)viewDidAppear:(BOOL)animated
{
    if (storedPurchase) {
    
        [[[UIAlertView alloc] initWithTitle:@"Thank you for purchasing!"
                                    message:@"We have stored your purchase details in the app. You can see it in the Account tab. By the way, would you like to share your plans with your friends?"
                                   delegate:self
                          cancelButtonTitle:@"Not Now"
                          otherButtonTitles:@"Sure!", nil]
         show];
        
        NSArray *tabItems = [self.tabBarController.tabBar items];
        UITabBarItem *accountTab = [tabItems lastObject];
        accountTab.badgeValue = @"1";
        storedPurchase = NO;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@"yes" forKey:@"newPurchase"];
        [prefs synchronize];
    }
}
#pragma mark - UI Customization
-(void)customizeUI
{
    [self.navigationItem setHidesBackButton:YES];
    [self.doneButton setBackgroundColor:[UIColor spn_buttonColor]];
    [self.doneButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
    
    [self.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.genresLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.userRatingLabel setFont:[UIFont spn_NeutraBoldMedium]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel setFont:[UIFont spn_NeutraSmallMedium]];
    [self.classificationLabel.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [self.showDateLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.showTimeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.ticketsLabel setFont:[UIFont spn_NeutraSmallMedium]];
    
    [self.confirmationLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.codeLabel setFont:[UIFont spn_NeutraBoldGiga]];
    [self.codeTitleLabel setFont:[UIFont spn_NeutraBoldMediumLarge]];
    
    [self.theatreBackgroundView setBackgroundColor:[UIColor spn_darkBrownColor]];
    
}

-(void)getBrandColors:(NSDictionary*)branding
{
    if (branding) {
        NSString *logo = [branding objectForKey:@"logo"];
        if ([logo length] > 0) {
            NSString *realLogo = [logo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self.theatreLogoImageView setImageURL:[NSURL URLWithString:realLogo]];
        }
        
        else {
            [self.brandLogoHeightConstraint setConstant:0];
            [self.scrollOriginTopConstraint setConstant:22];
        }
        
        if ([[branding objectForKey:@"color_background"] length] > 0) {
            mainColor = [UIColor colorFromHexString:[branding objectForKey:@"color_background"]];
            [self.view setBackgroundColor:mainColor];
            [self.theatreBackgroundView setBackgroundColor:mainColor];
        }
    }
    else {
        [self.brandLogoHeightConstraint setConstant:0];
        [self.scrollOriginTopConstraint setConstant:22];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Film initialization
-(void)initializeFilmData:(NSDictionary*)film
{
    NSString *movieTitle =[film objectForKey:@"name"] ? [film objectForKey:@"name"] : [film objectForKey:@"title"];
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.titleLabel.text = movieTitle;
        }
        else {
            self.titleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.titleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }
    
    NSString *urlImage;
    NSDictionary *posters = [film objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    
    [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];

    self.posterImageView.imageURL = [NSURL URLWithString:urlImage];
    
    [self.miniPosterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
    self.miniPosterImageView.imageURL = [NSURL URLWithString:urlImage];
   
    id genres =[film objectForKey:@"genres"];
    
    if ([genres isKindOfClass:[NSArray class]]) {
        self.genresLabel.text = [genres componentsJoinedByString:@" | "];
    }
    else if ([genres isKindOfClass:[NSString class]]) {
        self.genresLabel.text = genres;
    }
    else {
        self.genresLabel.text = @"No genre information available";
    }
    NSString *rating = [film objectForKey:@"rating"];
    
    if ([rating length] > 0) {
        self.classificationLabel.text = [NSString stringWithFormat:@"%@", rating];
    }
    else {
        self.classificationLabel.text = @"";
    }
    
    NSString *runTime = [film objectForKey:@"runtime"];
    if (!runTime || [runTime length] < 1) {
        runTime = @"0";
    }
    NSString *duration = [NSString stringWithFormat:@"%@ min", runTime];
    self.durationLabel.text = duration;
    
    NSDictionary *metacritic = [film objectForKey:@"metacritic"];
    if ([metacritic count] > 0) {
        NSString *metascore = [metacritic objectForKey:@"score"];
        [self.metaScoreLabel assignMetaScore:metascore];
    }
    else {
        [self.metaScoreLabel assignMetaScore:@"NR"];
    }
    NSDictionary *cinelifeReviews =  [film objectForKey:@"cinelife_reviews"];
    
    NSString *userValue = [cinelifeReviews objectForKey:@"rating"];
    NSNumber *userScore = [NSNumber numberWithFloat:[userValue floatValue]];
    if (userValue && ![userValue isEqualToString:@"0"]) {
        [self.userRatingLabel setText:[NSString stringWithFormat:@"%g", [userScore floatValue]]];
    }
    else {
        [self.userRatingLabel setText:@"No votes"];
    }
}


#pragma mark - Payment initialization
-(NSString*)initializeTicketData:(NSDictionary*)paymentInformation
{
    NSDictionary *tickets = [paymentInformation objectForKey:@"tickets"];
    NSString *totalTickets = @"";
    NSMutableArray *ticketList = [NSMutableArray new];
    for (NSString *ticketType in [tickets allKeys])
    {
        NSDictionary *ticket = [tickets objectForKey:ticketType];
        NSString *ticketAndAmount = [NSString stringWithFormat:@"-%@ (%@)", ticketType, [ticket objectForKey:@"amount"]];
        [ticketList addObject:ticketAndAmount];
    }
    totalTickets = [ticketList componentsJoinedByString:@"\r\n"];
    [self.ticketsLabel setText:totalTickets];
    return totalTickets;
   
}

#pragma mark - Passbook functions
-(void)addPassbook:(id)sender
{
    [self addToPassbook];
}

-(void)addToPassbook
{

    if (!self.generatedPass) {
        [UIAlertView showAlertViewWthTitle:@"Unable to generate a pass" andMessage:@"We could not generate the Passbook pass at the moment. Don't worry, you can still use your confirmation mail or the transaction code of the purchase stored on the device (under the Account tab, select My Purchases)"];
    }
    else {
        //init a pass library
        PKPassLibrary* passLib = [[PKPassLibrary alloc] init];
        
        //check if pass library contains this pass already
        if([passLib containsPass:self.generatedPass]) {
            
            //pass already exists in library, show an error message
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Pass Exists" message:@"The pass you are trying to add to Passbook is already present." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            
        } else {
            
            //present view controller to add the pass to the library
            PKAddPassesViewController *vc = [[PKAddPassesViewController alloc] initWithPass:self.generatedPass];
            [vc setDelegate:(id)self];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}

-(IBAction)doneWithPurchase:(id)sender
{
    NSArray *controllers = [self.navigationController childViewControllers];
    
    [self.navigationController popToViewController:[controllers objectAtIndex:1]
                                          animated:YES];
}

-(void)initializePurchaseData:(NSDictionary*)purchaseInformation
{
    // Date formatting
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEEdMMMM" options:0
                                                              locale:[NSLocale currentLocale]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:formatString];
    NSDate *selectedDate = [purchaseInformation objectForKey:@"showDate"];
    NSString *todayString = [dateFormatter stringFromDate:selectedDate];
    
    todayString = [@"Date: " stringByAppendingString:todayString];
    NSRange boldFontRange = NSMakeRange(0, 5);
    
    NSMutableAttributedString *dateText = [[NSMutableAttributedString alloc] initWithString:todayString];
    
    [dateText beginEditing];
    [dateText addAttribute:NSFontAttributeName
                     value:[UIFont spn_NeutraBoldMediumLarge]
                     range:boldFontRange];
    [dateText endEditing];
    [self.showDateLabel setAttributedText:dateText];
    
    // Time formatting
    NSDateFormatter *showtimeDateFormatter = [NSDateFormatter new];
    [showtimeDateFormatter setDateStyle:NSDateFormatterFullStyle];
    [showtimeDateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [showtimeDateFormatter setAMSymbol:@"am"];
    [showtimeDateFormatter setPMSymbol:@"pm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [showtimeDateFormatter setLocale:usLocale];
    [showtimeDateFormatter setDefaultDate:[NSDate date]];
    [showtimeDateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [showtimeDateFormatter dateFromString:[self.showtimeInformation objectForKey:@"showTime"]];
    [showtimeDateFormatter setDateFormat:@"h:mma"];
    NSString* showDate = [showtimeDateFormatter stringFromDate:date];
    
    NSString *showString = [NSString stringWithFormat:@"Showtime: %@",showDate];
    boldFontRange = NSMakeRange(0, 9);
    NSMutableAttributedString *showWithTitle = [[NSMutableAttributedString alloc] initWithString:showString];
    
    [showWithTitle beginEditing];
    [showWithTitle addAttribute:NSFontAttributeName
                         value:[UIFont spn_NeutraBoldMediumLarge]
                         range:boldFontRange];
    [showWithTitle endEditing];
    [self.showTimeLabel setAttributedText:showWithTitle];
    
    // Ticket formatting
    NSString *tickets = [self initializeTicketData:purchaseInformation];
                                
    NSString *ticketString = [@"Ticket(s): \r\n" stringByAppendingString:tickets];
    boldFontRange = NSMakeRange(0, 10);
    
    NSMutableAttributedString *ticketsText = [[NSMutableAttributedString alloc] initWithString:ticketString];
    
    [ticketsText beginEditing];
    [ticketsText addAttribute:NSFontAttributeName
                        value:[UIFont spn_NeutraBoldMediumLarge]
                        range:boldFontRange];
    [ticketsText endEditing];
    [self.ticketsLabel setAttributedText:ticketsText];
    
    NSMutableDictionary *payInformation = [NSMutableDictionary new];
    
    [payInformation setObject:selectedDate
                            forKey:@"showDate"];
    [payInformation setObject:[self.showtimeInformation objectForKey:@"showTime"]
                            forKey:@"showTime"];
    [payInformation setObject:[self.movieInformation objectForKey:@"id"]
                            forKey:@"movieId"];
    [payInformation setObject:tickets
                            forKey:@"tickets"];
    [payInformation setObject:[self.movieInformation objectForKey:@"name"]
                            forKey:@"movieTitle"];
    [payInformation setObject:[self.transactionInformation objectForKey:@"barcode"]
                       forKey:@"transactionCode"];
    storedPurchase =[[LCPDatabaseHelper sharedInstance] addRecord:payInformation
                                                             forEntity:@"Purchases"];
    
 
    [self.codeLabel setText:[self.transactionInformation objectForKey:@"barcode"]];

}

#pragma mark - Scrollview functions
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.bounces = (scrollView.contentOffset.y < 30);
}


#pragma mark - Alert view delegate functions
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];

    if ([title isEqualToString:@"Sure!"]) {
        [self shareToMediaWithTickets];
    }
}
-(void)sharePurchase:(id)sender
{
    [self shareToMediaWithTickets];
}
#pragma mark Sharing functions
-(void)shareToMediaWithTickets
{
    NSString *title = @"Post your plans to your social media!";
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:title
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Facebook", @"Twitter", @"Google+", nil];
    
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:[self.view window]];
}

#pragma mark Action sheet functions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeFacebook];
            break;
        case 1:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeTwitter];
            break;
        case 2:
            [self checkInForSocialMedia:SPNShareSocialMediaTypeGooglePlus];
            break;
        default:
            break;
    }
}

-(void)checkInForSocialMedia:(SPNShareSocialMediaType)socialMedia
{
    switch (socialMedia) {
        case SPNShareSocialMediaTypeFacebook:
            [self shareWithFacebook:nil];
            break;
        case SPNShareSocialMediaTypeTwitter:
            [self shareWithTwitter:nil];
            break;
        default: //SPNShareSocialMediaTypeGooglePlus
            [self shareWithGoogle:nil];
            break;
    }
}

#pragma mark Google+ login functions

-(void)shareWithFacebook:(id)sender
{
    [self createShareDialog:nil];
}

-(void)createShareDialog:(NSNotification*)notification
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    
    NSString *postTitle = self.titleLabel.text;
    content.contentTitle = postTitle;
    
    NSString *urlImage;
    NSDictionary *posters = [self.movieInformation objectForKey:@"poster"];
    if ([posters count] > 0) {
        urlImage = [posters objectForKey:@"high"];
    }
    
    content.imageURL = [NSURL URLWithString:urlImage];
    content.contentDescription = [NSString stringWithFormat:@"I just bought tickets for %@ via @My_Cinelife", self.titleLabel.text];
    
    [FBSDKShareDialog  showFromViewController:self withContent:content delegate:self];
}


-(void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"succeeded");
}

-(void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    
    NSLog(@"cancel");
}

-(void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:@"Error sharing to Facebook" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

-(void)shareWithTwitter:(id)sender
{
    // Twitter takes the attached file as a link for 23 characters. Max Tweet length is 140, so this leaves 117 possible characters to share, but we also add the link to the app, which takes 22 - 23 characters, leaving us roughly 95 characters for input. Finally, we'll add @Ripple to each post, so we remove another 7 characters, leaving us with 88.
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    
    if ([[store existingUserSessions] count] > 0) {
        [self shareToTwitter];
    }
    else {
        [[Twitter sharedInstance] logInWithCompletion:^
         (TWTRSession *session, NSError *error) {
             if (session) {
                 NSLog(@"signed in as %@", [session userName]);
                 [self shareToTwitter];
             } else {
                 NSLog(@"error: %@", [error localizedDescription]);
             }
         }];
    }
}

-(void)shareToTwitter
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:[NSString stringWithFormat:@"I just bought tickets for %@ via @My_Cinelife", self.titleLabel.text]];
    [composer setImage:self.posterImageView.image];
    
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
        }
        
    }];
    
}

-(void)shareWithGoogle:(id)sender
{
    if ([[GIDSignIn sharedInstance] hasAuthInKeychain]) {
        [[GIDSignIn sharedInstance] signInSilently];
    }
    else {
        [[GIDSignIn sharedInstance] signIn];
    }
}

#pragma mark - GPP Sign in delegate functions
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (!error) {
        [self shareToGoogle:[NSURL URLWithString:@"https://cinelife.com/"]];
    }
    else {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
}

-(void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}


-(void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)finishedSharing:(BOOL)shared
{
    [[GPPShare sharedInstance] closeActiveNativeShareDialog];
}


-(void)shareToGoogle:(NSURL*)shareURL
{
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[shareURL absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
