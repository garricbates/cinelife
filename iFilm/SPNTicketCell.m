//
//  SPNTicketCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTicketCell.h"

@implementation SPNTicketCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.ticketsPurchased = 0;
    self.maxTicketsPossible = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
