//
//  Users.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/10/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "Users.h"
#import "Films.h"


@implementation Users

@dynamic userId;
@dynamic myFilms;

@end
