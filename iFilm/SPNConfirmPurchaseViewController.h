//
//  SPNConfirmPurchaseViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/4/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNConfirmPurchaseCell.h"  

#import "SPNCinelifeQueries.h"  

@interface SPNConfirmPurchaseViewController : UIViewController <SPNCinelifeQueriesDelegate>
@property NSDictionary *movieInformation;
@property NSDictionary *showtimeInformation;
@property NSDictionary *paymentInformation;
@property NSDictionary *cardInformation;
@property NSDictionary *ticketingInformation;
@property NSDictionary *branding;

@property NSString *email;
@property NSString *name;

@property IBOutlet UIScrollView *scrollView;

@property IBOutlet UILabel *swipeLabel;
@property IBOutlet UILabel *confirmLabel;

@property IBOutlet UIView *ticketContainerView;
@property IBOutlet UIView *sectionView;
// Movie information outlets
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *metaScoreLabel;
@property IBOutlet UILabel *userRatingLabel;
@property IBOutlet UILabel *genresLabel;
@property IBOutlet UILabel *classificationLabel;
@property IBOutlet UILabel *durationLabel;
@property IBOutlet UIImageView *posterImageView;

@property IBOutlet UIView *theatreBackgroundView;
@property IBOutlet UIImageView *theatreLogoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollOriginTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandLogoHeightConstraint;

// Tickets outlets
@property IBOutlet UILabel *dateLabel;
@property IBOutlet UILabel *showtimeLabel;
@property IBOutlet UILabel *ticketsLabel;
@property IBOutlet UILabel *ticketsTitleLabel;

@property IBOutlet UILabel *totalLabel;
@property IBOutlet UILabel *totalAmountLabel;

@property IBOutlet UILabel *totalConvenienceChargeLabel;
@property IBOutlet UILabel *convenienceChargeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreLeadConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaIconWidthConstraint;


//-------------------------//

@end
