//
//  NotificationsHelper.h
//  iFilm
//
//  Created by Vlad Getman on 04.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NotificationType)
{
    NotificationTypeGeneral = 0,
    NotificationTypeTheater,
    NotificationTypeFestival,
    NotificationTypeFilm,
    NotificationTypeMarketing
};

@interface NotificationsHelper : NSObject

+ (NotificationType)notificationTypeFromPushType:(NSInteger)type;

@end
