//
//  SPNFilmGWIQueueCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmGWIQueueCell.h"

@implementation SPNFilmGWIQueueCell

-(void)initializeCellForMovie:(NSDictionary *)movie
{
    [self.titleLabel setFont:[UIFont spn_NeutraBoldLarge]];
    
    [self.genreLabel setFont:[UIFont spn_NeutraSmall]];
    [self.scoreLabel setFont:[UIFont spn_NeutraSmall]];
    [self.durationLabel setFont:[UIFont spn_NeutraSmall]];
    [self.ratingLabel setFont:[UIFont spn_NeutraSmall]];
    
    [self.releaseYearLabel setFont:[UIFont spn_NeutraMedium]];
    [self.synopsisLabel setFont:[UIFont spn_NeutraMedium]];
    
    [self.releaseDateLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    [self.reviewButton.titleLabel setFont:[UIFont spn_NeutraBoldMedium]];
    
    self.reviewButton.layer.cornerRadius = 2.5;
    self.reviewButton.layer.masksToBounds = YES;
    
    self.titleLabel.text = [movie objectForKey:@"title"];
    NSString *description = [movie objectForKey:@"synopsis"];
    
    if (description.length > 0) {
        self.synopsisLabel.text = description;
    }
    else {
        self.synopsisLabel.text = @"No synopsis available";
    }
    
    NSArray *genres = [[movie objectForKey:@"genre"] componentsSeparatedByString:@"/"];
    NSString *joinedGenres = [genres componentsJoinedByString:@" | "];
    if (joinedGenres) {
        self.genreLabel.text =  joinedGenres;
    }
    else {
        self.genreLabel.text = @"No genre information available";
    }
    
    NSString *rating = [movie objectForKey:@"rate"];
    if (rating) {
        self.ratingLabel.text = rating;
    }
    else {
        self.ratingLabel.text = @"";
    }
    
    
    NSString *duration = [movie objectForKey:@"duration"];
    self.durationLabel.text = [NSString stringWithFormat:@"%@ min", duration];
    
    NSString *releaseYear = [movie objectForKey:@"year"];
    if (releaseYear) {
        self.releaseYearLabel.text = [NSString stringWithFormat:@"%@", releaseYear];
    }
    else {
        self.releaseYearLabel.text = @"Unkown year";
    }
    
    NSString *qualityRating = [movie objectForKey:@"rating"];
    NSNumber *score = [NSNumber numberWithFloat:[qualityRating floatValue]];
    if (score.floatValue > 0) {
        [self.scoreLabel setText:[NSString stringWithFormat:@"%.1f", score.floatValue*25]];
    }
    else {
        [self.scoreLabel setText:@"-"];
    }
}
@end
