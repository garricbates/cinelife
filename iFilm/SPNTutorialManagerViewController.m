//
//  SPNTutorialManagerViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTutorialManagerViewController.h"
#import "SPNTutorialStepsViewController.h"
#import "SPNNavigationController.h"

@interface SPNTutorialManagerViewController () {
    __weak IBOutlet NSLayoutConstraint *bottomConstraint;
    NSMutableArray *pageControllers;
}
@end

@implementation SPNTutorialManagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Tutorial";
    
    CGFloat bottomOffset = [UIDevice safeAreaInsets].bottom;
    if (bottomOffset > 0) {
        bottomConstraint.constant = bottomOffset;
        [self.view layoutIfNeeded];
    }
    
    [self customizeUI];
    [self initializeTutorial];
}

#pragma mark - Customization of UI
-(void)customizeUI
{
    [self.view setBackgroundColor:[UIColor spn_lightBrownColor]];
    [self.statusBarBackgroundView setBackgroundColor:[UIColor spn_darkGrayColor]];
    
    [self.remindMeLabel setFont:[UIFont spn_NeutraHuge]];
    [self.skipButton.titleLabel setFont:[UIFont spn_NeutraBoldHuge]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)skipButtonPressed:(id)sender {
    
    [self skipTutorial];
}

- (IBAction)remindMeSwitchValueChanged:(id)sender {
    UISwitch *btn = (UISwitch*)sender;
    [self remindMeLater:[btn isOn]];
}

#pragma mark tutorial functions
-(void)initializeTutorial
{
    self.tutorialController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.tutorialController.dataSource = self;
    
    [[self.tutorialController view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 70 - [UIDevice safeAreaInsets].bottom)];
    
    
    pageControllers = [NSMutableArray new];
    UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"TutorialStoryboard"
                                                                 bundle:nil];
    for (int i = 0; i < 3; i++) {
      
        
        SPNTutorialStepsViewController *tutorialController = [tutorialStoryboard instantiateViewControllerWithIdentifier:@"FirstSteps"];
        tutorialController.index = i;
        [pageControllers addObject:tutorialController];
    }
 
    
    //SPNTutorialStepsViewController *initialViewController = [self viewControllerAtIndex:0];
    
    @try {
    
        [self.tutorialController setViewControllers:@[[pageControllers firstObject]]  direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        
        
        [self addChildViewController:self.tutorialController];
        [[self view] addSubview:[self.tutorialController view]];
        
        
        [self.tutorialController didMoveToParentViewController:self];
        
        UIPageControl *pageControl = [UIPageControl appearance];
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.currentPageIndicatorTintColor  = [UIColor blackColor];
        pageControl.backgroundColor = [UIColor clearColor];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", exception);
    }
    @finally {
        
    }
}

-(void)skipTutorial
{
    if  ([[[[UIApplication sharedApplication] delegate] window] rootViewController] == self) {
        [self.delegate spnTutorialManagerDidClose];
    }
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];

  
    [self removeFromParentViewController];
   
}
    
-(void)remindMeLater:(BOOL)value
{
    NSString *willRemind = @"remind";
    if (!value) {
        willRemind = @"noRemind";
    }
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:willRemind
              forKey:@"tutorialOnStart"];
    [prefs synchronize];

}

#pragma mark - PageView functions
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [(SPNTutorialStepsViewController*) viewController index];
    index ++;
    
    if (index >2) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(SPNTutorialStepsViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (SPNTutorialStepsViewController *)viewControllerAtIndex:(NSUInteger)index
{
    return  [pageControllers objectAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}
@end
