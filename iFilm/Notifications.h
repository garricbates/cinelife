//
//  Notifications.h
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Notifications : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Notifications+CoreDataProperties.h"
