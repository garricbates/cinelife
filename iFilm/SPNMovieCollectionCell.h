//
//  SPNMovieCollectionCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/12/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNMovieCollectionCell : UICollectionViewCell

@property () IBOutlet UILabel *titleLabel;
@property () IBOutlet AsyncImageView *thumbnail;
@property IBOutlet UILabel *metaScoreLabel;

@property () NSString *imageURL;
-(void)initializeMovieSchedule:(NSDictionary*)schedule
             forCollectionView:(UICollectionView*)collectionView
                  forIndexPath:(NSIndexPath*)indexPath;
@end
