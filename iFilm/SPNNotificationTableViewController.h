//
//  SPNNotificationTableViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPNNotificationCell.h"
@interface SPNNotificationTableViewController : UITableViewController <UIAlertViewDelegate>

@property IBOutlet UISwitch *notificationSwitch;

@end
