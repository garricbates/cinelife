//
//  SPNIndieRSSCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNShowtimesCollectionView.h"

@interface SPNIndieRSSCell : UITableViewCell
{
    SPNShowtimesCollectionView *_horizontalCollectionView;
}

@property () IBOutlet SPNShowtimesCollectionView *horizontalCollectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

@end
