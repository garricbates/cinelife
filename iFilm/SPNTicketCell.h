//
//  SPNTicketCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTicketCell : UITableViewCell

@property IBOutlet UILabel* ticketNameLabel;
@property IBOutlet UILabel* totalTicketsLabel;
@property IBOutlet UILabel* priceLabel;

@property IBOutlet UIButton *addTicketButton;
@property IBOutlet UIButton *substractTicketButton;
@property NSInteger ticketsPurchased;
@property NSInteger maxTicketsPossible;
@end
