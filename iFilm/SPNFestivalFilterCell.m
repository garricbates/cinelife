//
//  SPNFestivalFilterCell.m
//  iFilm
//
//  Created by Vlad Getman on 25.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFestivalFilterCell.h"
#import "SPNAppDelegate.h"

@interface SPNFestivalFilterCell () <UISearchBarDelegate> {
    NSDictionary *fieldsData;
}

@property (nonatomic, weak) IBOutlet UIButton *filmFestivalsButton;
@property (weak, nonatomic) IBOutlet UIButton *filmsButton;
@property (nonatomic, weak) IBOutlet UIButton *qaButton;
@property (nonatomic, weak) IBOutlet UIButton *alternativeContentButton;
@property (weak, nonatomic) IBOutlet UIButton *artButton;
@property (weak, nonatomic) IBOutlet UIButton *liveArtsButton;
@property (nonatomic, weak) IBOutlet UIButton *nextDaysButton;
@property (nonatomic, weak) IBOutlet UIButton *nearbyButton;

@property (nonatomic, weak) IBOutlet UIButton *filterButton;

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *searchConstraints;

@end

@implementation SPNFestivalFilterCell

+ (NSDictionary *)titles {
    return @{@"filter_filmfest":@"Film Festivals",
             @"filter_filmspec":@"Films",
             @"filter_alt":@"Cinema Alternative Content",
             @"filter_qa":@"Q&As",
             @"filter_art":@"Art Shows, Exhibitions, Other",
             @"filter_live_art":@"Live Arts",
             @"starts_in_max_days":@"Next 30 Days",
             @"near_me":@"Near Me"};
}

+ (NSArray *)sortedKeys {
    return @[@"filter_filmfest", @"filter_filmspec", @"filter_alt", @"filter_qa", @"filter_art", @"filter_live_art", @"starts_in_max_days", @"near_me"];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    fieldsData = @{@"filter_filmfest":_filmFestivalsButton,
                   @"filter_filmspec":_filmsButton,
                   @"filter_alt":_alternativeContentButton,
                   @"filter_qa":_qaButton,
                   @"filter_art":_artButton,
                   @"filter_live_art":_liveArtsButton,
                   @"starts_in_max_days":_nextDaysButton,
                   @"near_me":_nearbyButton};
    
    [self.filterButton setBackgroundColor:[UIColor spn_buttonColor]];
    [self.filterButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    [keyboardDoneButtonView setBarTintColor:[UIColor spn_darkGrayColor]];
    keyboardDoneButtonView.translucent = YES;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                     style:UIBarButtonItemStylePlain target:self
                                                                    action:@selector(dismiss)];
    
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], cancelButton];
    
    self.searchBar.inputAccessoryView = keyboardDoneButtonView;
    
    if (@available(iOS 11.0, *)) {
        self.searchBar.searchBarStyle = UISearchBarStyleDefault;
        self.searchBar.tintColor = [UIColor clearColor];
        self.searchBar.backgroundImage = [UIImage imageWithSize:CGSizeMake(1, 1) andColor:[UIColor clearColor]];
        for (NSLayoutConstraint *constraint in self.searchConstraints) {
            constraint.constant += 5;
        }
        [self.contentView layoutIfNeeded];
    }
}

- (void)dismiss {
    if (self.searchBar.text.length == 0 && _filters[@"search"]) {
        [self applyFiltersAndCloseFilterCell:NO];
    }
    [self closeKeyboard];
}

- (void)closeKeyboard {
    [self endEditing:YES];
}

- (void)setFilters:(NSDictionary *)filters {
    _filters = filters;
    
    self.searchBar.text = filters[@"search"];
    
    for (NSString *key in filters.allKeys) {
        BOOL value = [filters[key] boolValue];
        UIButton *button = fieldsData[key];
        if (value != button.selected) {
            [self checkBoxAction:button fromUser:NO];
        }
    }
}

- (void)applyFiltersAndCloseFilterCell:(BOOL)close {
    NSMutableDictionary *filters = [NSMutableDictionary new];
    if (self.searchBar.text.length > 0) {
        filters[@"search"] = self.searchBar.text;
    }
    
    for (NSString *key in fieldsData.allKeys) {
        UIButton *button = fieldsData[key];
        if (button.selected) {
            filters[key] = @YES;
        }
    }
    [self.delegate didApplyFilters:filters andCloseFilterCell:close];
    [self closeKeyboard];
}

- (IBAction)filterAction {
    [self applyFiltersAndCloseFilterCell:YES];
}

- (IBAction)checkBoxAction:(id)sender {
    [self checkBoxAction:sender fromUser:YES];
}

- (void)checkBoxAction:(id)sender fromUser:(BOOL)fromUser {
    UIButton *button;
    if (![sender isKindOfClass:[UIButton class]]) {
        button = sender;
    } else {
        button = [[sender superview] viewWithTag:1];
    }
    if (_nearbyButton == button && [appDelegate myLocation] == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Could not find you" message:@"We could not determine your location. Make sure you have granted location services to the app and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return;
    }
    button.selected = !button.selected;
    button.backgroundColor = button.selected ? [UIColor spn_aquaColor] : [UIColor whiteColor];
    if (fromUser) {
        [self applyFiltersAndCloseFilterCell:NO];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self applyFiltersAndCloseFilterCell:NO];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (!searchBar.isFirstResponder && searchText.length == 0) {
        [self applyFiltersAndCloseFilterCell:NO];
    }
}


@end
