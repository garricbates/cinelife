//
//  SPNServerQueries.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/11/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define kMetaQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)

//static NSString* server = @"http://developmentlcp.com/spotlight/web/ws.php/";
static NSString* apiOldServer = @"http://developmentlcp.com/spotlight/web/wws.php/api";
//static NSString* apiServer = @"https://developmentlcp.com/cinelife/web/wws.php/api";

#if DEV
static NSString *apiServer = @"http://ec2-52-24-221-96.us-west-2.compute.amazonaws.com/web/wws.php/api";
#else
static NSString *apiServer = @"https://cinelife.com/web/wws.php/api";
#endif

static NSString* apiKey = @"B82599A8B22C8172638C472AD5EB5";
typedef NS_ENUM(NSInteger, SPNTheatreCategory)
{
    SPNTheatreCategoryNews = 0,
    SPNTheatreCategoryPromos,
    SPNTheatreCategoryEvents
};

@interface SPNServerQueries : NSObject

+(NSString*)posterURL;
+(void)setPosterURL:(NSString*)posterURL;

+(NSString*)trailerURL;
+(void)setTrailerURL:(NSString*)trailerURL;
//---------------------------------------Server Query--------------------------------------------//

/**
 * A synchronous method to send a petition to the server.
 * @author La Casa de los Pixeles
 * @param dataURL The URL to query. This includes the server and the method to call along with the designated parameters.
 * @return A dictionary (formatted from a JSON response) with the server's response, or nil if no connection was established, or if the server was unavailable.
 */
+ (NSDictionary*)queryURL:(NSURL*)dataURL;

//---------------------------------------Newsfeed queries --------------------------------------------//
/**
 * Method to obtain the top two news of a given theatre. News are divided into categories, but this method returns the latest news of any category, sorted by date.
 * @author La Casa de los Pixeles
 * @param theatreId The WestWorldMedia/Spotlight Network ID of the theatre.
 * @return An array with the top two news of the theatre, or nil, in case the theatre is not affiliated or does not contain any news.
 */
+(NSArray*)getTheatreLastPosts:(NSString*)theatreId;

/**
 * Method to obtain the list of news for a given theatre, sorted by date.
 * @author La Casa de los Pixeles
 *
 * @param theatreId The WestWorldMedia/Spotlight Network ID of the theatre.
 * @param category The SPNTheatreCategory, which can be: News, Promos and Events
 * @return An array of all posts for the theatre, or nil, in case the theatre is not affiliated or does not contain any news.
 */
+(NSArray*)getTheatreAllPosts:(NSString *)theatreId;


/**
 * Method to obtain the details of a news post of a given theatre. The details include title, summary, and the content which is in HTML text.
 * @author La Casa de los Pixeles
 *
 * @param postId The ID of the post to query.
 * @return A dictionary with the information of the news post.
 */
+ (NSDictionary*)getTheatrePostDetails:(NSString*)postId;


+(NSDictionary*)getComingSoonPostsForTheatre:(NSString*)theatreId;
//---------------------------------------Movie queries --------------------------------------------//
/**
 * Method to obtain the details of a particular movie.
 * @author La Casa de los Pixeles
 *
 * @param movieId The WestWorldMedia ID of the movie.
 * @return A dictionary with the information of the movie, with cast, synopsis, etc. */
+(NSDictionary*)getInfoForMovie:(NSString*)movieId;

/**
 * Method to obtain the poster URL of a particular movie. This follows the format used by WestWorldMedia to get movie posters and images.
 * @author La Casa de los Pixeles
 *
 * @param movieId The WestWorldMedia ID of the movie.
 * @return A string with the poster link of the specified movie */
+(NSString*)getMovieImageURL:(NSString*)movieId;

+(NSArray*)getMovieStills:(NSString*)movieId;

/**
 * Method to obtain the trailer URL of a particular movie. This follows the format used by WestWorldMedia to get movie trailers in .mp4 format, that the device can use.
 * @author La Casa de los Pixeles
 *
 * @param movieId The WestWorldMedia ID of the movie.
 * @return A string with the trailer link of the specified movie */
+(NSString*)getTrailerURLForMovie:(NSString*)movieId;

/**
 * Method to validate a URL. This method sends a synchronous request and looks at the result. If there is an error, or a 404 response, then the method returns NO.
 * @author La Casa de los Pixeles
 *
 * @param url The URL to validate.
 * @return YES, if the URL is valid, NO otherwise. */
+ (BOOL)isValidURL:(NSURL*)url;

/**
 * Method to add a review to a movie. Requires the user to be logged in. The review can contain a comment, and/or a rating.
 * @author La Casa de los Pixeles
 *
 * @param username The uid of the user.
 * @param movieId The WestWorldMedia ID of the movie.
 * @param comment The user's comments for the specified movie. Can be nil.
 * @param rating The user's rating on a movie. Can be nil.
 * @return YES, if the review was successfully added to the movie. NO otherwise. */
+(BOOL)addCommentOrRatingForUser:(NSString*)username
                        forMovie:(NSString*)movieId
                     withComment:(NSString*)comment
                       andRating:(NSUInteger)rating
                        andTitle:(NSString*)title;

/**
 * Method to obtain the reviews of a movie.
 * @author La Casa de los Pixeles
 *
 * @param movieId The WestWorldMedia ID of the movie.
 * @return A dictionary containing the reviews of the specified movie, along with the average rating of the users. They incude the user's name, the rating and comments.
 */
+(NSDictionary*)getCommentOrRatingForMovie:(NSString*)movieId;


+(NSDictionary*)getCommentOfUser:(NSString*)username forMovie:(NSString*)movieId;
/**
 * Method to obtain the cast and crew of a movie.
 * @author La Casa de los Pixeles
 *
 * @param The movie object to extract the cast and crew from.
 * @return A dictionary with the cast and crew for a movie, divided into two keys: cast, and crew.
 */
+(NSDictionary*)getCastAndCrewForMovie:(NSDictionary*)movie;

//---------------------------------------Theatre queries --------------------------------------------//

/**
 * Method to obtain the ticketing the ticketing information for a movie in a theatre, with the specified date and time.
 * @author La Casa de los Pixeles
 * @param theatreId The WestWorldMedia/Spotlight Network ID of the theatre.
 * @param movieId The WestWorldMedia ID of the movie.
 * @param date An NSDate with the specified date of the show. 
 * @param showtime A string representing the hour of the day on when the show starts. It must have a HH:mm format (i.e. 13:15)
 * @return If the theatre is affiliated and has access to the RTS POS, a dictionary with the type of tickets available will be returned, along with the performance Id, and the available seats. Otherwise, a string with the URL for the POS of the movie. This link can get to the ticketing system of the theatre, or to the main page of the theatre. Nil, in case no link was available.
 */
+(id)getTicketsForTheatre:(NSString*)theatreId
                 forMovie:(NSString*)movieId
                  forDate:(NSDate*)date
              andShowtime:(NSString*)showtime;


+(id)purchaseTickets:(NSArray*)tickets
           forAmount:(NSString*)amount
    forPerformanceId:(NSString*)performanceId
          forTheatre:(NSString*)rtsTheatreId
       forAuditorium:(NSString*)auditorium
 withCardInformation:(NSDictionary*)cardDetails
     andEmailAddress:(NSString*)email
      andFilmDetails:(NSDictionary*)filmDetails
     withTicketNames:(NSArray*)ticketNames;
/**
 * Method to obtain the theatre's address in a single string.
 * @author La Casa de los Pixeles
 * @param theatre The WestWorldMedia/Spotlight theatre object, obtained by any of the theatre queries.
 * @return A string with the theatre's address, including zip code, city, and street.
 */
+ (NSString*)getTheatreAddress:(NSDictionary*)theatre;

/**
 * Method to obtain the showtimes of a particular theatre. This method returns the title and movieId of each movie playing in a particular theatre.
 * @author La Casa de los Pixeles
 * @param theatreId The WestWorldMedia/Spotlight Network ID of the theatre.
 * @param date An NSDate with the specified date of the schedule.
 * @return An array with the different movies playing in a particular theatre.
 */

+ (NSDictionary*)getShowtimesForTheater:(NSString*)theaterId
                                andDate:(NSDate*)date;
//---------------------------------------Location queries --------------------------------------------//

/**
 * Method to obtain theatres on a specified zipcode, sorted by distance to the user.
 * @author La Casa de los Pixeles
 * @param zipcode The zipcode to search for thatres.
 * @param userLocation The CLLocation of the device. Required to sort the theatres by distance, but may be nil. In that case, the theatres will be unsorted.
 * @return A dictionary of theatre objects located near the specified zipcode. They are divided as Arthouse theatres and WestWorldMedia theatres.
 */
+(NSDictionary*)getTheatresForZipCode:(NSString *)zipcode
                     withUserLocation:(CLLocation*)userLocation
                            andRadius:(NSInteger)radius;


/**
 * Method to obtain theatres on a given coordinate pair (latitude, longitude), sorted by distance to the user.
 * @author La Casa de los Pixeles
 * @param coordinates A CLLocationCoordinate object containing latitude and longitude values
 * @param userLocation The CLLocation of the device. Required to sort the theatres by distance, but may be nil. In that case, the theatres will be unsorted.
 * @return A dictionary of theatre objects located near the specified zipcode. They are divided as Arthouse theatres and WestWorldMedia theatres.
 */
+(NSDictionary*)getTheatresForCoordinates:(CLLocationCoordinate2D)coordinates
                         withUserLocation:(CLLocation*)userLocation
                                andRadius:(NSInteger)radius;
 
/**
 * Method to obtain films playing on theatres on a specified zipcode, sorted alphabetically.
 * @author La Casa de los Pixeles
 * @param zipcode The zipcode to search for films.
 * @param date An NSDate with the specified date of the showtimes.
 * @return An array of film objects playing in theatres near the specified zipcode. These objects contain ids, and titles.
 */
+(NSArray*)getShowtimesForDate:(NSDate*)date
                    forZipcode:(NSString*)zipcode
                  withPartners:(BOOL)showPartners;

/**
 * Method to obtain films playing on theatres on a specified coordinate pair (latitude, longitude), sorted alphabetically.
 * @author La Casa de los Pixeles
 * @param coordinates A CLLocationCoordinate object containing latitude and longitude values
 * @param date An NSDate with the specified date of the showtimes.
 * @return An array of film objects playing in theatres near the specified zipcode. These objects contain ids, and titles.
 */
+(NSArray*)getShowtimesForDate:(NSDate*)date
                forCoordinates:(CLLocationCoordinate2D)coordinates
                  withPartners:(BOOL)showPartners;
/**
 * Method to obtain films that will be released in the near future.
 * @author La Casa de los Pixeles
 * @return An dictionary of film objects that will be released in the near future (up to three months from today). Each dictionary key corresponds to a date, and contains on or more film objects. These objects contain ids, and titles.
 */
+(NSDictionary*)getFutureReleases;

/**
 * Method to obtain films playing in indie festials. This method is a placeholder. Data is obtained from the Indieflix API. WILL CHANGE SOON
 * @author La Casa de los Pixeles
 * @return An array of film objects playing in indie festivals. NOT MEANT FOR PUBLIC RELEASE */
+(NSArray*)getFestivalFilms;

/**
 * Method to obtain showtimes of a particular movie playing near a specified zipcode, sorted by distance to the user.
 * @author La Casa de los Pixeles
 * @param movieId The WestWorldMedia ID of the movie.
 * @param date An NSDate with the specified date of the showtimes.
 * @param zipcode The zipcode to search for thatres.
 * @param userLocation The CLLocation of the device. Required to sort the theatres by distance, but may be nil. In that case, the theatres will be unsorted.
 * @return A dictionary of theatre objects, with showtimes of the selected movie. They are divided as Arthouse theatres and WestWorldMedia theatres.
 */
+(NSDictionary*)getShowtimesForFilm:(NSString*)movieId
                            forDate:(NSDate*)date
                         forZipcode:(NSString*)zipcode
                   withUserLocation:(CLLocation*)userLocation
                          andRadius:(NSInteger)radius;

/**
 * Method to obtain showtimes of a particular movie playing near a specified coordinate pair (latitude, longitude), sorted by distance to the user.
 * @author La Casa de los Pixeles
 * @param movieId The WestWorldMedia ID of the movie.
 * @param date An NSDate with the specified date of the showtimes.
 * @param coordinates A CLLocationCoordinate object containing latitude and longitude values
 * @param userLocation The CLLocation of the device. Required to sort the theatres by distance, but may be nil. In that case, the theatres will be unsorted.
 * @return A dictionary of theatre objects, with showtimes of the selected movie. They are divided as Arthouse theatres and WestWorldMedia theatres.
 */
+(NSDictionary*)getShowtimesForFilm:(NSString*)movieId
                            forDate:(NSDate*)date
                     forCoordinates:(CLLocationCoordinate2D)coordinates
                   withUserLocation:(CLLocation*)userLocation
                          andRadius:(NSInteger)radius;
/**
 * Method to obtain the hottest films according to RottenTomatoes.com. This method is a placeholder. WILL CHANGE SOON.
 * @author La Casa de los Pixeles
 * @return An array of film objects from Rotten Tomatoes. NOT MEANT FOR PUBLIC RELEASE
 */
+(NSArray*)getRottenTomatoFilms;

/**
 * Method to obtain a list of indie and festival news. This method is a placeholder. WILL CHANGE SOON.
 * @author La Casa de los Pixeles
 * @return An array of news objects. NOT MEANT FOR PUBLIC RELEASE
 */
+(NSDictionary*)getSundanceAndIndieNews;

/**
 * Method to obtain a film's detailed information from the title. This method is used to link movies from GoWatchIt or Rotten Tomatoes with WestWorldMedia.
 * @author La Casa de los Pixeles
 * @param title The title of the film to seach.
 * @return A film object, possibly matching the given title. This method is not 100% accurate and may not return a film at all.
 */
+(NSDictionary*)searchFilmByTitle:(NSString*)title;

/**
 * Method to obtain the list of posts of an affiliated theatre. This posts can be events, promos, news, etc. and the theatres upload them to the Spotlight dashobard.
 * @author La Casa de los Pixeles
 * @param theatreId The WWM theatreId of the affiliated theatre.
 * @param postCount The maximum number of posts to fetch.
 * @return An array of post objects. This posts contain a title, summary, date, and category. Returns nil, if there are no posts available, or if the theatre is not affiliated to Spotlight.
 */
+(NSArray*)getNewsfeedStoriesForPages:(NSArray*)newsfeedList
                        withPostCount:(NSInteger)postCount;

/**
 * Method to obtain the list of RSS posts of an affiliated theatre. These posts may come from their blog, Facebook, Twitter, etc. Provided that they are in atom format. This method uses a Google service to parse the posts into readable JSON dictionaries.
 * @author La Casa de los Pixeles
 * @param theatreId The WWM theatreId of the affiliated theatre.
 * @param limit The maximum number of posts to fetch.
 * @return An array of post objects. This posts contain a title, summary, date, and link. Returns nil, if there are no posts available.
 */
+(NSArray*)getRSSForTheatre:(NSString*)theatreId
                   andLimit:(NSInteger)limit;

+(NSArray*)getRSSForTheatre:(NSString*)theatreId
                   andLimit:(NSInteger)limit
                 forRSSType:(NSString*)type;

/**
 * Method to obtain the MetaScore rating of a particular film. This method can fail since it has to do a name comparison between WWM and MetaScrore.
 * @author La Casa de los Pixeles
 * @param movieTitle The WWM movie title to search.
 * @return A string with the MetaScore rating (0 - 100). Returns nil if the movie title could not be matched to the MetaScore title.
 */
+(NSString*)getMetascoreForMovie:(NSString*)movieTitle;

/**
 * Method to obtain the MetaScore reviews of a particular film. This method can fail since it has to do a name comparison between WWM and MetaScrore.
 * @author La Casa de los Pixeles
 * @param movieTitle The WWM movie title to search.
 * @return A dictionary containing the rating and reviews of a particular movie. Returns nil if the movie title could not be matched to the MetaScore title.
 */
+(NSDictionary*)getMetaReviewsForMovie:(NSString*)movieTitle;

/**
 * Method to obtain the RSS feed of CineLife. This RSS feed consists of several rss pages, mostly coming from IndieWire.
 * @author La Casa de los Pixeles
 * @return An array of feed objects for the CineLife RSS feed. They consist of the name and the link to the Atom10 feeds.
 */
+(NSArray*)getIndieWireFeeds;

/**
 * Method to obtain the Trivia used in the CineLife section. This trivia can be modified from the dashboard.
 * @author La Casa de los Pixeles
 * @return A dictionary containing the questions and answers, plus facts from the trivia.
 */
+(NSDictionary*)getCineLifeTrivia;

//DUMMY QUERY
+(NSDictionary*)getTicketingExample;
//DUMMY QUERY
+(NSArray*)getNewsfeedPages;

/**
 * Method to obtain the Passbook information for a ticketing purchase done through RTS.
 * @author La Casa de los Pixeles
 * @param passbookInformation The dictionary containing all the necessary information for the purchase, including the barcode, the number of tickets, the name of the movie, the show date and time, and the name of the theatre.
 * @return A PKPass object ready to be stored in the device's Passbook. Returns nil if there was an error (such as an invalid pass, etc.)
 */
+(PKPass*)getPassbookData:(NSDictionary*)passbookInformation;

/**
 * Method to fetch the base URL for the trailers and movie posters. This URL may vary (the API may change their root directory of posters, etc.), so it is necessary to get the updates in this query. The information is stored internally on the device and this method is only called once per app launch.
 * @author La Casa de los Pixeles
*/
+(void)getBaseURLsForTrailerAndPoster;

/**
 * Method to obtain a list of films coming from Spotlight's partner theatres. The partner theatre's list can be updated from the dashboard.
 * @author La Casa de los Pixeles
 * @return  An array of film objects playing in partner theatres. These objects contain ids, and titles.
 */
+(NSArray*)getFilmsFromPartners;

/**
 * Method to obtain the id list of the partner theatres. The partner theatre's list can be updated from the dashboard.
 * @author La Casa de los Pixeles
 * @return  An array of WWM theatre ids that belong to Spotlight's partners. This is used to identify and brand as "Partners" in the theatre searches.
 */
+(NSArray*)getCineLifePartners;
@end
