//
//  SPNIndiePostDetailsViewController.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface SPNIndiePostDetailsViewController : GAITrackedViewController

@property NSString *postURL;
@property IBOutlet UIWebView *postWebView;
@end
