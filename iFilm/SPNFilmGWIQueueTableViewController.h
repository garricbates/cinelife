//
//  SPNFilmGWIQueueTableViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/26/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNFilmGWIQueueTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *queuedFilms;
@property () NSString *lastKnownLocation;
@end
