//
//  SPNFestivalFilterCell.h
//  iFilm
//
//  Created by Vlad Getman on 25.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SPNFestivalFilterCellDelegate <NSObject>

- (void)didApplyFilters:(NSDictionary *)filters andCloseFilterCell:(BOOL)close;

@end

@interface SPNFestivalFilterCell : UITableViewCell

@property (nonatomic, weak) id <SPNFestivalFilterCellDelegate> delegate;
@property (nonatomic, strong) NSDictionary *filters;

+ (NSDictionary *)titles;
+ (NSArray *)sortedKeys;

@end
