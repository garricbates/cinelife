//
//  SPNTutorialStepsViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTutorialStepsViewController.h"

@interface SPNTutorialStepsViewController ()

@end

@implementation SPNTutorialStepsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Tutorial Screen";
    // Do any additional setup after loading the view.
    [self customizeUI];
    [self initializeTutorialScreen];
}


#pragma mark UI Customization
-(void)customizeUI
{
    [self.screenDescriptionLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.screenTitleLabel setFont:[UIFont spn_NeutraBoldTera]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#define ResourcePath(path)[[NSBundle mainBundle] pathForResource:path ofType:@"png"]

#define ImageWithPath(path)[UIImage imageWithContentsOfFile:path]

#pragma mark Tutorial functions
-(void)initializeTutorialScreen
{
    UIImage* image;
    NSString *title;
    NSString *description;
    switch (self.index) {
        case 1:
            image = [UIImage imageNamed:@"step1"];
            title = @"Find Films";
            description = @"Discover quality movies playing in theatres near you and track your favorite films.";
            break;
        case 0:
            title = @"Favorite Theatres";
            description = @"Select your favorite theatres by clicking on the heart icon. Adding theatres to the favorites list ensures you will have access to custom theatre content, special events and invitation-only screenings.";
            image = [UIImage imageNamed:@"step2"];
            break;
        case 2:
            title = @"My CineLife";
            description = @"Track your favorited films, read exclusive blogs, see what's playing at festivals and get up-to-date information on the world of art and independent film.";
            image = [UIImage imageNamed:@"step3"];
            break;
        default:
            image = [UIImage imageNamed:@"step1"];
            break;
    }

    image = [image imageWithAlignmentRectInsets:UIEdgeInsetsMake(-20, 0, 0, 0)];
    NSMutableAttributedString *screenText = [[NSMutableAttributedString alloc] initWithString:[title stringByAppendingString:[@"\r\n" stringByAppendingString:description]] attributes:@{NSFontAttributeName : [UIFont spn_NeutraMediumLarge]}];
    [screenText beginEditing];
    [screenText addAttribute:NSFontAttributeName
                       value:[UIFont spn_NeutraBoldTera]
                       range:NSMakeRange(0, [title length])];
    [screenText endEditing];
    [self.screenTitleLabel setAttributedText:screenText];

    
    [self.screenBackgroundImageView setImage:image];
}

@end
