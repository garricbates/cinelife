//
//  SPNPurchaseCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/17/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNPurchaseCell.h"

@implementation SPNPurchaseCell

-(void)initializePurchaseCell:(Purchases *)purchaseInformation forRowAtIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView withDateFormatter:(NSDateFormatter *)dateFormatter
{
    
    [self.movieTitleLabel setFont:[UIFont spn_NeutraBoldHuge]];
    [self.showDateLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.showTimeLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.ticketsLabel setFont:[UIFont spn_NeutraSmallMedium]];
    
    NSString *movieTitle = purchaseInformation.movieTitle;
    NSArray *separatedTitle = [movieTitle componentsSeparatedByString:@", The"];
    
    if ([separatedTitle count] < 2) {
        separatedTitle = [movieTitle componentsSeparatedByString:@", A"];
        if ([separatedTitle count] < 2) {
            self.movieTitleLabel.text = movieTitle;
        }
        else {
            self.movieTitleLabel.text = [NSString stringWithFormat:@"A %@", [separatedTitle firstObject]];
        }
    }
    else {
        self.movieTitleLabel.text = [NSString stringWithFormat:@"The %@", [separatedTitle firstObject]];
    }

    // Date formatting
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEEdMMMM" options:0
                                                              locale:[NSLocale currentLocale]];

    [dateFormatter setDateFormat:formatString];
    NSDate *selectedDate = purchaseInformation.showDate;
    NSString *todayString = [dateFormatter stringFromDate:selectedDate];
    
    todayString = [@"Date: " stringByAppendingString:todayString];
    NSRange boldFontRange = NSMakeRange(0, 5);
    
    NSMutableAttributedString *dateText = [[NSMutableAttributedString alloc] initWithString:todayString];
    
    [dateText beginEditing];
    [dateText addAttribute:NSFontAttributeName
                     value:[UIFont spn_NeutraBoldMediumLarge]
                     range:boldFontRange];
    [dateText endEditing];
    [self.showDateLabel setAttributedText:dateText];
    
    // Time formatting
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [dateFormatter dateFromString:purchaseInformation.showTime];
    [dateFormatter setDateFormat:@"h:mma"];
    NSString* showDate = [dateFormatter stringFromDate:date];
    
    NSString *showTimeString = [@"Showtime: " stringByAppendingString:showDate];
    boldFontRange = NSMakeRange(0, 9);
    
    NSMutableAttributedString *showTimeText = [[NSMutableAttributedString alloc] initWithString:showTimeString];
    
    [showTimeText beginEditing];
    [showTimeText addAttribute:NSFontAttributeName
                     value:[UIFont spn_NeutraBoldMediumLarge]
                     range:boldFontRange];
    [showTimeText endEditing];
    [self.showTimeLabel setAttributedText:showTimeText];
    
    // Ticket formatting
    NSString *ticketString = [@"" stringByAppendingString:purchaseInformation.tickets];
    boldFontRange = NSMakeRange(0, 10);
    
    NSMutableAttributedString *ticketsText = [[NSMutableAttributedString alloc] initWithString:ticketString];
    
    [ticketsText beginEditing];
    [ticketsText endEditing];
    [self.ticketsLabel setAttributedText:ticketsText];
    
    
 
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.posterImageView.image = [UIImage imageNamed:@"poster-not-available"];

    NSString *urlImage = [SPNServerQueries getMovieImageURL:purchaseInformation.movieId];
    urlImage = [urlImage stringByReplacingOccurrencesOfString:@"https://" withString:@"http://"];

    if (urlImage) {
        NSRange result = [urlImage rangeOfString:@"default_movie"];
        if (result.length < 1) {
            dispatch_async(kBgQueue, ^{
                NSInteger index = indexPath.row;
                [self.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
                [self.posterImageView setImage:[UIImage imageNamed:@"poster-not-available"]];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    SPNPurchaseCell *updateCell = (SPNPurchaseCell*)[tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell && index == indexPath.row) {
                        [self.posterImageView setImageURL:[NSURL URLWithString:urlImage]];
                    }
                });
            });
        }
    }

}

@end
