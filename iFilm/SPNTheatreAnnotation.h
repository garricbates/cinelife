//
//  SPNTheatreAnnotation.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/16/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPNTheatreAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, copy) NSString* city;
@property (nonatomic, copy) NSString* country;
@property (nonatomic, copy) NSString* state;
@property (nonatomic, copy) NSString* postalCode;
@property (nonatomic, copy) NSString* address;
@property (nonatomic, copy) NSString* objectId;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic) BOOL isFestival;

@end
