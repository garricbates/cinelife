//
//  Notification+CoreDataProperties.m
//  iFilm
//
//  Created by Vlad Getman on 19.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "Notification+CoreDataProperties.h"

@implementation Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
}

@dynamic notificationId;
@dynamic message;
@dynamic typeId;
@dynamic theaterId;
@dynamic festivalId;
@dynamic filmId;
@dynamic date;

@end
