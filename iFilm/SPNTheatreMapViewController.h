//
//  SPNTheatreMapViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/16/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPNBaseTheaterViewController.h"

@interface SPNTheatreMapViewController : SPNBaseTheaterViewController

@end
