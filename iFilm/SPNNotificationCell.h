//
//  SPNNotificationCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 10/14/15.
//  Copyright © 2015 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SPNNotificationCell;

@protocol SPNNotificationCellDelegate <NSObject>

- (void)didPressClose:(SPNNotificationCell *)cell;
- (void)didPressDelete:(SPNNotificationCell *)cell;

@end

@interface SPNNotificationCell : UITableViewCell

@property (nonatomic, weak) id <SPNNotificationCellDelegate> delegate;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

@end
