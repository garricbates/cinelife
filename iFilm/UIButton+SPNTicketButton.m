//
//  UIButton+SPNTicketButton.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "UIButton+SPNTicketButton.h"
#import <objc/runtime.h>

NSString * const kDHTheatreKey = @"kDTheatreIdKey";

@implementation UIButton (SPNTicketButton)

-(void)setTheatreId:(NSString *)theatreId
{
    objc_setAssociatedObject(self, (__bridge const void *)(kDHTheatreKey), theatreId, OBJC_ASSOCIATION_COPY);
}

-(NSString *)theatreId{
    return objc_getAssociatedObject(self, (__bridge const void *)(kDHTheatreKey));
}
@end
