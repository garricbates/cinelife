//
//  SPNPaymentInfoCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/3/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNPaymentInfoCell : UITableViewCell

@property IBOutlet UILabel *paymentTitleLabel;

@property IBOutlet UITextField *cardNumberTextField;
@property IBOutlet UITextField *cardCVCTextField;
@property IBOutlet UITextField *cardExpirationDateTextField;
@property IBOutlet UITextField *postalTextField;

@end
