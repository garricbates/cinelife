//
//  SPNFestivalViewController.h
//  iFilm
//
//  Created by Vlad Getman on 19.04.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Festival;

@interface SPNFestivalViewController : UIViewController

@property (nonatomic, retain) Festival *festival;

@end
