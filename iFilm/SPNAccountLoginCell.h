//
//  SPNAccountLoginCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNAccountLoginCell : UITableViewCell

@property () IBOutlet UILabel *loginLabel;

@property () IBOutlet UIButton *facebookButton;
@property () IBOutlet UIButton *twitterButton;
@property () IBOutlet UIButton *googlePlusButton;
@property () IBOutlet UIButton *spotlightButton;

@property () IBOutlet UIButton *forgotPasswordButton;

-(void)initializeCell;

@end
