//
//  SPNFilmSearchViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/18/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmSearchViewController.h"
#import "SPNFilmDetailsViewController.h"

#import "SPNFilmSearchCell.h"
#import "SPNMovieCollectionCell.h"
#import "SPNFilterOptionCell.h"

#import "SPNFilmSearchHeaderView.h"


#define kFilterCellHeight 37
#define kFilterOptionCellHeight 194

static NSString *FilterCellIdentifier = @"FilterCell";

@interface SPNFilmSearchViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, CLLocationManagerDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, SPNCinelifeQueriesDelegate>
{
    CLLocation *userLocation;
    SPNFilmType selectedFilmType;
    
    NSDictionary *selectedFilmForDetails;
    
    NSArray *futureFilmDates;
    
    BOOL shouldFetchThearesNearby, shouldShowFilterOptions;
    NSDateFormatter *titleFormatter;
    
    BOOL checkedPartners, checkedNearby, isFetchingLocation;
    UIToolbar *keyboardDoneButtonView;
    UIButton *myCheckedPartner, *myCheckedNearby;
    UIView *searchBarBackgroundView;
    UILabel *filterTextLabel;
    
}

@property (nonatomic, retain) NSDictionary *sortedFutureFilms;
@property (nonatomic, retain) NSArray *filmsInRegion;
@property (nonatomic, retain) NSArray *futureFilms;

@property (nonatomic, retain) UISearchBar *zipcodePicker;

@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic, retain) SPNFilterCell *prototypeFilterCell;

@property (nonatomic, weak) IBOutlet UITableView *filmTableView;

@property (nonatomic, weak) IBOutlet UISegmentedControl *filmSearchOptionSegmentedControl;

- (IBAction)filmSearchOptionSegmentedControlPressed:(id)sender;

@end

@implementation SPNFilmSearchViewController


-(CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
#ifdef __IPHONE_8_0
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            [_locationManager requestWhenInUseAuthorization];
        }
#endif
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    return _locationManager;
}

-(SPNFilterCell *)prototypeFilterCell
{
    if (!_prototypeFilterCell) {
        _prototypeFilterCell = (SPNFilterCell*)[self.filmTableView dequeueReusableCellWithIdentifier:FilterCellIdentifier];
    }
    return _prototypeFilterCell;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.filmTableView reloadData];
    [[SPNCinelifeQueries sharedInstance] setDelegate:self];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingLocation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Film Search";
    isFetchingLocation = NO;
    
    
    shouldShowFilterOptions = NO;

    titleFormatter = [NSDateFormatter new];
    [titleFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
   
    // Do any additional setup after loading the view.
      selectedFilmType = SPNFilmTypeFilmsInRegion;
    
    [self initializeKeyboard];
    
    [self.filmTableView registerCellClass:[SPNFilterCell class] withIdentifier:FilterCellIdentifier];
    [self.filmTableView registerCellClass:[SPNFilmSearchCell class] withIdentifier:@"FilmCell"];
    [self.filmTableView setTableFooterView:[UIView new]];
    
    checkedNearby = NO;
    checkedPartners = YES;
    [self getPartnerFilms];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureCell:(SPNFilterCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;
{
    [cell.filterTextLabel setText:filterTextLabel.text];
}

-(void)checkPartners:(id)sender
{
    if (checkedPartners) {
        [myCheckedPartner setBackgroundColor:[UIColor whiteColor]];
    }
    else {
        [myCheckedPartner setBackgroundColor:[UIColor spn_aquaColor]];
    }
    checkedPartners = !checkedPartners;
    if (checkedPartners) {
        [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"All films " withString:@"Art and Indie films "]];
        [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"(enter city or zip code below)" withString:@""]];
//        [filterTextLabel setText:[@"Art and Indie films " stringByAppendingString:filterTextLabel.text ]] ;
    }
    else {
        [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"Art and Indie films " withString:@"All films "]];
        if (!checkedNearby) {
            [filterTextLabel setText:[filterTextLabel.text stringByAppendingString:@"(enter city or zip code below)"]];
        }
    }
   
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];
}

-(void)checkNearby:(id)sender
{
    if (checkedNearby) {
        [myCheckedNearby setBackgroundColor:[UIColor whiteColor]];
        [self.zipcodePicker setUserInteractionEnabled:YES];
        [searchBarBackgroundView setBackgroundColor:[UIColor whiteColor]];

    }
    else {
        [myCheckedNearby setBackgroundColor:[UIColor spn_aquaColor]];
        [self.zipcodePicker setUserInteractionEnabled:NO];
        [searchBarBackgroundView setBackgroundColor:[UIColor grayColor]];
    }
    checkedNearby = !checkedNearby;
    [filterTextLabel setText:@""];
    
    if (checkedPartners) {
        [filterTextLabel setText:@"Art and Indie films "];
    }
    else {
        if ([filterTextLabel.text length] > 0) {
            [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"Art and Indie films " withString:@"All films "]];
        }
        else {
             [filterTextLabel setText:@"All films (enter city or zip code below)"];
        }
    }

    if (checkedNearby) {
        [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"(enter city or zip code below)" withString:@""]] ;
        [filterTextLabel setText:[filterTextLabel.text stringByAppendingString:@"near me"]] ;
    }
    else {
        [filterTextLabel setText:[filterTextLabel.text stringByReplacingOccurrencesOfString:@"near me" withString:@""]];
    }
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];

}

-(void)searchFilms
{
    shouldShowFilterOptions = NO;
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];

    [self.view endEditing:YES];
    
    if (!checkedNearby && [self.zipcodePicker.text length] < 1) {
        [myCheckedPartner setBackgroundColor:[UIColor spn_aquaColor]];
        checkedPartners = YES;
        [self getPartnerFilms];
        [filterTextLabel setText:@"Art and Indie films "];
    }
    else {
        if (checkedNearby) {
            shouldFetchThearesNearby = YES;
            [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
            isFetchingLocation = YES;
            [self.locationManager startUpdatingLocation];
        }
        else {
            [self getLocationWithUserInput:self.zipcodePicker.text];
        }
    }
}
#pragma mark - Table view functions
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
            return [self.sortedFutureFilms count];
            break;
        default:
            return 2;
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedFilmType == SPNFilmTypeFilmsInRegion && indexPath.section < 1) {
        
        if (indexPath.row < 1) {
            SPNFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:FilterCellIdentifier];
            filterTextLabel = cell.filterTextLabel;
            return cell;
        }
        else {
            static NSString *FilterOptionCellIdentifier = @"FilterOptionsCell";
            SPNFilterOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:FilterOptionCellIdentifier];
            self.zipcodePicker = cell.zipPicker;
            [cell.zipPicker setInputAccessoryView:keyboardDoneButtonView];
            [cell.zipPicker setDelegate:self];


            myCheckedPartner = cell.checkPartnersButton;
            myCheckedNearby = cell.checkNearbyButton;
            searchBarBackgroundView = cell.searchBarBackgroundView;
            [cell.checkNearbyContainerButton addTarget:self
                                                action:@selector(checkNearby:)
                                      forControlEvents:UIControlEventTouchUpInside];
            [cell.checkPartnersContainerButton addTarget:self
                                                action:@selector(checkPartners:)
                                      forControlEvents:UIControlEventTouchUpInside];
            
            [cell.filterButton addTarget:self
                                  action:@selector(searchFilms)
                        forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        
    }
    else {
        static NSString *CellIdentifier = @"FilmCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSArray *selectedFilmList;
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
            return 1;
            break;
        default:
            if (section < 1) {
                return 2;
            }
            selectedFilmList = self.filmsInRegion;
            break;
    }
    NSInteger rowCount = [selectedFilmList count] / 3;
    if ([selectedFilmList count] % 3 != 0) {
        rowCount +=1;
    }
    return rowCount;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SPNFilmSearchCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor spn_lightBrownColor];
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
            [cell setCollectionViewDataSourceDelegate:self index:indexPath.section];
            break;
        default:
            if (indexPath.section > 0) {
                [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
            }
            else {
                cell.backgroundColor = [UIColor spn_brownColor];
            }
            break;
    }
   
   
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont spn_NeutraBoldLarge]];
    header.contentView.backgroundColor =[UIColor spn_darkBrownColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *selectedFilmList;
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
        {
            NSString *filmDate = [futureFilmDates objectAtIndex:indexPath.section];
            selectedFilmList = [self.sortedFutureFilms objectForKey:filmDate];
            CGFloat size = MAX(1,[selectedFilmList count] / 3)*173;
            return size;
        }
            break;
        default:
            if (indexPath.section < 1) {
                if (indexPath.row < 1) {
                    [self configureCell:self.prototypeFilterCell
                      forRowAtIndexPath:indexPath];
                    CGSize size = [_prototypeFilterCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                    return size.height+1;
                }
                else {
                    if (shouldShowFilterOptions) {
                        CGFloat height = kFilterOptionCellHeight;
                        if (@available(iOS 11.0, *)) {
                            height += 10;
                        }
                        return height;
                    } else {
                        return 0;
                    }
                }
            }
            selectedFilmList = self.filmsInRegion;
            break;
    }
    return 173;
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
        {
            NSDate *filmDate = [futureFilmDates objectAtIndex:section];
            [titleFormatter setDateFormat:@"MMM d"];
            NSString *formattedFilmDate = [titleFormatter stringFromDate:filmDate];
            return formattedFilmDate;
        }
            break;
        default:
            return @"Films";
            break;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
            return 30;
            break;
        default:
            return 0;
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if  (selectedFilmType == SPNFilmTypeFilmsInRegion && indexPath.section < 1 && indexPath.row < 1) {
        shouldShowFilterOptions = !shouldShowFilterOptions;
        [self.view endEditing:YES];
        [self.filmTableView beginUpdates];
        [self.filmTableView endUpdates];

    }
}

#pragma mark - Collection View Functions
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(SPNShowtimesCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (selectedFilmType) {
        case SPNFilmTypeFilmsInRegion:
            return MIN(3, [self.filmsInRegion count] - 3*collectionView.index);
            break;
        case SPNFilmTypeFutureReleases:
        {
         
            NSString *filmDate = [futureFilmDates objectAtIndex:collectionView.index];
            NSInteger cont = [[self.sortedFutureFilms objectForKey:filmDate] count];
            return cont;
        }
            break;
        default:
             return MIN(3, [self.filmsInRegion count] - 3*collectionView.index);
            break;
    }
}

-(UICollectionViewCell *)collectionView:(SPNShowtimesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* FilmCellIdentifier = @"MovieCell";
    SPNMovieCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FilmCellIdentifier  forIndexPath:indexPath];
    
    NSArray *selectedFilmList;
    NSInteger rowOffset = collectionView.index*3;
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
        {
            NSString *filmDate = [futureFilmDates objectAtIndex:collectionView.index];
            selectedFilmList = [self.sortedFutureFilms objectForKey:filmDate];
            rowOffset = 0;
        }
            break;
        default:
            selectedFilmList = self.filmsInRegion;
            break;
    }
    
    NSDictionary *movieInformation = [selectedFilmList objectAtIndex:indexPath.row + rowOffset];
   

    NSString *metaScore = [movieInformation objectForKey:@"metascore"];
    [cell.metaScoreLabel assignMetaScore:metaScore];
    [cell.metaScoreLabel setHidden:NO];
    
        [cell initializeMovieSchedule:movieInformation
                    forCollectionView:collectionView
                         forIndexPath:indexPath];

    return cell;
}

-(void)collectionView:(SPNShowtimesCollectionView *)collectionView didEndDisplayingCell:(SPNMovieCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[AsyncImageLoader sharedLoader] cancelLoadingURL:cell.thumbnail.imageURL];
    cell.thumbnail.image = [UIImage imageNamed:@"poster-not-available"];
}

-(void)collectionView:(UICollectionView*)collectionView willDisplayItem:(SPNMovieCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell.thumbnail.image = [UIImage imageNamed:@"poster-not-available"];
}

-(void)collectionView:(SPNShowtimesCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *selectedMovie;
    NSArray* selectedFilmList;
    NSInteger rowOffset = collectionView.index*3;
    switch (selectedFilmType) {
        case SPNFilmTypeFutureReleases:
        {
            NSString *filmDate = [futureFilmDates objectAtIndex:collectionView.index];
            selectedFilmList = [self.sortedFutureFilms objectForKey:filmDate];
            selectedMovie = [selectedFilmList objectAtIndex:indexPath.row];
            rowOffset = 0;
        }
            break;
        default:
            selectedFilmList = self.filmsInRegion;
            selectedMovie = [selectedFilmList objectAtIndex:indexPath.row+rowOffset];
            break;
    }

    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    NSString *movieId =[selectedMovie objectForKey:@"id"];
    [[SPNCinelifeQueries sharedInstance] getMovieInformation:movieId
                                                  forZipcode:nil
                                               orCoordinates:nil
                                                  withRadius:15
                                             withCredentials:[SPNUser getCurrentUserId]];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6_PLUS) {
         return CGSizeMake(self.view.frame.size.width*0.3-10, 168);
    }
    else {
        return CGSizeMake(self.view.frame.size.width*0.3-5, 168);
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (IS_IPHONE_6_PLUS) {
        return UIEdgeInsetsMake(5, 20, 0, 0);
    }
    else if (IS_IPHONE_6) {
        return UIEdgeInsetsMake(5, 20, 0, 5);
    }
    else {
        return UIEdgeInsetsMake(5, 5, 0, 5);
    }
}


#pragma mark Custom keyboard functions
-(void)initializeKeyboard
{
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(doneClicked:)];
    
    UIBarButtonItem* useSavedLocButton = [[UIBarButtonItem alloc] initWithTitle:@"Saved"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(savedLocation:)];
    
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelClicked:)];
    
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleBlack;
    keyboardDoneButtonView.translucent = YES;
    keyboardDoneButtonView.tintColor = nil;
    [keyboardDoneButtonView sizeToFit];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, useSavedLocButton, cancelButton, nil]];
}

-(void)doneClicked:(id)sender
{
    shouldShowFilterOptions = NO;
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];
    [self.zipcodePicker resignFirstResponder];
    
    
    if ([self.zipcodePicker.text length] < 1) {
        checkedPartners = YES;
        [myCheckedPartner setBackgroundColor:[UIColor spn_aquaColor]];
        [self getPartnerFilms];
    }
    else {
        [self getLocationWithUserInput:self.zipcodePicker.text];
    }
}

-(void)activateGPSLocation:(id)sender
{
    shouldShowFilterOptions = NO;
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];
    shouldFetchThearesNearby = YES;
    [self.zipcodePicker resignFirstResponder];
    isFetchingLocation = YES;
    [self.locationManager startUpdatingLocation];
}

-(void)savedLocation:(id)sender
{
    shouldShowFilterOptions = NO;
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];
    [self.zipcodePicker resignFirstResponder];
    [self getLastSavedLocation];
}

-(void)cancelClicked:(id)sender
{
    [self.zipcodePicker resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
        shouldShowFilterOptions = NO;
        [self.filmTableView beginUpdates];
        [self.filmTableView endUpdates];
    }
    [self getLocationWithUserInput:self.zipcodePicker.text];
}


#pragma mark - Location Manager functions

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    userLocation = [locations lastObject];
    [self.locationManager stopUpdatingLocation];
    
    if (isFetchingLocation) {
        isFetchingLocation = NO;
        if (shouldFetchThearesNearby) {
            
            CLGeocoder *geocoder = [CLGeocoder new];
            [geocoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                if (zip) {
                    [self.zipcodePicker setText:zip];
                    [[SPNCinelifeQueries sharedInstance] getMoviesForZipcode:nil
                                                               orCoordinates:userLocation
                                                                  withRadius:15];
                    
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                }
                
            }];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (isFetchingLocation) {
        isFetchingLocation = NO;
        
        [[[UIAlertView alloc] initWithTitle:@"Could not find you" message:@"We could not determine your location. Make sure you have granted location services to the app and try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    
    NSLog(@"Error locating");
}

-(void)getLocationWithUserInput:(NSString*)address
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if(placemarks.count > 0) {
            CLPlacemark *usPlacemark = [placemarks firstObject];
            for (CLPlacemark* placemark in placemarks) {
                if ([placemark.ISOcountryCode isEqualToString:@"US"]) {
                    usPlacemark = placemark;
                }
            }
            [geoCoder reverseGeocodeLocation:usPlacemark.location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place = [placemarks firstObject];
                
                NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
                if (zip) {
                    [self.zipcodePicker setText:zip];
                    [self getFilmShowtimes];
                    [self setCityAndStateToSearchBar:place.addressDictionary];
                    }
            }];
        }
        else if (error.domain == kCLErrorDomain) {
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            switch (error.code)
            {
                case kCLErrorDenied:
                    self.zipcodePicker.text = @"Location Services Denied by User";
                    break;
                case kCLErrorNetwork:
                    self.zipcodePicker.text = @"No Network";
                    break;
                case kCLErrorGeocodeFoundNoResult:
                    self.zipcodePicker.text = @"No Result Found";
                    break;
                default:
                    self.zipcodePicker.text = error.localizedDescription;
                    break;
            }
        }
        else {
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            self.zipcodePicker.text = error.localizedDescription;
        }
        
    }];
}

-(void)setCityAndStateToSearchBar:(NSDictionary*)placeDictionary
{
    NSString *country = [placeDictionary objectForKey:@"CountryCode"];
    NSString *zipcode = [placeDictionary objectForKey:@"ZIP"];
    NSString *nearLocation;
    if ([country isEqualToString:@"US"]) {
        NSString *city = [placeDictionary objectForKey:@"City"];
        NSString *state = [placeDictionary objectForKey:@"State"];
        NSString *location = [NSString stringWithFormat:@"%@, %@ (%@)", city, state, zipcode];
        [self.zipcodePicker setText:location];
        nearLocation = [NSString stringWithFormat:@"near %@", location];
      }
    else {
        [self.zipcodePicker setText:zipcode];
        nearLocation = [NSString stringWithFormat:@"near %@", zipcode];
    }
    NSString *filterTitle = @"";
    [filterTextLabel setText:@""];
    if (checkedPartners) {
        filterTitle = @"Art and Indie films ";
    }
    else {
         filterTitle = @"All films ";
    }
    if (checkedNearby) {
        nearLocation = [filterTitle stringByAppendingString:@"near me"];
    }
    else {
        nearLocation = [filterTitle stringByAppendingString:nearLocation];
    }
    
    [filterTextLabel setText:[filterTextLabel.text stringByAppendingString:nearLocation]];
    [self.filmTableView beginUpdates];
    [self.filmTableView endUpdates];
}

# pragma mark Film search functions

- (IBAction)filmSearchOptionSegmentedControlPressed:(id)sender {
 
    [self.zipcodePicker resignFirstResponder];

    UISegmentedControl *filmSearchOptions = (UISegmentedControl*)sender;
    switch (filmSearchOptions.selectedSegmentIndex) {
        case 0:
            if (self.filmsInRegion) {
                selectedFilmType = SPNFilmTypeFilmsInRegion;
                [self.filmTableView reloadData];
            }
            else {
                [self getFilmShowtimes];
            }
            break;
        case 1:
            selectedFilmType = SPNFilmTypeFutureReleases;
            [self getFutureFilmShowtimes];
            break;
        default:
            break;
    }
}

-(void)getPartnerFilms
{
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [[SPNCinelifeQueries sharedInstance] getPartnerMoviesWithCompletion:^(NSArray *movies, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                                 animated:YES];
        selectedFilmType = SPNFilmTypeFilmsInRegion;
        self.filmsInRegion = [SPNCinelifeQueries sortMoviesByName:movies
                                                withPartnersFirst:YES];
        [self.filmTableView reloadData];
    }];
}

-(void)getFilmShowtimes
{
    NSNumberFormatter *possibleZip = [[NSNumberFormatter alloc] init];
    [possibleZip setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *zip = [possibleZip numberFromString:self.zipcodePicker.text];
    
    if (zip) {
        NSString *zipcodeString = self.zipcodePicker.text;
        
        [[SPNCinelifeQueries sharedInstance] getMoviesForZipcode:zipcodeString
                                                   orCoordinates:nil
                                                      withRadius:15];
    }
    else {
        selectedFilmType = SPNFilmTypeFilmsInRegion;
        [self.filmTableView reloadData];
    }
}

-(void)getFutureFilmShowtimes
{
    if ([self.sortedFutureFilms count] > 0) {
        selectedFilmType = SPNFilmTypeFutureReleases;
        [self.filmTableView reloadData];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        [[SPNCinelifeQueries sharedInstance] getFutureReleases];
    }
}

-(void)getLastSavedLocation
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* storedZipcode = [prefs objectForKey:@"zip"];
    if (storedZipcode) {
        [self getLocationWithUserInput:storedZipcode];
    }
    else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeNoSavedLocation];
    }
}

#pragma mark Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"FilmDetails"]) {
      SPNFilmDetailsViewController *spnFilmDetails = [segue destinationViewController];
      [spnFilmDetails setFilmInformation:selectedFilmForDetails];
      [spnFilmDetails setCurrentUserLocation:userLocation];
      [spnFilmDetails setLastKnownLocation:self.zipcodePicker.text];
      if (selectedFilmType != SPNFilmTypeFilmsInRegion) {
          [spnFilmDetails setShouldBeReviewable:YES];
      }
      else {
          [spnFilmDetails setShouldBeReviewable:NO];
      }
  }
}

#pragma mark - Cinelife queries
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery errorOcurred:(NSError *)error
{
    NSString *errorText = @"";
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
    if ([error.userInfo objectForKey:@"error_msg"]) {
        errorText = [error.userInfo objectForKey:@"error_msg"];
    }
    else {
        errorText = error.localizedDescription;
    }
    
    
    [[[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                message:errorText
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    NSLog(@"error");
}
-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieList:(NSArray *)movies
{
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                             animated:YES];
    selectedFilmType = SPNFilmTypeFilmsInRegion;
    self.filmsInRegion = [SPNCinelifeQueries sortMoviesByName:movies
                                            withPartnersFirst:checkedPartners];
    
    if ([self.filmsInRegion count] <1) {
        [[[UIAlertView alloc] initWithTitle:@"Something went wrong..."
                                    message:@"We did not find any films in the specified location."
                                   delegate:self
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil] show];
    }
    [self.filmTableView reloadData];
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery futureList:(NSArray *)movies
{
    // Sort stuff
    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                             animated:YES];

    NSDictionary *futureShows = [SPNCinelifeQueries sortShowdatesForMovies:movies];
    futureFilmDates = [[futureShows allKeys] sortedArrayUsingSelector:@selector(compare:)];
    selectedFilmType = SPNFilmTypeFutureReleases;
    self.sortedFutureFilms = futureShows;
    [self.filmTableView reloadData];
    NSLog(@"");
}

-(void)spnCinelifeQueries:(SPNCinelifeQueries *)cinelifeQuery movieInformation:(NSDictionary *)movie
{
     [MBProgressHUD hideAllHUDsForView:self.tabBarController.view
                              animated:YES];
    selectedFilmForDetails = movie;
    [self performSegueWithIdentifier:@"FilmDetails" sender:self];
}
@end
