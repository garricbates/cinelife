//
//  SPNPickerCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/8/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNPickerCell : UITableViewCell

@property IBOutlet UIButton *changeLocationBtn;
@property IBOutlet UIButton *changeDateBtn;

@property IBOutlet UILabel *dateLabel;
@property IBOutlet UILabel *zipLabel;

@property IBOutlet UIView *dateContainerView;

@property IBOutlet UICollectionView *dateCollectionView;
@end
