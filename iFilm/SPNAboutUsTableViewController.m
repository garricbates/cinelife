//
//  SPNAboutUsTableViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNAboutUsTableViewController.h"

@interface SPNAboutUsTableViewController ()

@end

@implementation SPNAboutUsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *AboutUsCellIdentifier = @"AboutUsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AboutUsCellIdentifier];
    
    [cell.textLabel setFont:[UIFont spn_NeutraMedium]];
    [cell.textLabel setNumberOfLines:0];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    if (indexPath.section < 1) {
        [cell.textLabel setText:@"CineLife features theatres screening the best in art and independent film as well as all of the Hollywood favorites.  CineLife is the one mobile source connecting you to your favorite theatres, quality film, exclusive events and screenings, festivals and industry news.\r\n\r\nContact Us\r\nsupport@CineLife.com"];
    }
    else {
        [cell.textLabel setText:@"Invite friends"];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 1) {
        return kAccountAboutUsCellHeight;
    }
    else {
        return kAccountOptionsCellHeight;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section < 1) {
        return @"CineLife v1.0";
    }
    else {
        return @"";
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section > 0) {
        [self performSegueWithIdentifier:@"InviteFriends" sender:self];
    }
}

@end
