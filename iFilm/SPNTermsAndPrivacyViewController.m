//
//  SPNTermsAndPrivacyViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/2/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTermsAndPrivacyViewController.h"
@interface SPNTermsAndPrivacyViewController ()

@end

@implementation SPNTermsAndPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *terms = self.isTOS ? [SPNCinelifeQueries getCinelifeTermsOfService] : [SPNCinelifeQueries getCinelifePrivacyPolicy];
    [self.webView loadRequest:[NSURLRequest
                               requestWithURL:[NSURL
                                               URLWithString:terms]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doneButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
