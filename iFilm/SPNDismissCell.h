//
//  SPNDismissCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/22/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNDismissCell : UITableViewCell

@property IBOutlet UILabel *messageLabel;
@property IBOutlet UILabel *messageTwoLabel;
@property IBOutlet UILabel *xLabel;

@end
