//
//  DataModel.m
//  iFilm
//
//  Created by Vlad Getman on 19.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "DataModel.h"
#import "Notification+CoreDataClass.h"

@implementation DataModel

+ (NSArray *)fetchNotifications {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Notification"
                                   inManagedObjectContext:[LCPDatabaseHelper sharedInstance].managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]]];
    NSError *error = nil;
    NSArray *recordList = [[LCPDatabaseHelper sharedInstance].managedObjectContext executeFetchRequest:fetchRequest
                                                                                                 error:&error];
    return recordList;
}

+ (NSArray *)mapNotifications:(id)json {
    NSArray *items = [DataModel deserializeCollection:json[@"aMessages"]
                                         usingMapping:[self notificationMapping]];
    [[LCPDatabaseHelper sharedInstance].managedObjectContext save:nil];
    return items;
}

// Deserialize

+ (id)deserializeObject:(NSDictionary *)object usingMapping:(FEMMapping *)mapping {
    if (![object isKindOfClass:[NSDictionary class]] || object.count == 0 || (!object || !mapping || ![LCPDatabaseHelper sharedInstance].managedObjectContext))
        return nil;
    
    id mapped = nil;
    @try {
        mapped = [FEMDeserializer objectFromRepresentation:object
                                                   mapping:mapping
                                                   context:[LCPDatabaseHelper sharedInstance].managedObjectContext];
    } @catch (NSException *exception) {
        NSLog(@"deserializeObject exception: %@, object: %@", exception.description, object);
        return nil;
    } @finally {
        return mapped;
    }
}

+ (NSArray *)deserializeCollection:(NSArray *)items usingMapping:(FEMMapping *)mapping {
    
    if (![items isKindOfClass:[NSArray class]] || items.count == 0 || (!items || !mapping || ![LCPDatabaseHelper sharedInstance].managedObjectContext))
        return nil;
    
    NSArray *mapped = nil;
    @try {
        mapped = [FEMDeserializer collectionFromRepresentation:items
                                                       mapping:mapping
                                                       context:[LCPDatabaseHelper sharedInstance].managedObjectContext];
    } @catch (NSException *exception) {
        NSLog(@"deserializeCollection exception: %@, items: %@", exception.description, items);
        return nil;
    } @finally {
        return mapped;
    }
}

// Mapping

+ (FEMMapping *)notificationMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:NSStringFromClass([Notification class])];
    mapping.primaryKey = @"notificationId";
    
    [mapping addAttributesFromArray:@[@"message"]];
    
    [mapping addAttributesFromDictionary:@{@"firstName":    @"firstname",
                                           @"lastName":     @"lastname",
                                           @"photoURL":     @"photo",
                                           @"thumbnailURL": @"photo_thumbnail",
                                           @"phone":        @"phone_number"}];
    
    NSDictionary *numbersMapping = @{@"notificationId": @"id",
                                     @"typeId":         @"type_id",
                                     @"theaterId":      @"theater_id",
                                     @"festivalId":     @"festival_id",
                                     @"filmId":         @"film_id"};
    
    [DataModel numberAttributesForKeys:numbersMapping
                               mapping:mapping];
    
    [mapping addAttribute:[FEMAttribute mappingOfProperty:@"date" toKeyPath:@"date" dateFormat:@"yyyy-MM-dd HH:mm:ss"]];
    
    return mapping;
}

+ (void)numberAttributesForKeys:(NSDictionary *)numbersMapping mapping:(FEMMapping *)mapping {
    for (NSString *key in numbersMapping.allKeys) {
        FEMAttribute *numberAttribute = [[FEMAttribute alloc] initWithProperty:key keyPath:numbersMapping[key] map:^id(id value) {
            if ([value isKindOfClass:[NSString class]]) {
                if ([value isEqual:@"YES"]) {
                    return @YES;
                } else if ([value isEqual:@"NO"]) {
                    return @NO;
                }
                return @([value floatValue]);
            } else if ([value isKindOfClass:[NSNumber class]]) {
                return value;
            }
            return nil;
        } reverseMap:^id(id value) {
            return [value stringValue];
        }];
        [mapping addAttribute:numberAttribute];
    }
}

@end
