//
//  SPNMovieStillCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/12/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNMovieStillCell : UICollectionViewCell

@property IBOutlet AsyncImageView *photoImageView;
@end
