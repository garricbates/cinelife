//
//  SPNFilterOptionCell.m
//  iFilm
//
//  Created by Eduardo Salinas on 12/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilterOptionCell.h"

@interface SPNFilterOptionCell ()

@property (nonatomic, strong) IBOutletCollection(NSLayoutConstraint) NSArray *searchConstraints;

@end

@implementation SPNFilterOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.filterButton setBackgroundColor:[UIColor spn_buttonColor]];
    [self.filterButton.layer setShadowColor:[UIColor spn_buttonShadowColor].CGColor];
    if (@available(iOS 11.0, *)) {
        self.zipPicker.searchBarStyle = UISearchBarStyleDefault;
        for (NSLayoutConstraint *constraint in self.searchConstraints) {
            constraint.constant += 5;
        }
        [self.contentView layoutIfNeeded];
    }
}


@end
