//
//  SPNAccountEditSettingsTableViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/23/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNAccountEditSettingsTableViewController : UITableViewController <UITextFieldDelegate, CLLocationManagerDelegate>

@end
