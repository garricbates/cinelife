//
//  NotificationsHelper.m
//  iFilm
//
//  Created by Vlad Getman on 04.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "NotificationsHelper.h"

@implementation NotificationsHelper

+ (NotificationType)notificationTypeFromPushType:(NSInteger)type {
    switch (type) {
        case 1:
            return NotificationTypeTheater;
            
        case 2:
            return NotificationTypeFilm;
            
        case 3:
            return NotificationTypeFestival;
            
        case 4:
            return NotificationTypeMarketing;
            
        default:
            return NotificationTypeGeneral;
    }
}

@end
