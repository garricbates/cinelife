//
//  SPNFilmReviewCell.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/20/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNFilmReviewCell.h"

@implementation SPNFilmReviewCell


-(void)initializeReviewCell:(NSDictionary*)filmReview
{
    NSInteger rating = [[filmReview objectForKey:@"rating"] integerValue];
    
    [self.star1ImageView setImage:[UIImage imageNamed:@"review-star-1"]];
    [self.star2ImageView setImage:[UIImage imageNamed:@"review-star-1"]];
    [self.star3ImageView setImage:[UIImage imageNamed:@"review-star-1"]];
    [self.star4ImageView setImage:[UIImage imageNamed:@"review-star-1"]];
    [self.star5ImageView setImage:[UIImage imageNamed:@"review-star-1"]];
    
    if (rating > 4) {
        [self.star5ImageView setImage:[UIImage imageNamed:@"review-star-2"]];
      
    }
    if (rating > 3) {
        [self.star4ImageView setImage:[UIImage imageNamed:@"review-star-2"]];
    }
    if (rating > 2) {
        [self.star3ImageView setImage:[UIImage imageNamed:@"review-star-2"]];
    }
    if (rating > 1) {
        [self.star2ImageView setImage:[UIImage imageNamed:@"review-star-2"]];
    }
    if (rating > 0) {
        [self.star1ImageView setImage:[UIImage imageNamed:@"review-star-2"]];
    }
    
    NSString *title = [filmReview objectForKey:@"title"] ? [filmReview objectForKey:@"title"] : @"Review";
    
   
    [self.reviewTitleLabel setText:title];
    
    self.reviewLabel.text = [NSString stringWithFormat:@"%@", [filmReview objectForKey:@"message"]];
    [self.reviewLabel setFont:[UIFont spn_NeutraMedium]];
   
    
    CGFloat mainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    [self.reviewLabel setPreferredMaxLayoutWidth:mainScreenWidth-16];
    [self.reviewTitleLabel setPreferredMaxLayoutWidth:mainScreenWidth-16];
    
}
@end
