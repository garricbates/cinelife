//
//  SPNTicketsTitleCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTicketsTitleCell : UITableViewCell

@property IBOutlet UILabel *ticketsNameLabel;
@property IBOutlet UILabel *ticketsPriceLabel;
@property IBOutlet UILabel *ticketsAmountLabel;

@end
