//
//  SPNMetascoreOperation.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/14/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNMetascoreOperation.h"

@implementation SPNMetascoreOperation

-(void)cancelOperationWithMovieId:(NSString *)movieId
{
    if ([self.movieId isEqualToString:movieId]) {
        [self cancel];
    }
}
@end
