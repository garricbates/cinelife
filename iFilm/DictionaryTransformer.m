//
//  DictionaryTransformer.m
//  iFilm
//
//  Created by Vlad Getman on 04.05.17.
//  Copyright © 2017 La Casa de los Pixeles. All rights reserved.
//

#import "DictionaryTransformer.h"

@implementation DictionaryTransformer

+ (Class)transformedValueClass {
    return [NSDictionary class];
}

+ (BOOL)allowsReverseTransformation {
    return YES;
}

- (id)transformedValue:(id)value {
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value {
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}

@end
