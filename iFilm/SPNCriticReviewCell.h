//
//  SPNCriticReviewCell.h
//  iFilm
//
//  Created by Eduardo Salinas on 11/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNCriticReviewCell : UITableViewCell

@property IBOutlet UILabel *criticNameLabel;
@property IBOutlet UILabel *criticSiteLabel;
@property IBOutlet UILabel *criticScoreLabel;
@property IBOutlet UILabel *criticSnippetLabel;
@property IBOutlet UIButton *readFullReviewButton;

@end
