//
//  SPNTheatreShowtimeCell.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTheatreShowtimeCell : UITableViewCell
@property () IBOutlet UILabel *theatreNameLabel;
@property () IBOutlet UILabel *distanceLabel;
@property () IBOutlet UILabel *showtimesLabel;
@property () IBOutlet UIView *showtimesContainerView;
@property () IBOutlet UIButton *buyButton;

-(void)initializeTheatreCellContents:(NSDictionary*)theatre
                        forIndexPath:(NSIndexPath*)indexPath;

-(NSArray*)getShowsForThisTheatre:(NSIndexPath*)indexPath
                      forSchedule:(NSDictionary*)movieShowtimes
                          forDate:(NSString*)dateString;

-(void)setShowForCell:(NSArray*)shows;

@end
