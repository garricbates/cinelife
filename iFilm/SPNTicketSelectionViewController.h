//
//  SPNTicketSelectionViewController.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 10/25/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPNTicketSelectionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property NSDictionary *movieInformation;
@property NSDictionary *showtimeInformation;
@property NSDictionary *ticketingInformation;

@property NSDictionary *branding;
// Movie information outlets
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *userRatingLabel;
@property IBOutlet UILabel *genresLabel;
@property IBOutlet UILabel *classificationLabel;
@property IBOutlet UILabel *durationLabel;
@property IBOutlet UIImageView *posterImageView;
@property IBOutlet UILabel *metaScoreLabel;

@property IBOutlet UIView *theatreBackgroundView;
@property IBOutlet UIImageView *theatreLogoImageView;
//-------------------------//

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaScoreLeadConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *metaIconWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandLogoHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsTopAnchorConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsBottomAnchorConstraint;
@property IBOutlet UITableView *ticketSelectionTableView;

@property IBOutlet UILabel *totalAmountLabel;
@property UILabel *totalConvenienceChargeLabel;

@property IBOutlet UIButton *creditCardButton;
@property IBOutlet UIButton *giftCardButton;
@property IBOutlet UIButton *membershipCardButton;

-(IBAction)buyTickets:(id)sender;

-(IBAction)changePaymentMethod:(id)sender;
@end
