//
//  SPNCinelifeQueries.m
//  iFilm
//
//  Created by Eduardo Salinas on 8/3/15.
//  Copyright (c) 2015 La Casa de los Pixeles. All rights reserved.
//

#import "SPNCinelifeQueries.h"
#import "SPNRSSParser.h"
#import "Festival.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SPNAppDelegate.h"
#import "DataModel.h"

@interface SPNCinelifeQueries ()

@end

@implementation SPNCinelifeQueries

+(SPNCinelifeQueries *)sharedInstance
{
    static SPNCinelifeQueries *sharedInstance = nil;
    if (sharedInstance == nil) {
        sharedInstance = [(SPNCinelifeQueries *)[self alloc] init];
        [sharedInstance initSession];
    }
    return sharedInstance;
    
}

-(void)initSession
{
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // 2
    self.serverSession =
    [NSURLSession sessionWithConfiguration:sessionConfig
                                  delegate:self
                             delegateQueue:nil];
}

+(NSDictionary *)getCineLifeInitialConfiguration
{
    NSString *query = [NSString stringWithFormat:@"%@/configurations", server];
    NSURL *queryURL = [NSURL URLWithString:query];
    NSDictionary *result  = [self queryURL:queryURL];
    return result;
}
+(NSMutableArray *)sortTheatersByDistance:(NSArray*)theaterList withLocation:(CLLocation*)userLocation withPartnersFirst:(BOOL)partnersFirst
{
    NSMutableArray *sorters = [NSMutableArray new];
    
    
    NSMutableArray *sortedTheatres = [NSMutableArray new];
    double lat, lon;
    if (userLocation)
    {
        for (NSDictionary *theater in theaterList)
        {
            NSMutableDictionary *newTheater = [NSMutableDictionary dictionaryWithDictionary:theater];
            NSString *latString =[theater objectForKey:@"lat"];
            NSString *lonString =[theater objectForKey:@"lng"];
            if (!latString || !lonString) {
                [newTheater setObject:@0 forKey:@"distance"];
            }
            else
            {
                lat = [latString doubleValue];
                lon = [lonString doubleValue];
                
                CLLocation *theatreDistance = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
                CLLocationDistance currentDistance = [theatreDistance distanceFromLocation:userLocation];
                currentDistance = currentDistance / 1000 * 0.621371192; // Convert meters into miles
                
                [newTheater setObject:[NSNumber numberWithDouble:currentDistance] forKey:@"distance"];
            }
            [sortedTheatres addObject:newTheater];
        }
    }
    else {
        [sortedTheatres addObjectsFromArray:theaterList];
    }
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    [sorters addObject:sorter];
    
    sortedTheatres = [[sortedTheatres sortedArrayUsingDescriptors:sorters] mutableCopy];
    [sorters removeAllObjects];
    if (partnersFirst) {
        NSSortDescriptor *sorter2 = [[NSSortDescriptor alloc] initWithKey:@"is_partner" ascending:NO];
        [sorters addObject:sorter2];
    }
    NSSortDescriptor *sorter3 = [[NSSortDescriptor alloc] initWithKey:@"is_favorite" ascending:NO];
    [sorters addObject:sorter3];
    
    sortedTheatres = [[sortedTheatres sortedArrayUsingDescriptors:sorters] mutableCopy];

    return sortedTheatres;
}

+(NSDictionary *)sortShowdatesForTheaters:(NSArray *)theaterList
{
    NSMutableDictionary *theaterWithShowdates = [NSMutableDictionary new];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    for (NSDictionary *theater in theaterList)
    {
        NSArray *showdates = [theater objectForKey:@"showtimes"];
        for (NSDictionary *showtimes in showdates)
        {
            NSMutableArray *showsForDate;
            NSString *showDate = [showtimes objectForKey:@"showdate"];
            NSDate *date = [formatter dateFromString:showDate];
            
            if (![[theaterWithShowdates allKeys] containsObject:date]) {
                showsForDate = [NSMutableArray new];
            }
            else {
                showsForDate = [theaterWithShowdates objectForKey:date];
            }
            
            NSInteger index = [showsForDate indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [[obj objectForKey:@"id"] isEqualToString:[theater objectForKey:@"id"]];
            }];

            NSMutableDictionary *showForTheater;
            if (index == NSNotFound) {
                showForTheater = [NSMutableDictionary new];
                [showForTheater setObject:[theater objectForKey:@"name"]
                                   forKey:@"name"];
                [showForTheater setObject:[theater objectForKey:@"id"]
                                   forKey:@"id"];
              
                [showForTheater setObject:[theater objectForKey:@"is_partner"]
                                   forKey:@"is_partner"];
                [showsForDate addObject:showForTheater];
                [showForTheater setObject:[showtimes objectForKey:@"showtime"]
                                   forKey:@"showtime"];

            }
            else {
                showForTheater = [showsForDate objectAtIndex:index];
                NSArray *previousShows = [showForTheater objectForKey:@"showtime"];
                NSArray *newShows = [showtimes objectForKey:@"showtime"];
                NSArray *finalShows = [[NSSet setWithArray:[previousShows arrayByAddingObjectsFromArray:newShows]] allObjects];
                [showForTheater setObject:finalShows forKey:@"showtime"];
            }
            if ([theater objectForKey:@"distance"]) {
                [showForTheater setObject:[theater objectForKey:@"distance"]
                                   forKey:@"distance"];
            }
            
            if (date) {
                [theaterWithShowdates setObject:showsForDate
                                         forKey:date];
            }
        }
    }
    //NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"is_partner" ascending:NO];
    NSMutableDictionary *theaterWithPartners = [NSMutableDictionary new];
    
    for (NSDate *date in [theaterWithShowdates allKeys]) {
        NSMutableArray *sortedTheaters = [theaterWithShowdates objectForKey:date];
     //   NSArray *newSortedTheaters = [sortedTheaters sortedArrayUsingDescriptors:@[sorter]];
        [theaterWithPartners setObject:sortedTheaters forKey:date];
    }
    
    return theaterWithPartners;
}

+(NSDictionary *)sortShowdatesForOneTheatre:(NSArray *)movieList
{
    if ([movieList count] < 1) {
        return [NSDictionary dictionaryWithObject:@[]
                                           forKey:[NSDate date]];
    }
    NSMutableDictionary *theaterWithShowdates = [NSMutableDictionary new];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSString *todayDateString = [formatter stringFromDate:[NSDate date]];
    NSDate *todayDate = [formatter dateFromString:todayDateString];
    
    for (NSDictionary *movie in movieList)
    {
        id showdates = [movie objectForKey:@"showtimes"];
        if ([showdates isKindOfClass:[NSDictionary class]]) {
            NSMutableArray *showsForDate;
            NSString *showDate = [showdates objectForKey:@"showdate"];
            NSDate *date = [formatter dateFromString:showDate];
            
            if (![[theaterWithShowdates allKeys] containsObject:date]) {
                showsForDate = [NSMutableArray new];
            }
            else {
                showsForDate = [theaterWithShowdates objectForKey:date];
            }
            NSMutableDictionary *showForTheater = [NSMutableDictionary new];
            [showForTheater setObject:[movie objectForKey:@"name"]
                               forKey:@"name"];
            [showForTheater setObject:[movie objectForKey:@"id"]
                               forKey:@"id"];
            [showForTheater setObject:[showdates objectForKey:@"showtime"]
                               forKey:@"showtime"];
            [showForTheater setObject:[movie objectForKey:@"poster"]
                               forKey:@"poster"];
            [showForTheater setObject:[movie objectForKey:@"rating"]
                               forKey:@"rating"];
            if ([movie objectForKey:@"metascore"]) {
                [showForTheater setObject:[movie objectForKey:@"metascore"]
                                   forKey:@"metascore"];
            }
            [showsForDate addObject:showForTheater];
            
            if (date && [date timeIntervalSince1970] >= [todayDate timeIntervalSince1970] ) {
                [theaterWithShowdates setObject:showsForDate
                                         forKey:date];
            }

        }
        else {
            for (NSDictionary *showtimes in showdates)
            {
                NSMutableArray *showsForDate;
                NSString *showDate = [showtimes objectForKey:@"showdate"];
                NSDate *date = [formatter dateFromString:showDate];
                
                if (![[theaterWithShowdates allKeys] containsObject:date]) {
                    showsForDate = [NSMutableArray new];
                }
                else {
                    showsForDate = [theaterWithShowdates objectForKey:date];
                }
                
                NSInteger index = [showsForDate indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                    return [[obj objectForKey:@"id"] isEqualToString:[movie objectForKey:@"id"]];
                }];
                NSMutableDictionary *showForTheater;
                if (index == NSNotFound) {
                    showForTheater = [NSMutableDictionary new];
                    [showForTheater setObject:[showtimes objectForKey:@"showtime"]
                                       forKey:@"showtime"];
                    [showForTheater setObject:[movie objectForKey:@"name"]
                                       forKey:@"name"];
                    [showForTheater setObject:[movie objectForKey:@"id"]
                                       forKey:@"id"];
                    
                    [showForTheater setObject:[movie objectForKey:@"poster"]
                                       forKey:@"poster"];
                    [showForTheater setObject:[movie objectForKey:@"rating"]
                                       forKey:@"rating"];
                    [showsForDate addObject:showForTheater];
                    
                }
                else {
                    showForTheater = [showsForDate objectAtIndex:index];
                    
                    NSArray *previousShows = [showForTheater objectForKey:@"showtime"];
                    NSArray *newShows = [showtimes objectForKey:@"showtime"];
                    NSArray *finalShows = [[NSSet setWithArray:[previousShows arrayByAddingObjectsFromArray:newShows]] allObjects];
                    [showForTheater setObject:finalShows forKey:@"showtime"];
                }
                
                if ([movie objectForKey:@"metascore"]) {
                    [showForTheater setObject:[movie objectForKey:@"metascore"]
                                       forKey:@"metascore"];
                }

                
                if (date && [date timeIntervalSince1970] >= [todayDate timeIntervalSince1970] ) {
                    [theaterWithShowdates setObject:showsForDate
                                             forKey:date];
                }
            }
        }
        
    }
    //NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSMutableDictionary *theaterWithPartners = [NSMutableDictionary new];
    
    for (NSDate *date in [theaterWithShowdates allKeys]) {
        NSMutableArray *sortedTheaters = [theaterWithShowdates objectForKey:date];
       // NSArray *newSortedTheaters = [sortedTheaters sortedArrayUsingDescriptors:@[sorter]];
        [theaterWithPartners setObject:sortedTheaters forKey:date];
    }
    
    return theaterWithPartners;
}

+(NSDictionary *)sortShowdatesForMovies:(NSArray *)movieList
{
    NSMutableDictionary *moviesWithShowdates = [NSMutableDictionary new];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    for (NSDictionary *movie in movieList)
    {
        NSMutableArray *showsForDate;
        NSString *showDate = [movie objectForKey:@"release_date"];
        NSDate *date = [formatter dateFromString:showDate];
        
        if (![[moviesWithShowdates allKeys] containsObject:date]) {
            showsForDate = [NSMutableArray new];
        }
        else {
            showsForDate = [moviesWithShowdates objectForKey:date];
        }
        NSMutableDictionary *futureMovie = [NSMutableDictionary new];
        [futureMovie setObject:[movie objectForKey:@"name"]
                           forKey:@"name"];
        [futureMovie setObject:[movie objectForKey:@"id"]
                           forKey:@"id"];
        [futureMovie setObject:[movie objectForKey:@"poster"]
                           forKey:@"poster"];
        if ([movie objectForKey:@"metascore"]) {
            [futureMovie setObject:[movie objectForKey:@"metascore"]
                            forKey:@"metascore"];
        }
        [showsForDate addObject:futureMovie];
        if (date) {
            [moviesWithShowdates setObject:showsForDate
                                    forKey:date];
        }
    }
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSMutableDictionary *theaterWithPartners = [NSMutableDictionary new];
    
    for (NSDate *date in [moviesWithShowdates allKeys]) {
        NSMutableArray *sortedMovies = [moviesWithShowdates objectForKey:date];
        NSArray *newSortedTheaters = [sortedMovies sortedArrayUsingDescriptors:@[sorter]];
        [theaterWithPartners setObject:newSortedTheaters forKey:date];
    }
    
    return theaterWithPartners;
}


+(NSArray *)sortMoviesByName:(NSArray *)movies withPartnersFirst:(BOOL)orderPartners
{
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *orderedMovies = [movies sortedArrayUsingDescriptors:@[sorter]];
    
    NSSortDescriptor *sorter2 = [[NSSortDescriptor alloc] initWithKey:@"in_partner" ascending:NO];
    NSArray *orderedMovies2 = [orderedMovies sortedArrayUsingDescriptors:@[sorter2]];
    
    NSMutableSet* existingNames = [NSMutableSet set];
    NSMutableArray* filteredArray = [NSMutableArray array];
    for (id object in orderedMovies2) {
        if (![existingNames containsObject:[object objectForKey:@"id"]]) {
            [existingNames addObject:[object objectForKey:@"id"]];
            [filteredArray addObject:object];
        }
    }
    if (orderPartners) {
        NSMutableArray *filteredOnlyPartners = [NSMutableArray new];
        for (NSDictionary *movie in filteredArray)
        {
            if (![movie objectForKey:@"in_partner"] || [[movie objectForKey:@"in_partner"] boolValue]) {
                [filteredOnlyPartners addObject:movie];
            }
        }
        return filteredOnlyPartners;
    }
    else {
        return filteredArray;
    }
}

+ (NSString*)getTheatreAddress:(NSDictionary*)theatre
{
    NSString *street = [theatre objectForKey:@"street"];
    NSString * state = [theatre objectForKey:@"state"];
    NSString * postalCode = [theatre objectForKey:@"zip"];
    NSString * city = [theatre objectForKey:@"city"];
    NSString *fullAddress = [NSString stringWithFormat:@"%@, %@, %@ %@", street, city, state, postalCode];
    return fullAddress;
}

-(void)getTheatersForZipcode:(NSString *)zipcode orCoordinates:(CLLocation *)location withRadius:(NSInteger)radius withCredentials:(NSDictionary*)credentials
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters?distance=%ld", server, (long)radius];

    if (zipcode) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&zip=%@", zipcode]];
    }
    else {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&lat=%f&lng=%f",  location.coordinate.latitude, location.coordinate.longitude]];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                                theaterList:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

- (void)getFestivalsForZipcode:(NSString *)zipcode orCoordinates:(CLLocation *)location withRadius:(NSInteger)radius onlyPinned:(BOOL)onlyPinned filters:(NSDictionary *)filters {
    
    if (filters[@"near_me"] && radius < 30) {
        radius = 30;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/festivals?distance=%ld&limit=100", server, (long)radius];
    
    if (zipcode) {
        urlString = [urlString stringByAppendingFormat:@"&zip=%@", zipcode];
    } else if (location) {
        urlString = [urlString stringByAppendingFormat:@"&lat=%f&lng=%f",  location.coordinate.latitude, location.coordinate.longitude];
    }
    if (onlyPinned) {
        urlString = [urlString stringByAppendingString:@"&featured_for_theaters=1"];
    }
    
    if (filters) {
        for (NSString *key in filters) {
            if (![key isEqual:@"near_me"] && ![key isEqual:@"starts_in_max_days"]) {
                NSString *value = filters[key];
                if ([value isKindOfClass:[NSDate class]]) {
                    value = @([((NSDate *)value) timeIntervalSince1970]).stringValue;
                }
                urlString = [urlString stringByAppendingFormat:@"&%@=%@", key, value];
            } else if ([key isEqual:@"starts_in_max_days"]) {
                urlString = [urlString stringByAppendingFormat:@"&%@=30", key];
            }
        }
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    NSArray *festivals = [Festival festivalsFromJSON:json];
                    if (onlyPinned && festivals.count > 4) {
                        NSMutableArray *_festivals = festivals.mutableCopy;
                        for (NSInteger i = festivals.count - 1; i >= 3; i--) {
                            [_festivals removeObjectAtIndex:i];
                        }
                        festivals = _festivals.copy;
                    }
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                             festivalList:festivals];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getTheaterInformation:(NSString *)theaterId withCredentials:(NSDictionary*)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/%@", server, theaterId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                       theaterInformation:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getPartnerMoviesWithCompletion:(void (^)(NSArray *, NSError *))completion
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/partners", server];

    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(json, nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getFutureReleases
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/releases", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                               futureList:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getMoviesForZipcode:(NSString *)zipcode orCoordinates:(CLLocation *)location withRadius:(NSInteger)radius
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies?distance=%lu", server, (long) radius];
    
    if (zipcode) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&zip=%@", zipcode]];
    }
    else if (location) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&lat=%f&lng=%f", location.coordinate.latitude, location.coordinate.longitude]];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                                movieList:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getMovieInformation:(NSString *)movieId forZipcode:(NSString *)zipcode orCoordinates:(CLLocation *)location withRadius:(NSInteger)radius withCredentials:(NSDictionary*)credentials
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@?distance=%lu", server, movieId, (long)radius];
    
    if (zipcode) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&zip=%@", zipcode]];
    }
    else if (location) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&lat=%f&lng=%f", location.coordinate.latitude, location.coordinate.longitude]];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                         movieInformation:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postSignUpNewUser:(NSDictionary *)user
{
    NSString *urlString = [NSString stringWithFormat:@"%@/signup", server];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    
    /*
    // post body
    NSMutableData *body = [NSMutableData data];
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----WebKitFormBoundaryE19zNvXGzXaLvS5C";
    // add params (all params are strings)
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    for (NSString *param in user) {
        
        NSString *bounding = [NSString stringWithFormat:@"--%@\r\n", BoundaryConstant];
        [body appendData: [bounding dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSString *name  = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param];
        [body appendData:[name dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *value = [NSString stringWithFormat:@"%@\r\n", [user objectForKey:param]];
        [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *bounding = [NSString stringWithFormat:@"--%@\r\n", BoundaryConstant];
    [body appendData: [bounding dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    */
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:user
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;

    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    NSDictionary *errors = [json objectForKey:@"errors"];
                    NSString *errorList = @"";
                    for (NSString* error in errors) {
                        
                        errorList = [errorList stringByAppendingString:[[errors objectForKey:error] firstObject]];
                        errorList = [errorList stringByAppendingString:@"\r\n"];
                    }
                    errorList = [errorList substringToIndex:[errorList length]-2];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not followed", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" : errorList}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                           userRegistered:json withParams:user];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postSignIn:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/signin/", server];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:credentials
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                             userLoggedIn:json withCredentials:credentials];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getMyCineLife
{
//    NSLog(server);
    NSString *urlString = [NSString stringWithFormat:@"%@/mycinelife", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                          myCinelifeTiles:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postReview:(NSDictionary *)review forMovieId:(NSString *)movieId withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@/review", server, movieId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:review
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                             reviewPosted:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getReviewsForMovieId:(NSString *)movieId
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@/reviews", server, movieId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                                  reviews:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

- (void)favoriteFestival:(NSNumber *)festivalId completion:(void(^)(NSError *))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/festivals/favorite?id=%@", server, festivalId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(nil);
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddedFestivalToFavorites
                                                                            object:self
                                                                          userInfo:@{@"id" : festivalId}];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

- (void)unfavoriteFestival:(NSNumber *)festivalId completion:(void(^)(NSError *))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/festivals/unfavorite?id=%@", server, festivalId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(nil);
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRemovedFestivalFromFavorites
                                                                            object:self
                                                                          userInfo:@{@"id" : festivalId}];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postAddFavoriteTheater:(NSString *)theaterId withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/%@/favorite", server, theaterId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];

    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSDictionary *info = @{@"id" : theaterId};
                        [self.delegate spnCinelifeQueries:self
                                             addedTheater:info];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAddedTheaterToFavorites
                                                                            object:self
                                                                          userInfo:info];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postAddFavoriteMovie:(NSString *)movieId withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@/favorite", server, movieId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                               addedMovie:@{@"id": movieId}];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postRemoveFavoriteTheater:(NSString *)theaterId withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/%@/unfavorite", server, theaterId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSDictionary *info = @{@"id" : theaterId};
                        [self.delegate spnCinelifeQueries:self
                                           removedTheater:info];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRemovedTheaterFromFavorites
                                                                            object:self
                                                                          userInfo:info];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postRemoveFavoriteMovie:(NSString *)movieId withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@/unfavorite", server, movieId];
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                             removedMovie:@{@"id" : movieId}];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getFavoriteTheatersWithCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/favorites", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                         favoriteTheaters:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getFavoritesTheatersAndFestivalsWithCompletion:(void(^)(NSArray *theaters, NSArray *festivals, NSError *error))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/customers/combine_favorite_content", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(nil, nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(nil, nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg":[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(nil, nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(json[@"api_theater_favorites"], [Festival festivalsFromJSON:json[@"api_festival_favorites"]], nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)getFavoritesMoviesAndFestivalsWithCompletion:(void(^)(NSArray *movies, NSArray *festivals, NSError *error))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/customers/combine_favorite_content?add_movie=1&add_festival=1&add_theater=0", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    [request setValue:[credentials objectForKey:@"access_token"]
   forHTTPHeaderField:@"X-CinelifeAuth"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(nil, nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(nil, nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(nil, nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(json[@"api_movies_favorites"], [Festival festivalsFromJSON:json[@"api_festival_favorites"]], nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postTicketingForTheater:(NSString *)theaterId forMovie:(NSString *)movieId forShowdate:(NSString *)date andShowtime:(NSString *)showtime
{
    NSString *urlString = [NSString stringWithFormat:@"%@/tickets/relay", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    NSError *error = nil;
    NSDictionary *showtimeInformation = @{@"house_id" : theaterId, @"movie_id" : movieId, @"showdate" : date, @"showtime" : showtime };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:showtimeInformation
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Sorry, but there is a problem with ticketing for this film. Please try again later."}];
                        [self.delegate spnCinelifeQueries:self
                                           ticketingError:customError];
                    });
                    
                }
                else {
                    if ([json count] < 1) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                       code:400
                                                                   userInfo:@{@"error_msg" :@"Sorry, but there is a problem with ticketing for this film. Please try again later."}];
                            [self.delegate spnCinelifeQueries:self
                                               ticketingError:customError];
                        });
                    }
                    else if ([json objectForKey:@"url"]) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self.delegate spnCinelifeQueries:self
                                                 ticketingURL:[json objectForKey:@"url"]];
                        });
                    }
                    else {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self.delegate spnCinelifeQueries:self
                                               ticketingFromRTS:json];
                        });
                    }
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postPurchaseTickets:(NSArray *)tickets forAmount:(NSString *)amount forPerformanceId:(NSString *)performanceId forTheatre:(NSDictionary *)theaterDetails forAuditorium:(NSString *)auditorium withTicketFee:(NSString *)fee withCardInformation:(NSDictionary *)cardDetails andCustomer:(NSDictionary *)customer andFilmDetails:(NSDictionary *)filmDetails
{
    /*
    NSMutableDictionary *parameters = [NSMutableDictionary new];
   
    [parameters setObject:@"989656745315"
                   forKey:@"barcode"];
    [parameters setObject:@"656745"
                   forKey:@"transaction_id"];
    
    [parameters setObject:@"elduddits@gmail.com"
                   forKey:@"customer_email"];
    [parameters setObject:@"656745"
                   forKey:@"transaction"];
    [self.delegate spnCinelifeQueries:self
                     purchasedTickets:parameters];
    
    return;
    */
    NSString *urlString = [NSString stringWithFormat:@"%@/tickets/purchase", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    NSMutableDictionary *purchaseDetails = [NSMutableDictionary new];
    [purchaseDetails setObject:performanceId
                        forKey:@"performance_id"];
    [purchaseDetails setObject:[theaterDetails objectForKey:@"rts_id"]
                        forKey:@"rts_id"];
    [purchaseDetails setObject:auditorium
                        forKey:@"auditorium"];
    [purchaseDetails setObject:fee
                        forKey:@"ticket_fee"];

    NSMutableDictionary *cardData = [NSMutableDictionary new];
    NSString *cardType = [cardDetails objectForKey:@"cardType"];
    [cardData setObject:cardType
                   forKey:@"type"];
    
    if ([cardType isEqualToString:@"CreditCard"]) {
        NSString *expDate = [cardDetails objectForKey:@"cardExpirationDate"];
        expDate = [expDate stringByReplacingOccurrencesOfString:@"/" withString:@""];
        [cardData setObject:expDate
                       forKey:@"cc_expiration"];
        
        [cardData setObject:[cardDetails objectForKey:@"cardCVC"]
                       forKey:@"cc_cid"];
        
        [cardData setObject:[cardDetails objectForKey:@"cardName"]
                       forKey:@"cc_name_on_card"];
        
        [cardData setObject:[cardDetails objectForKey:@"cardNumber"]
                       forKey:@"cc_number"];
        [cardData setObject:[cardDetails objectForKey:@"cardPostal"]
                     forKey:@"cc_postal"];
    }
    else {
        [cardData setObject:[cardDetails objectForKey:@"cardNumber"]
                       forKey:@"gc_number"];
    }
    [cardData setObject:amount
                 forKey:@"amount"];
    
    [purchaseDetails setObject:cardData forKey:@"payment_info"];
    [purchaseDetails setObject:tickets forKey:@"tickets"];
    [purchaseDetails setObject:filmDetails forKey:@"movie"];
    [purchaseDetails setObject:customer forKey:@"customer"];
    NSDictionary *theater = @{@"id" : [theaterDetails objectForKey:@"id"], @"name" : [theaterDetails objectForKey:@"name"]};
    [purchaseDetails setObject:theater forKey:@"theater"];
    
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:purchaseDetails
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
//    NSLog(@"%@", [[NSString alloc] initWithData: postData  encoding:NSUTF8StringEncoding]);
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Sorry, but there is a problem with ticketing for this film. Please try again later."}];
                        [self.delegate spnCinelifeQueries:self
                                           ticketingError:customError];
                    });
                    
                }
  
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                         purchasedTickets:json];
                    });
                }
             
            }
        }
    }];
    [postDataTask resume];
}


+(NSArray*)getNewsfeedStoriesForPages:(NSArray*)newsfeedList withPostCount:(NSInteger)postCount
{
//    NSString *numOfEntries = [NSString stringWithFormat:@"&num=%ld", (long)postCount];
//    NSString *firstLink = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=";
    NSMutableArray *fullEntries = [NSMutableArray new];
    for (NSDictionary *page in newsfeedList)
    {
        NSString *rssFeed = [page objectForKey:@"feed"];

        SPNRSSParser *parser = [SPNRSSParser new];
        NSDictionary *data = [parser parseRSS: rssFeed numberOfEntities: postCount];
        

        id responseData = [data objectForKey:@"responseData"];
        
        if (![responseData isKindOfClass:[NSNull class]]) {
            NSDictionary *feed = [responseData objectForKey:@"feed"];
            if (feed) {
                NSArray *entries = [feed objectForKey:@"entries"];
                [fullEntries addObjectsFromArray:entries];
            }
        }
    }
    return fullEntries;
}

+(NSDictionary*)queryURL:(NSURL*)dataURL
{
    if (!dataURL) {
        return nil;
    }
    
    NSURLRequest* request = [NSURLRequest requestWithURL:dataURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
    NSHTTPURLResponse* response = nil;
    NSError* error = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!responseData) {
        NSLog(@"Error: %@", error.localizedDescription);
        return nil;
    }
    
    id json = [NSJSONSerialization
               JSONObjectWithData:responseData
               options:NSJSONReadingMutableContainers
               error:&error];
    if (!json) {
        NSLog(@"Error JSON: %@", error.localizedDescription);
        return nil;
    }
    return json;
}

- (void)readAllNotifications {
    NSString *urlString = [NSString stringWithFormat:@"%@/devices/reset_badge?udid=%@", server, [SPNAppDelegate uuid]];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"readAllNotifications error: %@", error.localizedDescription);
        }
    }];
    [postDataTask resume];
}

- (void)getNotificationsWithCompletion:(void(^)(NSArray *notifications, NSError *error))completion {
    completion([DataModel fetchNotifications], nil);
    NSString *urlString = [NSString stringWithFormat:@"%@/push_notifications_v2/index?udid=%@", server, [SPNAppDelegate uuid]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil, error);
        } else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                completion(nil, error);
            } else {
                [DataModel mapNotifications:json];
                completion([DataModel fetchNotifications], nil);
            }
        }
    }];
    [postDataTask resume];
}

- (void)getNotificationsBadgeWithCompletion:(void (^)(NSString *badge))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/devices/get_badge?udid=%@", server, [SPNAppDelegate uuid]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completion(nil);
        } else {
            NSDictionary *json = [NSJSONSerialization
                                  JSONObjectWithData:data
                                  options:NSJSONReadingAllowFragments
                                  error:&error];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!json) {
                    NSLog(@"Error JSON: %@", error.localizedDescription);
                    completion(nil);
                } else {
                    completion(json[@"current_badge"]);
                }
            });
        }
    }];
    [postDataTask resume];
}

- (void)removeNotificationWithId:(NSNumber *)notificationId completion:(void(^)(NSError *))completion {
    NSString *urlString = [NSString stringWithFormat:@"%@/push_notifications_v2/delete_message?udid=%@&message_id=%@", server, [SPNAppDelegate uuid], notificationId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(error);
        });
    }];
    [postDataTask resume];
}

-(void)postRegisterDeviceForPushNotificationsWithUDID:(NSString *)udid
                                             andToken:(NSString *)token
                                       withDeviceName:(NSString *)deviceName
                                       andDeviceModel:(NSString *)deviceModel
                                          withVersion:(NSString *)version
                                             forBadge:(BOOL)enableBadge
                                             forAlert:(BOOL)enableAlert
                                             forSound:(BOOL)enableSound
                                      withCredentials:(NSDictionary *)credentials
{
    NSString *urlString = [NSString stringWithFormat:@"%@/devices/register", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSError *error = nil;
    NSDictionary *deviceInformation = [NSDictionary dictionaryWithObjects:@[udid, token, deviceName, deviceModel, version, @(enableBadge), @(enableAlert), @(enableSound)]
                                                                  forKeys:@[@"udid", @"token", @"name", @"model", @"version", @"push_badge", @"push_alert", @"push_sound"]];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:deviceInformation
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
            }
            else {
                if (httpResponse.statusCode != 200) {
                    NSLog(@"REGISTER DEVICE: Server error");
                }
                else {
                    NSLog(@"REGISTER DEVICE: All good");
                }
            }
        }
    }];
    [postDataTask resume];
}

-(void)postUnregisterDeviceForPushNotificationsWithUDID:(NSString *)udid
{
    NSString *urlString = [NSString stringWithFormat:@"%@/devices/unregister", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    NSError *error = nil;
    NSDictionary *deviceInformation = [NSDictionary dictionaryWithObjects:@[udid]
                                                                  forKeys:@[@"udid"]];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:deviceInformation
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
            }
            else {
                if (httpResponse.statusCode != 200) {
                    NSLog(@"UNREGISTER DEVICE: Server error");
                }
                else {
                    NSLog(@"UNREGISTER DEVICE: All good");
                }
            }
        }
    }];
    [postDataTask resume];
}

+(NSArray*)getStoriesForTheaterWithFeeds:(NSArray*)newsfeedList withPostCount:(NSInteger)postCount
{
//    NSString *numOfEntries = [NSString stringWithFormat:@"&num=%ld", (long)postCount];
//    NSString *firstLink = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=";
    NSMutableArray *fullEntries = [NSMutableArray new];
    for (NSDictionary *page in newsfeedList)
    {
        NSString *rssFeed = [page objectForKey:@"source"];
        
        SPNRSSParser *parser = [SPNRSSParser new];
        NSDictionary *data = [parser parseRSS: rssFeed numberOfEntities: postCount];
        
        id responseData = [data objectForKey:@"responseData"];
        
        if (![responseData isKindOfClass:[NSNull class]]) {
            NSDictionary *feed = [responseData objectForKey:@"feed"];
            if (feed) {
                NSArray *entries = [feed objectForKey:@"entries"];
                
                NSMutableDictionary *newEntry;
                for (NSDictionary *entry in entries)
                {
                    newEntry = [NSMutableDictionary dictionaryWithDictionary:entry];
                    [newEntry setObject:[page objectForKey:@"type"] forKey:@"page"];
                    [newEntry setObject:rssFeed forKey:@"feedUrl"];
                    [fullEntries addObject:newEntry];
                }
                
            }
        }
        
    }
    return fullEntries;
}

+ (BOOL)isValidURL:(NSURL*)url
{
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *res = nil;
    NSError *err = nil;
    [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
    BOOL isGoodURL = (err == nil && [res statusCode] != 404);
    return isGoodURL;
}

+(NSString*)getCinelifeTermsOfService
{
    return [NSString stringWithFormat:@"%@/cinelife/tos", server];
}

+(NSString *)getCinelifePrivacyPolicy
{
    return [NSString stringWithFormat:@"%@/cinelife/privacy", server];
}

-(void)getCineLifePostsForTheater:(NSString *)theaterId
{
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/%@/posts", server, theaterId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not get any posts for this theater. Please check again later."}];
                        [self.delegate spnCinelifeQueries:self
                                           errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                             theaterPosts:json];
                    });
                }

            }
        }
    }];
    [postDataTask resume];
}

- (void)getCineLifePostsForFestival:(NSString *)festivalId {
    NSString *urlString = [NSString stringWithFormat:@"%@/festivals/posts?id=%@", server, festivalId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not get any posts for this theater. Please check again later."}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                            festivalPosts:json];
                    });
                }
                
            }
        }
    }];
    [postDataTask resume];
}

-(void)postResetPassword:(NSString *)email
{
    NSString *urlString = [NSString stringWithFormat:@"%@/forgot_password", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:@{@"email" : email}
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not get any posts for this theater. Please check again later."}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self.delegate spnCinelifeQueries:self
                                         passwordDidReset:YES];
                    });
                }
                
            }
        }
    }];
    [postDataTask resume];
}

-(void)postCreatePassbookForPurchase:(NSDictionary *)purchaseInformation
{
    NSString *urlString = [NSString stringWithFormat:@"%@/tickets/passbook_code", server];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSError *error;
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:purchaseInformation
                                                       options:0
                                                         error:&error];
    
    request.HTTPBody = postData;
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not generate passbook information. Please try again later."}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *passError;
                        PKPass *pass = [[PKPass alloc] initWithData:data error:&passError];
                    
                        if (!pass) {
                            [self.delegate spnCinelifeQueries:self
                                                 errorOcurred:passError];
                        }
                        else {
                            [self.delegate spnCinelifeQueries:self
                                             createdPassbook:pass];
                        }
                    });
            }
            
        }
    }];
    [postDataTask resume];
}

+(NSArray*)getFacebookFeed:(NSString *)facebookFeed withLimit:(NSInteger)limit
{
    NSString *urlString = [NSString stringWithFormat:@"%@/feeds/facebook?limit=%ld&source=%@", server, (long) limit, facebookFeed];
    NSURL *url = [NSURL URLWithString:urlString];

    id result = [self queryURL:url];
    return result;
    /*
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not get facebook feed. Please try again later."}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        [self.delegate spnCinelifeQueries:self
                                             facebookFeed:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
     */
}

+(NSArray*)getTwitterFeed:(NSString *)twitterFeed withLimit:(NSInteger)limit
{
 
    NSString *urlString = [NSString stringWithFormat:@"%@/feeds/twitter?limit=%ld&source=%@", server, (long) limit, twitterFeed];
    NSURL *url = [NSURL URLWithString:urlString];
    
    id result = [self queryURL:url];
    return result;
    /*
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"UNREGISTER ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :@"Could not get twitter feed. Please try again later."}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        [self.delegate spnCinelifeQueries:self
                                             twitterFeed:json];
                    });
                }
            }
        }
    }];
    [postDataTask resume];
     */
}


+(NSDictionary*)getCastAndCrewForMovie:(NSDictionary*)movie
{
    
    NSMutableDictionary *movieCast = [NSMutableDictionary new];
    
    NSArray *elementsInCast = @[@"actors", @"directors", @"writers", @"producers"];
    for (NSString *castType in elementsInCast)
    {
        id castMembers = [movie objectForKey:castType];
        if ([castMembers isKindOfClass:[NSArray class]]) {
            [movieCast setObject:castMembers
                          forKey:castType];
        }
        else if ([castMembers isKindOfClass:[NSString class]]) {
            [movieCast setObject:@[castMembers]
                          forKey:castType];
        }
    }
    
    return movieCast;
}


- (void)getTheater:(NSString *)theaterId completion:(void(^)(NSDictionary *theaterInfo, NSError *error))completion {
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/theaters/%@", server, theaterId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        completion(nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(json, nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

- (void)getFestival:(NSString *)festivalId completion:(void(^)(Festival *festival, NSError *error))completion {
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/festivals?id=%@", server, festivalId];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        completion(nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion([Festival festivalsFromJSON:json].firstObject, nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

- (void)getMovie:(NSString *)movieId completion:(void (^)(NSDictionary *, NSError *))completion {
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/movies/%@?distance=30", server, movieId];
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&lat=%f&lng=%f", [appDelegate myLocation].coordinate.latitude, [appDelegate myLocation].coordinate.longitude]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        completion(nil, customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(json, nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}


- (void)saveLocation:(CLLocation *)location completion:(void(^)(NSError *))completion {
    NSDictionary *credentials = [SPNUser getCurrentUserId];
    NSString *urlString;
    if (credentials) {
        urlString = [NSString stringWithFormat:@"%@/customers/update_location?lat=%f&lng=%f", server, location.coordinate.latitude, location.coordinate.longitude];
    } else {
        urlString = [NSString stringWithFormat:@"%@/devices/update_location?lat=%f&lng=%f&udid=%@", server, location.coordinate.latitude, location.coordinate.longitude, [SPNAppDelegate uuid]];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    
    if (credentials) {
        [request setValue:[credentials objectForKey:@"access_token"]
       forHTTPHeaderField:@"X-CinelifeAuth"];
    }
    
    NSURLSessionDataTask *postDataTask = [self.serverSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (error) {
            NSLog(@"ERROR: %@", error.localizedDescription);
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self.delegate spnCinelifeQueries:self
                                     errorOcurred:error];
                completion(error);
            });
        }
        else {
            id json = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:NSJSONReadingAllowFragments
                       error:&error];
            if (!json) {
                NSLog(@"Error JSON: %@", error.localizedDescription);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate spnCinelifeQueries:self
                                         errorOcurred:error];
                    completion(error);
                });
            }
            else {
                if (httpResponse.statusCode != 200) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        NSError *customError = [NSError errorWithDomain:NSLocalizedString(@"User not registered", nil)
                                                                   code:httpResponse.statusCode
                                                               userInfo:@{@"error_msg" :[json objectForKey:@"message"]}];
                        [self.delegate spnCinelifeQueries:self
                                             errorOcurred:customError];
                        completion(customError);
                    });
                    
                }
                else {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        completion(nil);
                    });
                }
            }
        }
    }];
    [postDataTask resume];
}

@end
