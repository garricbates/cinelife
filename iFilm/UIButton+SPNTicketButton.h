//
//  UIButton+SPNTicketButton.h
//  iFilm
//
//  Created by La Casa de los Pixeles on 9/19/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (SPNTicketButton)

@property (nonatomic, copy) NSString* theatreId;
@end
