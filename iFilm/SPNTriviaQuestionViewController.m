//
//  SPNTriviaQuestionViewController.m
//  iFilm
//
//  Created by Eduardo Salinas on 11/5/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTriviaQuestionViewController.h"

@interface SPNTriviaQuestionViewController ()

@end

@implementation SPNTriviaQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Trivia";
    [self.nextTriviaButton setBackgroundColor:[UIColor spn_buttonColor]];
    self.nextTriviaButton.layer.shadowColor = [UIColor spn_buttonShadowColor].CGColor;
    [self.nextTriviaButton.titleLabel setFont:[UIFont spn_NeutraMedium]];
    [self.typeOfTriviaLabel setFont:[UIFont spn_NeutraBoldMega]];
    [self.triviaTextLabel setFont:[UIFont spn_NeutraMediumLarge]];
    [self.triviaOptionsLabel setFont:[UIFont spn_NeutraMediumLarge]];

    // Do any additional setup after loading the view.
    
    if (self.isLandingTrivia) {
        [self.nextTriviaButton addTarget:self
                                  action:@selector(nextTrivia)
                        forControlEvents:UIControlEventTouchUpInside];;
    }
    else {
        [self.nextTriviaButton setTitle:@"Next" forState:UIControlStateNormal];
        NSArray *triviaElements = [self.triviaQuestion allKeys];
        [self.triviaOptionsLabel setText:@""];
        

        if ([self.triviaQuestions count] < 1) {
            [self.triviaTextLabel setText:@"Hope you enjoyed our film trivia. \r\n Come back tomorrow for more!"];
            [self.typeOfTriviaLabel setText:@""];
            [self.nextTriviaButton setTitle:@"Done"
                                   forState:UIControlStateNormal];
            [self.nextTriviaButton addTarget:self
                                      action:@selector(quitTrivia:)
                            forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            if (self.answer) {
                [self.typeOfTriviaLabel setText:@"Answer"];
                [self.triviaTextLabel setText:self.answer];
                self.answer = nil;
            }
            else if ([triviaElements indexOfObject:@"fact"] != NSNotFound) {
                [self.typeOfTriviaLabel setText:@"Fact"];
                [self.triviaTextLabel setText:[self.triviaQuestion objectForKey:@"fact"]];
            }
            else  {
                [self.typeOfTriviaLabel setText:@"Question"];
                [self.triviaTextLabel setText:[self.triviaQuestion objectForKey:@"question"]];
                NSArray *options = [self.triviaQuestion objectForKey:@"options"];
                if (options) {
                    [self.triviaOptionsLabel setText:[options componentsJoinedByString:@"\r\n"]];
                }
                self.answer = [self.triviaQuestion objectForKey:@"answer"];
                [self.nextTriviaButton setTitle:@"Get Answer" forState:UIControlStateNormal];
            }
            [self.nextTriviaButton addTarget:self
                                      action:@selector(nextTrivia)
                            forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)nextTrivia
{
    UIStoryboard *triviaStoryboard = [UIStoryboard storyboardWithName:@"WhatsNextStoryboard"
                                                               bundle:nil];
    SPNTriviaQuestionViewController *questionController =  [triviaStoryboard instantiateViewControllerWithIdentifier:@"TriviaQuestion"];
    
    if (self.answer) {
        [questionController setAnswer:self.answer];
    }
    else {
        [self.triviaQuestions removeLastObject];
    }
    [questionController setTriviaQuestion:[self.triviaQuestions lastObject]];
    [questionController setTriviaQuestions:self.triviaQuestions];
    
    NSMutableArray *viewControllers = [NSMutableArray new];
    [viewControllers addObject:[[self.navigationController viewControllers] firstObject]];
    [viewControllers addObject:questionController];
    [self.navigationController setViewControllers:viewControllers animated:YES];
    [questionController.navigationItem setHidesBackButton:YES animated:NO];
}

-(void)quitTrivia:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
