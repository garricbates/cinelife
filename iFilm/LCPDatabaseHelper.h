//
//  LCPDatabaseHelper.h
//  Universades
//
//  Created by La Casa de los Pixeles on 7/24/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LCPDatabaseHelper : NSObject


@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;


+(LCPDatabaseHelper *) sharedInstance;

/**
 * Initialization with the specified managedObjectContext. This should initialize with the managed object context provided by the application using CoreData.
 * @author La Casa de los Pixeles
 * @param context The managed object context from th app.
 * @return The singleton instance of the Database helper.
 * @code [[LCPDatabaseHelper sharedInstance] setManagedObjectContext:self.managedObjectContext];
 **/
-(id)initWithContext:(NSManagedObjectContext *)context;

/// Addition functions
/**
 * Method to add a new record to the given database table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param newRecordInformation A dictionary with the specified values to add to the table. The dictionary keys must match the attributes of the specified table. (i. e. If the table Employee has two attributes: name and employeeID, the dictionary must contain two objects with keys named as name and employeeID.)
 * @param entityName The name of the table in the database in which to insert the new record.
 * @return YES if the record was added. NO, otherwise.
 */
-(BOOL)addRecord:(NSDictionary *)newRecordInformation
       forEntity:(NSString*)entityName;

/// Deletion functions
/**
 * Method to delete record in the given database table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param recordToDelete The object in the database to delete. Normally, this parameter can be obtained by one of the different fetch requests described in this class.
 * @return YES if the record was deleted. NO, otherwise.
 */
-(BOOL)deleteRecord:(NSManagedObject*)recordToDelete;

/**
 * Method to delete record in the given database table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param attribute The value to match the search for which to delete a record with. 
 * @param attributeKey The name of the key in the table to compare the attribute's value. For example, if you would like to delete the employee John from the Employee table, the attribute parameter would be "John", and the attributeKey would be "name".
 * @param entityName The name of the table in the database in which to insert the new record.
 * @return YES if the record was deleted. NO, otherwise. This method deletes ALL the records that match the search.
 */
-(BOOL)deleteRecordWithAttribute:(id)attribute
                 forAttributeKey:(NSString*)attributeKey
                       forEntity:(NSString*)entityName;

/// Updating functions
/**
 * Method to update a record in the given database table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param attribute The value of the attribute to match the search for.
 * @param newAttribute The new value to replace the old one.
 * @param attributeKey The name of the key in the table to compare the attribute's value. For example, if you would like to change the name of the employee John to Johnny from the Employee table, the attribute parameter would be "John", the newAttribute parameter would be "Johnny" and the attributeKey would be "name".
 * @param entityName The name of the table in the database in which to update the record.
 * @return YES if the record was updated. NO, otherwise. This method updates ALL the records that match the search.
 */
-(BOOL)updateRecordWithAttribute:(id)attribute
                withNewAttribute:(id)newAttribute
                 forAttributeKey:(NSString*)attributeKey
                       forEntity:(NSString*)entityName;

/**
 * Method to update a record in the given database table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param recordToUpdate The object in the database to update. Normally, this parameter can be obtained by one of the different fetch requests described in this class. * @param newAttribute The new value to replace the old one.
 * @param attributeKey The name of the key in the table to compare the attribute's value. For example, if you would like to change the name of the employee John to Johnny from the Employee table, the attribute parameter would be "John", the newAttribute parameter would be "Johnny" and the attributeKey would be "name".
 * @return YES if the record was updated. NO, otherwise. This method updates ALL the records that match the search.
 */
-(BOOL)updateRecord:(NSManagedObject*)recordToUpdate
   withNewAttribute:(id)newAttribute
    forAttributeKey:(NSString*)attributeKey;

///Fetching functions
/**
 * Method to retrieve all records from a given table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param entityName The name of the table in the database to obtain all records from.
 * @return An array with all managed objects from a given table, or nil if an error occured.
 */
-(NSArray *)fetchAllRecordsForEntity:(NSString*)entityName;

/**
 * Method to retrieve all records with a specified attribute from a given table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param attribute The value of the attribute to match the search for.
 * @param attributeKey The name of the key in the table to compare the attribute's value. For example, if you would like to search for employees with last name "Smith", the attribute would be "Smith" and the attributeKey would be "lastName".
 * @param entityName The name of the table in the database to obtain all records from.
 * @return An array with all managed objects from a given table, or nil if an error occured.
 */
-(NSArray *)fetchRecordsWithAttribute:(id)attribute
                      forAttributeKey:(NSString *)attributeKey
                            forEntity:(NSString *)entityName;

/**
 * Method to retrieve the first record with a specified attribute from a given table. Before calling this method, the singleton instance should be initialized with the managedObjectContext of the app.
 * @author La Casa de los Pixeles
 * @param attribute The value of the attribute to match the search for.
 * @param attributeKey The name of the key in the table to compare the attribute's value. For example, if you would like to search for an employee with last name "Smith", the attribute would be "Smith" and the attributeKey would be "lastName".
 * @param entityName The name of the table in the database to obtain all records from.
 * @return A managed object matching the attribute to search for.
 */
-(NSManagedObject*)fetchFirstRecordWithAttribute:(id)attribute
                                 forAttributeKey:(NSString*)attributeKey
                                       forEntity:(NSString*)entityName;

///Existence functions
-(BOOL)recordExistsOnDatabase:(NSManagedObject *)record
                    forEntity:(NSString*)entityName;


// Placeholder methods
-(BOOL)addMovieToFavourites:(NSDictionary*)movie
                    forUser:(NSString*)username;

-(NSArray *)getAllMoviesForUser:(NSString*)username;

-(NSArray *)getMovieIdsForUser:(NSString*)username;

-(BOOL)deleteMovie:(NSString*)movieId ofUser:(NSString*)username;


-(BOOL)addCreditCardToDatabase:(NSDictionary*)creditCard;

-(NSDictionary *)getCardInformation;

-(BOOL)deleteCardsInDatabase;
 

@end
