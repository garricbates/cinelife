//
//  SPNmapViewController.m
//  iFilm
//
//  Created by La Casa de los Pixeles on 6/16/14.
//  Copyright (c) 2014 La Casa de los Pixeles. All rights reserved.
//

#import "SPNTheatreMapViewController.h"
#import "SPNTheatreAnnotation.h"
#import "SPNTheatreDetailsViewController.h"
#import "Festival.h"
#import "SPNFestivalViewController.h"

@interface SPNTheatreMapViewController () <MKMapViewDelegate> {
    CLLocation *desiredLocation;
}

@property (nonatomic, retain) CLLocation *currentLocation;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UIButton *searchMapButton;

- (IBAction)searchMapButtonPressed:(id)sender;

@end


@implementation SPNTheatreMapViewController

-(void)dealloc {
    self.mapView.delegate = nil;
    [self applyMapViewMemoryHotFix];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeFromSuperview];
    
    self.mapView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Theatre Map";
    
    [self.searchMapButton.titleLabel setFont:[UIFont spn_NeutraButtonMedium]];
    self.searchMapButton.layer.shadowColor = [UIColor spn_aquaShadowColor].CGColor;
    
    [self reloadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ((self.selectedTheaterType == SPNTheatreTypeInRegion && self.theatersNearby.count == 0) ||
        (self.selectedTheaterType == SPNTheatreTypeFavourites && self.myTheaters.count == 0 && self.myFestivals.count == 0)) {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)applyMapViewMemoryHotFix {
    switch (self.mapView.mapType) {
        case MKMapTypeHybrid:
        {
            self.mapView.mapType = MKMapTypeStandard;
        }
            
            break;
        case MKMapTypeStandard:
        {
            self.mapView.mapType = MKMapTypeHybrid;
        }
            
            break;
        default:
            break;
    }
}

#pragma mark - Map view functions

- (void)reloadData {
    NSMutableArray *annotations = [NSMutableArray new];
    NSMutableArray *theaters = [NSMutableArray new];
    NSMutableArray *festivals = [NSMutableArray new];
    BOOL isFavorite = self.selectedTheaterType == SPNTheatreTypeFavourites;
    switch (self.selectedTheaterType) {
        case SPNTheatreTypeInRegion:
            for (id object in self.theatersNearby) {
                if (![object isKindOfClass:[Festival class]]) {
                    [theaters addObject:object];
                }
            }
            break;
            
        case SPNTheatreTypeFavourites:
            [theaters addObjectsFromArray:self.myTheaters];
            [festivals addObjectsFromArray:self.myFestivals];
            break;
            
        case SPNTheatreTypeFestival:
            [festivals addObjectsFromArray:self.festivals];
            break;
    }
    
    for (NSDictionary *theatre in theaters) {
        SPNTheatreAnnotation *theatrePin = [SPNTheatreAnnotation new];
        theatrePin.title = [theatre objectForKey:@"name"];
        theatrePin.subtitle = [SPNCinelifeQueries getTheatreAddress:theatre];
        theatrePin.objectId = [theatre objectForKey:@"id"];
        
        theatrePin.address = [theatre objectForKey:@"street"];
        theatrePin.state = [theatre objectForKey:@"state"];
        theatrePin.postalCode = [theatre objectForKey:@"zip"];
        theatrePin.city = [theatre objectForKey:@"city"];
        theatrePin.country = [theatre objectForKey:@"country"];
        
        if (isFavorite) {
            theatrePin.isFavorite = YES;
        }
        else {
            theatrePin.isFavorite = [[theatre objectForKey:@"is_favorite"] boolValue];
        }
        
        CLLocationCoordinate2D movieCoords;
        NSString *latString = [theatre objectForKey:@"lat"];
        NSString *lonString = [theatre objectForKey:@"lng"];
        
        if (!latString || !lonString) {
            movieCoords.latitude = 0.0;
            movieCoords.longitude = 0.0;
        } else {
            movieCoords.latitude = [latString doubleValue];
            movieCoords.longitude = [lonString doubleValue];
        }
        theatrePin.coordinate = movieCoords;
        
        [annotations addObject:theatrePin];
    }
    
    for (Festival *festival in festivals) {
        SPNTheatreAnnotation *festivalPin = [SPNTheatreAnnotation new];
        festivalPin.title = festival.summary;
        festivalPin.subtitle = festival.venueAddress;
        festivalPin.objectId = festival.festivalId.stringValue;
        festivalPin.address = festival.venueAddress;
        if (isFavorite) {
            festivalPin.isFavorite = YES;
        } else {
            festivalPin.isFavorite = festival.isFavorited.boolValue;
        }
        
        festivalPin.coordinate = CLLocationCoordinate2DMake(festival.lat.doubleValue, festival.lng.doubleValue);
        festivalPin.isFestival = YES;
        
        [annotations addObject:festivalPin];
    }
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView showAnnotations:annotations animated:YES];
    
    //  [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.currentLocation.coordinate, 2000, 2000) animated:YES];
    [self.mapView setShowsUserLocation:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *pinAnnotation = nil;
    if(annotation != self.mapView.userLocation)
    {
        SPNTheatreAnnotation *pin = (SPNTheatreAnnotation*)annotation;
        
        static NSString *defaultPinID = @"myPin";
        pinAnnotation = (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        
        if (!pinAnnotation) {
            pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
            pinAnnotation.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinAnnotation.canShowCallout = YES;
        } else {
            pinAnnotation.annotation = annotation;
        }
        pinAnnotation.pinColor = pin.isFestival ? MKPinAnnotationColorPurple : MKPinAnnotationColorRed;
        if (pin.isFavorite) {
            pinAnnotation.image = [UIImage imageNamed:@"fav-geo-icon"];
        } else {
            pinAnnotation.image = [UIImage imageNamed:@"geo-icon"];
        }
    }
    return pinAnnotation;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([view annotation] != self.mapView.userLocation) {
      
        [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        SPNTheatreAnnotation *location = view.annotation;
       
        if (location.isFestival) {
            [[SPNCinelifeQueries sharedInstance] getFestival:location.objectId completion:^(Festival *festival, NSError *error) {
                if (festival) {
                    [self performSegueWithIdentifier:@"FestivalDetails" sender:festival];
                }
                [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            }];
        } else {
            [[SPNCinelifeQueries sharedInstance] getTheater:location.objectId completion:^(NSDictionary *theaterInfo, NSError *error) {
                if (theaterInfo) {
                    [self performSegueWithIdentifier:@"DetailsFromMap" sender:theaterInfo];
                }
                [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
            }];
        }
    }
}

#pragma mark Navigation functions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[SPNTheatreDetailsViewController class]]) {
        SPNTheatreDetailsViewController *controller = [segue destinationViewController];
        controller.theatreInformation = sender;
    } else if ([segue.destinationViewController isKindOfClass:[SPNFestivalViewController class]]) {
        SPNFestivalViewController *controller = [segue destinationViewController];
        controller.festival = sender;
    }
}

#pragma mark - Theatre functions

- (IBAction)searchMapButtonPressed:(id)sender {
    
    BOOL forFestivals = self.segmentedControl.selectedSegmentIndex == 2;
    if (self.segmentedControl.selectedSegmentIndex == 1) {
        self.segmentedControl.selectedSegmentIndex = 0;
    }
    
    CLLocationCoordinate2D centerRegion = [self.mapView centerCoordinate];
    desiredLocation = [[CLLocation alloc] initWithLatitude:centerRegion.latitude
                                                 longitude:centerRegion.longitude];
  //  double searchRadius = [self getMapDistanceForSearch:self.mapView];
    // TODO: Add server query to get theatres
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self.geoCoder reverseGeocodeLocation:desiredLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *place = [placemarks firstObject];
        
        NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
        if (zip) {
            [self.zipcodePicker setText:zip];
        }
        
        if (forFestivals) {
            [self getFestivalsWithZip:nil orCoordinates:desiredLocation];
        } else {
            [self getTheatersWithZip:nil orCoordinates:desiredLocation];
        }
        
        [self setCityAndStateToSearchBar:place.addressDictionary];
    }];
}

#pragma mark - Location manager delegate functions

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *userLocation = [locations lastObject];
    
    self.currentLocation = userLocation;
    [self.locationManager stopUpdatingLocation];
    [self.mapView setCenterCoordinate:self.currentLocation.coordinate];
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.currentLocation.coordinate, 2000, 2000) animated:NO];
    [self.geoCoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *place = [placemarks firstObject];
        
        NSString *zip = [place.addressDictionary objectForKey:@"ZIP"];
        if (zip) {
            [self.zipcodePicker setText:zip];
            [self getTheatersWithZip:nil orCoordinates:userLocation];
            [self setCityAndStateToSearchBar:place.addressDictionary];
        } else {
            [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        }
    }];
}

#pragma mark - Search in map elements
-(double) getDistanceMetresBetweenLocationCoordinates:(CLLocationCoordinate2D) coord1 andCoordinates:(CLLocationCoordinate2D)coord2
{
    CLLocation* location1 = [[CLLocation alloc] initWithLatitude: coord1.latitude longitude: coord1.longitude];
    CLLocation* location2 = [[CLLocation alloc] initWithLatitude: coord2.latitude longitude: coord2.longitude];
    
    return [location1 distanceFromLocation:location2];
}

-(double)getMapDistanceForSearch:(MKMapView*)mapView
{
    MKMapRect mRect = self.mapView.visibleMapRect;
    MKMapPoint neMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), mRect.origin.y);
    MKMapPoint swMapPoint = MKMapPointMake(mRect.origin.x, MKMapRectGetMaxY(mRect));
    CLLocationCoordinate2D neCoord = MKCoordinateForMapPoint(neMapPoint);
    CLLocationCoordinate2D swCoord = MKCoordinateForMapPoint(swMapPoint);
    
    double size = [self getDistanceMetresBetweenLocationCoordinates:neCoord
                                                     andCoordinates:swCoord];
    return size / 1000;
}

#pragma mark - Search bar functions
#pragma mark - Text field functions

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)doneClicked {
    [self decodeAddress:self.zipcodePicker.text
            forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
}

- (void)activateGPSLocation {
    [self.locationManager startUpdatingLocation];
}

- (void)savedLocation {
    [self getLastSavedLocation];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if ([searchBar isFirstResponder]) {
        [searchBar resignFirstResponder];
    }
    [self decodeAddress:self.zipcodePicker.text
            forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
}

-(void)getLastSavedLocation {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* storedZipcode = [prefs objectForKey:@"zip"];
    if (storedZipcode) {
        [self decodeAddress:storedZipcode
                forFesivals:self.selectedTheaterType == SPNTheatreTypeFestival];
    } else {
        [UIAlertView showAlertViewForError:SPNAlertViewErrorCodeNoSavedLocation];
    }
}

@end
