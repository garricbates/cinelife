iFilm 
=========

This project is created specifically for Spotlight Networks. Do not clone unless permission is granted.

The iFilm app is designed to obtain information about theatres, films, festivals and events involving mostly independent cinemas. 
The app provides full scheduling information for all affiliated theatres. Theatres are sorted by distance and display the current films playing.
The app also shows a list of future releases, festival films coming from indieflix, and top box office films (from RT, pending changes).

The user can also login with Twitter, Facebook, Google+, or otherwise create an account linked to the application. Then he or she is able to add elements to favorites, post reviews, share news, films, etc.

